class cfgPatches
{
    class Aux501_Patch_DC15X
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] =
        {
            "Aux501_Weaps_DC15X"
        };
    };
};

class CowsSlot;
class MuzzleSlot;
class UnderBarrelSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless : Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_DC15X : Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] DC-15X";
        baseWeapon = "Aux501_Weaps_DC15X";
        picture = "\MRC\JLTS\weapons\DC15X\data\ui\DC15X_ui_ca.paa";
        model = "\MRC\JLTS\weapons\DC15X\DC15X.p3d";
        handAnim[] = {"OFP2_ManSkeleton", "\MRC\JLTS\weapons\DC15X\anims\DC15X_handanim.rtm"};
        recoil = "recoil_dmr_01";
        recoilProne = "recoil_single_prone_mx";
        magazines[] =
        {
            "Aux501_Weapons_Mags_40mw5"
        };
        modelOptics = "\A3\Weapons_F\acc\reticle_sniper_F";
        modes[] = {"Single"};
        class Single : Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound : BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\DC15X\sounds\dc15x_fire", 2.5, 1, 1800};
                soundBegin[] = {"begin1", 1};
            };
            reloadTime = 1.5;
            dispersion = 0.0003;
            minRange = 2;
            minRangeProbab = 0.5;
            midRange = 250;
            midRangeProbab = 0.7;
            maxRange = 450;
            maxRangeProbab = 0.3;
        };
        class OpticsModes
        {
            class Ironsights
            {
                opticsID = 1;
                useModelOptics = 0;
                opticsFlare = "true";
                opticsPPEffects[] = {"OpticsCHAbera5", "OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.25;
                opticsZoomMax = 1.1;
                opticsZoomInit = 0.75;
                memoryPointCamera = "eye";
                visionMode[] = {};
                distanceZoomMin = 100;
                distanceZoomMax = 100;
            };
        };
        class WeaponSlotsInfo : WeaponSlotsInfo
        {
            class CowsSlot : CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] =
                {
                    "Aux501_cows_LRPS"
                };
            };
            class MuzzleSlot : MuzzleSlot
            {
                linkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName="$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                compatibleItems[] =
                {
                    "Aux501_muzzle_flash"
                };
            };
            class UnderBarrelSlot: UnderBarrelSlot
            {
                iconPicture="\A3\Weapons_F_Mark\Data\UI\attachment_under.paa";
                iconPinpoint="Bottom";
                linkProxy="\A3\Data_F_Mark\Proxies\Weapon_Slots\UNDERBARREL";
                compatibleItems[] =
                {
                    "bipod_01_F_blk"
                };
            };
        };
    };
};