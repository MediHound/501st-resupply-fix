class cfgPatches
{
    class Aux501_Patch_Valken38X
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_Valken38X"
        };
    };
};

class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_Valken38X: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName="[501st] Valken38X";
        baseWeapon="Aux501_Weaps_Valken38X";
        picture = "\Aux501\Weapons\Republic\Valken38x\valken_ui.paa";
        model = "327th_weapons\327th_valken\38X.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\A3\Weapons_F_Mark\LongRangeRifles\DMR_02\data\Anim\DMR_02.rtm"};
        recoil = "3AS_recoil_DC15A";
        modelOptics = "3AS\3AS_Weapons\Data\3AS_2D_Optic.p3d";
        useModelOptics = 1;
        maxRecoilSway = 0;
        swayDecaySpeed = 0;
        inertia = 0.5;
        dexterity = 1.5;
        initSpeed = -1;
        maxZeroing = 2500;
        magazines[]=
        {
            "Aux501_Weapons_Mags_20mwdp30", 
            "Aux501_Weapons_Mags_30mw10"
        };
        modes[] = {"Single"};
        class Single: Single
        {
            reloadTime=0.2;		
            dispersion=0.0006;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "DefaultRifle";
                begin1[] = {"kobra\442_weapons\sounds\sniper\sniper1.wss",1,1,900};
                closure2[] = {"A3\sounds_f\weapons\closure\zafir_closure_2",0.5,1,10};
                soundBegin[] = {"begin1",1};
                soundClosure[] = {"closure2",1};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "DefaultRifle";
                begin1[] = {"kobra\442_weapons\sounds\sniper\sniper1.wss",1,1,900};
                closure2[] = {"A3\sounds_f\weapons\closure\zafir_closure_2",0.5,1,10};
                soundBegin[] = {"begin1",1};
                soundClosure[] = {"closure2",1};
            };
        };
        class OpticsModes
        {
            class Ironsights
            {
                opticsID = 1;
                useModelOptics = 0;
                opticsFlare = "true";
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.375;
                opticsZoomMax = 1.1;
                opticsZoomInit = 0.75;
                memoryPointCamera = "eye";
                visionMode[] = {};
                distanceZoomMin = 100;
                distanceZoomMax = 100;
            };
            class Snip
            {
                useModelOptics = 1;
                opticsFlare = 1;
                opticsID = 2;
                cameraDir = "";
                memoryPointCamera = "op_eye";
                modelOptics = "\3AS\3AS_Weapons\Data\3AS_2D_Optic.p3d";
                opticsPPEffects[] = {"OpticsCHAbera1"};
                weaponInfoType = "RscWeaponRangeZeroingFOV";
                opticsZoomMin = "0.25/12";
                opticsZoomMax = "0.25/6";
                opticsZoomInit = "0.25/6";
                discreteinitIndex = 0;
                discretefov[] = {"0.25/6","0.25/12"};
                discreteDistanceInitIndex = 1;
                distanceZoomMin = 300;
                distanceZoomMax = 1200;
                thermalMode[] = {0,1};
                visionMode[] = {"Normal","NVG","TI"};
                opticsDisplayName = "VALKEN";
                opticsDisablePeripherialVision = 0;
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                scope = 0;
                compatibleItems[] = 
                {
                    "Aux501_cows_DMS_TI",
                    "Aux501_cows_DMS_2_TI",
                    "Aux501_cows_DMS_3_TI",
                    "Aux501_cows_DMS_4_TI"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                linkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName="$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                iconPosition[] = {0.24,0.35};
                iconScale = 0.2;
                compatibleItems[]=
                {
                    "Aux501_muzzle_flash"
                };
            };
            class PointerSlot: PointerSlot
            {
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot";
                compatibleItems[] = 
                {
                    "acc_flashlight","acc_pointer_IR"
                }; 
            };
            class UnderBarrelSlot: UnderBarrelSlot
            {
                iconPicture="\A3\Weapons_F_Mark\Data\UI\attachment_under.paa";
                iconPinpoint="Bottom";
                linkProxy="\A3\Data_F_Mark\Proxies\Weapon_Slots\UNDERBARREL";
                compatibleItems[] = 
                {
                    "bipod_01_f_blk"
                };
            };
        };
    };
};