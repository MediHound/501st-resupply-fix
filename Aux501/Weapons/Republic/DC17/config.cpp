class cfgPatches
{
    class Aux501_Patch_DC17
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
           "Aux501_Weaps_DC17",
           "Aux501_Weaps_DC17_Shield",
           "Aux501_Weaps_DC17A",
           "Aux501_Weaps_DC17A_Shield",
           "Aux501_Weaps_DC17_Signal",
           "Aux501_Weaps_DC17_Dual"
        };
    };
};

class Mode_FullAuto;

class CowsSlot;

class cfgWeapons
{
    class Pistol_Base_F;
    class hgun_P07_F;
    class Aux501_stun_muzzle;
    class Aux501_pistol_base: hgun_P07_F
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_DC17: Aux501_pistol_base
    {
        scope = 2;
        displayName = "[501st] DC-17";
        baseWeapon = "Aux501_Weaps_DC17";
        JLTS_canHaveShield = 1;
        JLTS_shieldedWeapon = "Aux501_Weaps_DC17_Shield";
        picture = "\MRC\JLTS\weapons\DC17SA\data\ui\DC17SA_ui_ca.paa";
        model = "\MRC\JLTS\weapons\DC17SA\DC17SA.p3d";
        magazines[] =
        {
            "Aux501_Weapons_Mags_20mwdp20"
        };
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC17\sounds\DC17Fire.wss",1,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo{};
    };
    class Aux501_Weaps_DC17_Shield: Aux501_Weaps_DC17
    {
        scope = 1;
        displayName = "[501st] DC-17 (Shield)";
        baseWeapon = "Aux501_Weaps_DC17_Shield";
        JLTS_isShielded = 1;
        JLTS_baseWeapon = "Aux501_Weaps_DC17";
        model = "\MRC\JLTS\weapons\DC17SA\DC17SA_shielded.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\DC17SA\anims\DC17SA_shielded_handanim.rtm"};
        inertia = 0.8;
        recoil = "recoil_pdw";
        reloadAction="";
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            mass=110;
            class UnderBarrelSlot
            {
                linkProxy="\A3\Data_F_Mark\Proxies\Weapon_Slots\UNDERBARREL";
                iconPicture="\A3\Weapons_F_Mark\Data\UI\attachment_under.paa";
                iconPinpoint="Bottom";
                compatibleItems[]=
                {
                    "JLTS_riot_shield_attachment",
                    "JLTS_riot_shield_212_attachment",
                    "JLTS_riot_shield_501_attachment",
                    "JLTS_riot_shield_101_attachment",
                    "JLTS_riot_shield_CG_attachment",
                    "JLTS_riot_shield_GD_attachment",
                    "JLTS_riot_shield_droid_attachment"
                };
            };
        };
    };
    class Aux501_Weaps_DC17A: Aux501_Weaps_DC17
    {
        displayName = "[501st] DC-17A";
        baseWeapon = "Aux501_Weaps_DC17A";
        JLTS_canHaveShield = 1;
        JLTS_shieldedWeapon = "Aux501_Weaps_DC17A_Shield";
        magazines[] =
        {
            "Aux501_Weapons_Mags_20mw30"
        };
        modes[] = {"FullAuto","Single"};
        recoil = "recoil_SMG_03";
        class FullAuto: Single
        {
            autoFire=1;
            reloadTime=0.1;
            displayName="$STR_DN_MODE_FULLAUTO";
            textureType="fullAuto";
            aiDispersionCoefY=3;
            aiDispersionCoefX=2;
            soundBurst=0;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC17\sounds\DC17Fire.wss",1,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo{};
    };
    class Aux501_Weaps_DC17A_Shield: Aux501_Weaps_DC17A
    {
        scope = 1;
        displayName = "[501st] DC-17A (Shield)";
        baseWeapon = "Aux501_Weaps_DC17A_Shield";
        JLTS_isShielded = 1;
        JLTS_baseWeapon = "Aux501_Weaps_DC17A";
        model = "\MRC\JLTS\weapons\DC17SA\DC17SA_shielded.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\DC17SA\anims\DC17SA_shielded_handanim.rtm"};
        inertia = 0.8;
        recoil = "recoil_pdw";
        reloadAction="";
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            mass=110;
            class UnderBarrelSlot
            {
                linkProxy="\A3\Data_F_Mark\Proxies\Weapon_Slots\UNDERBARREL";
                iconPicture="\A3\Weapons_F_Mark\Data\UI\attachment_under.paa";
                iconPinpoint="Bottom";
                compatibleItems[]=
                {
                    "JLTS_riot_shield_attachment",
                    "JLTS_riot_shield_212_attachment",
                    "JLTS_riot_shield_501_attachment",
                    "JLTS_riot_shield_101_attachment",
                    "JLTS_riot_shield_CG_attachment",
                    "JLTS_riot_shield_GD_attachment",
                    "JLTS_riot_shield_droid_attachment"
                };
            };
        };
    };
    class Aux501_Weaps_DC17_Signal: Aux501_Weaps_DC17
    {
        scope = 2;
        displayName = "[501st] DC-17 Signal Pistol";
        baseWeapon = "Aux501_Weaps_DC17_Signal";
        JLTS_canHaveShield = 0;
        magazines[] =
        {
            "Aux501_Weapons_Mags_GL_flare_White1",
            "Aux501_Weapons_Mags_GL_flare_IR1".
            "Aux501_Weapons_Mags_GL_flare_Green10",
            "Aux501_Weapons_Mags_GL_flare_Red1",
            "Aux501_Weapons_Mags_GL_flare_Yellow1",
            "Aux501_Weapons_Mags_GL_flare_Blue1",
            "Aux501_Weapons_Mags_GL_flare_Cyan1",
            "Aux501_Weapons_Mags_GL_flare_Purple1",
            "1Rnd_Smoke_Grenade_shell",
            "1Rnd_SmokePurple_Grenade_shell",
            "1Rnd_SmokeYellow_Grenade_shell",
            "1Rnd_SmokeRed_Grenade_shell",
            "1Rnd_SmokeGreen_Grenade_shell",
            "1Rnd_SmokeBlue_Grenade_shell",
            "1Rnd_SmokeOrange_Grenade_shell",
            "ACE_HuntIR_M203"
        };
        class WeaponSlotsInfo: WeaponSlotsInfo{};
    };
    class Aux501_Weaps_DC17_Dual: Aux501_Weaps_DC17
    {
        displayName = "[501st] Dual DC-17s";
        baseWeapon = "Aux501_Weaps_DC17_Dual";
        JLTS_canHaveShield = 0;
        picture = "\Aux501\Weapons\Republic\DC17\DC17_Dual_ui.paa";
        model = "LF_Weapon_Unit\dc17dualarc\dc17arcdual.p3d";
        handAnim[] = {"OFP2_ManSkeleton","LF_Weapon_Unit\dc17dualarc\anim\attempt1.rtm"};
        magazines[] =
        {
            "Aux501_Weapons_Mags_20mwdp40_dual"
        };
        reloadAction = "3AS_GestureReloadDualDC17S";
        class Stun: Aux501_stun_muzzle
        {
            displayName = "High Energy Stun Mode";
            magazines[] =
            {
                "Aux501_Weapons_Mags_stun5"
            };
            reloadAction = "3AS_GestureReloadDualDC17S";
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                compatibleItems[] = {};
            };
        };
    };
};