class cfgPatches
{
    class Aux501_Patch_DC17M
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_DC17M"
        };
    };
};

class Pointerslot;

class cfgWeapons
{
    class Aux501_stun_muzzle;
    class arifle_MX_Base_F;
    class Aux501_rifle_base: arifle_MX_Base_F
    {
        class Single;
        class FullAuto;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_DC17M: Aux501_rifle_base
    {
        scope = 2;
        displayName="[501st] DC-17M";
        baseWeapon="Aux501_Weaps_DC17M";
        picture="\Aux501\Weapons\Republic\DC17M\dc17m_ui.paa";
        model="\3AS\3AS_Weapons\DC17M\3AS_DC17M_f";
        handAnim[]=
        {
            "OFP2_ManSkeleton",
            "3AS\3AS_Weapons\Data\Anim\DC17M.rtm"
        };
        reloadTime=0.1;
        reloadAction="3AS_GestureReload_DC17M";
        reloadMagazineSound[] = {"\3AS\3AS_Main\Sounds\DC15A\DC15aReload",1,1,30};
        recoil = "recoil_SMG_03";
        magazines[]=
        {
            "Aux501_Weapons_Mags_10mw71"
        };
        modes[] = {"FullAuto","Single"};
        class stun: Aux501_stun_muzzle{};
        class Single:Single
        {
            reloadTime=0.047;
            dispersion=0.0006;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\SWLW_clones_spec\sounds\DC17M_blaster_fire.wss",1,1,1800};
                begin2[] = {"\SWLW_clones_spec\sounds\DC17M_blaster_fire.wss",1,1,1800};
                begin3[] = {"\SWLW_clones_spec\sounds\DC17M_blaster_fire.wss",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        class FullAuto: FullAuto
        {
            reloadTime = 0.047;
            dispersion = 0.0006;
            soundContinuous = 0;
            soundBurst = 0;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\SWLW_clones_spec\sounds\DC17M_blaster_fire.wss",1,1,1800};
                begin2[] = {"\SWLW_clones_spec\sounds\DC17M_blaster_fire.wss",1,1,1800};
                begin3[] = {"\SWLW_clones_spec\sounds\DC17M_blaster_fire.wss",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        modelOptics = "3AS\3AS_Weapons\Data\3AS_2D_Optic.p3d";
        class OpticsModes
        {
            class Ironsights
            {
                opticsID = 1;
                useModelOptics = 0;
                opticsFlare = "true";
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.25;
                opticsZoomMax = 1.25;
                opticsZoomInit = 0.75;
                memoryPointCamera = "eye";
                visionMode[] = {};
                distanceZoomMin = 100;
                distanceZoomMax = 100;
            };
            class Scope: Ironsights
            {
                opticsID = 2;
                useModelOptics = 1;
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.125;
                opticsZoomMax = 0.125;
                opticsZoomInit = 0.125;
                memoryPointCamera = "opticView";
                visionMode[] = {"Normal","NVG"};
                opticsFlare = "true";
                distanceZoomMin = 100;
                distanceZoomMax = 100;
                cameraDir = "";
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {    
            class PointerSlot: PointerSlot
            {
                compatibleItems[] = {"acc_flashlight","acc_pointer_IR"};
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot";  
            };
        };
    };
};