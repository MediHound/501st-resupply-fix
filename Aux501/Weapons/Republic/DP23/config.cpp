class cfgPatches
{
    class Aux501_Patch_DP23
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_DP23",
            "Aux501_Weaps_DP23_shield"
        };
    };
};

class PointerSlot;
class UnderBarrelSlot;

class cfgWeapons
{
    class Aux501_stun_muzzle;
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_DP23: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName="[501st] DP-23";
        baseWeapon="Aux501_Weaps_DP23";
        picture="\MRC\JLTS\weapons\DP23\data\ui\DP23_ui_ca.paa";
        JLTS_canHaveShield = 1;
        JLTS_shieldedWeapon = "Aux501_Weaps_DP23_shield";
        model="\MRC\JLTS\weapons\DP23\DP23.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\DP23\anims\DP23_handanim.rtm"};
        reloadMagazineSound[] = {"\3AS\3AS_Main\Sounds\DC15A\DC15aReload",1,1,30};
        recoil = "JLTS_recoil_DP23";
        magazines[]=
        {
            "Aux501_Weapons_Mags_shotgun_scatter20"
        };
        modes[] = {"Single"};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"swlw_rework\sounds\shotgun\DP23_shot.wss",1,1,1800};
                begin2[] = {"swlw_rework\sounds\shotgun\DP23_shot.wss",1,1,1800};
                begin3[] = {"swlw_rework\sounds\shotgun\DP23_shot.wss",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.34};
            };
            reloadTime = 0.5;
            dispersion = 0.00073;
            minRange = 1;
            minRangeProbab = 0.5;
            midRange = 30;
            midRangeProbab = 0.7;
            maxRange = 60;
            maxRangeProbab = 0.3;
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class PointerSlot: PointerSlot
            {
                compatibleItems[] = {"acc_flashlight","acc_pointer_IR"};
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot"; 
            };
        };
    };
    class Aux501_Weaps_DP23_shield: Aux501_Weaps_DP23
    {
        displayName= "[501st] DP-23 (Shield)";
        baseWeapon = "Aux501_Weaps_DP23_shield";
        scope = 1;
        JLTS_isShielded = 1;
        JLTS_baseWeapon = "Aux501_Weaps_DP23";
        model = "\MRC\JLTS\weapons\DP23\DP23_shielded.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\DP23\data\DP23_co.paa","\MRC\JLTS\weapons\Shield\data\shield_co.paa"};
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\DP23\anims\DP23_shielded_handanim.rtm"};
        reloadAction = "GestureReload";
        inertia = 0.8;
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            mass = 110;
            class UnderBarrelSlot: UnderBarrelSlot
            {
                compatibleItems[] = 
                {
                    "JLTS_riot_shield_attachment",
                    "JLTS_riot_shield_212_attachment",
                    "JLTS_riot_shield_501_attachment",
                    "JLTS_riot_shield_101_attachment",
                    "JLTS_riot_shield_CG_attachment",
                    "JLTS_riot_shield_GD_attachment",
                    "JLTS_riot_shield_droid_attachment"
                };
            };
        };
    };
};