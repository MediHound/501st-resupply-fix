class cfgPatches
{
    class Aux501_Patch_WestarM5
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_WestarM5"
        };
    };
};

class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;

class Mode_SemiAuto;

class cfgWeapons
{
    class Aux501_stun_muzzle;
    class arifle_MX_Base_F;
    class Aux501_rifle_base: arifle_MX_Base_F
    {
        class Single;
        class FullAuto;
        class WeaponSlotsInfo;
    };
    class UGL_F;

    class Aux501_Weaps_WestarM5: Aux501_rifle_base
    {
        scope = 2;
        displayName="[501st] Westar M5";
        baseWeapon="Aux501_Weaps_WestarM5";
        picture="\SWLW_clones\smgs\westar_m5\data\ui\WestarM5_ui.paa";
        model="3AS\3AS_Weapons\WestarM5\3AS_Westar_M5_GL.p3d";
        handAnim[]=
        {
            "OFP2_ManSkeleton",
            "\3AS\3AS_Weapons\WestarM5\Data\Anim\WestarM5_handanim.rtm"
        };
        recoil = "recoil_SMG_03";
        muzzles[]=
        {
            "this",
            "Stun",
            "Aux501_WestarM5_UGL_F"
        };
        magazines[]=
        {
            "Aux501_Weapons_Mags_10mwsp40",
            "Aux501_Weapons_Mags_10mw50"
        };
        modes[] = {"FullAuto","Single"};
        class stun: Aux501_stun_muzzle{};
        class FullAuto: FullAuto
        {
            class BaseSoundModeType;
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"3AS_Westar_Shot_SoundSet"};
            };
            class SilencedSound
            {
                begin1[] = {"327th_weapons\327th_dc17v2\Laser Gun Sound Effect 2.ogg",0.9,1,1800};
                closure1[] = {};
                closure2[] = {};
                soundBegin[] = {"begin1",1};
                soundClosure[] = {};
                weaponSoundEffect = "";
            };	
            reloadTime=0.1;
        };
        class Single: Single
        {
            class BaseSoundModeType;
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"3AS_Westar_Shot_SoundSet"};
            };
            class SilencedSound
            {
                begin1[] = {"327th_weapons\327th_dc17v2\Laser Gun Sound Effect 2.ogg",0.9,1,1800};
                closure1[] = {};
                closure2[] = {};
                soundBegin[] = {"begin1",1};
                soundClosure[] = {};
                weaponSoundEffect = "";
            };	
            reloadTime=0.1;
        };
        class Aux501_WestarM5_UGL_F: UGL_F
        {
            displayName = "[501st] Underbarrel Grenade Launcher";
            descriptionShort = "Underbarrel GL Module for Westar";
            useModelOptics = 0;
            useExternalOptic = 0;
            magazines[] = 
            {
                "ACE_HuntIR_M203"
            };
            magazineWell[] = {};
            cameraDir = "OP_look";
            discreteDistance[] = {75,100,150,200,250,300,350,400};
            discreteDistanceCameraPoint[] = {"OP_eye_75","OP_eye_100","OP_eye_150","OP_eye_200","OP_eye_250","OP_eye_300","OP_eye_350","OP_eye_400"};
            discreteDistanceInitIndex = 1;
            reloadAction = "GestureReloadMXUGL";
            reloadMagazineSound[] = {"A3\Sounds_F\arsenal\weapons\Rifles\MX\Mx_UGL_reload",1,1,10};
            class Single: Mode_SemiAuto
            {
                sounds[] = {"StandardSound"};
                class BaseSoundModeType
                {
                    weaponSoundEffect = "";
                    closure1[] = {};
                    closure2[] = {};
                    soundClosure[] = {};
                };
                class StandardSound: BaseSoundModeType
                {
                    weaponSoundEffect = "";
                    begin1[] = {"SWLW_clones\rifles\gl\sounds\gl",1,1,1800};
                    begin2[] = {"SWLW_clones\rifles\gl\sounds\gl",1,1,1800};
                    begin3[] = {"SWLW_clones\rifles\gl\sounds\gl",1,1,1800};
                    soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
                };
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = 
                {
                    "Aux501_cows_RCO",
                    "Aux501_cows_RCO_2",
                    "Aux501_cows_RCO_3",
                    "Aux501_cows_Holosight",
                    "Aux501_cows_Holosight_2",
                    "Aux501_cows_Holosight_3",
                    "Aux501_cows_HoloScope",
                    "Aux501_cows_HoloScope_2",
                    "Aux501_cows_HoloScope_3",
                    "Aux501_cows_MRCO",
                    "Aux501_cows_MRCO_2",
                    "Aux501_cows_MRCO_3",
                    "Aux501_cows_DMS",
                    "Aux501_cows_DMS_2",
                    "Aux501_cows_DMS_3",
                    "Aux501_cows_DMS_4",
                    "Aux501_cows_reflex_optic",
                    "3AS_Optic_Scope_WestarM5"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                linkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName="$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                compatibleItems[]=
                {
                    "Aux501_muzzle_flash",
                    "Aux501_muzzle_surpressor"
                };
            };
            class PointerSlot: PointerSlot
            {
                compatibleItems[] = {"acc_flashlight","acc_pointer_IR"};
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot"; 
            };
        };
    };
};