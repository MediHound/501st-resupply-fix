class cfgPatches
{
    class Aux501_Patch_Z6
    {
        Author = "501st Aux Team";
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_Z6"
        };
    };
};

class Mode_FullAuto;
class MuzzleSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_Z6: Aux501_rifle_base_stunless
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] Z-6 Rotary Blaster Cannon";
        baseWeapon = "Aux501_Weaps_Z6";
        picture = "\MRC\JLTS\weapons\Z6\data\ui\Z6_ui_ca.paa";
        model = "\MRC\JLTS\weapons\Z6\Z6.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\Z6\data\Z6_1_co.paa","\MRC\JLTS\weapons\Z6\data\Z6_2_co.paa"};
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\Z6\anims\Z6_handanim.rtm"};
        recoil = "recoil_lim";
        reloadAction = "ReloadMagazine";
        magazines[]=
        {
            "Aux501_Weapons_Mags_10mw400"
        };
        modes[] = {"manual", "Overcharge"};
        class manual: Mode_FullAuto
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"327th_weapons\data\Addons_DBA_Weapons_Z6_Z6.ogg",1,1,1800};
                soundBegin[] = {"begin1",1};
            };
            reloadTime = 0.075;
            dispersion = 0.00102;
            soundContinuous = 0;
            soundBurst = 0;
            minRange = 0;
            minRangeProbab = 0.3;
            midRange = 5;
            midRangeProbab = 0.7;
            maxRange = 10;
            maxRangeProbab = 0.04;
            showToPlayer = 1;
        };
        class Overcharge: manual
        {
            dispersion = "0.00015*4";
            displayName = "Overcharge";
            reloadTime = "0.015";
            burst = 25;
            textureType = "fastAuto";
        };
        class close: manual
        {
            burst = 10;
            aiRateOfFire = 0.5;
            aiRateOfFireDistance = 50;
            minRange = 10;
            minRangeProbab = 0.05;
            midRange = 20;
            midRangeProbab = 0.7;
            maxRange = 50;
            maxRangeProbab = 0.04;
            showToPlayer = 0;
        };
        class short: close
        {
            burst = 8;
            aiRateOfFire = 2;
            aiRateOfFireDistance = 300;
            minRange = 50;
            minRangeProbab = 0.05;
            midRange = 150;
            midRangeProbab = 0.7;
            maxRange = 300;
            maxRangeProbab = 0.04;
        };
        class medium: close
        {
            burst = 7;
            aiRateOfFire = 4;
            aiRateOfFireDistance = 600;
            minRange = 200;
            minRangeProbab = 0.05;
            midRange = 300;
            midRangeProbab = 0.7;
            maxRange = 500;
            maxRangeProbab = 0.1;
        };
        class far_optic1: medium
        {
            requiredOpticType = 1;
            showToPlayer = 0;
            burst = 3;
            aiRateOfFire = 10;
            aiRateOfFireDistance = 1000;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 500;
            midRangeProbab = 0.4;
            maxRange = 650;
            maxRangeProbab = 0.01;
        };
        class far_optic2: far_optic1
        {
            burst = 3;
            requiredOpticType = 2;
            minRange = 400;
            minRangeProbab = 0.05;
            midRange = 750;
            midRangeProbab = 0.7;
            maxRange = 900;
            maxRangeProbab = 0.01;
            aiRateOfFire = 10;
            aiRateOfFireDistance = 900;
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class MuzzleSlot: MuzzleSlot
            {
                linkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName="$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                iconPosition[] = {0.24,0.35};
                iconScale = 0.2;
                compatibleItems[]=
                {
                    "Aux501_muzzle_flash"
                };
            };
        };
    };
};