class cfgPatches
{
    class Aux501_Patch_Drexl
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_Drexl"
        };
    };
};

class CowsSlot;
class PointerSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
        class FullAuto;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_Drexl: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName="[501st] SX-17 'Drexl'";
        baseWeapon="Aux501_Weaps_Drexl";
        picture="\Aux501\Weapons\Republic\Drexl\data\textures\UI\drexl_ui.paa";
        model="\Aux501\Weapons\Republic\Drexl\Drexl.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\A3\Weapons_F_beta\Rifles\MK20\Data\Anim\mk20G.rtm"};
        magazines[] =
        {
            "Aux501_Weapons_Mags_shotgun_slug24",
            "Aux501_Weapons_Mags_shotgun_scatter20",
            "Aux501_Weapons_Mags_shotgun_he10"
        };
        recoil = "recoil_MSBS65_ubs";
        reloadAction = "GestureReloadAK12";
        modes[] = {"FullAuto","Single"};
        class Single: Single
        {
            sounds[]={"StandardSound"};
            class StandardSound
            {
                soundSetShot[] = {"Msbs65_01_Shotgun_Shot_SoundSet","Msbs65_01_Shotgun_Tail_SoundSet"};
            };
            reloadTime = 0.35;
            dispersion = "5*0.00087";
            minRange = 2;
            minRangeProbab = 0.5;
            midRange = 400;
            midRangeProbab = 0.7;
            maxRange = 800;
            maxRangeProbab = 0.3;
        };
        class FullAuto: FullAuto
        {
            sounds[]={"StandardSound"};
            class StandardSound
            {
                soundSetShot[] = {"Msbs65_01_Shotgun_Shot_SoundSet","Msbs65_01_Shotgun_Tail_SoundSet"};
            };
            reloadTime = 0.275;
            dispersion = "5*0.00087";
            minRange = 2;
            minRangeProbab = 0.5;
            midRange = 400;
            midRangeProbab = 0.7;
            maxRange = 800;
            maxRangeProbab = 0.3;
            aiRateOfFire=2;
            aiRateOfFireDistance=500;
        };
        inertia = 1.4;
        dexterity=1.7;
        initSpeed=300;
        maxRecoilSway=0.0125;
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = 
                {
                    "Aux501_cows_Holosight",
                    "Aux501_cows_Holosight_2",
                    "Aux501_cows_Holosight_3",
                    "3AS_optic_reflex_DC15C"
                };
            };
            class PointerSlot: PointerSlot
            {
                compatibleItems[] = {"acc_flashlight","acc_pointer_IR"};
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot"; 
            };
        };
    };
};