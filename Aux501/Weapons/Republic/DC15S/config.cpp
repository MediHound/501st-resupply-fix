class cfgPatches
{
    class Aux501_Patch_DC15S
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_DC15S"
        };
    };
};

class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;

class cfgWeapons
{
    class Aux501_stun_muzzle;
    class arifle_MX_Base_F;
    class Aux501_rifle_base: arifle_MX_Base_F
    {
        class Single;
        class FullAuto;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_DC15S: Aux501_rifle_base
    {
        scope = 2;
        displayName="[501st] DC-15S";
        baseWeapon="Aux501_Weaps_DC15S";
        picture="\MRC\JLTS\weapons\DC15S\data\ui\DC15S_ui_ca.paa";
        JLTS_canHaveShield = 1;
        JLTS_shieldedWeapon = "Aux501_Weaps_DC15S_shield";
        model="\MRC\JLTS\weapons\DC15S\DC15S.p3d";
        handAnim[]=
        {
            "OFP2_ManSkeleton",
            "\MRC\JLTS\weapons\DC15S\anims\DC15S_handanim.rtm"
        };
        reloadTime=0.66;
        reloadAction="GestureReload_JLTS_DC15S";
        reloadMagazineSound[] = {"\3AS\3AS_Main\Sounds\DC15A\DC15aReload",1,1,30};
        recoil = "recoil_SMG_03";
        magazines[]=
        {
            "Aux501_Weapons_Mags_10mw50"
        };
        modes[] = {"FullAuto","Single"};
        class stun: Aux501_stun_muzzle{};
        class FullAuto: FullAuto
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC15S\sounds\dc15s_shot.wss",1,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC15S\sounds\dc15s_shot.wss",1,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            reloadTime = 0.09;
            dispersion = 0.00087;
        };
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC15S\sounds\dc15s_shot.wss",1,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC15S\sounds\dc15s_shot.wss",1,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            reloadTime = 0.09;
            dispersion = 0.00087;
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = 
                {
                    "Aux501_cows_RCO",
                    "Aux501_cows_RCO_2",
                    "Aux501_cows_RCO_3",
                    "Aux501_cows_Holosight",
                    "Aux501_cows_Holosight_2",
                    "Aux501_cows_Holosight_3",
                    "Aux501_cows_HoloScope",
                    "Aux501_cows_HoloScope_2",
                    "Aux501_cows_HoloScope_3",
                    "Aux501_cows_MRCO",
                    "Aux501_cows_MRCO_2",
                    "Aux501_cows_MRCO_3",
                    "Aux501_cows_DMS",
                    "Aux501_cows_DMS_2",
                    "Aux501_cows_DMS_3",
                    "Aux501_cows_DMS_4",
                    "Aux501_cows_reflex_optic"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                linkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName="$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                compatibleItems[]=
                {
                    "Aux501_muzzle_flash"
                };
            };
            class PointerSlot: PointerSlot
            {
                compatibleItems[] = {"acc_flashlight","acc_pointer_IR"};
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot"; 
            };
        };
    };
    class Aux501_Weaps_DC15S_shield: Aux501_Weaps_DC15S
    {
        displayName= "[501st] DC-15S (Shield)";
        baseWeapon = "Aux501_Weaps_DC15S_shield";
        scope = 1;
        JLTS_isShielded = 1;
        JLTS_baseWeapon = "Aux501_Weaps_DC15S";
        model = "\MRC\JLTS\weapons\DC15S\DC15S_shielded.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\DC15S\data\DC15S_co.paa","\MRC\JLTS\weapons\Shield\data\shield_co.paa"};
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\DC15S\anims\DC15S_shielded_handanim.rtm"};
        reloadAction = "GestureReload";
        inertia = 0.8;
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            mass = 110;
            class UnderBarrelSlot: UnderBarrelSlot
            {
                compatibleItems[] = 
                {
                    "JLTS_riot_shield_attachment",
                    "JLTS_riot_shield_212_attachment",
                    "JLTS_riot_shield_501_attachment",
                    "JLTS_riot_shield_101_attachment",
                    "JLTS_riot_shield_CG_attachment",
                    "JLTS_riot_shield_GD_attachment",
                    "JLTS_riot_shield_droid_attachment"
                };
            };
        };
    };
};