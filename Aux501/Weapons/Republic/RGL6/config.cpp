class cfgPatches
{
    class Aux501_Patch_RGL6
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_RGL6"
        };
    };
};

class CowsSlot;
class Mode_SemiAuto;
class UGL_F;
class CfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };
    class Aux501_Weaps_RGL6: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName="[501st] RGL-6";
        baseWeapon="Aux501_Weaps_RGL6";
        model = "swlw_rework\rifles\gl\GL.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\A3\Weapons_F_epa\LongRangeRifles\DMR_01\Data\Anim\dmr_01.rtm"};
        reloadAction = "ReloadMagazine";
        picture = "\swlw_clones\rifles\gl\data\ui\GL_ui.paa";
        cameraDir = "OP_look";
        discreteinitindex = 0;
        discretedistance[] = {100,200,300,400};
        discreteDistanceCameraPoint[] = {"OP_eye","OP_eye2","OP_eye3","OP_eye4"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_RGL_HE6",
            "Aux501_Weapons_Mags_RGL_AP6",
            "Aux501_Weapons_Mags_GL_smoke_white6"
        };
        modes[] = {"Single","close","short","medium"};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"swlw_rework\sounds\shotgun\DP20_shot.wss",1,1,1800};
                begin2[] = {"swlw_rework\sounds\shotgun\DP20_shot.wss",1,1,1800};
                begin3[] = {"swlw_rework\sounds\shotgun\DP20_shot.wss",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.34};
            };
            reloadTime = 0.15;
            dispersion = 0.003015;
            minRange = 5;
            minRangeProbab = 0.3;
            midRange = 25;
            midRangeProbab = 0.6;
            maxRange = 50;
            maxRangeProbab = 0.1;
            aiRateOfFire = 2;
            aiRateOfFireDistance = 25;
        };
        class close: Single
        {
            showToPlayer = 0;
            aiRateOfFire = 0.25;
            aiRateOfFireDistance = 400;
            minRange = 0;
            minRangeProbab = 0.05;
            midRange = 200;
            midRangeProbab = 0.7;
            maxRange = 400;
            maxRangeProbab = 0.2;
        };
        class short: close
        {
            aiRateOfFire = 0.5;
            aiRateOfFireDistance = 500;
            minRange = 300;
            minRangeProbab = 0.2;
            midRange = 400;
            midRangeProbab = 0.7;
            maxRange = 500;
            maxRangeProbab = 0.2;
        };
        class medium: close
        {
            aiRateOfFire = 1;
            aiRateOfFireDistance = 900;
            minRange = 400;
            minRangeProbab = 0.2;
            midRange = 700;
            midRangeProbab = 0.7;
            maxRange = 900;
            maxRangeProbab = 0.2;
        };
        initspeed = 80;
        maxRecoilSway = 0.0125;
        swayDecaySpeed = 1.25;
        inertia = 0.5;
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            mass = 180;
        };
    };
};