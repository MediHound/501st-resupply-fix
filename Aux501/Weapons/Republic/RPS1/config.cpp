class cfgPatches
{
    class Aux501_Patch_RPS1
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_RPS1"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_rps1_preload",
            "Aux501_Weapons_Mags_rps1_preload_fired"
        };
    };
};

class CBA_DisposableLaunchers 
{
    Aux501_Weaps_RPS1_base[] = {"Aux501_Weaps_RPS1","Aux501_Weaps_RPS1_used"};
};

class cfgWeapons
{
    class Aux501_launcher_base;
    class Aux501_Weaps_RPS1_base: Aux501_launcher_base
    {
        scope = 1;
        scopeArsenal = 1;
        displayName="[501st] RPS-1 Disposable Launcher";
        baseWeapon="Aux501_Weaps_RPS1";
        model = "\ls_weapons\tertiary\rps6\ls_weapon_rps6";
        hiddenSelectionsTextures[] = {"Aux501\Weapons\Republic\RPS1\data\rps1_co.paa"};
        hiddenSelections[]={"camo1"};
        picture = "\Aux501\Weapons\Republic\RPS1\data\rps1_ui.paa";
        uiPicture = "\Aux501\Weapons\Republic\RPS1\data\rps1_ui.paa";
        weaponInfoType = "RscWeaponEmpty";
        handAnim[] = 
        {
            "OFP2_ManSkeleton",
            "\SWLW_clones\launchers\rps6\anims\RPS6_handanim.rtm"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_rps1"
        };
        magazineReloadTime = 0.1;
        reloadMagazineSound[] = {"",1,1};
        modelOptics = "SWLW_clones\launchers\rps6\scope.p3d";
        class OpticsModes 
        {
            class optic 
            {
                cameraDir = "look";
                distanceZoomMax = 300;
                distanceZoomMin = 300;
                memoryPointCamera = "eye";
                opticsDisablePeripherialVision = 1;
                opticsFlare = 1;
                opticsID = 1;
                opticsPPEffects = ["OpticsCHAbera1","OpticsBlur1"];
                opticsZoomInit = 0.0875;
                opticsZoomMax = 0.0875;
                opticsZoomMin = 0.0875;
                useModelOptics = 1;
                visionMode = ["Normal","NVG"];
            };
        };
        class EventHandlers 
        {
            fired = "_this call CBA_fnc_firedDisposable";
        };
    };
    class Aux501_Weaps_RPS1: Aux501_Weaps_RPS1_base
    {
        scope = 2;
        scopeArsenal = 2;
        baseWeapon = "Aux501_Weaps_RPS1_loaded";
        magazines[] = {"CBA_FakeLauncherMagazine"};
    };
    class Aux501_Weaps_RPS1_used: Aux501_Weaps_RPS1_base
    {
        scope = 1;
        scopeArsenal = 1;
        displayName="[501st] RPS1 (used)";
        baseWeapon = "Aux501_Weaps_RPS1_used";
        magazines[] = {"CBA_FakeLauncherMagazine"};
    };
};

class CfgMagazines
{
    class CA_LauncherMagazine;
    
    class Aux501_Weapons_Mags_rps1: CA_LauncherMagazine
    {
        displayName = "[501st] RPS-1 Rocket";
        author = "501st Aux Team";
        picture = "\A3\Weapons_F\launchers\RPG32\data\UI\gear_rpg32_rocket_ca.paa";
        model = "\ls_weapons\tertiary\rps6\ls_magazine_rps6";
        modelSpecialIsProxy = 1;
        modelSpecial = "\ls_weapons\tertiary\rps6\ls_magazine_rps6";
        ammo = "Aux501_Weapons_Ammo_rps1";
        scope = 2;
        scopearsenal = 2;
        weight=1;
        mass = 1;
        initSpeed = 140;
        hit = 800;
    };
};

class CfgAmmo
{
    class R_PG32V_F;

    class Aux501_Weapons_Ammo_rps1: R_PG32V_F
    {
        aiAmmoUsageFlags = "64 + 128 + 256 + 512";
        allowAgainstInfantry = 1;
        hit = 600;
        indirectHit = 100;
        indirectHitRange = 10;
    };
};