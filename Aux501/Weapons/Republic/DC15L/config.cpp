class cfgPatches
{
    class Aux501_Patch_DC15L
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_DC15L"
        };
    };
};

class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class FullAuto;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_DC15L: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] DC-15L";
        baseWeapon = "Aux501_Weaps_DC15L";
        picture = "Aux501\Weapons\Republic\DC15L\dc15l_ui.paa";
        model = "3AS\3AS_Weapons\DC15L\3AS_DC15L_f.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\3AS\3AS_Weapons\DC15L\Data\Anim\DC15L_handanim.rtm"};
        recoil = "recoil_SMG_03";
        magazines[]=
        {
            "Aux501_Weapons_Mags_20mw240",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwup30"
        };
        modes[] = {"FullAuto"};
        class FullAuto: FullAuto
        {
            reloadTime = 0.1;		
            dispersion = 0.00075;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"Aux501\Weapons\Republic\DC15L\sounds\DC15LFire.wss",1,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                 weaponSoundEffect = "";
                begin1[] = {"Aux501\Weapons\Republic\DC15L\sounds\DC15LFire.wss",1,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class OpticsModes
        {
            class Ironsights
            {
                opticsID = 1;
                useModelOptics = 0;
                opticsFlare = "true";
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.375;
                opticsZoomMax = 1.1;
                opticsZoomInit = 0.75;
                memoryPointCamera = "eye";
                visionMode[] = {};
                distanceZoomMin = 100;
                distanceZoomMax = 100;
            };
            class Scope: Ironsights
            {
                opticsID = 2;
                useModelOptics = 1;
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.122173;
                opticsZoomMax = 0.122173;
                opticsZoomInit = 0.122173;
                memoryPointCamera = "opticView";
                visionMode[] = {"Normal","NVG"};
                opticsFlare = "true";
                distanceZoomMin = 100;
                distanceZoomMax = 100;
                cameraDir = "";
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                scope = 0;
                compatibleItems[] = 
                {
                    "3AS_Optic_DC15L",
                    "Aux501_cows_RCO",
                    "Aux501_cows_RCO_2",
                    "Aux501_cows_RCO_3",
                    "Aux501_cows_Holosight",
                    "Aux501_cows_Holosight_2",
                    "Aux501_cows_Holosight_3",
                    "Aux501_cows_HoloScope",
                    "Aux501_cows_HoloScope_2",
                    "Aux501_cows_HoloScope_3",
                    "Aux501_cows_MRCO",
                    "Aux501_cows_MRCO_2",
                    "Aux501_cows_MRCO_3",
                    "Aux501_cows_DMS",
                    "Aux501_cows_DMS_2",
                    "Aux501_cows_DMS_3",
                    "Aux501_cows_DMS_4"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                linkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName="$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                iconPosition[] = {0.24,0.35};
                iconScale = 0.2;
                compatibleItems[]=
                {
                    "Aux501_muzzle_flash"
                };
            };
            class PointerSlot: PointerSlot
            {
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot";
                compatibleItems[] = 
                {
                    "acc_flashlight", "acc_pointer_IR"
                }; 
            };
            class UnderBarrelSlot: UnderBarrelSlot
            {
                iconPicture="\A3\Weapons_F_Mark\Data\UI\attachment_under.paa";
                iconPinpoint="Bottom";
                linkProxy="\A3\Data_F_Mark\Proxies\Weapon_Slots\UNDERBARREL";
                compatibleItems[] = 
                {
                    "bipod_01_f_blk", "3AS_Bipod_DC15L_f"
                };
            };
        };
    };
};