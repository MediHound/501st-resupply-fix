class cfgPatches
{
    class Aux501_Patch_Razorblade
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
           "Aux501_Weaps_Razorblade"
        };
    };
};

class cfgWeapons
{
    class Pistol_Base_F;
    class hgun_P07_F;
    class Aux501_stun_muzzle;
    class Aux501_pistol_base: hgun_P07_F
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_Razorblade: Aux501_pistol_base
    {
        scope = 2;
        displayName = "[501st] DC-15SA 'Razorblade'";
        descriptionShort = "Heavy Caliber Sidearm";
        baseWeapon = "Aux501_Weaps_Razorblade";
        model = "\SWLW_clones_spec\DC15SA.p3d";
        picture = "\SWLW_clones_spec\data\ui\DC15SA_ui.paa";
        magazines[] =
        {
            "Aux501_Weapons_Mags_30mw7"
        };
        class Single: Single
        {
            reloadTime = 0.75;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"SWLW_clones_spec\sounds\DC15SA_fire.wss",1,1,1800};
                begin2[] = {"SWLW_clones_spec\sounds\DC15SA_fire.wss",1,1,1800};
                begin3[] = {"SWLW_clones_spec\sounds\DC15SA_fire.wss",1,1,1800};
                begin4[] = {"SWLW_clones_spec\sounds\DC15SA_fire.wss",1,1,1800};
                soundBegin[] = {"begin1",0.25,"begin2",0.25,"begin3",0.25,"begin4",0.25};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo{};
    };
};