class cfgPatches
{
    class Aux501_Patch_DC1911
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
           "Aux501_Weaps_DC1911"
        };
    };
};

class cfgWeapons
{
    class Pistol_Base_F;
    class hgun_P07_F;
    class Aux501_stun_muzzle;
    class Aux501_pistol_base: hgun_P07_F
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_DC1911: Aux501_pistol_base
    {
        scope = 2;
        displayName = "[501st] DC-1911 'Big Iron'";
        descriptionShort = "Heavy Caliber Sidearm";
        baseWeapon = "Aux501_Weaps_DC1911";
        picture = "\Aux501\Weapons\Republic\DC1911\DC1911_ui.paa";
        model = "327th_weapons\327th_1911\data\m1911SW.p3d";
        magazines[] =
        {
            "Aux501_Weapons_Mags_30mw7"
        };
        class Single: Single
        {
            reloadTime = 0.75;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"327th_weapons\327th_1911\data\1911sound",1,1.1,1800};
                soundBegin[] = {"begin1",1};
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo{};
    };
};