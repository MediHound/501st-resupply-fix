class CfgPatches
{
   class Aux501_Patch_Magazines
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] = 
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        requiredVersion = 0.1;
        units[] = {};
        weapons[]={};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_stun5",
            "Aux501_Weapons_Mags_10mw71",
            "Aux501_Weapons_Mags_10mw400",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp40_dual", 
            "Aux501_Weapons_Mags_20mwup30",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw240",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw10",
            "Aux501_Weapons_Mags_40mw5",
            "Aux501_Weapons_Mags_40mwemp1",
            "Aux501_Weapons_Mags_50mw10",
            "Aux501_Weapons_Mags_shotgun_scatter20",
            "Aux501_Weapons_Mags_shotgun_he10",
            "Aux501_Weapons_Mags_shotgun_slug24",
            "Aux501_Weapons_Mags_GL_AP2",
            "Aux501_Weapons_Mags_GL_HE3",
            "Aux501_Weapons_Mags_GL_HE1",
            "Aux501_Weapons_Mags_RGL_HE6",
            "Aux501_Weapons_Mags_RGL_AP6",
            "Aux501_Weapons_Mags_GL_smoke_white6",
            "Aux501_Weapons_Mags_GL_smoke_purple3",
            "Aux501_Weapons_Mags_GL_smoke_yellow3",
            "Aux501_Weapons_Mags_GL_smoke_red3",
            "Aux501_Weapons_Mags_GL_smoke_green3",
            "Aux501_Weapons_Mags_GL_smoke_blue3",
            "Aux501_Weapons_Mags_GL_smoke_orange3",
            "Aux501_Weapons_Mags_GL_flare_White3",
            "Aux501_Weapons_Mags_GL_flare_White1",
            "Aux501_Weapons_Mags_GL_flare_IR3",
            "Aux501_Weapons_Mags_GL_flare_IR1",
            "Aux501_Weapons_Mags_GL_flare_Green3",
            "Aux501_Weapons_Mags_GL_flare_Green1",
            "Aux501_Weapons_Mags_GL_flare_Red3",
            "Aux501_Weapons_Mags_GL_flare_Red1",
            "Aux501_Weapons_Mags_GL_flare_Yellow3",
            "Aux501_Weapons_Mags_GL_flare_Yellow1",
            "Aux501_Weapons_Mags_GL_flare_Blue3",
            "Aux501_Weapons_Mags_GL_flare_Blue1",
            "Aux501_Weapons_Mags_GL_flare_Cyan3",
            "Aux501_Weapons_Mags_GL_flare_Cyan1",
            "Aux501_Weapons_Mags_GL_flare_Purple3",
            "Aux501_Weapons_Mags_GL_flare_Purple1",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_GL3"
        };
    };
};

class CfgMagazines
{
    class 30Rnd_65x39_caseless_mag;
    class 2Rnd_12Gauge_Pellets;
    class 1Rnd_HE_Grenade_shell;
    class JLTS_stun_mag_long;
    class 3Rnd_Smoke_Grenade_shell;
    class UGL_FlareWhite_F;

    ////////////////////////////////////////////////////
    ///////////////Blaster//Magazines///////////////////
    ////////////////////////////////////////////////////
    
    class Aux501_Weapons_Mags_10mw50: 30Rnd_65x39_caseless_mag
    {
        displayName = "[501st] 50Rnd 10MW Cell";
        displayNameShort = "50Rnd 10MW";
        author = "501st Aux Team";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc15c.paa";
        count = 50;
        ammo = "Aux501_Weapons_Ammo_10mw";
        initSpeed = 825;
        descriptionShort = "DC15S Medium Power magazine";
        mass=8;
        modelSpecial = "";
        modelSpecialIsProxy = 0;
        model = "\MRC\JLTS\weapons\DC15S\DC15S_mag.p3d";
        tracersEvery = 1;
    };
    class Aux501_Weapons_Mags_stun5: Aux501_Weapons_Mags_10mw50
    {
        displayName="[501st] 5Rnd Stun Cell";
        displayNameShort = "5rnd Stun";
        descriptionShort = "Stun Rounds to stop muhfuckas from running";
        picture="\MRC\JLTS\weapons\Core\data\ui\stun_mag_ui_ca";
        model = "\MRC\JLTS\weapons\Core\stun_mag.p3d";
        modelSpecial = "";
        modelSpecialIsProxy = 0;
        count=5;
        mass=4;
        ammo = "Aux501_Weapons_Ammo_Stun";
        initSpeed = 50;
        JLTS_stunDuration = 30;
    };
    class Aux501_Weapons_Mags_10mw71: Aux501_Weapons_Mags_10mw50
    {
        displayName = "[501st] 71Rnd 10MW Cell";
        displayNameShort = "71Rnd 10MW";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc15s.paa";
        count = 71;
        model = "SWLW_clones\smgs\westar_m5\WestarM5_mag.p3d";
        modelSpecial = "3AS\3AS_Weapons\DC17M\attachments\blaster_Barrel.p3d";
        modelSpecialIsProxy = 1;
        descriptionShort = "DC17M Medium Power Drum";
    };
    class Aux501_Weapons_Mags_10mw400: Aux501_Weapons_Mags_10mw50
    {
        displayName = "[501st] 400Rnd 10MW Cell";
        displayNameShort = "400Rnd 10MW";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_z6.paa";
        count = 400;
        ammo = "Aux501_Weapons_Ammo_10mw";
        descriptionShort="Z6 Box Magazine";
        model = "\MRC\JLTS\weapons\z6\z6_mag.p3d";
    };
    class Aux501_Weapons_Mags_10mwsp40: Aux501_Weapons_Mags_10mw50
    {
        displayName = "[501st] 40Rnd 10MW SP Cell";
        displayNameShort = "40Rnd 10MW SP";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_10mw_sp.paa";
        count = 50;
        ammo = "Aux501_Weapons_Ammo_10mwsp";
        descriptionShort="Westar Silenced Plasma magazine";
        model = "\MRC\JLTS\weapons\DC15S\DC15S_mag.p3d";
    };
    class Aux501_Weapons_Mags_20mw40: Aux501_Weapons_Mags_10mw50
    {
        displayName = "[501st] 40Rnd 20MW Cell";
        displayNameShort = "40Rnd 20MW";
        picture="\Aux501\Weapons\Magazines\data\Aux501_icon_mag_15mw.paa";
        count = 40;
        ammo = "Aux501_Weapons_Ammo_20mw";
        initSpeed=1050;
        descriptionShort="DC15C magazine";
        model = "\MRC\JLTS\weapons\DC15A\DC15A_mag.p3d";
    };
    class Aux501_Weapons_Mags_20mw30: Aux501_Weapons_Mags_20mw40
    {
        displayName = "[501st] 30Rnd 20MW Cell";
        displayNameShort = "30Rnd 20MW";
        picture="\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc17a.paa";
        count = 30;
        mass=4;
        model = "\MRC\JLTS\weapons\DC17SA\DC17SA_mag.p3d";
        descriptionShort="DC17A magazine";
    };
    class Aux501_Weapons_Mags_20mwdp30: Aux501_Weapons_Mags_20mw40
    {
        displayName = "[501st] 30Rnd 20MW DP Cell";
        displayNameShort = "30Rnd 20MW DP";
        picture="\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc15a_dp.paa";
        count = 30;
        ammo = "Aux501_Weapons_Ammo_20mwdp";
        descriptionShort="DC15A dense plasma magazine";
        model = "\MRC\JLTS\weapons\DC15A\DC15A_mag.p3d";
    };
    class Aux501_Weapons_Mags_20mwdp20: Aux501_Weapons_Mags_20mwdp30
    {
        displayName = "[501st] 20Rnd 20MW DP Cell";
        displayNameShort = "20Rnd 20MW DP";
        picture="\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc17.paa";
        count = 20;
        mass=4;
        descriptionShort="DC17 Pistol Magazine";
        model = "\MRC\JLTS\weapons\DC17SA\DC17SA_mag.p3d";
    };
    class Aux501_Weapons_Mags_20mwdp40_dual: Aux501_Weapons_Mags_20mwdp30
    {
        displayName = "[501st] 40Rnd 20MW DP Dual Cell";
        displayNameShort = "40Rnd 20MW DP";
        count = 40;
        mass=4;
        descriptionShort="Dual DC17 Pistol Magazine";
    };
    class Aux501_Weapons_Mags_20mwup30: Aux501_Weapons_Mags_20mwdp30
    {
        displayName = "[501st] 30Rnd 20MW UP Cell";
        displayNameShort = "30Rnd 20MW UP";
        picture="\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc15a_up.paa";
        count = 30;
        ammo = "Aux501_Weapons_Ammo_20mwup";
        initSpeed=900;
        descriptionShort="DC15A unstable plasma magazine";
        model = "\MRC\JLTS\weapons\DC15A\DC15A_mag.p3d";
    };
    class Aux501_Weapons_Mags_20mw240: Aux501_Weapons_Mags_20mw40
    {
        displayName = "[501st] 240Rnd 20MW Cell";
        displayNameShort = "240Rnd 20MW";
        picture="\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc15l.paa";
        count = 240;
        ammo = "Aux501_Weapons_Ammo_20mw";
        descriptionShort="DC15L Box Magazine";
        model="\MRC\JLTS\weapons\DC15S\DC15S_mag.p3d";
    };
    class Aux501_Weapons_Mags_30mw7: Aux501_Weapons_Mags_20mw40
    {
        displayName = "[501st] 7Rnd 30MW Cell";
        displayNameShort = "7Rnd 30MW";
        picture="\Aux501\Weapons\Magazines\data\Aux501_icon_mag_razorblade.paa";
        count = 10;
        ammo = "Aux501_Weapons_Ammo_30mw";
        initSpeed=900;
        model = "\MRC\JLTS\weapons\DC17SA\DC17SA_mag.p3d";
        descriptionShort="Razorblade magazine";
    };
    class Aux501_Weapons_Mags_30mw10: Aux501_Weapons_Mags_20mw40
    {
        displayName = "[501st] 10Rnd 30MW Cell";
        displayNameShort = "10Rnd 30MW";
        picture="\Aux501\Weapons\Magazines\data\Aux501_icon_mag_valken38.paa";
        count = 10;
        ammo = "Aux501_Weapons_Ammo_30mw";
        initSpeed=1500;
        descriptionShort="Valken 38x High power magazine";
        model="\MRC\JLTS\weapons\DC15S\DC15S_mag.p3d";
    };
    class Aux501_Weapons_Mags_40mw5: Aux501_Weapons_Mags_20mw40
    {
        displayName = "[501st] 5Rnd 40MW Cell";
        displayNameShort = "5Rnd 40MW";
        picture="\Aux501\Weapons\Magazines\data\Aux501_icon_mag_valken38.paa";
        count = 5;
        ammo = "Aux501_Weapons_Ammo_40mw";
        initSpeed=1100;
        descriptionShort="DC15X High power magazine";
        model = "\MRC\JLTS\weapons\DC15x\DC15x_mag.p3d";
    };
    class Aux501_Weapons_Mags_40mwemp1: Aux501_Weapons_Mags_40mw5
    {
        displayName = "[501st] 1Rnd 40MW EMP Cell";
        displayNameShort = "1Rnd 40MW EMP";
        picture="\Aux501\Weapons\Magazines\data\Aux501_icon_mag_valken38.paa";
        count = 1;
        ammo = "Aux501_Weapons_Ammo_40mwemp";
        descriptionShort="DC15X High power magazine";
        model = "\MRC\JLTS\weapons\DC15x\DC15x_mag.p3d";
    };
    class Aux501_Weapons_Mags_50mw10: Aux501_Weapons_Mags_40mw5
    {
        displayName = "[501st] 10Rnd 50MW Cell";
        displayNameShort = "10Rnd 50MW";
        picture = "\MRC\JLTS\weapons\DC15A\data\ui\DC15A_mag_ui_ca.paa";
        count = 10;
        ammo = "Aux501_Weapons_Ammo_50mw";
        descriptionShort="RAMR High Power magazine";
        model = "\MRC\JLTS\weapons\DC15A\DC15A_mag.p3d";
    };

    ////////////////////////////////////////////////////
    ////////////////Shotgun//Magazines//////////////////
    ////////////////////////////////////////////////////

    class Aux501_Weapons_Mags_shotgun_scatter20: 2Rnd_12Gauge_Pellets
    {
        displayName = "[501st] 20Rnd SP Magazine";
        displayNameShort = "20Rnd SP Mag";
        author = "501st Aux Team";
        picture = "\a3\Weapons_F_Enoch\MagazineProxies\data\UI\icon_2rnd_12gauge_slugs_CA.paa";
        count = 20;
        ammo = "Aux501_Weapons_Ammo_shotgun_scatter";
        mass=8;
        initSpeed=600;
        descriptionShort = "Scatter Plasma Magazine";	
        model="\MRC\JLTS\weapons\DC15S\DC15S_mag.p3d";
        tracersEvery = 1;
    };
    class Aux501_Weapons_Mags_shotgun_he10: Aux501_Weapons_Mags_shotgun_scatter20
    {
        scope=2;
        displayName = "[501st] 10Rnd UP Magazine";
        displayNameShort = "10Rnd UP Mag";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_shotgun_up.paa";
        count=10;
        ammo = "Aux501_Weapons_Ammo_shotgun_he";
        descriptionShort = "Unstable Plasma Magazine";
    };
    class Aux501_Weapons_Mags_shotgun_slug24: Aux501_Weapons_Mags_shotgun_scatter20
    {
        scope=2;
        displayName = "[501st] 24Rnd Slug Magazine";
        displayNameShort = "24Rnd Slug Mag";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_shotgun_slug.paa";
        initSpeed=800;
        count=24;
        ammo = "Aux501_Weapons_Ammo_30mw";
        descriptionShort = "30MW Slug Magazine";	
    };

    ////////////////////////////////////////////////////
    ///////////Grenade//Launcher//Magazines/////////////
    ////////////////////////////////////////////////////

    class Aux501_Weapons_Mags_GL_AP2: 1Rnd_HE_Grenade_shell
    {
        author = "501st Aux Team";
        scope = 2;
        displayName = "[501st] 2Rnd AP Grenade";
        displayNameShort = "2Rnd AP";
        model = "\SWLW_clones\machineguns\Z6\Z6_g_mag.p3d";
        picture = "\SWLW_clones\machineguns\Z6\data\ui\Z6_g_mag_ui.paa";
        ammo = "Aux501_Weapons_Ammo_GL_AP";
        count = 2;
        nameSound = "";
        descriptionShort = "2Rnd AP Grenade";
        mass = 15;
    };
    class Aux501_Weapons_Mags_GL_HE3: Aux501_Weapons_Mags_GL_AP2
    {
        displayName = "[501st] 3Rnd HE Grenade";
        displayNameShort = "3Rnd HE";
        count = 3;
        ammo = "Aux501_Weapons_Ammo_GL_HE";
        descriptionShort = "3Rnd HE DC-15A Grenade";
    };
    class Aux501_Weapons_Mags_GL_HE1: Aux501_Weapons_Mags_GL_AP2
    {
        displayName = "[501st] 1Rnd HE Grenade";
        displayNameShort = "1Rnd HE";
        count = 1;
        ammo = "Aux501_Weapons_Ammo_GL_light_HE";
        descriptionShort = "1Rnd HE Grenade";
    };
    class Aux501_Weapons_Mags_RGL_HE6: Aux501_Weapons_Mags_GL_HE3
    {
        displayName = "[501st] 6Rnd HE RGL Magazine";
        displayNameShort = "6Rnd HE";
        count = 6;
        descriptionShort = "6Rnd HE RGL Grenades";
    };
    class Aux501_Weapons_Mags_RGL_AP6: Aux501_Weapons_Mags_GL_AP2
    {
        displayName = "[501st] 6Rnd AP RGL Magazine";
        displayNameShort = "6Rnd AP";
        count = 6;
        descriptionShort = "6Rnd AP RGL Grenades";
    };
    class Aux501_Weapons_Mags_GL_smoke_white6: 3Rnd_Smoke_Grenade_shell
    {
        author = "501st Aux Team";
        scope = 2;
        displayName = "[501st] 6Rnd Smoke (White)";
        displayNameShort = "6Rnd White";
        descriptionShort = "UGL White Smoke Grenade";
        picture = "\A3\Weapons_f\Data\UI\gear_UGL_Smokeshell_white_CA.paa";
        count = 6;
        ammo = "Aux501_Weapons_Ammo_smoke_white";
        model = "kobra\442_weapons\explosive\basic_smoke.p3d";
    };
    class Aux501_Weapons_Mags_GL_smoke_purple3: Aux501_Weapons_Mags_GL_smoke_white6
    {
        displayName = "[501st] 3Rnd Smoke (Purple)";
        displayNameShort = "3Rnd Purple";
        descriptionShort = "UGL Purple Smoke Grenade";
        picture = "\A3\Weapons_f\Data\UI\gear_UGL_Smokeshell_purple_CA.paa";
        count = 3;
        ammo = "Aux501_Weapons_Ammo_smoke_purple";
    };
    class Aux501_Weapons_Mags_GL_smoke_yellow3: Aux501_Weapons_Mags_GL_smoke_purple3
    {
        displayName = "[501st] 3Rnd Smoke (Yellow)";
        displayNameShort = "3Rnd Yellow";
        descriptionShort = "UGL Yellow Smoke Grenade";
        picture = "\A3\Weapons_f\Data\UI\gear_UGL_Smokeshell_yellow_CA.paa";
        ammo = "Aux501_Weapons_Ammo_smoke_yellow";
    };
    class Aux501_Weapons_Mags_GL_smoke_red3: Aux501_Weapons_Mags_GL_smoke_purple3
    {
        displayName = "[501st] 3Rnd Smoke (Red)";
        displayNameShort = "3Rnd Red";
        descriptionShort = "UGL Red Smoke Grenade";
        picture = "\A3\Weapons_f\Data\UI\gear_UGL_Smokeshell_red_CA.paa";
        ammo = "Aux501_Weapons_Ammo_smoke_red";
    };
    class Aux501_Weapons_Mags_GL_smoke_green3: Aux501_Weapons_Mags_GL_smoke_purple3
    {
        displayName = "[501st] 3Rnd Smoke (Green)";
        displayNameShort = "3Rnd Green";
        descriptionShort = "UGL Green Smoke Grenade";
        picture = "\A3\Weapons_f\Data\UI\gear_UGL_Smokeshell_green_CA.paa";
        ammo = "Aux501_Weapons_Ammo_smoke_green";
    };
    class Aux501_Weapons_Mags_GL_smoke_blue3: Aux501_Weapons_Mags_GL_smoke_purple3
    {
        displayName = "[501st] 3Rnd Smoke (Blue)";
        displayNameShort = "3Rnd Blue";
        descriptionShort = "UGL Blue Smoke Grenade";
        picture = "\A3\Weapons_f\Data\UI\gear_UGL_Smokeshell_blue_CA.paa";
        ammo = "Aux501_Weapons_Ammo_smoke_blue";
    };
    class Aux501_Weapons_Mags_GL_smoke_orange3: Aux501_Weapons_Mags_GL_smoke_purple3
    {
        displayName = "[501st] 3Rnd Smoke (Orange)";
        displayNameShort = "3Rnd Orange";
        descriptionShort = "UGL Orange Smoke Grenade";
        picture = "\A3\Weapons_f\Data\UI\gear_UGL_Smokeshell_orange_CA.paa";
        ammo = "Aux501_Weapons_Ammo_smoke_orange";
    };
    class Aux501_Weapons_Mags_GL_flare_White3: UGL_FlareWhite_F
    {
        author = "501st Aux Team";
        scope = 2;
        type = 16;
        displayName = "[501st] 3Rnd Flare (White)";
        displayNameShort = "White Flares";
        descriptionShort = "UGL White Flares";
        picture = "\A3\Weapons_F\Data\UI\gear_UGL_Flare_CA.paa";
        model = "\A3\Weapons_F\Ammo\UGL_Flare";
        ammo = "Aux501_Weapons_Ammo_flare_white";
        initSpeed = 80;
        count = 3;
        nameSound = "";
        weaponPoolAvailable = 1;
        mass = 4;
    };
    class Aux501_Weapons_Mags_GL_flare_White1: Aux501_Weapons_Mags_GL_flare_white3
    {
        displayName = "[501st] 1Rnd Flare (White)";
        displayNameShort = "White Flare";
        descriptionShort = "Signal Pistol White Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_IR3: Aux501_Weapons_Mags_GL_flare_white3
    {
        displayName = "[501st] 3Rnd Flare (IR)";
        displayNameShort = "IR Flares";
        descriptionShort = "UGL IR Flares";
        ammo = "Aux501_Weapons_Ammo_flare_ir";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_ugl_flare_IR.paa";
    };
    class Aux501_Weapons_Mags_GL_flare_IR1: Aux501_Weapons_Mags_GL_flare_IR3
    {
        displayName = "[501st] 1Rnd Flare (IR)";
        displayNameShort = "IR Flare";
        descriptionShort = "Signal Pistol IR Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_Green3: Aux501_Weapons_Mags_GL_flare_white3
    {
        displayName = "[501st] 3Rnd Flare (Green)";
        displayNameShort = "Green Flares";
        descriptionShort = "UGL Green Flares";
        ammo = "Aux501_Weapons_Ammo_flare_green";
        picture = "\A3\Weapons_F\Data\UI\gear_UGL_Flare_Green_CA.paa";
    };
    class Aux501_Weapons_Mags_GL_flare_Green1: Aux501_Weapons_Mags_GL_flare_Green3
    {
        displayName = "[501st] 1Rnd Flare (Green)";;
        displayNameShort = "Green Flare";
        descriptionShort = "Signal Pistol Green Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_Red3: Aux501_Weapons_Mags_GL_flare_white3
    {
        displayName = "[501st] 3Rnd Flare (Red)";
        displayNameShort = "Red Flares";
        descriptionShort = "UGL Red Flares";
        ammo = "Aux501_Weapons_Ammo_flare_red";
        picture = "\A3\Weapons_F\Data\UI\gear_UGL_Flare_Red_CA.paa";
    };
    class Aux501_Weapons_Mags_GL_flare_Red1: Aux501_Weapons_Mags_GL_flare_Red3
    {
        displayName = "[501st] 1Rnd Flare (Red)";
        displayNameShort = "Red Flare";
        descriptionShort = "Signal Pistol Red Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_Yellow3: Aux501_Weapons_Mags_GL_flare_white3
    {
        displayName = "[501st] 3Rnd Flare (Yellow)";
        displayNameShort = "Yellow Flares";
        descriptionShort = "UGL Yellow Flares";
        ammo = "Aux501_Weapons_Ammo_flare_yellow";
        picture = "\A3\Weapons_F\Data\UI\gear_UGL_Flare_yelow_CA.paa";
    };
    class Aux501_Weapons_Mags_GL_flare_Yellow1: Aux501_Weapons_Mags_GL_flare_Yellow3
    {
        displayName = "[501st] 1Rnd Flare (Yellow)";
        displayNameShort = "Yellow Flare";
        descriptionShort = "Signal Pistol Yellow Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_Blue3: Aux501_Weapons_Mags_GL_flare_white3
    {
        displayName = "[501st] 3Rnd Flare (Blue)";
        displayNameShort = "Blue Flares";
        descriptionShort = "UGL Blue Flares";
        ammo = "Aux501_Weapons_Ammo_flare_blue";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_ugl_flare_blue.paa";
    };
    class Aux501_Weapons_Mags_GL_flare_Blue1: Aux501_Weapons_Mags_GL_flare_Blue3
    {
        displayName = "[501st] 1Rnd Flare (Blue)";;
        displayNameShort = "Blue Flare";
        descriptionShort = "Signal Pistol Blue Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_Cyan3: Aux501_Weapons_Mags_GL_flare_white3
    {
        displayName = "[501st] 3Rnd Flare (Cyan)";
        displayNameShort = "Cyan Flares";
        descriptionShort = "UGL Cyan Flares";
        ammo = "Aux501_Weapons_Ammo_flare_cyan";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_ugl_flare_cyan.paa";
    };
    class Aux501_Weapons_Mags_GL_flare_Cyan1: Aux501_Weapons_Mags_GL_flare_Cyan3
    {
        displayName = "[501st] 1Rnd Flare (Cyan)";
        displayNameShort = "Cyan Flare";
        descriptionShort = "Signal Pistol Cyan Flare";
        mass = 1;
        count = 1;
    };
    class Aux501_Weapons_Mags_GL_flare_Purple3: Aux501_Weapons_Mags_GL_flare_white3
    {
        displayName = "[501st] 3Rnd Flare (Purple)";
        displayNameShort = "Purple Flares";
        descriptionShort = "UGL Purple Flares";
        ammo = "Aux501_Weapons_Ammo_flare_Purple";
        picture = "\A3\Weapons_F\Data\UI\gear_UGL_Flare_Purple_CA.paa";
    };
    class Aux501_Weapons_Mags_GL_flare_Purple1: Aux501_Weapons_Mags_GL_flare_Purple3
    {
        displayName = "[501st] 1Rnd Flare (Purple)";
        displayNameShort = "Purple Flare";
        descriptionShort = "Signal Pistol Purple Flare";
        mass = 1;
        count = 1;
    };
};