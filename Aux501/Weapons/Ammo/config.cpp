class CfgPatches
{
    class Aux501_Patch_Ammo
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "JLTS_weapons_Core"
        };
        requiredVersion = 0.1;
        units[] = {};
        weapons[] = {};
    };
};

class CfgAmmo
{
    class JLTS_bullet_stun;
    class JLTS_bullet_carbine_blue;
    class JLTS_bullet_carbine_red;
    class JLTS_bullet_carbine_yellow;
    class JLTS_bullet_carbine_green;
    class B_12Gauge_Pellets_Submunition_Deploy;
    class G_40mm_HE;
    class SmokeShell;
    class F_40mm_White;

    ////////////////////////////////////////////////////
    /////////////////Blaster//Ammo//////////////////////
    ////////////////////////////////////////////////////

    class Aux501_Weapons_Ammo_Stun: JLTS_bullet_stun
    {
        model = "\MRC\JLTS\weapons\Core\effects\stun.p3d";
        hit = 0.9;
        JLTS_isStunAmmo = 1;
        typicalSpeed = 30;
        timetolive = 3;
        aiAmmoUsageFlags = 0;
        //RD501_stunDuration=30;
    };

    class Aux501_Weapons_Ammo_base_blue: JLTS_bullet_carbine_blue
    {
        ACE_ammoTempMuzzleVelocityShifts = [ -26.55, -25.47, -22.85, -20.12, -16.98, -12.8, -7.64, -1.53, 5.96, 15.17, 26.19 ];
        ACE_ballisticCoefficients = [0.322];
        ACE_barrelLengths = [ 508, 660.4, 711.2 ];
        ACE_bulletLength = 39.573;
        ACE_bulletMass = 16.2;
        ACE_caliber = 8.585;
        ACE_damageType = "bullet";
        ACE_dragModel = 7;
        ACE_muzzleVelocities = [ 880, 915, 925 ];
        ACE_muzzleVelocityVariationSD = 0.3;
        ACE_standardAtmosphere = "ICAO";
        ace_vehicle_damage_incendiary = 0.1;
        ACE_velocityBoundaries = [];
        aiAmmoUsageFlags = "64 + 128 + 256";
        airfriction = 0;
        airlock = 1;
        animated = 0;
        artilleryCharge = 1;
        artilleryDispersion = 1;
        artilleryLock = 0;
        audibleFire = 80;
        autoSeekTarget = 0;
        caliber = 2;
        cartridge = "";
        cmImmunity = 1;
        coefgravity = 0;
        cost = 6;
        dangerRadiusBulletClose = 10;
        dangerRadiusHit = 14;
        deflecting = 0;
        deflectionSlowDown = 0.8;
        explosive = 0;
        fuseDistance = 0;
        hit = 16;
        icon = "";
        indirectHit = 0;
        indirectHitRange = 0;
        initTime = 0;
        irLock = 0;
        irtarget = 1;
        laserLock = 0;
        lockSeekRadius = 100;
        lockType = 0;
        maneuvrability = 1;
        manualControl = 0;
        maverickweaponIndexOffset = 0;
        maxControlRange = 350;
        maxSpeed = 0;
        minDamageForCamShakeHit = 0.55;
        mineBoundingDist = 3;
        mineBoundingTime = 3;
        mineDiveSpeed = 1;
        mineFloating = -1;
        mineInconspicuousness = 10;
        minePlaceDist = 0.5;
        mineTrigger = "RangeTrigger";
        minimumSafeZone = 0.1;
        minTimeToLive = 0;
        missileLockCone = 0;
        model = "SWLW_main\Effects\laser_blue.p3d";
        nvLock = 0;
        nvgonly = 0;
        shootDistraction = -1;
        sideAirFriction = 1;
        simulation = "shotBullet";
        simulationStep = 0.05;
        submunitionAmmo = "";
        suppressionRadiusBulletClose = 8;
        suppressionRadiusHit = 10;
        thrust = 210;
        thrustTime = 1.5;
        timeToLive = 4;
        tracerscale = 1.5;
        tracerstarttime = 0;
        tracerendtime = 10;
        trackLead = 1;
        trackOversteer = 1;
        typicalSpeed = 890;
        underwaterHitRangeCoef = 1;
        visibleFire = 3;
        visibleFireTime = 3;
        waterEffectOffset = 0.8;
        weaponLockSystem = 0;
        weaponType = "Default";
        whistleDist = 0;
        whistleOnFire = 0;
    };

    class Aux501_Weapons_Ammo_base_red: JLTS_bullet_carbine_red
    {
        ACE_ammoTempMuzzleVelocityShifts = [ -26.55, -25.47, -22.85, -20.12, -16.98, -12.8, -7.64, -1.53, 5.96, 15.17, 26.19 ];
        ACE_ballisticCoefficients = [0.322];
        ACE_barrelLengths = [ 508, 660.4, 711.2 ];
        ACE_bulletLength = 39.573;
        ACE_bulletMass = 16.2;
        ACE_caliber = 8.585;
        ACE_damageType = "bullet";
        ACE_dragModel = 7;
        ACE_muzzleVelocities = [ 880, 915, 925 ];
        ACE_muzzleVelocityVariationSD = 0.3;
        ACE_standardAtmosphere = "ICAO";
        ace_vehicle_damage_incendiary = 0.1;
        ACE_velocityBoundaries = [];
        aiAmmoUsageFlags = "64 + 128 + 256";
        airfriction = 0;
        airlock = 1;
        animated = 0;
        artilleryCharge = 1;
        artilleryDispersion = 1;
        artilleryLock = 0;
        audibleFire = 80;
        autoSeekTarget = 0;
        caliber = 2;
        cartridge = "";
        cmImmunity = 1;
        coefgravity = 0;
        cost = 6;
        dangerRadiusBulletClose = 10;
        dangerRadiusHit = 14;
        deflecting = 0;
        deflectionSlowDown = 0.8;
        explosive = 0;
        fuseDistance = 0;
        hit = 16;
        icon = "";
        indirectHit = 0;
        indirectHitRange = 0;
        initTime = 0;
        irLock = 0;
        irtarget = 1;
        laserLock = 0;
        lockSeekRadius = 100;
        lockType = 0;
        maneuvrability = 1;
        manualControl = 0;
        maverickweaponIndexOffset = 0;
        maxControlRange = 350;
        maxSpeed = 0;
        minDamageForCamShakeHit = 0.55;
        mineBoundingDist = 3;
        mineBoundingTime = 3;
        mineDiveSpeed = 1;
        mineFloating = -1;
        mineInconspicuousness = 10;
        minePlaceDist = 0.5;
        mineTrigger = "RangeTrigger";
        minimumSafeZone = 0.1;
        minTimeToLive = 0;
        missileLockCone = 0;
        model = "SWLW_main\Effects\laser_red.p3d";
        nvLock = 0;
        nvgonly = 0;
        shootDistraction = -1;
        sideAirFriction = 1;
        simulation = "shotBullet";
        simulationStep = 0.05;
        submunitionAmmo = "";
        suppressionRadiusBulletClose = 8;
        suppressionRadiusHit = 10;
        thrust = 210;
        thrustTime = 1.5;
        timeToLive = 4;
        tracerScale = 1.5;
        tracerstarttime = 0;
        tracerendtime = 10;
        trackLead = 1;
        trackOversteer = 1;
        typicalSpeed = 890;
        underwaterHitRangeCoef = 1;
        visibleFire = 3;
        visibleFireTime = 3;
        waterEffectOffset = 0.8;
        weaponLockSystem = 0;
        weaponType = "Default";
        whistleDist = 0;
        whistleOnFire = 0;
    };

    class Aux501_Weapons_Ammo_base_yellow: JLTS_bullet_carbine_yellow
    {
        ACE_ammoTempMuzzleVelocityShifts = [ -26.55, -25.47, -22.85, -20.12, -16.98, -12.8, -7.64, -1.53, 5.96, 15.17, 26.19 ];
        ACE_ballisticCoefficients = [0.322];
        ACE_barrelLengths = [ 508, 660.4, 711.2 ];
        ACE_bulletLength = 39.573;
        ACE_bulletMass = 16.2;
        ACE_caliber = 8.585;
        ACE_damageType = "bullet";
        ACE_dragModel = 7;
        ACE_muzzleVelocities = [ 880, 915, 925 ];
        ACE_muzzleVelocityVariationSD = 0.3;
        ACE_standardAtmosphere = "ICAO";
        ace_vehicle_damage_incendiary = 0.1;
        ACE_velocityBoundaries = [];
        aiAmmoUsageFlags = "64 + 128 + 256";
        airfriction = 0;
        airlock = 1;
        animated = 0;
        artilleryCharge = 1;
        artilleryDispersion = 1;
        artilleryLock = 0;
        audibleFire = 80;
        autoSeekTarget = 0;
        caliber = 2;
        cartridge = "";
        cmImmunity = 1;
        coefgravity = 0;
        cost = 6;
        dangerRadiusBulletClose = 10;
        dangerRadiusHit = 14;
        deflecting = 0;
        deflectionSlowDown = 0.8;
        explosive = 0;
        fuseDistance = 0;
        hit = 16;
        icon = "";
        indirectHit = 0;
        indirectHitRange = 0;
        initTime = 0;
        irLock = 0;
        irtarget = 1;
        laserLock = 0;
        lockSeekRadius = 100;
        lockType = 0;
        maneuvrability = 1;
        manualControl = 0;
        maverickweaponIndexOffset = 0;
        maxControlRange = 350;
        maxSpeed = 0;
        minDamageForCamShakeHit = 0.55;
        mineBoundingDist = 3;
        mineBoundingTime = 3;
        mineDiveSpeed = 1;
        mineFloating = -1;
        mineInconspicuousness = 10;
        minePlaceDist = 0.5;
        mineTrigger = "RangeTrigger";
        minimumSafeZone = 0.1;
        minTimeToLive = 0;
        missileLockCone = 0;
        model = "SWLW_main\Effects\laser_yellow.p3d";
        nvLock = 0;
        nvgonly = 0;
        shootDistraction = -1;
        sideAirFriction = 1;
        simulation = "shotBullet";
        simulationStep = 0.05;
        submunitionAmmo = "";
        suppressionRadiusBulletClose = 8;
        suppressionRadiusHit = 10;
        thrust = 210;
        thrustTime = 1.5;
        timeToLive = 4;
        tracerScale = 1.5;
        tracerstarttime = 0;
        tracerendtime = 10;
        trackLead = 1;
        trackOversteer = 1;
        typicalSpeed = 890;
        underwaterHitRangeCoef = 1;
        visibleFire = 3;
        visibleFireTime = 3;
        waterEffectOffset = 0.8;
        weaponLockSystem = 0;
        weaponType = "Default";
        whistleDist = 0;
        whistleOnFire = 0;
    };
    
    class Aux501_Weapons_Ammo_base_green: JLTS_bullet_carbine_green
    {
        ACE_ammoTempMuzzleVelocityShifts = [ -26.55, -25.47, -22.85, -20.12, -16.98, -12.8, -7.64, -1.53, 5.96, 15.17, 26.19 ];
        ACE_ballisticCoefficients = [0.322];
        ACE_barrelLengths = [ 508, 660.4, 711.2 ];
        ACE_bulletLength = 39.573;
        ACE_bulletMass = 16.2;
        ACE_caliber = 8.585;
        ACE_damageType = "bullet";
        ACE_dragModel = 7;
        ACE_muzzleVelocities = [ 880, 915, 925 ];
        ACE_muzzleVelocityVariationSD = 0.3;
        ACE_standardAtmosphere = "ICAO";
        ace_vehicle_damage_incendiary = 0.1;
        ACE_velocityBoundaries = [];
        aiAmmoUsageFlags = "64 + 128 + 256";
        airfriction = 0;
        airlock = 1;
        animated = 0;
        artilleryCharge = 1;
        artilleryDispersion = 1;
        artilleryLock = 0;
        audibleFire = 80;
        autoSeekTarget = 0;
        caliber = 2;
        cartridge = "";
        cmImmunity = 1;
        coefgravity = 0;
        cost = 6;
        dangerRadiusBulletClose = 10;
        dangerRadiusHit = 14;
        deflecting = 0;
        deflectionSlowDown = 0.8;
        explosive = 0;
        fuseDistance = 0;
        hit = 16;
        icon = "";
        indirectHit = 0;
        indirectHitRange = 0;
        initTime = 0;
        irLock = 0;
        irtarget = 1;
        laserLock = 0;
        lockSeekRadius = 100;
        lockType = 0;
        maneuvrability = 1;
        manualControl = 0;
        maverickweaponIndexOffset = 0;
        maxControlRange = 350;
        maxSpeed = 0;
        minDamageForCamShakeHit = 0.55;
        mineBoundingDist = 3;
        mineBoundingTime = 3;
        mineDiveSpeed = 1;
        mineFloating = -1;
        mineInconspicuousness = 10;
        minePlaceDist = 0.5;
        mineTrigger = "RangeTrigger";
        minimumSafeZone = 0.1;
        minTimeToLive = 0;
        missileLockCone = 0;
        model = "SWLW_main\Effects\laser_green.p3d";
        nvLock = 0;
        nvgonly = 0;
        shootDistraction = -1;
        sideAirFriction = 1;
        simulation = "shotBullet";
        simulationStep = 0.05;
        submunitionAmmo = "";
        suppressionRadiusBulletClose = 8;
        suppressionRadiusHit = 10;
        thrust = 210;
        thrustTime = 1.5;
        timeToLive = 4;
        tracerScale = 1.5;
        tracerstarttime = 0;
        tracerendtime = 10;
        trackLead = 1;
        trackOversteer = 1;
        typicalSpeed = 890;
        underwaterHitRangeCoef = 1;
        visibleFire = 3;
        visibleFireTime = 3;
        waterEffectOffset = 0.8;
        weaponLockSystem = 0;
        weaponType = "Default";
        whistleDist = 0;
        whistleOnFire = 0;
    };

    class Aux501_Weapons_Ammo_10mw: Aux501_Weapons_Ammo_base_blue
    {
        hit = 12.5;
        typicalSpeed = 900;
        caliber = 1;
        waterFriction = -0.009;
    };
    class Aux501_Weapons_Ammo_10mwsp : Aux501_Weapons_Ammo_10mw
    {
        visibleFire = 1;
        audibleFire = 5;
        visibleFireTime = 3;
    };
    class Aux501_Weapons_Ammo_20mw: Aux501_Weapons_Ammo_base_blue
    {
        hit = 17.5;
        typicalSpeed = 1050;
        caliber = 2.8;
        waterFriction = -0.009;
    };
    class Aux501_Weapons_Ammo_20mwdp: Aux501_Weapons_Ammo_20mw
    {
        hit = 24;
    };
    class Aux501_Weapons_Ammo_20mwup: Aux501_Weapons_Ammo_20mw
    {
        hit = 20;
        typicalSpeed = 900;
        indirectHit = 3;
        indirectHitRange = 0.5;
        explosive = 0.4;
        caliber = 0.6;
        ExplosionEffects = "3AS_ImpactPlasma";
        craterEffects = "";
    };
    class Aux501_Weapons_Ammo_30mw: Aux501_Weapons_Ammo_base_blue
    {
        hit = 45;
        typicalSpeed = 1500;
        caliber = 2.4;
        airFriction = 0;
        waterFriction = -0.009;
    };
    class Aux501_Weapons_Ammo_40mw: Aux501_Weapons_Ammo_base_blue
    {
        hit = 60;
        typicalSpeed = 1100;
        caliber = 3.6;
        airFriction = 0;
        waterFriction = -0.009;
    };
    class Aux501_Weapons_Ammo_40mwemp: Aux501_Weapons_Ammo_40mw
    {
        hit = 0.01;
        caliber = 1;
        // 501aux_emp_vehicle_enabled=1;
        // 501aux_emp_vehicle_duration=15;
    };
    class Aux501_Weapons_Ammo_50mw: Aux501_Weapons_Ammo_base_blue
    {
        hit = 300;
        explosive = 0.5;
        typicalSpeed = 1100;
        caliber = 5;
        ExplosionEffects = "3AS_ImpactPlasma";
        craterEffects = "";
    };
    class Aux501_Weapons_Ammo_shotgun_scatter: Aux501_Weapons_Ammo_10mw
    {
        hit = 20;
        simulation = "shotSpread";
		fireSpreadAngle = 3;
    };
    class Aux501_Weapons_Ammo_shotgun_scatter_sub: B_12Gauge_Pellets_Submunition_Deploy
    {
        hit = 12;
        model = "SWLW_main\Effects\laser_blue.p3d";
    };
    class Aux501_Weapons_Ammo_shotgun_HE: Aux501_Weapons_Ammo_shotgun_scatter
    {
        submunitionConeAngle = 4;
        submunitionAmmo = "Aux501_Weapons_Ammo_shotgun_HE_sub";
        model = "SWLW_main\Effects\laser_green.p3d";
    };
    class Aux501_Weapons_Ammo_shotgun_HE_sub: Aux501_Weapons_Ammo_shotgun_scatter_sub
    {
        hit = 8;
        indirectHit = 8;
        indirectHitRange = 0.9;
        model = "SWLW_main\Effects\laser_green.p3d";
    };
    class Aux501_Weapons_Ammo_GL_HE: G_40mm_HE
    {
        hit = 60;
        indirectHit = 50;
        indirectHitRange = 5;
        timeToLive = 30;
        model = "\MRC\JLTS\weapons\Core\effects\laser_green.p3d";
        fuseDistance = 3;
    };
    class Aux501_Weapons_Ammo_GL_AP: Aux501_Weapons_Ammo_GL_HE
    {
        hit = 350;
        indirectHit = 0;
        indirectHitRange = 0.1;
        model = "\MRC\JLTS\weapons\Core\effects\laser_yellow.p3d";
        explosive = 0.0001;
        deflecting = 5;
        caliber = 5;
        ace_frag_enabled = 0;
        ace_frag_force = 0;
        ace_frag_classes[] = {""};
        ace_frag_metal = 0;
        ace_frag_charge = 0;
        ace_frag_gurney_c = 0;
        ace_frag_gurney_k = "0";
        ace_rearm_caliber = 0;
    };
    class Aux501_Weapons_Ammo_GL_light_HE: Aux501_Weapons_Ammo_GL_HE
    {
        hit = 30;
        indirectHit = 25;
        indirectHitRange = 2.5;
        timeToLive = 15;
        model = "\MRC\JLTS\weapons\Core\effects\laser_orange.p3d";
        fuseDistance = 1.5;
    };
        class Aux501_Weapons_Ammo_smoke_white: SmokeShell
    {
        smokeColor[] = {1,1,1,1};
        effectsSmoke = "k_SmokeShellwhiteEffect";
        model = "kobra\442_weapons\explosive\basic_smoke.p3d";
    };
    class Aux501_Weapons_Ammo_smoke_purple: Aux501_Weapons_Ammo_smoke_white
    {
        smokeColor[] = {0.4341,0.1388,0.41439998,1};
        effectsSmoke = "k_SmokeShellpurpleEffect";
    };
    class Aux501_Weapons_Ammo_smoke_yellow: Aux501_Weapons_Ammo_smoke_white
    {
        smokeColor[] = {0.9883,0.8606,0.0719,1};
        effectsSmoke = "k_SmokeShellyellowEffect";
    };
    class Aux501_Weapons_Ammo_smoke_red: Aux501_Weapons_Ammo_smoke_white
    {
        smokeColor[] = {0.8438,0.1383,0.1353,1};
        effectsSmoke = "k_SmokeShellRedEffect";
    };
    class Aux501_Weapons_Ammo_smoke_green: Aux501_Weapons_Ammo_smoke_white
    {
        smokeColor[] = {0.1383,0.8438,0.1353,1};
        effectsSmoke = "k_SmokeShellGreenEffect";
    };
    class Aux501_Weapons_Ammo_smoke_blue: Aux501_Weapons_Ammo_smoke_white
    {
        smokeColor[] = {0.1183,0.1867,1,1};
        effectsSmoke = "k_SmokeShellblueEffect";
    };
    class Aux501_Weapons_Ammo_smoke_orange: Aux501_Weapons_Ammo_smoke_white
    {
        smokeColor[] = {0.6697,0.2275,0.10053,1};
        effectsSmoke = "SmokeShellOrangeEffect";
    };
    class Aux501_Weapons_Ammo_flare_white: F_40mm_White
    {
        lightColor[] = {1, 1, 1 ,1};
        useFlare = 1;
        deflecting = 30;
        smokeColor[] = {1, 1, 1, 1};
        brightness = 1200;
        triggerTime = 3;
        triggerSpeedCoef = 1;
        audibleFire = 20;
        intensity  = 1000000;
        timeToLive = 120;
        flareMaxDistance = 300;
    };
    class Aux501_Weapons_Ammo_flare_green: Aux501_Weapons_Ammo_flare_white
    {
        lightColor[] = {0, 1, 0, 0};
    };
    class Aux501_Weapons_Ammo_flare_red: Aux501_Weapons_Ammo_flare_white
    {
        lightColor[] = {1, 0, 0, 0};
    };
    class Aux501_Weapons_Ammo_flare_yellow: Aux501_Weapons_Ammo_flare_white
    {
        lightColor[] = {1, 1, 0, 0};
    };
    class Aux501_Weapons_Ammo_flare_ir: Aux501_Weapons_Ammo_flare_white
    {
        lightColor[] = {0.5, 0.5, 0.25, 0};
    };
    class Aux501_Weapons_Ammo_flare_blue: Aux501_Weapons_Ammo_flare_white
    {
        lightColor[] = {0, 0, 1, 0};
    };

    class Aux501_Weapons_Ammo_flare_cyan: Aux501_Weapons_Ammo_flare_white
    {
        lightColor[] = {0, 1, 1, 0};
    };
    class Aux501_Weapons_Ammo_flare_purple: Aux501_Weapons_Ammo_flare_white
    {
        lightColor[] = {.7, 0, 1, 0};
    };
};

class CfgLights
{
    class Aux501_light_heavybolt_blue
    {
        color[] = {0,0,0,0};
        diffuse[] = {2,0,255,0.8};
        ambient[] = {0,0,0,0.5};
        intensity = 30000;
        class Attenuation
        {
            start = 0;
            constant = 0;
            linear = 0;
            quadratic = 255;
            hardLimitStart = "1e20 * 0.7";
            hardLimitEnd = 1e+20;
        };
        dayLight = 1;
        useFlare = 1;
        flareSize = 3;
        flareMaxDistance = 6000;
    };
    class Aux501_light_heavybolt_red: Aux501_light_heavybolt_blue
    {
        diffuse[] = {255,0,0,0.8};
    };
    class Aux501_light_heavybolt_yellow: Aux501_light_heavybolt_blue
    {
        diffuse[] = {255,255,0,1};
    };
    class Aux501_light_heavybolt_green: Aux501_light_heavybolt_blue
    {
        diffuse[] = {21,1254,69,1};
    };

    class Aux501_light_blasterbolt_blue: Aux501_light_heavybolt_blue
    {
        intensity = 625;
        flareSize = 0.35;
    };
    class Aux501_light_blasterbolt_red: Aux501_light_blasterbolt_blue
    {
        diffuse[] = {255,0,0,0.8};
    };
};

class Aux501_particle_effect_heavybolt_fly_blue
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_heavybolt_blue";
        position[] = {0,0,0};
    };
};
class Aux501_particle_effect_heavybolt_fly_red
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_heavybolt_red";
        position[] = {0,0,0};
    };
};
class Aux501_particle_effect_heavybolt_fly_yellow
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_heavybolt_yellow";
        position[] = {0,0,0};
    };
};
class Aux501_particle_effect_heavybolt_fly_green
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_heavybolt_green";
        position[] = {0,0,0};
    };
};

class Aux501_particle_effect_blasterbolt_fly_blue
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_blasterbolt_blue";
        position[] = {0,0,0};
    };
};
class Aux501_particle_effect_blasterbolt_fly_red
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_blasterbolt_red";
        position[] = {0,0,0};
    };
};