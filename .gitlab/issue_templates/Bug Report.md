# Description
A short description of the bug.

# Details
Full details of your bug/feature
- Including
- More
- Details
- Is
- Better

## Server
<!-- Place an x between the [ ] brackets to check it - or submit the issue and click the checkboxes once the issue is published. -->
- [ ] Server 1
- [ ] Server 2
- [ ] Server 3
- [ ] Server 4
- [ ] Server 5
- [ ] Server 6
- [ ] Server 7
- [ ] Server 8
- [ ] Server 9

## Modlist
<!-- The modlist you are using. -->
- [ ] 501st Main
- [ ] 501st Dev (Server 9)
- [ ] 501st Jabiim (Server 10)
- [ ] Other:
    - Please write what modlist you used here.

# Additional Information
## When did this Bug occur?
Who's op, what time if it was a funop, etc.

## What was going on when the bug occurred? (Actions that caused it)
What were you doing?

## How many people were on server when the bug Occurred?
10, 20, 60, etc.

## How many people witnessed the bug occur? (Please list names of those present)
10, 20, 60, etc.

## How many people directly effected by the bug?
Who was effected?

## What steps are necessary to repeat bug? (if able to give)
Tell us how you encountered the bug

## Video Evidence if Available
If you can record it happening, please do so.

# Notes
Additional notes.

/label ~pending-approval ~bug