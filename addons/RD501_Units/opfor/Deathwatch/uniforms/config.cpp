class CfgPatches
{
	class RD501_patch_deathwatch_uniforms
	{
		author = "RD501";
		requiredAddons[] = {"RD501_patch_opfor_deathwatch"};
		requiredVersion = 0.1;
		units[] = {};
		weapons[] = {};
	};
};
class CfgWeapons
{
	class UniformItem;
	class Uniform_Base;
	class RD501_Mandalorian_undersuit: Uniform_Base
	{
		scope = 2;
		author = "RD501";
		displayName = "[Mandos] Mandalorian Bodyglove";
		picture = "ls_armor_greenfor\uniform\mandalorian\_ui\icon_uniform_undersuit.paa";
		model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
		class ItemInfo: UniformItem
		{
			uniformmodel = "-";
			uniformClass = "RD501_Mandalorian_undersuit";
			containerClass = "Supply150";
			armor = 10;
			mass = 40;
		};
	};
	class RD501_Deathwatch_commander_uniform: Uniform_Base
	{
		scope = 2;
		author = "RD501";
		displayName = "[Mandos] Deathwatch Commander Armor";
		picture = "armor_unit_2\ui\Merc_Uniform_Kando.paa";
		model = "dst\armour_hyperion_rework.p3d";
		class ItemInfo: UniformItem
		{
			uniformmodel = "-";
			uniformClass = "RD501_opfor_unit_deathwatch_commander";
			containerClass = "Supply150";
			armor = 10;
			mass = 40;
		};
	};
	class RD501_Deathwatch_honorguard_uniform: Uniform_Base
	{
		scope = 2;
		author = "RD501";
		displayName = "[Mandos] Deathwatch Honorguard Armor";
		picture = "armor_unit_2\ui\Merc_Uniform_Kando.paa";
		model = "\armor_unit_2\Merc_Uniform_Kando.p3d";
		hiddenSelectionsTextures[] = 
		{
			"RD501_Units\textures\mandalorians\uniforms\deathwatch\deathwatch_honorguard_camo1.paa",
			"RD501_Units\textures\mandalorians\uniforms\deathwatch\deathwatch_honorguard_camo2.paa",
			"armor_unit_2\Merc_Kando\Kando_Under_co.paa",
			"armor_unit_2\Merc_Kando\Kando_Vest_co.paa"
		};
		class ItemInfo: UniformItem
		{
			uniformmodel = "-";
			uniformClass = "RD501_opfor_unit_deathwatch_honorguard";
			containerClass = "Supply150";
			armor = 10;
			mass = 40;
		};
	};

	//Vest
	class VestItem;
	class ls_mandalorian_deathwatchGrunt_vest;
	class rd501_mandalorian_deathwatch_vest_trooper: ls_mandalorian_deathwatchGrunt_vest
	{	
		author = "RD501";
		scope = 2;
		side = 3;
		displayName = "[Mandos] Deathwatch Soldier Armor";
		model = "\ls_armor_greenfor\vest\mandalorian\original\ls_mandalorian_original_vest.p3d";
		hiddenSelectionsMaterials[]=
        {
            "\ls_armor_greenfor\vest\mandalorian\original\data\vest_original.rvmat"
        };
		hiddenSelectionsTextures[] = {"\lsd_units_redfor\cis\deathwatch\textures\grunt_vest.paa"};
		class ItemInfo: VestItem
		{
			uniformModel = "\ls_armor_greenfor\vest\mandalorian\original\ls_mandalorian_original_vest.p3d";
			modelsides[] = {6};
			hiddenSelections[] = {"camo1"};
			hiddenSelectionsMaterials[]=
			{
				"\ls_armor_greenfor\vest\mandalorian\original\data\vest_original.rvmat"
			};
			containerClass="Supply100";
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 30;
					PassThrough = 0.3;
				};
				class Legs
				{
					hitpointName = "HitLegs";
					armor = 6;
					passThrough = 0.3;
				};
				class Arms
				{
					hitpointName = "HitArms";
					armor = 4;
					passThrough = 0.3;
				};
			};
		}; 
	};
	class rd501_mandalorian_deathwatch_vest_nco: rd501_mandalorian_deathwatch_vest_trooper
	{	
		displayName = "[Mandos] Deathwatch NCO Armor";
		hiddenSelectionsTextures[] = {"\lsd_units_redfor\cis\deathwatch\textures\leader_vest.paa"};
	};
	class rd501_mandalorian_deathwatch_vest_captain: rd501_mandalorian_deathwatch_vest_trooper
	{	
		displayName = "[Mandos] Deathwatch Captain Armor";
		hiddenSelectionsTextures[] = {"\lsd_units_redfor\cis\deathwatch\textures\sergeant_vest.paa"};
	};
	class rd501_mandalorian_deathwatch_vest_commander: ls_mandalorian_deathwatchGrunt_vest
	{
		scope = 2;
		scopeCurator = 2;
		displayName = "[Mandos] Deathwatch Commander Kama";
		picture = "armor_unit_2\ui\Merc_Vest_Kando.paa";
		model = "\Merc_Armor_Unit\Merc_Vest_ZanX.p3d";
		hiddenSelections[] = {"vest"};
		hiddenSelectionsTextures[] = {"\Merc_Armor_Unit\data\Merc_Vest_ZanX_co.paa"};
		hiddenSelectionsMaterials[] = {"\Merc_Armor_Unit\data\Vest_ZanX.rvmat"};
		class ItemInfo: VestItem
		{
			uniformModel = "\Merc_Armor_Unit\Merc_Vest_ZanX.p3d";
			containerClass = "Supply100";
			hiddenSelections[] = {"vest"};
			mass = 80;
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 30;
					PassThrough = 0.3;
				};
				class Legs
				{
					hitpointName = "HitLegs";
					armor = 6;
					passThrough = 0.3;
				};
				class Arms
				{
					hitpointName = "HitArms";
					armor = 4;
					passThrough = 0.3;
				};
			};
		};
	};
	class rd501_mandalorian_deathwatch_vest_honorguard: ls_mandalorian_deathwatchGrunt_vest
	{
		scope = 2;
		scopeCurator = 2;
		displayName = "[Mandos] Deathwatch Honorguard Kama";
		picture = "armor_unit_2\ui\Merc_Vest_Kando.paa";
		model = "\armor_unit_2\Merc_Vest_Kando.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\armor_unit_2\Merc_Kando\Kando_Vest_co.paa"};
		class ItemInfo: VestItem
		{
			uniformModel = "\armor_unit_2\Merc_Vest_Kando.p3d";
			containerClass = "Supply100";
			hiddenSelections[] = {"camo1"};
			mass = 80;
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 30;
					PassThrough = 0.3;
				};
				class Legs
				{
					hitpointName = "HitLegs";
					armor = 6;
					passThrough = 0.3;
				};
				class Arms
				{
					hitpointName = "HitArms";
					armor = 4;
					passThrough = 0.3;
				};
			};
		};
	};

	//helmets
	class H_HelmetB;
	class HeadgearItem;
	class RD501_Deathwatch_commander_helmet: H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		displayName = "[Mandos] Deathwatch Commander Helmet";
		model = "dst\helmet_thagoron_loki.p3d";
		hiddenSelections[] = {"camo1","visor"};
		hiddenSelectionsTextures[] = {"dst\rework\helmet\helmet_co.paa","dst\rework\helmet\visor_co.paa"};
		hiddenSelectionsMaterials[] = {"dst\rework\helmet\helmet.rvmat","dst\rework\helmet\visor.rvmat"};
		picture = "armor_unit_2\ui\Merc_Helmet_Kando.paa";
		class ItemInfo: HeadgearItem
		{
			mass = 10;
			uniformModel = "dst\helmet_thagoron_loki.p3d";
			hiddenSelections[] = {"camo1","visor"};
			hiddenSelectionsTextures[] = {"dst\rework\helmet\helmet_co.paa","dst\rework\helmet\visor_co.paa"};
			hiddenSelectionsMaterials[] = {"dst\rework\helmet\helmet.rvmat","dst\rework\helmet\visor.rvmat"};
			picture = "armor_unit_2\ui\Merc_Helmet_Kando.paa";
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 40;
					passThrough = 0.5;
				};
				class Face
				{
					hitPointName = "HitFace";
					armor = 20;
					passThrough = 0.3;
				};
			};
		};
	};
	class RD501_Deathwatch_honorguard_helmet: H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		displayName = "[Mandos] Deathwatch Honorguard Helmet";
		picture = "armor_unit_2\ui\Merc_Helmet_Kando.paa";
		model = "\armor_unit_2\Merc_Helmet_Kando.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"RD501_Units\textures\mandalorians\helmets\deathwatch\deathwatch_honorguard_helmet.paa"};
		hiddenSelectionsMaterials[] = {"armor_unit_2\Merc_Kando\Helmet.rvmat"};
		class ItemInfo: HeadgearItem
		{
			mass = 10;
			uniformModel = "\armor_unit_2\Merc_Helmet_Kando.p3d";
			hiddenSelections[] = {"camo1"};
			hiddenSelectionsTextures[] = 
			{
				"RD501_Units\textures\mandalorians\helmets\deathwatch\deathwatch_honorguard_helmet.paa"
			};
			hiddenSelectionsMaterials[] = {"armor_unit_2\Merc_Kando\Helmet.rvmat"};
			picture = "armor_unit_2\ui\Merc_Uniform_Kando.paa";
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 40;
					passThrough = 0.5;
				};
				class Face
				{
					hitPointName = "HitFace";
					armor = 20;
					passThrough = 0.3;
				};
			};
		};
	};
};
class CfgVehicles
{
	//Uniforms
	class OPTRE_Ins_ER_soldier_base;
	class RD501_Mandalorian_undersuit: OPTRE_Ins_ER_soldier_base
	{
		scope = 1;
		displayName = "[Mandos] Mandalorian Bodyglove";
		picture = "armor_unit\Ui\Zan_Armor_Gold.paa";
		hiddenSelectionsTextures[] = {"RD501_Units\textures\mandalorians\uniforms\mando_undersuit.paa"};
		hiddenSelections[] = {"camo"};
		armor = 10;
		armorStructural = 0.4;
		explosionShielding = 0.04;
		minTotalDamageThreshold = 0.001;
		impactDamageMultiplier = 0.5;
	};
	class LST_Armor_Mando_Thagoron;
	class RD501_Deathwatch_commander_armor: LST_Armor_Mando_Thagoron
	{
		scope = 1;
		displayName = "[Mandos] Deathwatch Commander Armor";
		picture = "armor_unit_2\ui\Merc_Uniform_Kando.paa";
		armor = 10;
		armorStructural = 0.4;
		explosionShielding = 0.04;
		minTotalDamageThreshold = 0.001;
		impactDamageMultiplier = 0.5;
	};
	class LST_Merc_Armor_Kando;
	class RD501_Deathwatch_honorguard_armor:LST_Merc_Armor_Kando
	{
		scope = 1;
		displayName = "[Mandos] Deathwatch Honorguard Armor";
		picture = "armor_unit_2\ui\Merc_Uniform_Kando.paa";
		uniformClass = "RD501_Deathwatch_honorguard_uniform";
		model = "\armor_unit_2\Merc_Uniform_Kando.p3d";
		hiddenSelections[] = {"РЎamo1","РЎamo2","Under","Vest"};
		hiddenSelectionsTextures[] = 
		{
			"RD501_Units\textures\mandalorians\uniforms\deathwatch\deathwatch_honorguard_camo1.paa",
			"RD501_Units\textures\mandalorians\uniforms\deathwatch\deathwatch_honorguard_camo2.paa",
			"armor_unit_2\Merc_Kando\Kando_Under_co.paa",
			"armor_unit_2\Merc_Kando\Kando_Vest_co.paa"
		};
		hiddenSelectionsMaterials[] = {"armor_unit_2\Merc_Kando\Camo1.rvmat","armor_unit_2\Merc_Kando\Camo2.rvmat","armor_unit_2\Merc_Kando\under.rvmat"};
		armor = 10;
		armorStructural = 0.4;
		explosionShielding = 0.02;
		minTotalDamageThreshold = 0.001;
		impactDamageMultiplier = 0.5;
	};
	//Jetpack
	class LST_Merc_Jet;
	class RD501_mandalorian_deathwatch_jetpack: LST_Merc_Jet
	{
		displayName = "[Mandos] Deathwatch Jetpack";
		picture = "Merc_Armor_Unit\ui\Merc_Jet_ca.paa";
		model = "\Merc_Armor_Unit\Merc_Jet.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"RD501_Units\textures\mandalorians\backpacks\deathwatch_jetpack.paa"};
	};
	class RD501_mandalorian_deathwatch_jetpack_at: RD501_mandalorian_deathwatch_jetpack
	{
		displayName = "[Mandos] Deathwatch AT Jetpack";
		class TransportMagazines
		{
			class _xx_at_mag
			{
				count=4;
				magazine="RD501_E60R_AT";
			};
			class _xx_he_mag
			{
				count=2;
				magazine="RD501_E60R_he";
			};
		};
	};
};