#include "../../../RD501_Main/config_macros.hpp"

class CfgPatches
{
    class RD501_patch_opfor_deathwatch
    {
        addonRootClass=MACRO_QUOTE(RD501_patch_units);
        requiredAddons[] =
        {
            MACRO_QUOTE(RD501_patch_units)
        };
        requiredVersion = 0.1;
        units[] = 
        {
            "RD501_opfor_unit_deathwatch_trooper",
			"RD501_opfor_unit_deathwatch_nco",
			"RD501_opfor_unit_deathwatch_captain",
			"RD501_opfor_unit_deathwatch_marksmen",
			"RD501_opfor_unit_deathwatch_at",	
			"RD501_opfor_unit_deathwatch_aa",
			"RD501_opfor_unit_deathwatch_shotgun",
			"RD501_opfor_unit_deathwatch_support",
			"RD501_opfor_unit_deathwatch_commander",
			"RD501_opfor_unit_deathwatch_honorguard"
        };
    };
};
class CfgVehicles
{
	class ls_mandalorian_assault_deathwatch;
	class RD501_opfor_unit_deathwatch_trooper: ls_mandalorian_assault_deathwatch
	{
		displayName = "Deathwatch Soldier";
		scope = 2;
		scopeCurator = 2;
		side = 0;
		faction="RD501_CIS_Faction";
		editorSubcategory=macro_editor_cat(Deathwatch);
		editorPreview = "\ls_units_greenfor\mandalorian\ui\ls_mando_base_assault.jpg";
		icon = "LSiconRifleman";
		role = "Rifleman";
		identityTypes[] = {"LanguageENG_F","Head_NATO"};
		uniformClass = "RD501_Mandalorian_undersuit";
		backpack = "RD501_mandalorian_deathwatch_jetpack";
        model = "\A3\Characters_F\Common\Suitpacks\suitpack_civilian_F.p3d";
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"RD501_Units\textures\mandalorians\uniforms\mando_undersuit.paa"};
		linkedItems[] = 
		{
			"rd501_mandalorian_deathwatch_vest_trooper",
			"ls_mandalorian_deathwatchGrunt_helmet",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"SWLB_comlink_hush98",
			"ls_mandalorian_rangefinder_nvg",
			"arc_b"
		};
		respawnLinkedItems[] = 
		{
			"rd501_mandalorian_deathwatch_vest_trooper",
			"ls_mandalorian_deathwatchGrunt_helmet",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"SWLB_comlink_hush98",
			"ls_mandalorian_rangefinder_nvg",
			"arc_b"
		};
		weapons[] = 
		{
			macro_new_weapon(Westar,35c),
			macro_new_weapon(Westar,35sa),
			"Throw",
			"Put"
		};
		respawnWeapons[] = 
		{
			macro_new_weapon(Westar,35c),
			macro_new_weapon(Westar,35sa),
			"Throw",
			"Put"
		};
		magazines[] = 
		{
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_thermaldet_cis_x1_mag",
			"macro_smoke"
		};
		respawnMagazines[] = 
		{
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_thermaldet_cis_x1_mag",
			"macro_smoke"
		};
		items[] = 
		{
			"ACE_flashlight_MX991",
			"ACE_MapTools",
			"ACE_microDAGR",
			"ACE_packingBandage",
			"ACE_packingBandage",
			"ACE_packingBandage"
		};
		respawnItems[] = 
		{
			"ACE_flashlight_MX991",
			"ACE_MapTools",
			"ACE_microDAGR",
			"ACE_packingBandage",
			"ACE_packingBandage",
			"ACE_packingBandage"
			
		};
	};
	class RD501_opfor_unit_deathwatch_nco : RD501_opfor_unit_deathwatch_trooper
	{
		displayName = "Deathwatch Sergeant";
		icon = "LSiconLeader";
		role = "Marksman";
        cost = 350000;
        accuracy = 3.9;
		linkedItems[] = 
		{
			"rd501_mandalorian_deathwatch_vest_nco",
			"ls_mandalorian_deathwatchSergeant_helmet",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"SWLB_comlink_hush98",
			"ls_mandalorian_rangefinder_nvg",
			"arc_b"
		};
		respawnLinkedItems[] = 
		{
			"rd501_mandalorian_deathwatch_vest_nco",
			"ls_mandalorian_deathwatchSergeant_helmet",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"SWLB_comlink_hush98",
			"ls_mandalorian_rangefinder_nvg",
			"arc_b"
		};
	};
	class RD501_opfor_unit_deathwatch_captain : RD501_opfor_unit_deathwatch_trooper
	{
		displayName = "Deathwatch Captain";
		icon = "LSiconLeader";
		weapons[] = 
		{
			macro_new_weapon(Westar,35sa),
			"Throw",
			"Put"
		};
		respawnWeapons[] = 
		{
			macro_new_weapon(Westar,35sa),
			"Throw",
			"Put"
		};
		linkedItems[] = 
		{
			"rd501_mandalorian_deathwatch_vest_captain",
			"ls_mandalorian_deathwatchCaptain_helmet",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"SWLB_comlink_hush98",
			"ls_mandalorian_rangefinder_nvg",
			"arc_b"
		};
		respawnLinkedItems[] = 
		{
			"rd501_mandalorian_deathwatch_vest_captain",
			"ls_mandalorian_deathwatchCaptain_helmet",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"SWLB_comlink_hush98",
			"ls_mandalorian_rangefinder_nvg",
			"arc_b"
		};
	};
	class RD501_opfor_unit_deathwatch_marksmen : RD501_opfor_unit_deathwatch_trooper
	{
		displayName = "Deathwatch Marksmen";
		icon = "LSiconMarksman";
		linkedItems[] = 
		{
			"rd501_mandalorian_deathwatch_vest_trooper",
			"ls_mandalorian_deathwatchNightowl_helmet",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"SWLB_comlink_hush98",
			"ls_mandalorian_rangefinder_nvg",
			"arc_b"
		};
		respawnLinkedItems[] = 
		{
			"rd501_mandalorian_deathwatch_vest_trooper",
			"ls_mandalorian_deathwatchNightowl_helmet",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"SWLB_comlink_hush98",
			"ls_mandalorian_rangefinder_nvg",
			"arc_b"
		};
		weapons[] = 
		{
			macro_new_weapon(iqa_scoped,11),
			macro_new_weapon(Westar,35sa),
			"Throw",
			"Put"
		};
		respawnWeapons[] = 
		{
			macro_new_weapon(iqa_scoped,11),
			macro_new_weapon(Westar,35sa),
			"Throw",
			"Put"
		};
		magazines[] = 
		{
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_thermaldet_cis_x1_mag",
			"macro_smoke"
		};
		respawnMagazines[] = 
		{
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,15),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_thermaldet_cis_x1_mag",
			"macro_smoke"
		};
	};
	class RD501_opfor_unit_deathwatch_at : RD501_opfor_unit_deathwatch_trooper
	{
		displayName = "Deathwatch Anti-Tank";
		icon = "LSiconAT";
		weapons[] = 
		{
			macro_new_weapon(Westar,35c),
			macro_new_weapon(Westar,35sa),
			"RD501_E60R",
			"Throw",
			"Put"
		};
		respawnWeapons[] = 
		{
			macro_new_weapon(Westar,35c),
			macro_new_weapon(Westar,35sa),
			"RD501_E60R",
			"Throw",
			"Put"
		};
		magazines[] = 
		{
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_E60R_AT",
			"RD501_E60R_HE",
			"RD501_thermaldet_cis_x1_mag",
			"macro_smoke"
		};
		respawnMagazines[] = 
		{
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_E60R_AT",
			"RD501_E60R_HE",
			"RD501_thermaldet_cis_x1_mag",
			"macro_smoke"
		};
	};
		backpack = "RD501_mandalorian_deathwatch_jetpack_at";
	class RD501_opfor_unit_deathwatch_aa : RD501_opfor_unit_deathwatch_at
	{
		displayName = "Deathwatch Anti-Air";
		weapons[] = 
		{
			macro_new_weapon(Westar,35c),
			macro_new_weapon(Westar,35sa),
			"launch_I_Titan_F",
			"Throw",
			"Put"
		};
		respawnWeapons[] = 
		{
			macro_new_weapon(Westar,35c),
			macro_new_weapon(Westar,35sa),
			"launch_I_Titan_F",
			"Throw",
			"Put"
		};
		magazines[] = 
		{
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"Titan_AA",
			"Titan_AA",
			"Titan_AA",
			"Titan_AA",
			"RD501_thermaldet_cis_x1_mag",
			"macro_smoke"
		};
		respawnMagazines[] = 
		{
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"Titan_AA",
			"Titan_AA",
			"Titan_AA",
			"Titan_AA",
			"RD501_thermaldet_cis_x1_mag",
			"macro_smoke"
		};
	};
	class RD501_opfor_unit_deathwatch_shotgun : RD501_opfor_unit_deathwatch_trooper
	{
		displayName = "Deathwatch Shotgunner";
		icon = "LSiconAT";
		weapons[] = 
		{
			"JLTS_SBB3",
			macro_new_weapon(Westar,35sa),
			"Throw",
			"Put"
		};
		respawnWeapons[] = 
		{
			"JLTS_SBB3",
			macro_new_weapon(Westar,35sa),
			"Throw",
			"Put"
		};
		magazines[] = 
		{
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_thermaldet_cis_x1_mag",
			"macro_smoke"
		};
		respawnMagazines[] = 
		{
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_thermaldet_cis_x1_mag",
			"macro_smoke"
		};
	};
	class RD501_opfor_unit_deathwatch_support : RD501_opfor_unit_deathwatch_trooper
	{
		displayName = "Deathwatch Support";
		icon = "LSiconMG";
		weapons[] = 
		{
			macro_new_weapon(dc_scoped,34),
			"Throw",
			"Put"
		};
		respawnWeapons[] = 
		{
			macro_new_weapon(dc_scoped,34),
			"Throw",
			"Put"
		};
		magazines[] = 
		{
			macro_new_mag(westar,60),
			macro_new_mag(westar,60),
			macro_new_mag(westar,60),
			macro_new_mag(westar,60),
			macro_new_mag(westar,60),
			macro_new_mag(westar,60),
			macro_new_mag(westar,60),
			macro_new_mag(westar,60)
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_thermaldet_cis_x1_mag",
			"macro_smoke"
		};
		respawnMagazines[] = 
		{
			macro_new_mag(westar,60),
			macro_new_mag(westar,60),
			macro_new_mag(westar,60),
			macro_new_mag(westar,60),
			macro_new_mag(westar,60),
			macro_new_mag(westar,60),
			macro_new_mag(westar,60),
			macro_new_mag(westar,60)
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_thermaldet_cis_x1_mag",
			"macro_smoke"
		};
	};
	class RD501_opfor_unit_deathwatch_commander : RD501_opfor_unit_deathwatch_captain
	{
		displayName = "Deathwatch Commander";
		icon = "LSiconLeader";
		uniformClass = "RD501_Deathwatch_commander_armor";
		nakedUniform = "RD501_Deathwatch_commander_armor";
		backpack = "LST_Merc_Jet_Thagoron_Z6";
        model = "dst\armour_hyperion_rework.p3d";
		hiddenSelections[] = {"Camo1","Camo2","CamoB","Uiform","heart"};
		hiddenSelectionsTextures[] = {"dst\rework\uniform\camo1_co.paa","dst\rework\uniform\camo2_co.paa","dst\rework\uniform\camob_co.paa","dst\rework\uniform\uniform_co.paa","dst\rework\uniform\camo1_co.paa"};
		hiddenSelectionsMaterials[] = {"dst\rework\uniform\camo1.rvmat","dst\rework\uniform\camo2.rvmat","dst\rework\uniform\camob.rvmat","dst\rework\uniform\uniform.rvmat","dst\rework\uniform\camo1.rvmat"};
		linkedItems[] = 
		{
			"rd501_mandalorian_deathwatch_vest_commander",
			"RD501_Deathwatch_commander_helmet",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"SWLB_comlink_hush98",
			"LST_Mecr_NVG_ZanX",
			"Merc_Cape_Des"
		};
		respawnLinkedItems[] = 
		{
			"rd501_mandalorian_deathwatch_vest_commander",
			"RD501_Deathwatch_commander_helmet",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"SWLB_comlink_hush98",
			"LST_Mecr_NVG_ZanX",
			"Merc_Cape_Des"
		};
		weapons[] = 
		{
			macro_new_weapon(Westar,35sa),
			"Throw",
			"Put"
		};
		respawnWeapons[] = 
		{
			macro_new_weapon(Westar,35sa),
			"Throw",
			"Put"
		};
		magazines[] = 
		{
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_thermaldet_cis_x1_mag",
			macro_new_mag(personal_shield,1)
		};
		respawnMagazines[] = 
		{
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_thermaldet_cis_x1_mag",
			macro_new_mag(personal_shield,1)
		};
	};
	class RD501_opfor_unit_deathwatch_honorguard : RD501_opfor_unit_deathwatch_trooper
	{
		displayName = "Deathwatch Honorguard";
		icon = "LSiconAT";
		uniformClass = "RD501_Deathwatch_honorguard_uniform";
		backpack = "SWLB_clone_bag";
       	model = "\armor_unit_2\Merc_Uniform_Kando.p3d";
		nakedUniform = "RD501_Deathwatch_honorguard_uniform";
		hiddenSelections[] = {"РЎamo1","РЎamo2","Under","Vest"};
		hiddenSelectionsTextures[] = 
		{
			"RD501_Units\textures\mandalorians\uniforms\deathwatch\deathwatch_honorguard_camo1.paa",
			"RD501_Units\textures\mandalorians\uniforms\deathwatch\deathwatch_honorguard_camo2.paa",
			"armor_unit_2\Merc_Kando\Kando_Under_co.paa",
			"armor_unit_2\Merc_Kando\Kando_Vest_co.paa"
		};
		hiddenSelectionsMaterials[] = {"armor_unit_2\Merc_Kando\Camo1.rvmat","armor_unit_2\Merc_Kando\Camo2.rvmat","armor_unit_2\Merc_Kando\under.rvmat"};
		linkedItems[] = 
		{
			"rd501_mandalorian_deathwatch_vest_honorguard",
			"RD501_Deathwatch_honorguard_helmet",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"SWLB_comlink_hush98",
			"SWLB_MPStealth_Nvg",
			"3as_Phase1_Lamps"
		};
		respawnLinkedItems[] = 
		{
			"rd501_mandalorian_deathwatch_vest_honorguard",
			"RD501_Deathwatch_honorguard_helmet",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"SWLB_comlink_hush98",
			"SWLB_MPStealth_Nvg",
			"3as_Phase1_Lamps"
		};
		weapons[] = 
		{
			macro_new_weapon(Westar_scoped,35s),
			"Throw",
			"Put"
		};
		respawnWeapons[] = 
		{
			macro_new_weapon(Westar_scoped,35s),
			"Throw",
			"Put"
		};
		magazines[] = 
		{
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_thermaldet_cis_x1_mag",
			macro_new_mag(dioxis,1)
		};
		respawnMagazines[] = 
		{
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			"JLTS_SBB3_mag",
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			macro_new_mag(westar,35),
			"RD501_thermaldet_cis_x1_mag",
			macro_new_mag(dioxis,1)
		};
	};
};
class CfgGroups
{
	class EAST
	{
		class macro_faction(CIS)
		{
			name="RD501 C.I.S Faction";
			class RD501_Deathwatch
			{
				name = "Deathwatch";
				class RD501_deathwatch_infantry_squad
				{
					name = "Infantry Squad";
					side = 0;
					faction = macro_faction(CIS);
					class Unit0
					{
						side = 0;
						vehicle = "RD501_opfor_unit_deathwatch_nco";
						rank = "SERGEANT";
						position[] = {0,0,0};
					};
					class Unit1: Unit0
					{
						vehicle = "RD501_opfor_unit_deathwatch_at_light";
						rank = "CORPORAL";
						position[] = {5,-5,0};
					};
					class Unit2: Unit1
					{
						vehicle = "RD501_opfor_unit_deathwatch_trooper";
						rank = "PRIVATE";
						position[] = {-5,-5,0};
					};
					class Unit3: Unit2
					{
						vehicle = "RD501_opfor_unit_deathwatch_support";
						position[] = {10,-10,0};
					};
					class Unit4: Unit2
					{
						position[] = {-10,-10,0};
					};
					class Unit5: Unit2
					{
						position[] = {15,-15,0};
					};
					class Unit6: Unit2
					{
						vehicle = "RD501_opfor_unit_deathwatch_shotgun";
						position[] = {-15,-15,0};
					};
					class Unit7: Unit3
					{
						vehicle = "RD501_opfor_unit_deathwatch_marksmen";
						position[] = {20,-20,0};
					};
					class Unit8: Unit2
					{
						vehicle = "RD501_opfor_unit_deathwatch_support";
						position[] = {-20,-20,0};
					};
				};
				class RD501_deathwatch_weapons_team: RD501_deathwatch_infantry_squad
				{
					name = "Weapons Team";
					class Unit0: Unit0{};
					class Unit1: Unit1
					{
						vehicle = "RD501_opfor_unit_deathwatch_trooper";
					};
					class Unit2: Unit2{};
					class Unit3: Unit3
					{
						vehicle = "RD501_opfor_unit_deathwatch_aa";
					};
					class Unit4: Unit4{};
					class Unit5: Unit5
					{
						vehicle = "RD501_opfor_unit_deathwatch_at_heavy";
					};
					class Unit6: Unit6{};

					class Unit7: Unit7{};
					
				};
				class RD501_deathwatch_infantry_team: RD501_deathwatch_infantry_squad
				{
					name = "Infantry Team";
					class Unit0: Unit0{};
					class Unit1: Unit1{};
					class Unit2: Unit2{};
					class Unit3: Unit3{};
				};
				class RD501_deathwatch_at_team: RD501_deathwatch_infantry_team
				{
					name = "$STR_A3_CfgGroups_West_BLU_F_Infantry_BUS_InfTeam_AT0";
					class Unit0: Unit0{};
					class Unit1: Unit1
					{
						vehicle = "RD501_opfor_unit_deathwatch_at_heavy";
					};
					class Unit2: Unit2
					{
						vehicle = "RD501_opfor_unit_deathwatch_at_light";
					};
					class Unit3: Unit3
					{
						vehicle = "RD501_opfor_unit_deathwatch_at_light";
					};
				};
				class RD501_deathwatch_aa_team: RD501_deathwatch_at_team
				{
					name = "AA Team";
					class Unit0: Unit0{};
					class Unit1: Unit1
					{
						vehicle = "RD501_opfor_unit_deathwatch_at_light";
					};
					class Unit2: Unit2
					{
						vehicle = "RD501_opfor_unit_deathwatch_aa";
					};
					class Unit3: Unit3
					{
						vehicle = "RD501_opfor_unit_deathwatch_aa";
					};
				};
				class RD501_deathwatch_infantry_sentry: RD501_deathwatch_aa_team
				{
					name = "Infantry Sentry";
					class Unit0: Unit0
					{
						rank = "CORPORAL";
					};
					class Unit1: Unit1
					{
						vehicle = "RD501_opfor_unit_deathwatch_trooper";
						rank = "PRIVATE";
					};
				};
				class RD501_deathwatch_sniper_team: RD501_deathwatch_infantry_squad
				{
					name = "Sniper Team";
					class Unit0: Unit0
					{
						vehicle = "RD501_opfor_unit_deathwatch_marksmen";
					};
					class Unit1: Unit1
					{
						vehicle = "RD501_opfor_unit_deathwatch_marksmen";
					};
				};
				class RD501_deathwatch_platoon_command: RD501_deathwatch_infantry_team
				{
					name = "Platoon Command";
					class Unit0
					{
						side = 0;
						vehicle = "RD501_opfor_unit_deathwatch_captain";
						rank = "CAPTAIN";
						position[] = {0,0,0};
					};
					class Unit1: Unit0
					{
						vehicle = "RD501_opfor_unit_deathwatch_nco";
						rank = "SERGEANT";
						position[] = {5,-5,0};
					};
					class Unit2
					{
						vehicle = "RD501_opfor_unit_deathwatch_trooper";
						rank = "PRIVATE";
						position[] = {-5,-5,0};
					};
					class Unit3:Unit2
					{
						vehicle = "RD501_opfor_unit_deathwatch_shotgun";
						position[] = {10,-10,0};
					};
					class Unit4:Unit3
					{
						position[] = {-10,-10,0};
					};
				};
				class RD501_deathwatch_command_team: RD501_deathwatch_platoon_command
				{
					name = "Command Team";
					class Unit0
					{
						side = 0;
						vehicle = "RD501_opfor_unit_deathwatch_commander";
						rank = "COLONEL";
						position[] = {0,0,0};
					};
					class Unit1: Unit0
					{
						vehicle = "RD501_opfor_unit_deathwatch_honorguard";
						rank = "LIEUTENANT";
						position[] = {5,-5,0};
					};
					class Unit2
					{
						vehicle = "RD501_opfor_unit_deathwatch_honorguard";
						rank = "LIEUTENANT";
						position[] = {-5,-5,0};
					};

				};
			};
		};
	};
};