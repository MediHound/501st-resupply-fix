//POOPY INFANTRY RTO BACKPACK
//Why is infantry so weird.

	
class RD501_JLTS_Clone_backpack: JLTS_Clone_backpack
{
	maximumload=700;
	Displayname="[501st] Combat Pack (Base)";
};
class RD501_JLTS_Clone_backpack_s: JLTS_Clone_backpack_s
{
	maximumload=700;
	Displayname="[501st] Combat Pack (Straps)";
};
class RD501_JLTS_Clone_backpack_invisible: JLTS_Clone_backpack_s
{
	maximumload=700;
	Displayname="[501st] Combat Invisible Backpack";
	model="RD501_Units\nothing.p3d";
};
class RD501_JLTS_Clone_backpack_medic: JLTS_Clone_backpack_medic
{
	maximumload=700;
	Displayname="[501st] Combat Pack (Medical)";
};
class RD501_JLTS_Clone_backpack_eod: JLTS_Clone_backpack_eod
{
	maximumload=700;
	Displayname="[501st] Combat Pack (EOD)";
};
class RD501_JLTS_Clone_backpack_bomb: JLTS_Clone_backpack_bomb
{
	maximumload=700;
	Displayname="[501st] Combat Pack (Engineer)";
	hiddenSelectionsTextures[] = {"\RD501_Vehicles\static\data\rd501_backpack_engineer.paa"};
};
class RD501_JLTS_Clone_backpack_RTO: JLTS_Clone_backpack_RTO
{
	maximumload=700;
	tf_range=35000;
	Displayname="[501st] Radio Pack (Large)";
};
class RD501_JLTS_Clone_backpack_s_RTO: JLTS_Clone_backpack_s_RTO
{
	maximumload=700;
	tf_range=35000;
	Displayname="[501st] Radio Pack (Straps)";
};
class RD501_JLTS_Clone_belt_bag: JLTS_Clone_belt_bag
{
	maximumload=700;
	Displayname="[501st] Belt Bag";
};
class RD501_JLTS_Clone_RTO_pack: JLTS_Clone_RTO_pack
{
	maximumload=700;
	tf_range=35000;
	Displayname="[GAR] Radio Pack (Small)";
};
class RD501_JLTS_Clone_RTO_pack_blue: JLTS_Clone_RTO_pack
{
	maximumload=700;
	tf_range=35000;
	Displayname="[501st] Radio Pack (Small)";
	hiddenSelectionsTextures[] = {"rd501_units\textures\republic\clones\LR_Small.paa"};
};
class RD501_JLTS_Clone_LR_attachment: JLTS_Clone_LR_attachment
{
	maximumload=700;
	tf_range=35000;
	Displayname="[501st] Radio Pack (Attachment)";
};
class RD501_JLTS_Clone__invis_LR_attachment: JLTS_Clone_LR_attachment
{
	maximumload=700;
	tf_range=35000;
	Displayname="[501st] Command Invisible Pack";
	model="RD501_Units\nothing.p3d";
};
class RD501_JLTS_Clone_ARC_backpack: JLTS_Clone_ARC_backpack
{
	maximumload=700;
	tf_range=35000;
	Displayname="[501st] Specialist Pack (ARC)";
};
class RD501_JLTS_Clone_Flip_backpack: JLTS_Clone_backpack
{
	maximumload = 0;
	Displayname="[501st] Grav Lift Pack";
	hiddenSelectionsTextures[] = {"rd501_units\textures\Republic\clones\mynock\mynock_backpack_flip.paa"};
	picture = "rd501_units\textures\Republic\clones\mynock\mynock_backpack_flip_icon_co.paa";

};
class lsd_gar_marine_backpack;
class RD501_LS_snow_backpack: lsd_gar_marine_backpack
{	
	maximumload = 700;
	Displayname = "[501st] Snow Pack";
};
class RD501_LS_snow_backpack_LR: lsd_gar_marine_backpack
{	
	maximumload = 700;
	Displayname = "[501st] Snow Pack (LR)";
	tf_dialog = "JLTS_clone_rto_radio_dialog";
	tf_dialogUpdate = "call TFAR_fnc_updateLRDialogToChannel;";
	tf_encryptionCode = "tf_west_radio_code";
	tf_hasLRradio = 1;
	tf_range=35000;
	tf_subtype = "digital_lr";
};
class RD501_JLTS_Breathmask_Backpack: JLTS_Clone_backpack
{
		displayname = "[501st] PURG-3 Fuel Tank";
		maximumLoad = 500;
		picture = "DBA_Republic\Addons\DBA_Armour\DBA_Backpacks\data\ui\Breathpack_CA.paa";
		model = "DBA_Republic\Addons\DBA_Armour\DBA_Backpacks\JLTS_Breathmask.p3d";
		hiddenSelectionsTextures[] = {"DBA_Republic\Addons\DBA_Armour\DBA_Backpacks\DBA_Breathmask_CO.paa"};
};