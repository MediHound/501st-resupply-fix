params ["_player", "_didJIP"];

["RD501_3DEN_event_ZM_setMarkerVisibilty", {
	_this call RD501_3DEN_fnc_ZM_setMarkerVisibility;
}] call CBA_fnc_addEventHandler;

diag_log (["RD501_3DEN player dumb", player] joinString " ");

if (not (isNull player)) then {
	[0] call RD501_3DEN_fnc_ZM_setMarkerVisibility;

	[{
		params ["_player"];

		diag_log (["RD501_3DEN inside spawn", _player, _this] joinString " ");

		if (not (isNull (getAssignedCuratorLogic _player))) then {
			["RD501_3DEN_event_ZM_setMarkerVisibilty", [1], _player] call CBA_fnc_targetEvent;
		};
	}, [player], 5] call CBA_fnc_waitAndExecute;
};