#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_arty_weapons
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons,
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			"RD501_CIS_155mm_mortar"
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class CfgWeapons
{   
    class CannonCore;
    class mortar_155mm_AMOS
    {
        class single1;
        class burst1;
    };

    class RD501_CIS_155mm_howitzer: mortar_155mm_AMOS
    {
		scope=1;
		displayName="CIS Artillery Cannon";
		fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
		nameSound="cannon";
		cursor="mortar";
		cursorAim="EmptyCursor";
		sounds[] = {"StandardSound"};
        class StandardSound
        {
            begin1[] = {"\RD501_Vehicle_Weapons\_sounds\arty\cis_arty_cannon.wss",2.51189,1,1500};
            soundBegin[] = {"begin1",1};
        };
		reloadSound[]=
		{
			"A3\Sounds_F\vehicles\armor\noises\reload_tank_cannon_2",
			31.622778,
			1,
			15
		};
		reloadTime=7;
		magazineReloadTime=7;
		autoReload=1;
		canLock=0;
		magazines[]=
		{
			"RD501_32Rnd_155mm_cis_he_mag",
			"RD501_32Rnd_155mm_cis_he_mag",
			"6Rnd_155mm_Mo_smoke",
			"6Rnd_155mm_Mo_smoke_O",
			"2Rnd_155mm_Mo_guided",
			"4Rnd_155mm_Mo_guided",
			"4Rnd_155mm_Mo_guided_O",
			"2Rnd_155mm_Mo_LG",
			"4Rnd_155mm_Mo_LG",
			"4Rnd_155mm_Mo_LG_O",
			"6Rnd_155mm_Mo_mine",
			"6Rnd_155mm_Mo_mine_O",
			"2Rnd_155mm_Mo_Cluster",
			"2Rnd_155mm_Mo_Cluster_O",
			"6Rnd_155mm_Mo_AT_mine",
			"6Rnd_155mm_Mo_AT_mine_O"
		};
		modes[]=
		{
			"Single1",
			"Single2",
			"Single3",
			"Single4",
			"Single5",
			"Burst1",
			"Burst2",
			"Burst3",
			"Burst4",
			"Burst5"
		};
		class Single1: Mode_SemiAuto
		{
			displayName="$STR_A3_mortar_120mm_AMOS_Single10";
			sounds[] = {"StandardSound"};
            class StandardSound
            {
                begin1[] = {"\RD501_Vehicle_Weapons\_sounds\arty\cis_arty_cannon.wss",2.51189,1,1500};
                soundBegin[] = {"begin1",1};
            };
			reloadSound[]=
			{
				"A3\sounds_f\dummysound",
				1,
				1,
				20
			};
			reloadTime=4;
			artilleryDispersion=3.2;
			artilleryCharge=0.19;
			aiRateOfFire=1;
			aiRateOfFireDistance=10;
			minRange=0;
			minRangeProbab=0.0099999998;
			midRange=1;
			midRangeProbab=0.0099999998;
			maxRange=2;
			maxRangeProbab=0.0099999998;
		};
		class Single2: Single1
		{
			displayName="$STR_A3_mortar_120mm_AMOS_Single20";
			artilleryCharge=0.30000001;
		};
		class Single3: Single1
		{
			displayName="$STR_A3_mortar_120mm_AMOS_Single30";
			artilleryCharge=0.47999999;
		};
		class Single4: Single1
		{
			displayName="$STR_A3_mortar_120mm_AMOS_Single40";
			artilleryCharge=0.80000001;
		};
		class Single5: Single1
		{
			displayName="$STR_A3_mortar_120mm_AMOS_Single50";
			artilleryCharge=1;
		};
		class Burst1: Mode_Burst
		{
			showToPlayer=0;
			displayName="$STR_A3_mortar_120mm_AMOS_Burst10";
			burst=6;
			sounds[] = {"StandardSound"};
            class StandardSound
            {
                begin1[] = {"\RD501_Vehicle_Weapons\_sounds\arty\cis_arty_cannon.wss",2.51189,1,1500};
                soundBegin[] = {"begin1",1};
            };
            reloadSound[]=
			{
				"A3\sounds_f\dummysound",
				1,
				1,
				20
			};
			soundBurst=0;
			reloadTime=6;
			minRange=800;
			minRangeProbab=0.5;
			midRange=1500;
			midRangeProbab=0.69999999;
			maxRange=2000;
			maxRangeProbab=0.5;
			artilleryDispersion=5.3000002;
			artilleryCharge=0.19;
		};
		class Burst2: Burst1
		{
			showToPlayer=0;
			displayName="$STR_A3_mortar_120mm_AMOS_Burst20";
			minRange=2000;
			minRangeProbab=0.40000001;
			midRange=3000;
			midRangeProbab=0.60000002;
			maxRange=5200;
			maxRangeProbab=0.40000001;
			artilleryCharge=0.30000001;
		};
		class Burst3: Burst1
		{
			showToPlayer=0;
			displayName="$STR_A3_mortar_120mm_AMOS_Burst30";
			minRange=5200;
			minRangeProbab=0.30000001;
			midRange=8000;
			midRangeProbab=0.40000001;
			maxRange=13300;
			maxRangeProbab=0.30000001;
			artilleryCharge=0.47999999;
		};
		class Burst4: Burst1
		{
			showToPlayer=0;
			displayName="$STR_A3_mortar_120mm_AMOS_Burst40";
			minRange=14600;
			minRangeProbab=0.2;
			midRange=25000;
			midRangeProbab=0.30000001;
			maxRange=37000;
			maxRangeProbab=0.2;
			artilleryCharge=0.80000001;
		};
		class Burst5: Burst1
		{
			showToPlayer=0;
			displayName="$STR_A3_mortar_120mm_AMOS_Burst50";
			minRange=25000;
			minRangeProbab=0.1;
			midRange=40000;
			midRangeProbab=0.2;
			maxRange=58000;
			maxRangeProbab=0.1;
			artilleryCharge=1;
		};
    };
    class RD501_AV7_300mm_howtizer: CannonCore
	{
		scope = 1;
		displayName = "AV7 Heavy Cannon";
		nameSound = "cannon";
		cursor = "mortar";
		cursorAim = "EmptyCursor";
		sounds[] = {"StandardSound"};
		class StandardSound
		{
			begin1[] = {"\RD501_Vehicle_Weapons\_sounds\arty\rep_arty_cannon.wss",2.51189,1,1500};
			soundBegin[] = {"begin1",1};
		};
		reloadSound[] = {"A3\Sounds_F\vehicles\armor\noises\reload_tank_cannon_2",31.622778,1,15};
		reloadTime = 7;
		magazineReloadTime = 7;
		autoReload = 1;
		canLock = 0;
		magazines[] = 
        {
            "RD501_32Rnd_300mm_HE_shells",
            "3AS_12Rnd_300mm_Mo_smoke",
            "3AS_4Rnd_300mm_Mo_guided",
            "3AS_4Rnd_300mm_Mo_LG",
            "3AS_12Rnd_300mm_Mo_mine",
            "3AS_4Rnd_300mm_Mo_Cluster",
            "3AS_12Rnd_300mm_Mo_AT_mine"
        };
		modes[] = {"Single1","Single2","Single3","Single4","Single5","Burst1","Burst2","Burst3","Burst4","Burst5"};
		class GunParticles
		{
			class Effect1
			{
				effectName = "ArtilleryFired1";
				positionName = "Usti hlavne3";
				directionName = "Konec hlavne3";
			};
			class Effect2
			{
				effectName = "ArtilleryFiredL";
				positionName = "Usti hlavne";
				directionName = "Usti hlavne b";
			};
			class Effect3
			{
				effectName = "ArtilleryFiredR";
				positionName = "Usti hlavne";
				directionName = "Usti hlavne b";
			};
		};
		class Single1: Mode_SemiAuto
		{
			displayName = "$STR_A3_mortar_120mm_AMOS_Single10";
			sounds[] = {"StandardSound"};
			class StandardSound
			{
				begin1[] = {"\RD501_Vehicle_Weapons\_sounds\arty\rep_arty_cannon.wss",2.51189,1,1500};
				soundBegin[] = {"begin1",1};
			};
			reloadSound[] = {"A3\sounds_f\dummysound",1,1,20};
			reloadTime = 4;
			artilleryDispersion = 3.2;
			artilleryCharge = 0.19;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 10;
			minRange = 0;
			minRangeProbab = 0.01;
			midRange = 1;
			midRangeProbab = 0.01;
			maxRange = 2;
			maxRangeProbab = 0.01;
		};
		class Single2: Single1
		{
			displayName = "$STR_A3_mortar_120mm_AMOS_Single20";
			artilleryCharge = 0.3;
		};
		class Single3: Single1
		{
			displayName = "$STR_A3_mortar_120mm_AMOS_Single30";
			artilleryCharge = 0.48;
		};
		class Single4: Single1
		{
			displayName = "$STR_A3_mortar_120mm_AMOS_Single40";
			artilleryCharge = 0.8;
		};
		class Single5: Single1
		{
			displayName = "$STR_A3_mortar_120mm_AMOS_Single50";
			artilleryCharge = 1;
		};
		class Burst1: Mode_Burst
		{
			showToPlayer = 0;
			displayName = "$STR_A3_mortar_120mm_AMOS_Burst10";
			burst = 6;
			sounds[] = {"StandardSound"};
			class StandardSound
			{
				begin1[] = {"\RD501_Vehicle_Weapons\_sounds\arty\rep_arty_cannon.wss",2.51189,1,1500};
				soundBegin[] = {"begin1",1};
			};
			reloadSound[] = {"A3\sounds_f\dummysound",1,1,20};
			soundBurst = 0;
			reloadTime = 6;
			minRange = 800;
			minRangeProbab = 0.5;
			midRange = 1500;
			midRangeProbab = 0.7;
			maxRange = 2000;
			maxRangeProbab = 0.5;
			artilleryDispersion = 5.3;
			artilleryCharge = 0.19;
		};
		class Burst2: Burst1
		{
			showToPlayer = 0;
			displayName = "$STR_A3_mortar_120mm_AMOS_Burst20";
			minRange = 2000;
			minRangeProbab = 0.4;
			midRange = 3000;
			midRangeProbab = 0.6;
			maxRange = 5200;
			maxRangeProbab = 0.4;
			artilleryCharge = 0.3;
		};
		class Burst3: Burst1
		{
			showToPlayer = 0;
			displayName = "$STR_A3_mortar_120mm_AMOS_Burst30";
			minRange = 5200;
			minRangeProbab = 0.3;
			midRange = 8000;
			midRangeProbab = 0.4;
			maxRange = 13300;
			maxRangeProbab = 0.3;
			artilleryCharge = 0.48;
		};
		class Burst4: Burst1
		{
			showToPlayer = 0;
			displayName = "$STR_A3_mortar_120mm_AMOS_Burst40";
			minRange = 14600;
			minRangeProbab = 0.2;
			midRange = 25000;
			midRangeProbab = 0.3;
			maxRange = 37000;
			maxRangeProbab = 0.2;
			artilleryCharge = 0.8;
		};
		class Burst5: Burst1
		{
			showToPlayer = 0;
			displayName = "$STR_A3_mortar_120mm_AMOS_Burst50";
			minRange = 25000;
			minRangeProbab = 0.1;
			midRange = 40000;
			midRangeProbab = 0.2;
			maxRange = 58000;
			maxRangeProbab = 0.1;
			artilleryCharge = 1;
		};
	};
};
