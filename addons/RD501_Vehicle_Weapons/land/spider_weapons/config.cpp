
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_spider_weapons
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons,
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			macro_new_weapon(spider,homing_cannon),
			macro_new_weapon(spider,homing_blaster)
		};
	};
};

class CfgWeapons
{
	class 3AS_HeavyGATCannon_Base;
	class 3AS_GATCannon:3AS_HeavyGATCannon_Base
	{
		class manual;
	};
	class Mgun;
	class LMG_RCWS;
	class 3AS_HeavyRepeater_W;
	class macro_new_weapon(spider,homing_cannon) : 3AS_GATCannon
	{
		displayName = "Heavy Laser Cannon";
		fireLightDiffuse[] = {1,0,0};
		fireLightAmbient[] = {1,0,0};	
		magazines[] = 
		{
			macro_new_mag(homing_spider,10)
		};
		ballisticsComputer = 4;
		canLock = 2;
		weaponLockDelay = 3.5;
		weaponLockSystem = 8;
		ace_overpressure_angle = 30;
		ace_overpressure_range = 10;
		ace_overpressure_damage = 0.1;

		magazineReloadTime = 6;

		modes[] = {"manual","close"};
		class manual: manual
		{
			recoil = "empty";
			weaponSoundEffect = "DefaultRifle";
			soundContinuous = 0;
			reloadTime = 3;
			magazineReloadTime = 6;
			autoReload = 1;
			ballisticsComputer = 1;
			canLock = 2;
			autoFire = 0;
			dispersion = 0;
			sounds[] = {"StandardSound"};
			class StandardSound
			{
				begin1[] = {"kobra\442_weapons\sounds\cannon\cannon9.wss",10.25,1.75,5500};
				soundBegin[] = {"begin1",1};
			};
		};
		class close: manual
		{
			showToPlayer = 0;
			aiRateOfFire = .5;
			aiRateOfFireDistance = 10000;
			minRange = 0;
			minRangeProbab = 0.35;
			midRange = 500;
			midRangeProbab = 0.78;
			maxRange = 10000;
			maxRangeProbab = 1.0;
			aiDispersionCoefX = 2;
			aiDispersionCoefY = 2;
		};
	};
	class macro_new_weapon(spider,homing_blaster):3AS_HeavyRepeater_W
	{
		displayName = "CIS Heavy Repeating Blaster";
		magazineReloadTime = 5;
		ballisticsComputer = 2;
		aiDispersionCoefY = 0.5;
		aiDispersionCoefX = 0.5;
		fireLightDiffuse[] = {1,0,0};
		fireLightAmbient[] = {1,0,0};	
		class gunParticles
		{
			class effect2
			{
				positionName = "machinegun_end";
				directionName = "machinegun_beg";
				effectName = "MachineGun1";
			};
		};
		class manual: MGun
		{
			displayName = "Heavy Repeater";
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				closure1[] = {"A3\sounds_f\weapons\gatling\gatling_rotation_short_2",0.316228,1,20};
				closure2[] = {"A3\sounds_f\weapons\gatling\gatling_rotation_short_3",0.316228,1,20};
				soundClosure[] = {"closure1",0.5,"closure2",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"kobra\442_weapons\sounds\heavy\heavy11.wss",0.95,1,2000};
				soundBegin[] = {"begin1",1};
				volume = 1.0;
				range = 1800;
			};
			soundContinuous = 0;
			soundBurst = 0;
			multiplier = 1;
			reloadTime = 0.1;
			dispersion = 0.0115;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 10;
			minRange = 0;
			minRangeProbab = 0.01;
			midRange = 1;
			midRangeProbab = 0.01;
			maxRange = 2;
			maxRangeProbab = 0.01;
		};
		class close: manual
		{
			soundBurst = 0;
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 12;
			burstRangeMax = 42;
			aiRateOfFire = 0.5;
			aiRateOfFireDispersion = 1;
			aiRateOfFireDistance = 50;
			minRange = 0;
			minRangeProbab = 0.7;
			midRange = 0;
			midRangeProbab = 0.75;
			maxRange = 300;
			maxRangeProbab = 0.2;
		};
		class short: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 8;
			burstRangeMax = 36;
			aiRateOfFire = 1;
			aiRateOfFireDispersion = 2;
			aiRateOfFireDistance = 150;
			minRange = 0;
			minRangeProbab = 0.75;
			midRange = 300;
			midRangeProbab = 0.75;
			maxRange = 600;
			maxRangeProbab = 0.2;
		};
		class medium: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 8;
			burstRangeMax = 30;
			aiRateOfFire = 2;
			aiRateOfFireDispersion = 2;
			aiRateOfFireDistance = 300;
			minRange = 300;
			minRangeProbab = 0.75;
			midRange = 600;
			midRangeProbab = 0.65;
			maxRange = 800;
			maxRangeProbab = 0.1;
		};
		class far: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 8;
			burstRangeMax = 12;
			aiRateOfFire = 4;
			aiRateOfFireDispersion = 4;
			aiRateOfFireDistance = 800;
			minRange = 800;
			minRangeProbab = 0.65;
			midRange = 1000;
			midRangeProbab = 0.3;
			maxRange = 1500;
			maxRangeProbab = 0.05;
		};
		drySound[] = {"A3\Sounds_F\arsenal\weapons_vehicles\LMG_Minigun_65mm\LMGMinigun65mm_dry",1,1,10};
		magazines[] = 
        {
            macro_new_mag(homing_spider_eweb,250)
        };
	};
}; 

