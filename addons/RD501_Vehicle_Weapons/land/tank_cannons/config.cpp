
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_tank_cannons
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons,
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			macro_new_weapon(saber_gun,td),
			macro_new_weapon(saber_gun,mbt),
			macro_new_weapon(aat_cannon,mbt),
			macro_new_weapon(aat_cannon,king),
			macro_new_weapon(atte,mbt),
			macro_new_weapon(mynock_cannon,mbt),
			macro_new_weapon(mynock_cannon,td),
			"RD501_atap_top_turret"
		};
	};
};
class Mode_SemiAuto;
class CfgWeapons
{
	class CannonCore;
	class MGun;
	class LMG_RCWS: MGun
	{
		class manual: Mgun{};
		class close: manual{};
		class short: close{};
		class medium: close{};
		class far: close{};
	};
	class 3AS_HeavyAATCannon_Base:CannonCore
	{
		class manual;
	};
	class 3AS_HeavyAATCannon:3AS_HeavyAATCannon_Base
	{
		class manual:manual
		{};
	};
	class 3AS_AAT_Repeater:LMG_RCWS
	{
		class manual;
		class close;
		class short;
		class medium;
		class far;
	};
	class 3AS_Sabre_Cannons: MGun
	{
		class manual: Mgun{};
	};
	class 3AS_Sabre_Cannons_Super: 3AS_Sabre_Cannons
	{
		class manual: manual
		{};
	};
	class 3AS_ATAP_AT_Turret: 3AS_Sabre_Cannons_Super
	{
		class manual: manual
		{};
	};
	class 3AS_ParticleCannon;
	
	class RD501_aat_cannon_mbt: 3AS_HeavyAATCannon
	{

		displayName = "AAT Laser Cannon";
		displayNameMagazine = "AAT Cannon Rounds";
		author= "RD501";
		scope = 2;
		cursor = "EmptyCursor";
		cursorAim = "cannon";
		showAimCursorInternal = 0;
		nameSound = "cannon";
		fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
		reloadSound[] = {"A3\Sounds_F\arsenal\weapons_vehicles\cannon_125mm\Cannon_125mm_Reload_01",2.51189,1,10};
		reloadMagazineSound[] = {"A3\Sounds_F\arsenal\weapons_vehicles\cannon_125mm\Cannon_125mm_Reload_01",2.51189,1,10};
		magazines[] = 
		{
			"RD501_aat_mbt_x50_mag"
		};
		reloadTime = 6;
		magazineReloadTime = 6;
		autoReload = 1;
		canLock = 0;
		ballisticsComputer = "2 + 16";
		FCSMaxLeadSpeed = 25;
		FCSZeroingDelay = 1;
		aiDispersionCoefY = 2;
		aiDispersionCoefX = 2;
		autoFire = 0;
		modes[] = {"manual","close","short","medium","far"};
		class GunParticles{};
		class manual: Mode_SemiAuto
		{
			displayName = "AAT Cannon";
			sounds[] = {"StandardSound"};
			class StandardSound
			{
				begin1[] = {"swlb_core\data\sounds\vehicles\aat\weapon\aat_main_shot.wss",3.1622777,1,1500};
				begin2[] = {"swlb_core\data\sounds\vehicles\aat\weapon\aat_main_shot.wss",3.1622777,1,1500};
				soundBegin[] = {"begin1",0.1,"begin2",0.5};
				class SoundTails
				{
					class TailForest
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_forest",1,1,2200};
						frequency = 1;
						volume = "(1-interior/1.4)*forest";
					};
					class TailHouses
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_houses",1,1,2200};
						frequency = 1;
						volume = "(1-interior/1.4)*houses";
					};
					class TailInterior
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_interior",1.99526,1,2200};
						frequency = 1;
						volume = "interior";
					};
					class TailMeadows
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_meadows",1,1,2200};
						frequency = 1;
						volume = "(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailTrees
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_trees",1,1,2200};
						frequency = 1;
						volume = "(1-interior/1.4)*trees";
					};
				};
			};
			recoil = "Empty";
			reloadTime = 2;
			soundBurst = 0;
			dispersion = 0.006;
			showToPlayer = 1;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 150;
			midRangeProbab = 0.7;
			maxRange = 450;
			maxRangeProbab = 0.3;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 150;
		};
		class close: manual
		{
			showToPlayer = 0;
			soundBurst = 0;
			soundContinuous = 0;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 500;
			aiRateOfFireDispersion = 1;
			minRange = 0;
			minRangeProbab = 0.05;
			midRange = 100;
			midRangeProbab = 0.58;
			maxRange = 300;
			maxRangeProbab = 0.3;
		};
		class short: close
		{
			minRange = 150;
			minRangeProbab = 0.2;
			midRange = 500;
			midRangeProbab = 0.7;
			maxRange = 1500;
			maxRangeProbab = 0.4;
			aiRateOfFire = 3;
			aiRateOfFireDistance = 500;
			aiRateOfFireDispersion = 2;
		};
		class medium: close
		{
			minRange = 250;
			minRangeProbab = 0.2;
			midRange = 750;
			midRangeProbab = 0.7;
			maxRange = 1000;
			maxRangeProbab = 0.4;
			aiRateOfFire = 5;
			aiRateOfFireDistance = 700;
			aiRateOfFireDispersion = 3;
		};
		class far: close
		{
			minRange = 500;
			minRangeProbab = 0.2;
			midRange = 1200;
			midRangeProbab = 0.7;
			maxRange = 2100;
			maxRangeProbab = 0.3;
			aiRateOfFire = 7;
			aiRateOfFireDistance = 1000;
			aiRateOfFireDispersion = 3;
		};
	};

	class macro_new_weapon(aat_cannon,king) : 3AS_HeavyAATCannon
	{
		displayName = "Heavy Laser Cannon";
		author= "RD501";

		magazines[] = {
			macro_new_mag(aat_mbt,10)
		};
		fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};		
		ballisticsComputer = 4;
		canLock = 2;
		weaponLockDelay = 3.5;
		weaponLockSystem = 8;
		ace_overpressure_angle = 30;
		ace_overpressure_range = 10;
		ace_overpressure_damage = 0.1;

		magazineReloadTime = 6;
		modes[] = {"manual","close","short","medium","far"};
		class manual: Mode_SemiAuto
		{
			sounds[] = {"StandardSound"};
			class BaseSoundModeType;
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"\kobra\442_weapons\sounds\cannon\cannon10.wss",3.1622777,1,1500};
				soundBegin[] = {"begin1",1};
				volume = "(1-interior/1.4)*trees";
				class SoundTails
				{
					class TailForest
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_forest",1,1,2200};
						frequency = 1;
						volume = "(1-interior/1.4)*forest";
					};
					class TailHouses
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_houses",1,1,2200};
						frequency = 1;
						volume = "(1-interior/1.4)*houses";
					};
					class TailInterior
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_interior",1.99526,1,2200};
						frequency = 1;
						volume = "interior";
					};
					class TailMeadows
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_meadows",1,1,2200};
						frequency = 1;
						volume = "(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailTrees
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_trees",1,1,2200};
						frequency = 1;
						volume = "(1-interior/1.4)*trees";
					};
				};
			};
			recoil = "Empty";
			reloadTime = 2;
			soundBurst = 0;
			dispersion = 0.006;
			showToPlayer = 1;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 150;
			midRangeProbab = 0.7;
			maxRange = 450;
			maxRangeProbab = 0.3;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 150;
		};
		class close: manual
		{
			showToPlayer = 0;
			soundBurst = 0;
			soundContinuous = 0;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 500;
			aiRateOfFireDispersion = 1;
			minRange = 0;
			minRangeProbab = 0.05;
			midRange = 100;
			midRangeProbab = 0.58;
			maxRange = 300;
			maxRangeProbab = 0.3;
		};
		class short: close
		{
			minRange = 150;
			minRangeProbab = 0.2;
			midRange = 500;
			midRangeProbab = 0.7;
			maxRange = 1500;
			maxRangeProbab = 0.4;
			aiRateOfFire = 3;
			aiRateOfFireDistance = 500;
			aiRateOfFireDispersion = 2;
		};
		class medium: close
		{
			minRange = 250;
			minRangeProbab = 0.2;
			midRange = 750;
			midRangeProbab = 0.7;
			maxRange = 1000;
			maxRangeProbab = 0.4;
			aiRateOfFire = 5;
			aiRateOfFireDistance = 700;
			aiRateOfFireDispersion = 3;
		};
		class far: close
		{
			minRange = 500;
			minRangeProbab = 0.2;
			midRange = 1200;
			midRangeProbab = 0.7;
			maxRange = 2100;
			maxRangeProbab = 0.3;
			aiRateOfFire = 7;
			aiRateOfFireDistance = 1000;
			aiRateOfFireDispersion = 3;
		};
	};
	
	class RD501_mynock_cannon_mbt : macro_new_weapon(aat_cannon,king)
	{
		
		displayName = "Primary Mass Driver";
		author= "RD501";
		fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};		
		magazines[] = 
		{
			macro_new_mag(mynock_mbt_ap,12),
			macro_new_mag(mynock_mbt_he,12)
		};

		ballisticsComputer="2 + 16";
		canLock = 2;
		weaponLockDelay = 3.5;
		weaponLockSystem = 8;
		ace_overpressure_angle = 30;
		ace_overpressure_range = 10;
		ace_overpressure_damage = 0.1;

		magazineReloadTime = 6;

		modes[] = {"manual","close"};
		class manual: manual
		{
			recoil = "empty";
			weaponSoundEffect = "DefaultRifle";
			soundContinuous = 0;
			reloadTime = 3;
			magazineReloadTime = 6;
			autoReload = 1;
			ballisticsComputer = 1;
			canLock = 2;
			autoFire = 0;
			dispersion = 0;
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"\RD501_Vehicle_Weapons\_sounds\atap\at_ap_commander.wss",2.51189,1,1500};
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"\RD501_Vehicle_Weapons\_sounds\atap\at_ap_commander.wss",2.51189,1,1500};
				soundBegin[] = {"begin1",1};
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
		};
		class close: manual
		{
			showToPlayer = 0;
			aiRateOfFire = .5;
			aiRateOfFireDistance = 10000;
			minRange = 0;
			minRangeProbab = 0.35;
			midRange = 500;
			midRangeProbab = 0.78;
			maxRange = 10000;
			maxRangeProbab = 1.0;
			aiDispersionCoefX = 2;
			aiDispersionCoefY = 2;
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"\RD501_Vehicle_Weapons\_sounds\atap\at_ap_commander.wss",2.51189,1,1500};
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"\RD501_Vehicle_Weapons\_sounds\atap\at_ap_commander.wss",2.51189,1,1500};
				soundBegin[] = {"begin1",1};
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
		};
	};

	class RD501_mynock_cannon_td : macro_new_weapon(aat_cannon,king)
	{
		
		displayName = "Quantum Accelerator";
		author= "RD501";
		magazines[] =
		{
			macro_new_mag(mynock_mbt_td,12),
		};
		ballisticsComputer="2 + 16";
		canLock = 2;
		weaponLockDelay = 3.5;
		weaponLockSystem = 8;
		ace_overpressure_angle = 360;
		ace_overpressure_range = 10;
		ace_overpressure_damage = 100;

		magazineReloadTime = 6;

		modes[] = {"manual","close"};
		class manual: manual
		{
			recoil = "empty";
			weaponSoundEffect = "DefaultRifle";
			soundContinuous = 0;
			reloadTime = 12;
			magazineReloadTime = 12;
			autoReload = 1;
			ballisticsComputer = 1;
			canLock = 2;
			autoFire = 0;
			dispersion = 0;
		};
		class close: manual
		{
			showToPlayer = 0;
			aiRateOfFire = .5;
			aiRateOfFireDistance = 10000;
			minRange = 0;
			minRangeProbab = 0.35;
			midRange = 500;
			midRangeProbab = 0.78;
			maxRange = 10000;
			maxRangeProbab = 1.0;
			aiDispersionCoefX = 2;
			aiDispersionCoefY = 2;
		};
	};
	class RD501_AAT_Repeater: 3AS_AAT_Repeater
	{
		displayName = "AAT Point Defense Blaster";
		class manual: manual
		{
			displayName = "Heavy Repeater";
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				closure1[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",0.316228,1,20};
				closure2[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",0.316228,1,20};
				soundClosure[] = {"closure1",0.5,"closure2",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",1,1,500};
				soundBegin[] = {"begin1",1};
			};
			reloadTime = 0.17;
			dispersion = 0.0115;
		};
		class close: close
		{
			displayName = "Heavy Repeater";
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				closure1[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",0.316228,1,20};
				closure2[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",0.316228,1,20};
				soundClosure[] = {"closure1",0.5,"closure2",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",1,1,500};
				soundBegin[] = {"begin1",1};
			};
			reloadTime = 0.17;
			dispersion = 0.0115;
		};
		class short: short
		{
			displayName = "Heavy Repeater";
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				closure1[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",0.316228,1,20};
				closure2[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",0.316228,1,20};
				soundClosure[] = {"closure1",0.5,"closure2",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",1,1,500};
				soundBegin[] = {"begin1",1};
			};
			reloadTime = 0.17;
			dispersion = 0.0115;
		};
		class medium: medium
		{
			displayName = "Heavy Repeater";
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				closure1[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",0.316228,1,20};
				closure2[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",0.316228,1,20};
				soundClosure[] = {"closure1",0.5,"closure2",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",1,1,500};
				soundBegin[] = {"begin1",1};
			};
			reloadTime = 0.17;
			dispersion = 0.0115;
		};
		class far: far
		{
			displayName = "Heavy Repeater";
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				closure1[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",0.316228,1,20};
				closure2[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",0.316228,1,20};
				soundClosure[] = {"closure1",0.5,"closure2",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"lsd_sounds\vehicles\laat\DC-15R.wss",1,1,500};
				soundBegin[] = {"begin1",1};
			};
			reloadTime = 0.17;
			dispersion = 0.0115;
		};
		magazines[] = 
		{
			macro_new_mag(CIS_eweb_cell,250)
		};
	};
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////Turrets//////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	class RD501_repeater_weapon:3AS_ParticleCannon
	{
		displayName = "Dual Repeater Cannons";
		fireLightDiffuse[] = {1,0,0};
		fireLightAmbient[] = {1,0,0};
		magazines[]=
		{
			"RD501_CIS_Repeater_Mag_24"
		};
	};
	class RD501_proton_cannon_weapon:RD501_mynock_cannon_mbt
	{
		displayName = "Proton Cannon";
		fireLightDiffuse[] = {1,0,0};
		fireLightAmbient[] = {1,0,0};
		magazines[]=
		{
			"RD501_CIS_Proton_Mag_ap_12"
		};
		class manual
		{
			displayName = "Proton Cannon";
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"\kobra\442_weapons\sounds\cannon\cannon10.wss",2.51189,1,1500};
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"\kobra\442_weapons\sounds\cannon\cannon10.wss",2.51189,1,1500};
				soundBegin[] = {"begin1",1};
				volume = "(1-interior/1.4)*trees";

			};
			recoil = "Empty";
			reloadTime = 1;
			soundBurst = 0;
			dispersion = 0.006;
			showToPlayer = 1;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 150;
			midRangeProbab = 0.7;
			maxRange = 450;
			maxRangeProbab = 0.3;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 150;
		};
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////Walkers//////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	class RD501_atap_top_turret: 3AS_ATAP_AT_Turret
	{
		displayName = "Light Mass Driver Cannon";
		displayNameMagazine = "Light MD Shells";
		fireLightDiffuse[] = {0,0,1};
		fireLightAmbient[] = {0,0,1};	
		magazines[] = {"RD501_15rnd_atap_top_mag"};
		class manual: manual
		{
			displayName = "Light";
			magazineReloadTime = 5;
			reloadTime = 2;
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"\RD501_Vehicle_Weapons\_sounds\atap\at_ap_commander.wss",2.51189,1,1500};
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"\RD501_Vehicle_Weapons\_sounds\atap\at_ap_commander.wss",2.51189,1,1500};
				soundBegin[] = {"begin1",1};
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
		};
	};
	class RD501_atap_heavy_turret : macro_new_weapon(mynock_cannon,mbt)
	{
		
		displayName = "Heavy Mass Driver Cannon";
		author= "RD501";
		fireLightDiffuse[] = {0,0,1};
		fireLightAmbient[] = {0,0,1};	
		magazines[] = 
		{
			macro_new_mag(mynock_mbt_ap,12),
			macro_new_mag(mynock_mbt_he,12)
		};
		ballisticsComputer = 4;
		canLock = 2;
		weaponLockDelay = 3.5;
		weaponLockSystem = 8;
		ace_overpressure_angle = 30;
		ace_overpressure_range = 10;
		ace_overpressure_damage = 0.1;
		magazineReloadTime = 6;
		modes[] = {"manual","close"};
		class manual: manual
		{
			displayName = "Heavy";
			recoil = "empty";
			weaponSoundEffect = "DefaultRifle";
			soundContinuous = 0;
			reloadTime = 3;
			magazineReloadTime = 6;
			autoReload = 1;
			ballisticsComputer = 1;
			canLock = 2;
			autoFire = 0;
			dispersion = 0;
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"\RD501_Vehicle_Weapons\_sounds\atap\at_ap_gunner.wss",2.51189,1,1500};
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"\RD501_Vehicle_Weapons\_sounds\atap\at_ap_gunner.wss",2.51189,1,1500};
				soundBegin[] = {"begin1",1};
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
		};
		class close: manual
		{
			showToPlayer = 0;
			aiRateOfFire = .5;
			aiRateOfFireDistance = 10000;
			minRange = 0;
			minRangeProbab = 0.35;
			midRange = 500;
			midRangeProbab = 0.78;
			maxRange = 10000;
			maxRangeProbab = 1.0;
			aiDispersionCoefX = 2;
			aiDispersionCoefY = 2;
		};
	};
}; 

