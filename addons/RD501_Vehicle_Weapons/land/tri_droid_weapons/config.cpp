
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_dsd_weapons
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons,
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			macro_new_weapon(otco,cannon)
		};
	};
};
class CfgWeapons
{
	class MGun;
	class LMG_RCWS: MGun
	{
		class manual: MGun{};
	};
    class macro_new_weapon(otco,cannon): LMG_RCWS
	{
		displayName = "80mw Tri-Droid Heavy Cannon";
		ace_overpressure_angle = 45;
		ace_overpressure_range = 10;
		ace_overpressure_damage = 0.25;
		fireLightDiffuse[] = {1,0,0};
		fireLightAmbient[] = {1,0,0};	
		magazines[] = 
        {
            macro_new_mag(octo_cannon_charge,50)
        };
		modes[] = {"manual","close","short","medium","far"};
		magazineReloadTime = 3.3333;
		class manual
		{
			displayName = "HH-10G";
			textureType = "fullAuto";
			sounds[] = {"StandardSound"};
			reloadTime = 3.3333333;
			class StandardSound
			{
				begin1[] = {"kobra\442_weapons\sounds\cannon\cannon12.wss",10.25,1.75,5500};
				soundBegin[] = {"begin1",1};
			};
			autoFire = 1;
			soundContinuous = 0;
			burst = 1;
			soundBurst = 0;
			multiplier = 1;
			dispersion = 0.00290888;
			aiRateOfFire = 1;
			showToPlayer = 1;
			aiRateOfFireDistance = 10;
			minRange = 0;
			minRangeProbab = 0.01;
			midRange = 0;
			midRangeProbab = 1;
			maxRange = 150;
			maxRangeProbab = 1;
		};
		class close: manual
		{
			soundBurst = 0;
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 1;
			burstRangeMax = 1;
			aiRateOfFire = 0.1;
			aiRateOfFireDispersion = 0;
			aiRateOfFireDistance = 500;
			minRange = 0;
			minRangeProbab = 0.88;
			midRange = 0;
			midRangeProbab = 0.9;
			maxRange = 750;
			maxRangeProbab = 1;
		};
		class short: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 1;
			burstRangeMax = 1;
			aiRateOfFire = 0.1;
			aiRateOfFireDispersion = 0;
			aiRateOfFireDistance = 750;
			minRange = 0;
			minRangeProbab = 0.9;
			midRange = 0;
			midRangeProbab = 0.9;
			maxRange = 2000;
			maxRangeProbab = 0.9;
		};
		class medium: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 1;
			burstRangeMax = 1;
			aiRateOfFire = 0.1;
			aiRateOfFireDispersion = 0;
			aiRateOfFireDistance = 1300;
			minRange = 0;
			minRangeProbab = 0.9;
			midRange = 3000;
			midRangeProbab = 0.9;
			maxRange = 3750;
			maxRangeProbab = 0.9;
		};
		class far: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 1;
			burstRangeMax = 1;
			aiRateOfFire = 0.1;
			aiRateOfFireDispersion = 0;
			aiRateOfFireDistance = 2500;
			minRange = 0;
			minRangeProbab = 0.98;
			midRange = 4750;
			midRangeProbab = 0.98;
			maxRange = 5250;
			maxRangeProbab = 0.9;
		};
	};
};

