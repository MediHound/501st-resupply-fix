#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_resupply
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons,
            "A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={

		};
	};
};

class CfgWeapons
{
    class GBU12BombLauncher;

    class macro_new_weapon_nofam(guided_resupply_pod_launcher): GBU12BombLauncher
    {
        displayName = "Supply Pod";
        descriptionShort = "Guided Resupply Pod Launcher";

        // Magazines are hard coded due to scripts - do
        // not use macros for these names!

        magazines[]={
            "RD501_Guided_Resupply_Ammo_Resupply_Magazine", 
            "RD501_Guided_Resupply_Medical_Resupply_Magazine"
        };

        class EventHandlers
        {
            fired = "_this remoteExec ['RD501_fnc_GURE_grplFired', 2];";
        };
    };
};