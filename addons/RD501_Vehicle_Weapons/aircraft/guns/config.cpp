#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_aircraft_weapons_guns
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			RD501_Republic_Aircraft_Laser_Cannon,
			RD501_Republic_Aircraft_Laser_Repeater,
			RD501_Republic_Aircraft_Laser_AA,
			RD501_Republic_Aircraft_Laser_Turbo,
			RD501_CIS_Laser_AA
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class CfgWeapons
{
	class MGun;
	class CannonCore;

	class SmokeLauncher;
	class RD501_CMFlareLauncher: SmokeLauncher
	{
		scope=2;
		displayName="CM Single";
		magazines[]=
		{
			"RD501_CMFlare_Magazine_200"
		};
		magazineReloadTime=30;
		simulation="cmlauncher";
		modes[]=
		{
			"Single",
			"SBurst",
			"MBurst",
			"LBurst",
			"AIBurst"
		};
		class Single: Mode_SemiAuto
		{
			displayName="CM Single";
			reloadTime=0.05;
			burst=1;
			sounds[]=
			{
				"StandardSound"
			};
			class StandardSound
			{
				begin1[]=
				{
					"A3\Sounds_F\weapons\HMG\HMG_grenade",
					1,
					1,
					300
				};
				soundBegin[]=
				{
					"begin1",
					1
				};
			};
			showToPlayer=1;
			multiplier=1;
			soundBurst=0;
			dispersion=0.40000001;
			aiRateOfFire=1;
			aiRateOfFireDistance=10;
			minRange=0;
			minRangeProbab=0.0099999998;
			midRange=1;
			midRangeProbab=0.0099999998;
			maxRange=2;
			maxRangeProbab=0.0099999998;
		};
		class SBurst: Mode_Burst
		{
			displayName="CM 10x";
			reloadTime=0.1;
			burst=10;
			sounds[]=
			{
				"StandardSound"
			};
			class StandardSound
			{
				begin1[]=
				{
					"A3\Sounds_F\weapons\HMG\HMG_grenade",
					1,
					1,
					300
				};
				soundBegin[]=
				{
					"begin1",
					1
				};
			};
			showToPlayer=1;
			multiplier=1;
			soundBurst=0;
			dispersion=0.40000001;
			aiRateOfFire=1;
			aiRateOfFireDistance=10;
			minRange=0;
			minRangeProbab=0.0099999998;
			midRange=1;
			midRangeProbab=0.0099999998;
			maxRange=2;
			maxRangeProbab=0.0099999998;
		};
		class MBurst : SBurst
		{
			displayName="CM 20x";
			burst = 20;
		};
		class LBurst : SBurst
		{
			displayName="CM 40x";
			burst = 40;
		};
		class AIBurst: SBurst
		{
			burst=10;
			burstRangeMax=7;
			showToPlayer=0;
			soundBurst=0;
			minRange=100;
			minRangeProbab=1;
			midRange=1000;
			midRangeProbab=1;
			maxRange=25000;
			maxRangeProbab=1;
		};
	};

	class RD501_CannonCore
	{
		//config stuff
		scope = 1;
		type = 65536;

		//ammo and mag relation
		magazines[] = 
		{
			"",
		};
		displayName = "";
		descriptionShort = "";
		
		initSpeed = 0; //normally defined in mag this is used to overwrite
		lockAcquire = 1;
		magazineReloadTime = 0;

		showEmpty = 1;
		model = "";
		modelSpecial = "";
		modes[] = 
		{
			"LowROF",
			
		};

		//muzzle stuff
		muzzleEnd = "konec hlavne";
		muzzlePos = "usti hlavne";
		muzzles[] = {"this"};
		nameSound = "cannon";
		
		//used for artillery shells
		artilleryCharge = 1;
		artilleryDispersion = 1;

		//fire controls
		autoReload = 1;
		backgroundReload = 1;
		showToPlayer = 1;

		cmImmunity = 1;
		ballisticsComputer = 8;
		FCSMaxLeadSpeed = 75;
		canLock = 2;
		
		weaponLockDelay = 0;
		weaponLockSystem = 0;

		class LowROF
		{
			displayName = "Laser Cannon";
			autoFire = 1;
			showToPlayer = 1;

			reloadtime = 0.4;
			dispersion = 0.0001;
			burst = 1;
			burstRangeMax = 1;
			multiplier = 1;

			aiRateOfFire = 1;
			aiRateOfFireDispersion = 1;
			aiRateOfFireDistance = 200;		

			minRange = 25;
			minRangeProbab = 0.4;
			midRange = 500;
			midRangeProbab = 0.8;
			maxRange = 1000;
			maxRangeProbab = 0.5;

			ffCount = 6;
			ffFrequency = 11;
			ffMagnitude = 0.5;

			flash = "gunfire";
			flashSize = 0.1;
			recoil = "Empty";
			
			soundContinuous = 0;
			sounds = ["StandardSound"];
			class StandardSound {
				begin1[] = {"A3\Sounds_F\arsenal\weapons_vehicles\gatling_30mm\30mm_01_burst",5.62341,1,1500,[25704,32159]};
				soundBegin = ["begin1",1];
			};
		};

		//custom cursors outside hud stuff
		cursor = "EmptyCursor";
		cursorAim = "mg";
		cursorAimOn = "";
		cursorSize = 1;

		distanceZoomMax = 400;
		distanceZoomMin = 400;
		enableAttack = 1;

		//animations
		fireAnims = [];
		fireLightAmbient = [0,0,0];
		fireLightDiffuse = [0.937,0.631,0.259];
		fireLightDuration = 0.05;
		fireLightIntensity = 0.2;
		fireSpreadAngle = 3;
		forceOptics = 0;

		class GunClouds {};
		class GunFire {};
		class GunParticles
		{
			class Effect 
			{
				directionName = "Gatling_barrels_start";
				effectName = "MachineGun3";
				positionName = "Gatling_barrels_end";
			};
		};

		lockedTargetSound = ["",0.000316228,6];
		lockingTargetSound = ["",0.000316228,2];
		
		//optic stuff
		optics = 1;
		opticsDisablePeripherialVision = 0.67;
		opticsFlare = 1;
		opticsID = 0;
		opticsPPEffects = [];
		opticsZoomInit = 0.75;
		opticsZoomMax = 1.25;
		opticsZoomMin = 0.25;

		//stuff
		picture = "";
		primary = 10;
		reloadAction = "";
		reloadMagazineSound = ["",1,1];
		reloadSound = ["",1,1];
		canDrop = 1;
		canShootInWater = 0;
		cartridgePos = "nabojnicestart";
		cartridgeVel = "nabojniceend";
		changeFiremodeSound = ["",1,1];
		count = 0; //not used in arma
		selectionFireAnim = "Gatling_muzzleflash";
		shotFromTurret = 0;
		showAimCursorInternal = 0;
		shownUnderwaterSelections[] = {};
		showSwitchAction = 0;
		simulation = "Weapon";
		swayDecaySpeed = 2;
		textureType = "default";
		uiPicture = "";
		useAsBinocular = 0;
		useModelOptics = 0;
		modelOptics = "";
		value = 2;
		weaponSoundEffect = "";
		weight = 0;

		//random shit
		zeroingSound[] = {"",1,1};
		detectRange = 0;
		handAnim[] = {};
		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};
		hiddenUnderwaterSelections[] = {};
		hiddenUnderwaterSelectionsTextures[] = {};
		holdsterAnimValue = 1;
		inertia = 0.5;
		irDistance = 0;
		irDotIntensity = 0.001;
		irLaserEnd = "laser dir";
		irLaserPos = "laser pos";
		laser = 0;
		class Library {
			libTextDesc = "";
		};
		magazineReloadSwitchPhase = 1;
		maxRecoilSway = 0.008;
		memoryPointCamera = "eye";
	};
	
	class SmokeLauncherMK2: MGun
	{
		scope=2;
		displayName="SmokeLauncher MK2";
		sounds[]=
		{
			"StandardSound"
		};
		class StandardSound
		{
			soundSetShot[]=
			{
				"UGL_shot_SoundSet",
				"UGL_Tail_SoundSet",
				"UGL_InteriorTail_SoundSet"
			};
		};
		magazines[]=
		{
			"SmokeLauncherMagMK2",
		};
		reloadTime=3;
		magazineReloadTime=60;
		canLock=0;
		autoFire=0;
		simulation="cmlauncher";
		showToPlayer=1;
		minRange=0;
		maxRange=10000;
		textureType="semi";
	};
	//laat cannon
	class RD501_Aircraft_Laser_Cannon : RD501_CannonCore
	{
		magazines[] = 
		{
			"RD501_Republic_Aircraft_Laser_Cannon_Mag_100",
		};
		displayName = "Laser Cannon";
		lockAcquire = 1;
		magazineReloadTime = 0;
		autoReload = 1;
		backgroundReload = 1;
		cmImmunity = 1;
		ballisticsComputer = 8;
		FCSMaxLeadSpeed = 75;
		canLock = 2;
		modes[] = 
		{
			"LowROF"			
		};
		class LowROF
		{
			displayName = "Laser Cannon";
			autoFire = 1;
			showToPlayer = 1;

			reloadtime = 0.4;
			dispersion = 0.0;
			burst = 1;
			burstRangeMax = 1;
			multiplier = 1;

			aiRateOfFire = 1;
			aiRateOfFireDispersion = 1;
			aiRateOfFireDistance = 200;		

			minRange = 25;
			minRangeProbab = 0.4;
			midRange = 500;
			midRangeProbab = 0.8;
			maxRange = 1000;
			maxRangeProbab = 0.5;

			ffCount = 6;
			ffFrequency = 11;
			ffMagnitude = 0.5;

			flash = "gunfire";
			flashSize = 0.1;
			recoil = "Empty";
			
			soundContinuous = 0;
			sounds[] = {"StandardSound"};
			class StandardSound 
			{
				begin1[] = {"RD501_Vehicle_Weapons\_sounds\laser_cannon_shot.ogg",5.62341,1,1500,{25704,32159}};
				soundBegin[] = {"begin1",1};
			};
		};

	};


	class RD501_Republic_Aircraft_Laser_Blaster : RD501_CannonCore
	{
		magazines[] = 
		{
			"RD501_Republic_Aircraft_Laser_Blaster_Mag_75",
		};
		displayName = "Laser Blaster";
		lockAcquire = 0;
		magazineReloadTime = 25;
		autoReload = 1;
		backgroundReload = 1;
		cmImmunity = 1;
		ballisticsComputer = 8;
		FCSMaxLeadSpeed = 75;
		canLock = 0;
		modes[] = 
		{
			"LowROF"			
		};
		class LowROF
		{
			displayName = "Laser Blaster";
			autoFire = 1;
			showToPlayer = 1;

			reloadtime = 0.1;
			dispersion = 0.0;
			burst = 1;
			burstRangeMax = 1;
			multiplier = 1;

			aiRateOfFire = 1;
			aiRateOfFireDispersion = 1;
			aiRateOfFireDistance = 200;		

			minRange = 25;
			minRangeProbab = 0.4;
			midRange = 300;
			midRangeProbab = 0.8;
			maxRange = 600;
			maxRangeProbab = 0.5;

			ffCount = 6;
			ffFrequency = 11;
			ffMagnitude = 0.5;

			flash = "gunfire";
			flashSize = 0.1;
			recoil = "Empty";
			
			soundContinuous = 0;
			sounds[] = {"StandardSound"};
			class StandardSound 
			{
				begin1[] = {"RD501_Vehicle_Weapons\_sounds\laser_cannon\laser_cannon_shot.ogg",5.62341,1,1500,{25704,32159}};
				soundBegin[] = {"begin1",1};
			};
		};

	};

	class RD501_CIS_AA_Laser_Blaster : RD501_Republic_Aircraft_Laser_Blaster
	{
		magazines[] = 
		{
			"RD501_CIS_AA_Laser_Blaster_Mag_75"
		};
		magazineReloadTime = 45;
		modes[] = 
		{
			"LowROF"			
		};
		class LowROF
		{
			displayName = "Laser Blaster";
			autoFire = 1;
			showToPlayer = 1;

			reloadtime = 0.35;
			dispersion = 0.0;
			burst = 5;
			burstRangeMax = 3;
			multiplier = 1;

			aiRateOfFire = 1;
			aiRateOfFireDispersion = 1;
			aiRateOfFireDistance = 500;		

			minRange = 25;
			minRangeProbab = 0.4;
			midRange = 300;
			midRangeProbab = 0.8;
			maxRange = 600;
			maxRangeProbab = 0.5;

			ffCount = 6;
			ffFrequency = 11;
			ffMagnitude = 0.5;

			flash = "gunfire";
			flashSize = 0.1;
			recoil = "Empty";
			
			soundContinuous = 0;
			sounds[] = {"StandardSound"};
			class StandardSound 
			{
				begin1[] = {"RD501_Vehicle_Weapons\_sounds\laser_cannon\laser_cannon_shot.ogg",5.62341,1,1500,{25704,32159}};
				soundBegin[] = {"begin1",1};
			};
		};
		
	};

	//Coaxium
	class RD501_Republic_Aircraft_Laser_Turbo : RD501_CannonCore
	{

		magazines[] = 
		{
			"RD501_Republic_Aircraft_Laser_Turbo_Mag_15",
		};
		displayName = "Coaxium";
		lockAcquire = 1;
		magazineReloadTime = 0;
		autoReload = 1;
		backgroundReload = 1;
		cmImmunity = 1;
		ballisticsComputer = 8;
		FCSMaxLeadSpeed = 75;
		canLock = 2;
		modes[] = 
		{
			"LowROF"
			
		};
		class LowROF
		{
			displayName = "Coaxium";
			autoFire = 0;
			showToPlayer = 1;

			reloadtime = 0.4;
			dispersion = 0;
			burst = 1;
			burstRangeMax = 1;
			multiplier = 1;

			aiRateOfFire = 1;
			aiRateOfFireDispersion = 1;
			aiRateOfFireDistance = 200;		

			minRange = 25;
			minRangeProbab = 0.4;
			midRange = 500;
			midRangeProbab = 0.8;
			maxRange = 1000;
			maxRangeProbab = 0.5;

			ffCount = 6;
			ffFrequency = 11;
			ffMagnitude = 0.5;

			flash = "gunfire";
			flashSize = 0.1;
			recoil = "Empty";
			
			soundContinuous = 0;
			sounds[] = {"StandardSound"};
			class BaseSoundModeType;
			class StandardSound : BaseSoundModeType
			{
				begin1[] = {"RD501_Vehicle_Weapons\_sounds\coax_test_1.wav",5.62341,1,1500,{25704,32159}};
				soundBegin[] = {"begin1",1};
			};
		};
	};

	//voltic
	class RD501_Republic_Aircraft_Laser_Repeater : RD501_CannonCore
	{

		magazines[] = 
		{
			"RD501_Republic_Aircraft_Laser_Repeater_Mag_1000"
		};
		displayName = "Voltic";
		lockAcquire = 1;
		magazineReloadTime = 0;
		autoReload = 1;
		backgroundReload = 1;
		cmImmunity = 1;
		ballisticsComputer = 1;
		FCSMaxLeadSpeed = 75;
		canLock = 2;
		modes[] = 
		{
			"LowROF"			
		};
		class LowROF
		{
			displayName = "Voltic";
			autoFire = 1;
			showToPlayer = 1;

			reloadtime = 0.04;
			dispersion = 0.005;
			burst = 1;
			burstRangeMax = 1;
			multiplier = 1;

			aiRateOfFire = 1;
			aiRateOfFireDispersion = 1;
			aiRateOfFireDistance = 200;		

			minRange = 25;
			minRangeProbab = 0.4;
			midRange = 500;
			midRangeProbab = 0.8;
			maxRange = 1000;
			maxRangeProbab = 0.5;

			ffCount = 6;
			ffFrequency = 11;
			ffMagnitude = 0.5;

			flash = "gunfire";
			flashSize = 0.1;
			recoil = "Empty";
			
			soundContinuous = 0;
			sounds[] = {"StandardSound"};
			class StandardSound 
			{
				begin1[] = {"RD501_Vehicle_Weapons\_sounds\Kannon\Kannon_Trail_1.ogg",5.62341,1,1500,{25704,32159}};
				soundBegin[] = {"begin1",1};
			};
		};
	};

	class RD501_Republic_Aircraft_Laser_Repeater_Pod : RD501_Republic_Aircraft_Laser_Repeater
	{
		displayName="Voltic Gunpod";
		magazines[]=
		{
			"RD501_Republic_Aircraft_Laser_Repeater_Pod_Mag_500",
		};
		class LowROF: LowROF {
			dispersion=.01;
		};
	};

	//air supriority
	class RD501_Republic_Aircraft_Laser_AA : RD501_Republic_Aircraft_Laser_Repeater
	{
		magazines[] = 
		{
			"RD501_Republic_Aircraft_Laser_AA_Mag_600",
		};
		nameSound="cannon";
		cursor="EmptyCursor";
		cursorAim="mg";
		displayName = "Air Superiority laser";
		type=65536;
		scope=1;
		lockAcquire = 1;
		magazineReloadTime = 0;
		autoReload = 1;
		backgroundReload = 1;
		cmImmunity = 1;
		ballisticsComputer = "1 + 4";
		FCSMaxLeadSpeed = 500;
		canLock = 2;
		modes[] = 
		{
			"manual",
			"close",
			"medium",
			"far"
			
		};

		class manual
		{
			textureType="fullAuto";

			displayName = "Air Superiority laser";
			autoFire = 1;
			showToPlayer = 1;

			reloadtime = 0.04;
			dispersion = 0.0003;
			burst = 1;
			burstRangeMax = 1;
			multiplier = 1;

			aiRateOfFire = 0;
			aiRateOfFireDispersion = 1;
			aiRateOfFireDistance = 50;		

			minRange=1;
			minRangeProbab=0.0099999998;
			midRange=2;
			midRangeProbab=0.0099999998;
			maxRange=3;
			maxRangeProbab=0.0099999998;

			ffCount = 6;
			ffFrequency = 11;
			ffMagnitude = 0.5;

			flash = "gunfire";
			flashSize = 0.1;
			recoil = "Empty";
			
			soundContinuous = 0;
			sounds[] = {"StandardSound"};
			class StandardSound 
			{
				begin1[] = {"RD501_Vehicle_Weapons\_sounds\Kannon\Kannon_1.ogg",5.62341,1,1500,{25704,32159}};
				soundBegin[] = {"begin1",1};
			};
		};
		class close: manual
		{
			showToPlayer=0;
			burst=20;
			burstRangeMax=60;
			soundBurst=0;
			aiBurstTerminable=0;
			aiRateOfFire=0;
			aiRateOfFireDistance=1;
			aiRateOfFireDispersion=0;
			minRange=0;
			minRangeProbab=1;
			midRange=400;
			midRangeProbab=0.89999998;
			maxRange=600;
			maxRangeProbab=0.89999998;
		};
		class medium: close
		{
			burst=50;
			burstRangeMax=100;
			aiBurstTerminable=1;
			aiRateOfFire=0.75;
			aiRateOfFireDistance=1;
			minRange=600;
			minRangeProbab=0.85000002;
			midRange=700;
			midRangeProbab=0.82999998;
			maxRange=900;
			maxRangeProbab=0.5;
		};
	};

	class RD501_CIS_Aircraft_Laser_AA : RD501_Republic_Aircraft_Laser_AA
	{
		magazines[] = {"RD501_CIS_Aircraft_Laser_AA_Mag_600"};
	};


	class RD501_CIS_Laser_AA : CannonCore
	{

		type=65536;
		scope=1;
		displayName="SHORAD";
		nameSound="cannon";
		cursor="EmptyCursor";
		cursorAim="mg";
		FCSMaxLeadSpeed = 400;
		magazines[]=
		{
			"RD501_CIS_Laster_AA_Mag"
		};
		canLock=0;
		ballisticsComputer=4;
		modes[]=
		{
			"manual",
			"close",
			"medium",
			"far"
		};
		class GunParticles
		{
			class FirstEffect
			{
				effectName="MachineGun1";
				positionName="pos_fx";
				directionName="pos_fx_dir";
			};
			class SecondEffect
			{
				effectName="MachineGun1";
				positionName="pos_fx";
				directionName="pos_fx_dir";
			};
		};
		class manual
		{
			displayName="$STR_A3_Phalanx_weapon_name";
			autoFire=1;
			multiplier=1;
			sounds[]=
			{
				"StandardSound"
			};
			class StandardSound
			{
				begin1[]=
				{
					"\A3\Sounds_F_Jets\weapons\cannons\Fighter_Gun20mm_AA_1",
					2,
					1,
					3000
				};
				begin2[]=
				{
					"\A3\Sounds_F_Jets\weapons\cannons\Fighter_Gun20mm_AA_2",
					2,
					1,
					3000
				};
				begin3[]=
				{
					"\A3\Sounds_F_Jets\weapons\cannons\Fighter_Gun20mm_AA_3",
					2,
					1,
					3000
				};
				soundBegin[]=
				{
					"begin1",
					0.33000001,
					"begin2",
					0.33000001,
					"begin3",
					0.34
				};
			};
			weaponSoundEffect="DefaultRifle";
			reloadTime=0.013;
			dispersion=0.0044999998;
			soundContinuous=0;
			showToPlayer=1;
			burst=1;
			aiRateOfFire=0;
			aiRateOfFireDistance=50;
			minRange=1;
			minRangeProbab=0.0099999998;
			midRange=2;
			midRangeProbab=0.0099999998;
			maxRange=3;
			maxRangeProbab=0.0099999998;
			textureType="fullAuto";
		};
		class close: manual
		{
			showToPlayer=0;
			burst=100;
			soundBurst=0;
			aiBurstTerminable=1;
			aiRateOfFire=0;
			aiRateOfFireDistance=1;
			AIRateOfFireDispersion=0;
			minRange=0;
			minRangeProbab=0.94999999;
			midRange=600;
			midRangeProbab=0.89999998;
			maxRange=900;
			maxRangeProbab=0.89999998;
		};
		class medium: close
		{
			burst=50;
			burstRangeMax=100;
			aiBurstTerminable=1;
			aiRateOfFire=0.75;
			aiRateOfFireDistance=1;
			minRange=600;
			minRangeProbab=0.85000002;
			midRange=900;
			midRangeProbab=0.82999998;
			maxRange=1500;
			maxRangeProbab=0.5;
		};
		class far: close
		{
			burst=30;
			burstRangeMax=90;
			aiBurstTerminable=1;
			aiRateOfFire=1.5;
			aiRateOfFireDistance=1;
			minRange=900;
			minRangeProbab=0.82999998;
			midRange=1500;
			midRangeProbab=0.80000001;
			maxRange=3000;
			maxRangeProbab=0.1;
		};
	};

}; 

