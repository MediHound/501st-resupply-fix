
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_bombs
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			RD501_Republic_Aircraft_Bomb_Laser_Glide,
			RD501_Republic_Aircraft_Bomb_Dumb
		};
	};
};

class CfgWeapons
{
	class weapon_SDBLauncher;	
	class Mk82BombLauncher;

	//wraith
	class RD501_Republic_Aircraft_Bomb_Laser_Glide : weapon_SDBLauncher
	{
		
		displayName = "Wraith Small Diameter Bomb";
		displayNameShort  = "Wraith";
		magazines[] = {"RD501_Republic_Aircraft_Bomb_Laser_Glide_Mag_4"};
		salvo = 1;
		canLock = 2;
		magazineReloadTime = 5;
		ballisticsComputer=8;
	};

	class RD501_Republic_Aircraft_Bomb_Dumb : Mk82BombLauncher
	{
		displayName = "Slagger Unguided Bomb";
		displayNameShort  = "Slagger";
		magazines[] = {"RD501_Republic_Aircraft_Bomb_Dumb_Mag_20"};
		canLock = 0;
		salvo = 5;
		autoFire=1;
		reloadTime=0.25;
		magazineReloadTime = 5;
		ballisticsComputer = 8;	
	};
}; 

