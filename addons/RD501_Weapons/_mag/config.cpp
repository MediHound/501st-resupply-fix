#include "../../RD501_main/config_macros.hpp"

class CfgPatches
{
    class RD501_patch_magazines
    {
        author=RD501;
        addonRootClass=MACRO_QUOTE(RD501_patch_weapons);
        requiredAddons[]=
        {
            RD501_patch_weapons
        };
        requiredVersion=0.1;
        units[]={};
        weapons[]={};
    };
};

class CfgMagazines
{
	class 3AS_45Rnd_EC50_Mag;
	class 1Rnd_HE_Grenade_shell;
	class JLTS_stun_mag_long;
	class 1Rnd_Smoke_Grenade_shell;
	class UGL_FlareWhite_F;
	class 3AS_ThermalDetonator;
	class 3AS_BaridumCore;
	class 3AS_ThrowableCharge;
	class 2Rnd_12Gauge_Pellets;
	class SmokeShell;
	class 442_thermal_det_mag;
	class JLTS_E5_mag;
	class DBA_B2_Rocket;
	class DBA_B2_Mag;
	class JLTS_e5c_mag;
	////////////////////////////////////////////////////
	/////////////Rifle/Pistol Magazines/////////////////
	////////////////////////////////////////////////////

	class macro_new_mag(30mw,15) : 3AS_45Rnd_EC50_Mag
	{
		displayName="15 Round E5S Cell";
		count = 15;
		ammo=MACRO_QUOTE(macro_new_ammo(30mw_d));
		initSpeed=1000;
		descriptionShort="High power magazine";
	};
	class macro_new_mag(40mwemp,1) : 3AS_45Rnd_EC50_Mag
	{
		displayName="1 Round 40MW EMP Cell";
		count = 1;
		ammo=MACRO_QUOTE(macro_new_ammo(40mwemp));
		initSpeed=1100;
		picture="\RD501_Weapons\_mag\data\rd501_icon_mag_dc15a_20mw.paa";
		descriptionShort="DC15X EMP Round";
		model = "\MRC\JLTS\weapons\DC15x\DC15x_mag.p3d";
	};
	class macro_new_mag(50mw,8) : 3AS_45Rnd_EC50_Mag
	{
		displayName="8 Round 50MW Cell";
		count = 8;
		mass=4;
		ammo=MACRO_QUOTE(macro_new_ammo(50mw));
		initSpeed=1100;
		descriptionShort="High power magazine";
	};
	class macro_new_mag(Devastator,50) : 3AS_45Rnd_EC50_Mag
	{
		displayName="Devastator Primary Magazine";
		count = 50;
		mass=4;
		ammo=MACRO_QUOTE(macro_new_ammo(devastator_small));
		initSpeed=1100;
		descriptionShort="High power magazine";
	};
	class macro_new_mag(Devastator,2) : 3AS_45Rnd_EC50_Mag
	{
		displayName="Devastator High Energy Magazine";
		count = 2;
		mass=4;
		ammo=MACRO_QUOTE(macro_new_ammo(devastator_large));
		initSpeed=1100;
		descriptionShort="High power magazine";
	};
	class macro_new_mag(viper,5) : 3AS_45Rnd_EC50_Mag
	{
		displayName="Viper Magazine";
		count = 5;
		mass=4;
		ammo=MACRO_QUOTE(macro_new_ammo(viper));
		initSpeed=1100;
		descriptionShort="High power magazine";
	};
	class macro_new_mag(stun,5):JLTS_stun_mag_long
	{
		displayName="5 Round Stun Cell";
		descriptionShort = "5 Round Stun Cell";
		displayNameShort = "5 Round Stun Cell";
		count=5;
		picture="\MRC\JLTS\weapons\Core\data\ui\stun_mag_ui_ca";
		ammo=MACRO_QUOTE(macro_new_ammo(surrender_stun));
	};
	class macro_new_mag(r2_charge_pack,10):3AS_45Rnd_EC50_Mag
	{
		displayName="R2 Charge Pack";
		descriptionShort = "Charge Pack";
		displayNameShort = "Charge Pack";
		count=10;
		picture="\RD501_Weapons\_mag\data\rd501_icon_mag_battery.paa";
		ammo=MACRO_QUOTE(macro_new_ammo(40mw));
	};

	////////////////////////////////////////////////////
	/////////////////Mando Magazines////////////////////
	////////////////////////////////////////////////////
	class macro_new_mag(westar,35) : JLTS_E5_mag
	{
		displayName="35 Round Mando Cell";
		count = 35;
		picture="\MRC\JLTS\weapons\E5\data\ui\E5_mag_ui_ca.paa";
		ammo=MACRO_QUOTE(macro_new_ammo(Mando_standard));
		descriptionShort="Medium Mando magazine";
	};
	class macro_new_mag(westar,60) : JLTS_e5c_mag
	{
		displayName="60 Round Mando Cell";
		count = 60;
		picture="\MRC\JLTS\weapons\e5c\data\ui\e5c_mag_ui_ca.paa";
		ammo=MACRO_QUOTE(macro_new_ammo(Mando_standard));
		descriptionShort="Hi-cap Medium Mando magazine";
	};
	class macro_new_mag(westar,15) : JLTS_E5_mag
	{
		displayName="15 Round Mando Sniper Cell";
		count = 15;
		ammo=MACRO_QUOTE(macro_new_ammo(Mando_heavy));
		initSpeed=700;
		descriptionShort="High power mando magazine";
	};
	////////////////////////////////////////////////////
	/////////Grenade Launcher Magazines/////////////////
	////////////////////////////////////////////////////
	class macro_new_mag(UGL_EMP,1) : 1Rnd_HE_Grenade_shell
	{
		ammo = "RD501_grenade_emp_ammo";
		count = 1;
		descriptionShort = "1Rnd EMP DC-15A Grenade";
		displayName = "1 Rnd EMP DC-15A Grenades";
		displayNameShort = "1Rnd EMP DC-15A";
		picture = "\MRC\JLTS\weapons\grenades\data\ui\grenade_emp_ui_ca.paa";
		mass = 50;
		maxLeadSpeed = 25;
		maxThrowHoldTime = 2;
		maxThrowIntensityCoef = 1.4;
		minThrowIntensityCoef = 0.3;
		modelSpecial = "";
		nameSound = "";
		quickReload = 0 ;
		reloadAction = "";
		scope = 2;
		initSpeed = 220;
		selectionFireAnim = "zasleh";
		simulation = "ProxyMagazines";
		type = 16;
		useAction = 0;
		useActionTitle = "";
		value = 1;
		weaponpoolavailable = 1;
		weight = 0;
		class InventoryPlacements;
		class Library 
		{
			libTextDesc = "";
		};
	};
	class macro_new_mag(Devastator_stun,1):1Rnd_HE_Grenade_shell
	{
		displayName = "1 Rd Devastator Stun Magazine";
		displayNameShort = "1 Rnd Devastator Stun Magazine";
		count=1;
		ammo = MACRO_QUOTE(macro_new_ammo(Devastator_stun));
		descriptionShort = "1Rd Devastator Stun Magazine";
	};
	class macro_new_mag(Devastator_dioxis,1):1Rnd_Smoke_Grenade_shell
	{
		displayName = "1 Rd Devastator Dioxis Magazine";
		displayNameShort = "1 Rnd Devastator Dioxis Magazine";
		count=1;
		ammo = MACRO_QUOTE(macro_new_ammo(Devastator_dioxis));
		descriptionShort = "1Rd Devastator Dioxis Magazine";
	};

	////////////////////////////////////////////////////
	/////////////Grenades Magazines/////////////////////
	////////////////////////////////////////////////////
	class macro_new_mag(impacter,1):3AS_BaridumCore
	{
		author="RD501";
		scope=2;
		displayName = "Republic Thermal Impacter";
		displayNameShort = "Thermal Impacter";
		count=1;
		mass = 8;
		ammo = MACRO_QUOTE(macro_new_ammo(impacter));
		model = "ls_weapons\grenades\thermalDetN20\ls_grenade_thermalDet_n20";
		descriptionShort = "Clone Impact Grenade";
		picture = "\OPTRE_Weapons\explosives\icons\m9.paa";
	};
	class macro_new_mag(throwable_c_type,1):3AS_ThrowableCharge
	{
		author="RD501";
		scope=2;
		displayName = "Republic Throwable C-Type";
		displayNameShort = "Throwable C-Type";
		count=1;
		initSpeed=12;
		ammo = MACRO_QUOTE(macro_new_ammo(throwable_c_type));
		descriptionShort = "Clone Satchel Charge";
	};
	class macro_new_mag(thermaldet,1):442_thermal_det_mag
	{
		author="RD501";
		scope=2;
		displayName = "Republic Thermal Detonator";
		displayNameShort = "Thermal Detonator";
		count=1;
		mass = 8;
		ammo = MACRO_QUOTE(macro_new_ammo(thermaldet));
		descriptionShort = "Clone Frag Grenade";
		picture ="\RD501_Weapons\Grenades\data\rep_thermaldet_ui.paa";
	};
	class ls_mag_n20_thermalDet;
	class macro_new_mag(imploder,1):ls_mag_n20_thermalDet
	{
		author="RD501";
		scope=2;
		displayName = "Republic Thermal Imploder";
		displayNameShort = "Thermal Imploder";
		count=1;
		mass = 8;
		ammo = MACRO_QUOTE(macro_new_ammo(imploder));
		descriptionShort = "Clone Grenade";
		picture = "\SWLW_clones\grenades\beltgrenade\data\ui\icon_beltGrenade.paa";
	};
	class macro_new_mag(thermaldet_cis,1):442_thermal_det_mag
	{
		author="RD501";
		scope=2;
		displayName = "CIS Thermal Detonator";
		count=1;
		ammo = MACRO_QUOTE(macro_new_ammo(thermaldet_cis));
		descriptionShort = "Droid Frag Grenade";
		picture ="\RD501_Weapons\Grenades\data\cis_thermaldet_ui.paa";
	};
	class macro_new_mag(squad_shield_sigma,1):3AS_BaridumCore
	{
		author="RD501";
		scope=2;
		displayName = "Republic Squad Shield Sigma";
		displayNameShort = "Squad Shield";
		count=1;
		ammo = MACRO_QUOTE(macro_new_ammo(squad_shield_sigma));
		descriptionShort = "Sigma Squad Shield";
		model = "3as\3as_shield\SquadShield_Throwable.p3d";
		picture="\RD501_Weapons\_mag\data\rd501_icon_mag_imploder.paa";
	};
	class macro_new_mag(squad_shield_alpha,1):3AS_BaridumCore
	{
		author="RD501";
		scope=2;
		displayName = "Republic Squad Shield Alpha";
		displayNameShort = "Trench Shield";
		count=1;
		ammo = MACRO_QUOTE(macro_new_ammo(squad_shield_alpha));
		descriptionShort = "Alpha Shield";
		model = "3as\3as_shield\SquadShield_Throwable.p3d";
		picture="\RD501_Weapons\_mag\data\rd501_icon_mag_imploder.paa";
	};
	class macro_new_mag(personal_shield,1):3AS_BaridumCore
	{
		author="RD501";
		scope=2;
		displayName = "Republic Personal Shield";
		displayNameShort = "Personal Shield";
		count=1;
		ammo = MACRO_QUOTE(macro_new_ammo(personal_shield));
		descriptionShort = "Personal Shield";
		picture="\RD501_Weapons\_mag\data\rd501_icon_mag_imploder.paa";
	};
	class macro_new_mag(super_smoke,1):SmokeShell
	{
		author="RD501";
		scope=2;
		displayName = "Republic J00-L VapeNation";
		displayNameShort = "VapeNation";
		simulation = "shotSmokeX";
		count=1;
		ammo=MACRO_QUOTE(macro_new_ammo(super_smoke));
		model = "212th\Weapons\212th_Grenades\data\HM-1.p3d";
		picture = "\212th\Weapons\212th_Grenades\data\icons\HM-1BaseIcon.paa";
	};
	class macro_new_mag(dioxis,1):SmokeShell
	{
		author="RD501";
		scope=2;
		displayName = "Dioxis Grenade";
		displayNameShort = "Dioxis";
		count=1;
		ammo=MACRO_QUOTE(macro_new_ammo(dioxis));
	};
	class macro_new_mag(blueshadow,1):SmokeShell
	{
		author="RD501";
		scope=2;
		displayName = "Blue Shadow Grenade";
		displayNameShort = "Blue Shadow";
		count=1;
		ammo=MACRO_QUOTE(macro_new_ammo(blueshadow));
	};
	class RD501_MashRequestSmoke_Mag : SmokeShell
	{
		author="RD501";
		scope=2;
		displayName="Republic MASH Request Beacon";
		displayNameShort="MASH Beacon";
		count=1;
		ammo="RD501_MashRequestSmoke_Ammo";
	};
	////////////////////////////////////////////////////
	/////////////Missile Magazines//////////////////////
	////////////////////////////////////////////////////
	class RPG32_F;
	class RPG32_HE_F;
	class RD501_E60R_AT: RPG32_F
	{
		author="RD501";
		displayName="[CIS] E-60R AT Rocket";
		descriptionShort="CIS AT Rocket";
		ammo="RD501_e60r_at_ammo";
	};
	class RD501_E60R_HE: RPG32_HE_F
	{
		author="RD501";
		displayName="[CIS] E-60R HE Rocket";
		descriptionShort="CIS HE Rocket";
		ammo="RD501_e60r_he_ammo";
	};

	////////////////////////////////////////////////////
	/////////////Flare    Magazines/////////////////////
	////////////////////////////////////////////////////
};