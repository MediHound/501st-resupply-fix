#include "../../RD501_main/config_macros.hpp"

class CfgPatches
{
    class RD501_patch_ammo
    {
        author=RD501;
        addonRootClass = MACRO_QUOTE(RD501_patch_weapons);
        requiredAddons[]=
        {
            RD501_patch_weapons
        };
        requiredVersion=0.1;
        units[]={};
        weapons[]={};
    };
};

class CfgAmmo
{
	class 3AS_EC20_BluePlasma;
	class 3AS_EC30_BluePlasma;
	class 3AS_EC40_BluePlasma;
	class 3AS_EC50_BluePlasma;
	class 3AS_EC60_BluePlasma;
	class 3AS_EC70_BluePlasma;
	class 3AS_EC80_BluePlasma;
	class JLTS_bullet_carbine_red;
	class G_40mm_HE;
	class G_40mm_Smoke;
	class 3AS_CoreDetonator_1RND;
	class 3AS_Detonator_1RND;
	class B_12Gauge_Pellets_Submunition;
	class B_12Gauge_Pellets_Submunition_Deploy;
	class FlareBase;
	class SmokeShell;
	class SmokeShellOrange;
	class JLTS_bullet_stun;
	class 442_thermal_det;
	class JLTS_bullet_carbine_orange;
	class JLTS_bullet_carbine_yellow;
	class JLTS_bullet_carbine_blue;
	class 212th_HM1_SmokeWhiteAmmo;
	class FlameRound;

	class macro_new_ammo(30mw_d) : JLTS_bullet_carbine_red
	{
		hit=25;
		airLock=1;
		typicalSpeed=1000;
		caliber=2.4;
		airFriction=0;
	};
	class macro_new_ammo(40mwemp) : JLTS_bullet_carbine_blue
	{
		hit=0.01;
		airLock=1;
		typicalSpeed=1100;
		caliber=1;
		airFriction=0;
		explosive = 0;
		rd501_emp_vehicle_enabled=1;
		rd501_emp_vehicle_duration=15;
		model = "SWLW_main\Effects\laser_blue.p3d";
		tracerscale = 1.5;
	};
	class macro_new_ammo(mar1) : 3AS_EC80_BluePlasma
	{
		hit=1000;
		thrust=500;
		explosive=0;
		thrustTime=1.5;
		airLock=1;
		typicalSpeed=3000;
		caliber=50;
		airFriction=0;
		model="\RD501_Vehicle_Weapons\_ammo\data\laser_medium_blue.p3d";
	};
	class macro_new_ammo(devastator_small):JLTS_bullet_carbine_orange
	{
		hit=25;
		airLock=1;
		typicalSpeed=1000;
		caliber=2.4;
		airFriction=0;
		waterFriction=-0.009;
		explosive = 0;
	};
	class macro_new_ammo(devastator_large):JLTS_bullet_carbine_orange
	{
		cost=50;
		caliber=3;
		dangerRadiusBulletClose=16;
		dangerRadiusHit=40;
		explosionAngle=60;
		explosive=0.80000001;
		hit=100;
		effectFlare="FlareShell";
		effectsFire="CannonFire";
		explosionDir="explosionDir";
		explosionEffects="ATMissileExplosion";
		explosionEffectsDir="explosionDir";
		explosionPos="explosionPos";
		explosionType="explosive";
		indirectHit=5;
		indirectHitRange=3;
		muzzleEffect="BIS_fnc_effectFiredRocket";
		tracerScale=3;
	};
	class macro_new_ammo(viper) : 3AS_EC80_BluePlasma
	{
		hit=100;
		thrust=210;
		explosive=0.4;
		thrustTime=1.5;
		airLock=1;
		typicalSpeed=1100;
		caliber=5;
		airFriction=0;
		tracersevery=1;
		tracerScale=1;
		tracerStartTime=0;
		tracerEndTime=10;
		model = "\MRC\JLTS\weapons\Core\effects\laser_orange.p3d";
	};
///////////////////////////////////////////////////////////////////////
///////////////////////////Mando Ammo////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
	class macro_new_ammo(Mando_standard): JLTS_bullet_carbine_yellow
	{
		model = "SWLW_main\Effects\laser_yellow.p3d";
		tracerscale = 1.5;
	};
	class macro_new_ammo(Mando_heavy): JLTS_bullet_carbine_yellow
	{
		model = "SWLW_main\Effects\laser_yellow.p3d";
		tracerscale = 1.5;
		hit=35;
		airLock=1;
		typicalSpeed=1000;
		caliber=2.4;
		airFriction=0;
	};
///////////////////////////////////////////////////////////////////////
///////////////////////////UGL////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
	class RD501_grenade_emp_ammo: G_40mm_HE
    {
		JLTS_isEMPAmmo=1;
		hit = 0.1;
		indirectHit = 0.01;
		indirectHitRange = 12;
		timeToLive =30;
		model = "\MRC\JLTS\weapons\Core\effects\emp_blue.p3d";
		fuseDistance = 1;
		explosive = 0.0001;
		deflecting = 5;
		caliber = 5;
		ace_frag_enabled = 0;
		ace_frag_force = 0;
		ace_frag_classes[] = {""};
		ace_frag_metal = 0;
		ace_frag_charge = 0;
		ace_frag_gurney_c = 0;
		ace_frag_gurney_k = "0";
		ace_rearm_caliber = 0;
		explosionEffects="JLTS_fx_exp_EMP";
		grenadeBurningSound[]=
		{
			"EMPSoundLoop1",
			0.5
		};
		EMPSoundLoop1[]=
		{
			"MRC\JLTS\weapons\Grenades\sounds\grenade_burning.wss",
			0.125893,
			1,
				70
			};
		SoundSetExplosion[]=
		{
			"JLTS_GrenadeEMP_Exp_SoundSet",
			"JLTS_GrenadeEMP_Tail_SoundSet",
			"Explosion_Debris_SoundSet"
		};
		aiAmmoUsageFlags=0;
		class CamShakeExplode
		{
			distance=10;
			duration=1;
			frequency=20;
			power=0;
		};
    };
	class macro_new_ammo(UGL_Supersmoke):G_40mm_Smoke
	{
		hit = 2;
		indirectHit = 1;
		indirectHitRange = 1;
		timeToLive =30;
		model = "212th\Weapons\212th_Grenades\data\HM-1.p3d";
		effectsSmoke="RD501_ThickSmokeShellWhiteEffect";
	};
	class macro_new_ammo(Devastator_dioxis) :G_40mm_Smoke
	{
		hit = 2;
		indirectHit = 1;
		indirectHitRange = 1;
		timeToLive =30;
		model="\3AS\3AS_Equipment\model\3AS_thermaldet.p3d";
		effectsSmoke="RD501_DioxisSmokeShellEffect";
		smokeColor[]={0.21250001,0.75580001,0.35909998,1};
	};
	class macro_new_ammo(Devastator_stun) :G_40mm_HE
	{
		hit = 1;
		indirectHit = 1;
		indirectHitRange = 6;
		timeToLive =30;
		explosionEffects="JLTS_fx_exp_EMP";
		model="\3AS\3AS_Equipment\model\3AS_thermaldet.p3d";
		RD501_stunDuration=30;
	};
///////////////////////////////////////////////////////////////////////
///////////////////////////Grenades///////////////////////////////////
//////////////////////////////////////////////////////////////////////

	class grenade;
	class Imploder_Exp_SoundSet
	{
		soundShaders[]=
		{
			RD501_Imploder_SoundShader
		};
		volumeFactor=1;
		volumeCurve="InverseSquare2Curve";
		spatial=1;
		doppler=0;
		loop=0;
		sound3DProcessingType="ExplosionLight3DProcessingType";
		distanceFilter="explosionDistanceFreqAttenuationFilter";
	};
	class macro_new_ammo(impacter) : 3AS_CoreDetonator_1RND
	{
		hit=50;
		indirectHit=45;
		indirectHitRange=2;
		fuseDistance=0.5;
		explosionTime=0;
		simulation="shotShell";
		model = "kobra\442_weapons\explosive\impact_grenade.p3d";
		soundFly[] = {"kobra\442_weapons\sounds\impact_grenade\impact_grenade_cook.wss",1.5,1,90};
	};
	class macro_new_ammo(throwable_c_type) : 3AS_CoreDetonator_1RND
	{
		hit=1500;
		indirectHit=1500;
		indirectHitRange=10;
		typicalspeed=40;
		explosionTime=15;
		timeToLive=20;
		model = "\A3\Weapons_F\Explosives\satchel";
		simulation = "shotGrenade";
		ExplosionEffects="MineNondirectionalExplosion";
		CraterEffects="MineNondirectionalCrater";
	};
	class macro_new_ammo(thermaldet) : 3AS_Detonator_1RND
	{
		hit=18;
		indirectHit=14;
		indirectHitRange=8;
		model = "kobra\442_weapons\explosive\thermal_det.p3d";
	};
	class macro_new_ammo(imploder) : Grenade
	{
		displayName = "Republic Thermal Imploder";
		ace_advanced_throwing_torqueDirection[] = {1,1,0};
		ace_advanced_throwing_torqueMagnitude = "(50 + random 100) * selectRandom [1, -1]";
		hit = 9;
		indirectHit= 14;
		indirectHitRange = 2;
		simulation = "shotGrenade";
		soundFly[] = {"DBA_Republic\Addons\DBA_Explosives\data\ThermalImploder\thermalimploderFly.ogg",1.5,1,90};
		multiSoundHit[] = {"soundHit1",0.33,"soundHit2",0.33,"soundHit3",0.33};
		soundHit1[] = {"DBA_Republic\Addons\DBA_Explosives\data\ThermalImploder\imploderExplode1.ogg",2,1,2000};
		soundHit2[] = {"DBA_Republic\Addons\DBA_Explosives\data\ThermalImploder\imploderExplode2.ogg",2,1,2000};
		soundHit3[] = {"DBA_Republic\Addons\DBA_Explosives\data\ThermalImploder\imploderExplode3.ogg",2,1,2000};
		SoundSetExplosion = ["Imploder_Exp_SoundSet"];
		ace_frag_enabled = 1;
		ace_frag_skip = 0;
		ace_frag_force = 1;
		ace_frag_classes[] = {"ace_frag_tiny_HD"};
		ace_frag_metal = 210;
		ace_frag_charge = 185;
		ace_frag_gurney_c = 2843;
		ace_frag_gurney_k = "3/5";
		model = "ls_weapons\grenades\thermalDetN20\ls_grenade_thermalDet_n20";
	};

	class macro_new_ammo(thermaldet_cis) : 442_thermal_det
	{
		hit=10;
		indirectHit=8;
		indirectHitRange=6;
		cost=80;
		model = "ls_weapons\grenades\thermalDet\ls_grenade_thermaldet";

	};
	class macro_new_ammo(squad_shield_sigma) : 3AS_CoreDetonator_1RND
	{
		hit=0;
		indirectHit=0;
		indirectHitRange=0;
		model = "3as\3as_shield\SquadShield_Throwable.p3d";
		rd501_fired_deployable = 1;
        rd501_fired_deployable_object = "RD501_Squad_Shield";
        rd501_fired_deployable_timeToLive = 60;
		simulation="shotShell";
	};
	class macro_new_ammo(squad_shield_alpha) : 3AS_CoreDetonator_1RND
	{
		hit=0;
		indirectHit=0;
		indirectHitRange=0;
		model="\RD501_Weapons\Grenades\data\icecream.p3d";
		rd501_fired_deployable = 1;
        rd501_fired_deployable_object = "RD501_Squad_Shield_trench";
        rd501_fired_deployable_timeToLive = 120;
		simulation="shotShell";
	};
	class macro_new_ammo(personal_shield) : 3AS_CoreDetonator_1RND
	{
		hit=0;
		indirectHit=0;
		indirectHitRange=0;
		model="\RD501_Weapons\Grenades\data\icecream.p3d";
		rd501_fired_deployable = 1;
        rd501_fired_deployable_object = "RD501_Personal_Shield";
        rd501_fired_deployable_timeToLive = 120;
        rd501_fired_deployable_personal = 1;
        rd501_fired_deployable_personal_offset[] = {0.1, 0.9, 0.4};
        rd501_fired_deployable_personal_bone = "pelvis";
		simulation="shotShell";
	};
	class macro_new_ammo(super_smoke) : 212th_HM1_SmokeWhiteAmmo
	{
		effectsSmoke="RD501_ThickSmokeShellWhiteEffect";
		model = "212th\Weapons\212th_Grenades\data\HM-1.p3d";
		simulation = "shotSmokeX";
	};
	class macro_new_ammo(dioxis) : SmokeShell
	{
		effectsSmoke="RD501_DioxisSmokeShellEffect";
	};
	class macro_new_ammo(blueshadow) : SmokeShell
	{
		effectsSmoke="RD501_DioxisBlueSmokeShellEffect";
	};
	class RD501_MashRequestSmoke_Ammo : SmokeShellOrange 
	{
		rd501_fired_script_enabled = 1;
		rd501_fired_script = "_this call RD501_fnc_internal_CRML_launchMissileFromMashGrenade;";
	};
	
//////////////////////////////////////////////////////////////////////
/////////////////////////Flamethrower/////////////////////////////////
//////////////////////////////////////////////////////////////////////
	class RD501_flamethrower_ammo: FlameRound
	{
		submunitionAmmo = "FlameRound_sub";
		submunitionConeAngle = 1;
		submunitionConeType[] = {"poissondisc",1};
	};
	
//////////////////////////////////////////////////////////////////////
/////////////////////////Flares///////////////////////////////////////
//////////////////////////////////////////////////////////////////////
    class macro_new_ammo(40mm_white): FlareBase
	{
		model = "\A3\Weapons_F\Ammo\UGL_Flare";
	
		lightColor[] = {1, 1, 1 ,1};
		useFlare = 1;
		deflecting = 30;
		smokeColor[] = {1, 1, 1, 1};

		brightness = 1200;
		size = 3;//1
		triggerTime = 3;
		triggerSpeedCoef = 1;
		audibleFire = 20;
		
		intensity  = 1000000;
		flareSize = 3;//1
		timeToLive = 120;
        flareMaxDistance = 300;
		
	};


	class macro_new_ammo(40mm_green): macro_new_ammo(40mm_white)
	{
		model = "\A3\Weapons_F\Ammo\UGL_Flare";
		
		lightColor[] = {0, 1, 0, 0};
	};
	class macro_new_ammo(40mm_red): macro_new_ammo(40mm_white)
	{
		model = "\A3\Weapons_F\Ammo\UGL_Flare";
		
		lightColor[] = {1, 0, 0, 0};
	};
	class macro_new_ammo(40mm_yellow): macro_new_ammo(40mm_white)
	{
		model = "\A3\Weapons_F\Ammo\UGL_Flare";
		
		lightColor[] = {1, 1, 0, 0};
	};
	class macro_new_ammo(40mm_CIR): macro_new_ammo(40mm_white)
	{
		model = "\A3\Weapons_F\Ammo\UGL_Flare";
		lightColor[] = {0.5, 0.5, 0.25, 0};
	};

	class macro_new_ammo(40mm_blue): macro_new_ammo(40mm_white)
	{
		model = "\A3\Weapons_F\Ammo\UGL_Flare";
	
		lightColor[] = {0, 0, 1, 0};
	};

	class macro_new_ammo(40mm_cyan): macro_new_ammo(40mm_white)
	{
		model = "\A3\Weapons_F\Ammo\UGL_Flare";
	
		lightColor[] = {0, 1, 1, 0};
	};

	class macro_new_ammo(40mm_purple): macro_new_ammo(40mm_white)
	{
		model = "\A3\Weapons_F\Ammo\UGL_Flare";
		
		lightColor[] = {.7, 0, 1, 0};
	};
///////////////////////////////////////////////////////////////////////
/////////////////////////Rockets///////////////////////////////////////
//////////////////////////////////////////////////////////////////////
	class ammo_Penetrator_RPG32V;
	class ACE_Javelin_FGM148_static;
	class R_PG32V_F;
	class Mo_cluster_AP;
	class R_TBG32V_F;
	/*class macro_new_ammo(striker):ACE_Javelin_FGM148_static
	{
		hit=1650;
		effectsMissileInit="RocketBackEffectsStaticRPG";
		initTime=0.1;
		irLock = 1;
        laserLock = 0;
        airLock = 0;
		class ace_missileguidance {
            enabled = 1;

            minDeflection = 0.00005;      // Minium flap deflection for guidance
            maxDeflection = 0.025;       // Maximum flap deflection for guidance
            incDeflection = 0.00005;      // The incrmeent in which deflection adjusts.

            canVanillaLock = 0;

            // Guidance type for munitions
            defaultSeekerType = "Optic";
            seekerTypes[] = { "Optic" };

            defaultSeekerLockMode = "LOBL";
            seekerLockModes[] = { "LOBL" };

            seekerAngle = 180;           // Angle in front of the missile which can be searched
            seekerAccuracy = 1;         // seeker accuracy multiplier

            seekerMinRange = 0;
            seekerMaxRange = 2500;      // Range from the missile which the seeker can visually search

            seekLastTargetPos = 1;      // seek last target position [if seeker loses LOS of target, continue to last known pos]

            // Attack profile type selection
            defaultAttackProfile = "JAV_TOP";
            attackProfiles[] = { "JAV_TOP", "JAV_DIR" };
            useModeForAttackProfile = 1;
        };
	};*/
	class macro_new_ammo(rps4heat):R_PG32V_F
	{
		hit=150;
		submunitionAmmo="ammo_Penetrator_Titan_AT";
	};
	class macro_new_ammo(rps4emp):R_PG32V_F
	{
		hit=1;
		submunitionAmmo="";
		submunitionDirectionType="";
		CraterEffects="";
		explosionEffects="JLTS_fx_exp_EMP";
		rd501_emp_vehicle_enabled=1;
		rd501_emp_vehicle_duration=25;
	};
	class macro_new_ammo(rps4burst):R_PG32V_F
	{
		triggerTime  = 1;
		submunitionConeType[] = {"poissondisc", 8};
		submunitionAmmo[] = {macro_new_ammo(super_smoke),1};
		triggerOnImpact=0;
		deleteParentWhenTriggered=1;
		//submunitionParentSpeedCoef = 0;
		submunitionInitSpeed = 50;
		submunitionDirectionType = "SubmunitionAutoLeveling";
		
	};
	class macro_new_ammo(rps4burstsub):Mo_cluster_AP
	{
		hit=12;
		indirectHit=8;
		indirectHitRange=8;
	};
	class RD501_e60r_penetrator_ammo: ammo_Penetrator_RPG32V
	{
		hit = 650;
		caliber = 43.3333;
		airFriction = -0.28;
		thrust = 210;
		thrustTime = 1.5;
		typicalSpeed = 1000;
	};
	class RD501_e60r_at_ammo : R_PG32V_F
	{
		soundFly[] = {"swlw_rework\sounds\launcher\E60R_fly.wss",6,1.5,700};
		effectsMissile = "RD501_particle_effect_E60R_fly";
		thrust = 100;
		thrustTime = 5;
		timeToLive = 5;
		submunitionAmmo = "RD501_e60r_penetrator_ammo";
	};
	
	
	class RD501_e60r_he_ammo: R_TBG32V_F
	{
		soundFly[] = {"swlw_rework\sounds\launcher\E60R_fly.wss",6,1.5,700};
		effectsMissile = "RD501_particle_effect_E60R_fly";
		thrust = 100;
		thrustTime = 5;
		timeToLive = 5;
	};
};
class CfgSoundShaders
{
	class RD501_Imploder_SoundShader
	{
		samples[] = 
		{
			{"DBA_Republic\Addons\DBA_Explosives\data\ThermalImploder\imploderExplode1.ogg",2,1,2000},
			{"DBA_Republic\Addons\DBA_Explosives\data\ThermalImploder\imploderExplode2.ogg",2,1,2000},
			{"DBA_Republic\Addons\DBA_Explosives\data\ThermalImploder\imploderExplode3.ogg",2,1,2000}
			
		};
		volume = 1;
		range = 800;
	};
};