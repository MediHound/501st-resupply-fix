#define COMPONENT DC34
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"

class CfgPatches
{
	class RD501_patch_dc_34
	{
		author=DANKAUTHORS;
		addonRootClass = MACRO_QUOTE(RD501_patch_weapons);
		requiredAddons[]=
		{
			"RD501_patch_weapons",
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
        {
			macro_new_weapon(dc,34)
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class SlotInfo;
class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;
class CfgWeapons
{
	class Rifle;
	class Aux327th_DC34: Rifle
	{
		class WeaponSlotsInfo;
		class GunParticles;
	};
	class macro_new_weapon(dc,34): Aux327th_DC34
	{
		displayName = "Mandalorian DC-34";
		picture = "\RD501_Weapons\Mando_Weapons\DC_34\dc34_ui.paa";
		scope = 2;
		scopeArsenal = 2;
		mass = 100;
		ACE_Overheating_mrbs = 0;
		recoil = "3AS_recoil_dc15l";
		dispersion = 0.099999994;
		magazines[] = 
        {
            macro_new_mag(westar,60)
        };
		reloadAction = "GestureReloadMX";
		magazineReloadSwitchPhase = 0.4;
		discreteDistanceInitIndex = 0;
		swayDecaySpeed = 1.25;
		inertia = 0.5;
		dexterity = 1.5;
		initSpeed = -1;
		maxZeroing = 100;
		class GunParticles: GunParticles
		{
			class SecondEffect
			{
				positionName = "Nabojnicestart";
				directionName = "Nabojniceend";
				effectName = "CaselessAmmoCloud";
			};
		};
		opticsZoomMin = 0.25;
		opticsZoomMax = 1.25;
		opticsZoomInit = 0.75;
		distanceZoomMin = 400;
		distanceZoomMax = 400;
		descriptionShort = "DC-34";
		handAnim[] = {"OFP2_ManSkeleton","\3AS\3AS_Weapons\DC15L\Data\Anim\DC15L_handanim.rtm"};
		selectionFireAnim = "zasleh";
		flash = "gunfire";
		flashSize = 2;
		modes[] = {"FullAuto","close","medium","far_optic1","far_optic2"};
		class FullAuto: Mode_FullAuto
		{
			reloadTime = 0.1;
			dispersion = 0.00133;
			minRange = 0;
			minRangeProbab = 0.9;
			midRange = 15;
			midRangeProbab = 0.7;
			maxRange = 30;
			maxRangeProbab = 0.1;
			soundContinuous = 0;
			soundBurst = 0;
            sounds[] = {"StandardSound"};
			class BaseSoundModeType;
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"\ls_sounds\weapons\shot\westar35s_1",1,1,1800};
				begin2[] = {"\ls_sounds\weapons\shot\westar35s_1",1,1,1800};
				begin3[] = {"\ls_sounds\weapons\shot\westar35s_1",1,1,1800};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
			};
			class SilencedSound: BaseSoundModeType
			{
				soundSetShot[] = {""};
			};
		};
		class close: FullAuto
		{
			burst = 10;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
			minRange = 10;
			minRangeProbab = 0.05;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.04;
			showToPlayer = 0;
		};
		class short: close
		{
			burst = 8;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 300;
			minRange = 50;
			minRangeProbab = 0.05;
			midRange = 150;
			midRangeProbab = 0.7;
			maxRange = 300;
			maxRangeProbab = 0.04;
		};
		class medium: close
		{
			burst = 7;
			aiRateOfFire = 4;
			aiRateOfFireDistance = 600;
			minRange = 200;
			minRangeProbab = 0.05;
			midRange = 300;
			midRangeProbab = 0.7;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
		class far_optic1: medium
		{
			requiredOpticType = 1;
			showToPlayer = 0;
			burst = 3;
			aiRateOfFire = 10;
			aiRateOfFireDistance = 1000;
			minRange = 300;
			minRangeProbab = 0.05;
			midRange = 500;
			midRangeProbab = 0.4;
			maxRange = 650;
			maxRangeProbab = 0.01;
		};
		class far_optic2: far_optic1
		{
			burst = 3;
			requiredOpticType = 2;
			minRange = 400;
			minRangeProbab = 0.05;
			midRange = 750;
			midRangeProbab = 0.7;
			maxRange = 900;
			maxRangeProbab = 0.01;
			aiRateOfFire = 10;
			aiRateOfFireDistance = 900;
		};
		aiDispersionCoefY = 6;
		aiDispersionCoefX = 4;
		caseless[] = {"",1,1,1};
		soundBullet[] = {"caseless",1};
		drySound[] = {"\3AS\3AS_Main\Sounds\Blaster_empty",2,1,20};
		reloadMagazineSound[] = {"\3AS\3AS_Main\Sounds\Old\Blaster_reload.wss",1,1,30};
        class WeaponSlotsInfo: WeaponSlotsInfo
		{
			class CowsSlot: CowsSlot
			{
				compatibleItems[] = {"Aux327th_Holosight_blu"};
			};
			class PointerSlot: PointerSlot
			{
				linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
				compatibleItems[] = {"acc_flashlight","acc_pointer_IR"};
			};
			class MuzzleSlot: MuzzleSlot
			{
				linkProxy = "\A3\data_f\proxies\weapon_slots\MUZZLE";
				displayName = "$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
				compatibleItems[] = {"Aux501_muzzle_flash"};
				iconPicture = "\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
				iconPinpoint = "Center";
			};
			class UnderBarrelSlot: UnderBarrelSlot
			{
				linkProxy = "\A3\Data_F_Mark\Proxies\Weapon_Slots\UNDERBARREL";
				compatibleItems[] = {"bipod_01_F_blk"};
				iconPicture = "\A3\Weapons_F_Mark\Data\UI\attachment_under.paa";
				iconPinpoint = "Bottom";
			};
		};
	};
	class macro_new_weapon(dc_scoped,34): macro_new_weapon(dc,34)
	{
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "Aux327th_Holosight_blu";
				slot = "CowsSlot";
			};
			class LinkedItemsMuzzle
			{
				item = "Aux501_muzzle_flash";
				slot = "MuzzleSlot";
			};
			class LinkedItemsUnderBarrel
			{
				item = "bipod_01_F_blk";
				slot = "UnderBarrelSlot";
			};
		};
	};
};