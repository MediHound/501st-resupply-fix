#define COMPONENT Westar_35sa
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_westar_35sa
	{
		author="RD501";
		addonRootClass = MACRO_QUOTE(RD501_patch_weapons);
		requiredAddons[]=
		{
			"RD501_patch_weapons",
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			macro_new_weapon(westar,35sa),
		};
	};
};
class SWLW_Westar35SA;
class CfgWeapons
{	
	class macro_new_weapon(Westar,35sa):SWLW_Westar35SA
	{
		displayName= "Mandalorian Westar 35SA";
		fireLightDiffuse[] = {0.5,0.5,0.25};
		ACE_Overheating_mrbs = 0;
		magazines[]=
		{
			macro_new_mag(westar,35)
		};
	};
};
