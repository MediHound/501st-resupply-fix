#define COMPONENT Westar_35s
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_westar_35s
	{
		author="RD501";
		addonRootClass = MACRO_QUOTE(RD501_patch_weapons);
		requiredAddons[]=
		{
			"RD501_patch_weapons",
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			macro_new_weapon(westar,35s),
		};
	};
};
class SWLW_westar35s;
class Mode_SemiAuto;
class UGL_F;
class CfgWeapons
{	
	class WeaponSlotsInfo;
	class macro_new_weapon(Westar,35s):SWLW_westar35s
	{
		displayName= "Mandalorian Westar 35S";
		dispersion = 0.003015;
		recoil = "SWLW_recoil";
		canShootInWater=1;
		ACE_Overheating_mrbs = 0;
		scope = 2;
		fireLightDiffuse[] = {0.5,0.5,0.25};
		magazines[]=
		{
			macro_new_mag(westar,35)
		};
		class Single: Mode_SemiAuto
		{
			reloadTime = 0.096;
			dispersion = 0.003015;
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",1,1,1800};
				begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",1,1,1800};
				begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",1,1,1800};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
			};
		};
        class Scatter: UGL_F
       {
			magazines[] = {"JLTS_SBB3_mag"};
			displayName = "Scattergun mode";
			displayNameShort = "Scattergun";
			cursor = "";
			useModelOptics = 0;
			useExternalOptic = 1;
			magazineWell[] = {};
			cameraDir = "eye";
			memoryPointCamera = "";
			discreteDistance[] = {100};
			discreteDistanceCameraPoint[] = {"eye"};
			discreteDistanceInitIndex = 0;
			initSpeed = -1;
			muzzleEnd = "shotgun pos";
			muzzlePos = "shotgun dir";
			reloadMagazineSound[] = {"\SWLW_main\sounds\scatter_reload",0.56234133,1,30};
			class Single: Mode_SemiAuto
			{
				sounds[] = {"StandardSound"};
				class BaseSoundModeType
				{
					closure1[] = {"A3\Sounds_F\arsenal\weapons\UGL\Closure_UGL",1,1,10};
					soundClosure[] = {"closure1",1};
				};
				class StandardSound: BaseSoundModeType
				{
					begin1[] = {"\SWLW_main\sounds\scatter",0.70794576,1,200};
					begin2[] = {"\SWLW_main\sounds\scatter",0.70794576,1,200};
					soundBegin[] = {"begin1",0.5,"begin2",0.5};
				};
				recoil = "SWLW_scatter_recoil";
				minRange = 0;
				minRangeProbab = 0.1;
				midRange = 200;
				midRangeProbab = 0.7;
				maxRange = 400;
				maxRangeProbab = 0.05;
			};
		};
	};
	class macro_new_weapon(Westar_scoped,35s): macro_new_weapon(Westar,35s)
	{
		scope = 1;
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "SWLW_Westar35S_scope";
				slot = "CowsSlot";
			};
		};
	};
};
