#define COMPONENT Westar_35c
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_westar_35c
	{
		author="RD501";
		addonRootClass = MACRO_QUOTE(RD501_patch_weapons);
		requiredAddons[]=
		{
			"RD501_patch_weapons",
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			macro_new_weapon(westar,35c),
		};
	};
};
class SWLW_westar35c;
class Mode_SemiAuto;
class Mode_FullAuto;
class CfgWeapons
{	
	class WeaponSlotsInfo;
	class macro_new_weapon(Westar,35c):SWLW_westar35c
	{
		displayName= "Mandalorian Westar 35C";
		dispersion = 0.003015;
		recoil = "SWLW_recoil";
		ACE_Overheating_mrbs = 0;
		canShootInWater=1;
		scope = 2;
		fireLightDiffuse[] = {0.5,0.5,0.25};
		magazines[]=
		{
			macro_new_mag(westar,35)
		};
		class FullAuto: Mode_FullAuto
		{
			reloadTime = 0.15;
			dispersion = 0.003015;
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",1,1,1800};
				begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",1,1,1800};
				begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",1,1,1800};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
			};
		};
		class Single: Mode_SemiAuto
		{
			reloadTime = 0.096;
			dispersion = 0.003015;
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",1,1,1800};
				begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",1,1,1800};
				begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",1,1,1800};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
			};
		};
	};
};
