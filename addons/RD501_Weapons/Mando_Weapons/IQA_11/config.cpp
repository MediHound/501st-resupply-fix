#define COMPONENT IQA_11
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"
class CfgPatches
{
	class RD501_patch_dw32s
	{
		author=DANKAUTHORS;
		addonRootClass = MACRO_QUOTE(RD501_patch_weapons);
		requiredAddons[]=
		{
			"RD501_patch_weapons",
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			macro_new_weapon(iqa,11)
		};
	};
};

class CfgWeapons
{
	class SWLW_rifle_base;
    class SWLW_sniper: SWLW_rifle_base
    {
        class Single;
		class WeaponSlotsInfo
		{
			class SlotInfo;
		};
    };
	class macro_new_weapon(iqa,11) : SWLW_sniper
	{
		scope=2;
		baseWeapon=MACRO_QUOTE(macro_new_weapon(iqa,11));
		picture = "\RD501_Weapons\Mando_Weapons\IQA_11\iqa11_ui.paa";
		model = "LF_Weapon_Unit\hi12\hi12.p3d";
		handAnim[] = {"OFP2_ManSkeleton","LF_Weapon_Unit\hi12\anims\hi12_handanim.rtm"};
		hiddenSelectionsTextures[] = {"LF_Weapon_Unit\hi12\data\camo_co.paa","LF_Weapon_Unit\hi12\data\camo1_co.paa"};
		hiddenSelections[] = {"camo1"};
		displayName= "Mandalorian IQA-11 Sniper Rifle";
		magazines[]={macro_new_mag(westar,15)};
		ACE_Overheating_mrbs = 0;
		fireLightDiffuse[] = {0.5,0.5,0.25};
		fireLightAmbient[] = {0.5,0.5,0.25};
		modes[] = {"Single","single_medium_optics1","single_far_optics2"};
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass = 60;
			class CowsSlot
			{
				compatibleItems[] = 
				{
					""
				};
			};
			class MuzzleSlot
			{
				compatibleItems[] = {"Aux501_muzzle_flash"};
			};
		};			
		class Single:Single
		{
			reloadTime=1.2;
			recoil = "recoil_single_mx";
			recoilProne = "recoil_single_prone_mx";
			dispersion = 0.00073;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 250;
			midRangeProbab = 0.7;
			maxRange = 450;
			maxRangeProbab = 0.3;
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"LF_Weapon_Unit\main\sounds\Westar_1.ogg",1,1,1800};
				soundBegin[] = {"begin1",1,};
			};
		};
		class single_medium_optics1: Single
		{
			dispersion = 0.00073;
			requiredOpticType = 1;
			minRange = 2;
			minRangeProbab = 0.2;
			midRange = 450;
			midRangeProbab = 0.7;
			maxRange = 650;
			maxRangeProbab = 0.2;
			aiRateOfFire = 6;
			aiRateOfFireDistance = 600;
		};
		class single_far_optics2: single_medium_optics1
		{
			dispersion = 0.00073;
			requiredOpticType = 2;
			minRange = 100;
			minRangeProbab = 0.2;
			midRange = 550;
			midRangeProbab = 0.7;
			maxRange = 1200;
			maxRangeProbab = 0.05;
			aiRateOfFire = 8;
			aiRateOfFireDistance = 1200;
		};
		modelOptics = "LF_Weapon_Unit\Amban\Ambanscope.p3d";
		class OpticsModes
		{
			class hi12scope_sights
			{
				opticsID = 1;
				useModelOptics = 0;
				opticsPPEffects[] = {"Default"};
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.25;
				opticsZoomInit = 0.75;
				memoryPointCamera = "eye";
				visionMode[] = {};
				distanceZoomMin = 200;
				distanceZoomMax = 200;
				cameraDir = "";
			};
			class hi12scope_scope: hi12scope_sights
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera2","OpticsBlur3"};
				opticsZoomMin = 0.0625;
				opticsZoomMax = 0.125;
				opticsZoomInit = 0.125;
				discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000,1100,1200};
				discreteDistanceInitIndex = 1;
				distanceZoomMin = 300;
				distanceZoomMax = 1200;
				discretefov[] = {"0.25/3","0.25/6","0.25/9","0.25/12","0.25/15","0.25/20"};
				discreteInitIndex = 0;
				memoryPointCamera = "eye";
				visionMode[] = {"Normal","NVG","TI"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				cameraDir = "";
			};
		};
	};
	class macro_new_weapon(iqa_scoped,11): macro_new_weapon(iqa,11)
	{
		class LinkedItems
		{
			class LinkedItemsMuzzle
			{
				item = "Aux501_muzzle_flash";
				slot = "MuzzleSlot";
			};
		};
	};
};