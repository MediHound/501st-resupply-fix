class CfgPatches
{
	class RD501_patch_RD501_E_60R
	{
		author=DANKAUTHORS;
		addonRootClass = MACRO_QUOTE(RD501_patch_weapons);
		requiredAddons[]=
		{
			"RD501_patch_weapons",
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			RD501_E60R
		};
	};
};
class CfgWeapons
{
	class launch_RPG32_F
	{
		class Single;
	};
	class RD501_E60R: launch_RPG32_F
	{
        displayName = "CIS E60R";
		scope = 2;
		descriptionShort = "$STR_JLTS_descs_PortableMissileLauncher";
		picture = "\MRC\JLTS\weapons\E60R\data\ui\E60R_ui_ca.paa";
		uiPicture = "\MRC\JLTS\weapons\E60R\data\ui\E60R_ui_ca.paa";
		model = "\MRC\JLTS\weapons\E60R\E60R.p3d";
		modelSpecial = "";
		handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\E60R\anims\E60R_handanim.rtm"};
		hiddenSelections[] = {"camo1","illum"};
		hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\E60R\data\E60R_co.paa"};
		hiddenSelectionsMaterials[] = {"","\a3\characters_f_bootcamp\common\data\vrarmoremmisive.rvmat"};
		magazines[] = {"RD501_E60R_AT","RD501_E60R_HE"};
		magazineWell[] = {};
		class Single: Single
		{
			class BaseSoundModeType{};
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"swlw_rework\sounds\launcher\E60R_shot.wss",10,1,2000};
				soundBegin[] = {"begin1",1};
			};
		};
		magazineReloadTime = 60;
	};
};