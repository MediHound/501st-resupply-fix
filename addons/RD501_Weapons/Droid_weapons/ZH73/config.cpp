#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"

class CfgPatches
{
	class RD501_patch_zh73
	{
		author=DANKAUTHORS;
		addonRootClass = MACRO_QUOTE(RD501_patch_weapons);
		requiredAddons[]=
		{
			"RD501_patch_weapons",
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
        {
			macro_new_weapon(zh,73)
		};
	};
};
class CowsSlot;
class MuzzleSlot;
class Mode_SemiAuto;
class cfgWeapons 
{
    class Rifle_Long_Base_F
    {
        class WeaponSlotsInfo
		{
			class SlotInfo;
		};
		class GunParticles;
		class AnimationSources;
    };
	class macro_new_weapon(zh,73): Rifle_Long_Base_F
	{
		scope = 2;
		model = "\SWLW_merc\rifles\ZH73\ZH73.p3d";
		handAnim[] = {"OFP2_ManSkeleton","\A3\Weapons_F_epa\LongRangeRifles\DMR_01\Data\Anim\dmr_01.rtm"};
		reloadAction = "ReloadMagazine";
		picture = "\SWLW_merc\rifles\ZH73\data\ui\SWLW_ZH73_ui.paa";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"SWLW_merc\rifles\ZH73\data\ZH73_co.paa"};
		magazines[]={"RD501_30mw_x15_mag"};
		displayname = "CIS ZH-73";
		descriptionShort = "";
		selectionFireAnim = "zasleh";
		class Library
		{
			libTextDesc = "";
		};
		drySound[] = {"",0.39810717,1,20};
		reloadMagazineSound[] = {"SWLW_clones\pistols\dc17\sounds\DC17_reload.wss",0.56234133,1,30};
		soundBullet[] = {};
		modes[] = {"Single","close","short","medium"};
		fireLightDuration = 0.05;
		fireLightIntensity = 0.2;
		fireLightDiffuse[] = {1,0,0};
		fireLightAmbient[] = {1,0,0};	
		class Single: Mode_SemiAuto
		{
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				weaponSoundEffect = "";
				closure1[] = {};
				closure2[] = {};
				soundClosure[] = {};
			};
			class StandardSound: BaseSoundModeType
			{
				weaponSoundEffect = "";
				begin1[] = {"SWLW_merc\rifles\zh73\sounds\zh73",1,1,1800};
				begin2[] = {"SWLW_merc\rifles\zh73\sounds\zh73",1,1,1800};
				begin3[] = {"SWLW_merc\rifles\zh73\sounds\zh73",1,1,1800};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
			};
			reloadTime = 0.15;
			dispersion = 0.003015;
			minRange = 5;
			minRangeProbab = 0.3;
			midRange = 25;
			midRangeProbab = 0.6;
			maxRange = 50;
			maxRangeProbab = 0.1;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 25;
		};
        class close: Single
		{
			showToPlayer = 0;
			aiRateOfFire = 0.25;
			aiRateOfFireDistance = 400;
			minRange = 0;
			minRangeProbab = 0.05;
			midRange = 200;
			midRangeProbab = 0.7;
			maxRange = 400;
			maxRangeProbab = 0.2;
		};
		class short: close
		{
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 500;
			minRange = 300;
			minRangeProbab = 0.2;
			midRange = 400;
			midRangeProbab = 0.7;
			maxRange = 500;
			maxRangeProbab = 0.2;
		};
		class medium: close
		{
			aiRateOfFire = 1;
			aiRateOfFireDistance = 900;
			minRange = 400;
			minRangeProbab = 0.2;
			midRange = 700;
			midRangeProbab = 0.7;
			maxRange = 900;
			maxRangeProbab = 0.2;
		};
		inertia = 0.3;
		dexterity = 1.7;
		initSpeed = 400;
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass = 80;
			class CowsSlot: CowsSlot
			{
				compatibleItems[] = {"SWLW_ZH73_scope"};
			};
			class MuzzleSlot: MuzzleSlot
			{
				compatibleItems[] = {};
			};
		};
    };
	class macro_new_weapon(zh_scope,73): macro_new_weapon(zh,73)
	{
		class LinkedItems
		{
			class LinkedItemsOptic
			{
				item = "SWLW_ZH73_scope";
				slot = "CowsSlot";
			};
		};
	};
};

