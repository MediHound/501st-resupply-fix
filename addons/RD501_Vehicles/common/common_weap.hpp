
//DC-15 Family//
class _transport_dc15a
{
	weapon = "Aux501_Weaps_DC15A";
	count = 5;
};
class _transport_dc15s
{
	weapon = "Aux501_Weaps_DC15S";
	count = 5;
};
class _transport_dc15c
{
	weapon = "Aux501_Weaps_DC15C";
	count = 5;
};
class _transport_dc15l
{
	weapon = "Aux501_Weaps_DC15L";
	count = 2;
};
class _transport_dc15x
{
	weapon = "Aux501_Weaps_DC15X";
	count = 1;
};
class _transport_dc17
{
	weapon = "Aux501_Weaps_DC17";
	count = 2;
};

//other weapons//
class _transport_rgl6
{
	weapon = "Aux501_Weaps_RGL6";
	count = 1;
};
class _transport_z6
{
	weapon = "Aux501_Weaps_Z6";
	count = 2;
};
class _transport_z1000
{
	weapon = "Aux501_Weaps_Z1000";
	count = 1;
};
class _transport_valken38x
{
	weapon = "Aux501_Weaps_Valken38X";
	count = 2;
};
//launchers//
class _transport_plx
{
	weapon = "Aux501_Weaps_PLX1";
	count = 1;
};
class _transport_rps6
{
	weapon = "Aux501_Weaps_RPS6";
	count = 2;
};
class _transport_rps4
{
	weapon = "Aux501_Weaps_RPS4";
	count = 2;
};
class _transport_rps1
{
	weapon = "Aux501_Weaps_RPS1";
	count = 2;
};

