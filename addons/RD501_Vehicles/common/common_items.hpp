
class _item_RD_SquadShield_Mag
{
    name = MACRO_QUOTE(macro_new_mag(squad_shield_sigma,1));
    count = 2;
};
class _item_ACE_HuntIR_monitor
{
    name = "ACE_HuntIR_monitor";
    count = 1;
};
class _item_optics_Aux501_rco
{
    name = "Aux501_cows_rco";
    count = 2;
};
class _item_optics_Aux501_rco2
{
    name = "Aux501_cows_rco_2";
    count = 2;
};
class _item_optics_Aux501_rco3
{
    name = "Aux501_cows_rco_3";
    count = 2;
};
class _item_optics_Aux501_mrco
{
    name = "Aux501_cows_mrco";
    count = 2;
};
class _item_optics_Aux501_mrco2
{
    name = "Aux501_cows_mrco_2";
    count = 2;
};
class _item_optics_Aux501_mrco3
{
    name = "Aux501_cows_mrco_3";
    count = 2;
};
class _item_optics_Aux501_holosight
{
    name = "Aux501_cows_Holosight";
    count = 2;
};
class _item_optics_Aux501_holosight2
{
    name = "Aux501_cows_Holosight_2";
    count = 2;
};
class _item_optics_Aux501_holosight3
{
    name = "Aux501_cows_Holosight_3";
    count = 2;
};
class _item_optics_Aux501_holoscope
{
    name = "Aux501_cows_HoloScope";
    count = 2;
};
class _item_optics_Aux501_holoscope2
{
    name = "Aux501_cows_HoloScope_2";
    count = 2;
};
class _item_optics_Aux501_holoscope3
{
    name = "Aux501_cows_HoloScope_3";
    count = 2;
};
class _item_optics_Aux501_dms
{
    name = "Aux501_cows_DMS";
    count = 2;
};
class _item_optics_Aux501_dms2
{
    name = "Aux501_cows_DMS_2";
    count = 2;
};
class _item_optics_Aux501_dms3
{
    name = "Aux501_cows_DMS_3";
    count = 2;
};
class _item_optics_Aux501_dms4
{
    name = "Aux501_cows_DMS_4";
    count = 2;
};
class _item_optics_Aux501_dms_ti
{
    name = "Aux501_cows_DMS_TI";
    count = 2;
};
class _item_optics_Aux501_dms_ti2
{
    name = "Aux501_cows_DMS_2_TI";
    count = 2;
};
class _item_optics_Aux501_dms_ti3
{
    name = "Aux501_cows_DMS_3_TI";
    count = 2;
};
class _item_optics_Aux501_dms_ti4
{
    name = "Aux501_cows_DMS_4_TI";
    count = 2;
};
class _item_optics_Aux501_pistol
{  
    name = "Aux501_cows_pistol";
    count = 2;
};
class _item_optics_Aux501_pistol2
{  
    name = "Aux501_cows_pistol_2";
    count = 2;
};
class _item_optics_Aux501_lrps
{  
    name = "Aux501_cows_LRPS";
    count = 2;
};
class _item_optics_Aux501_lescope
{  
    name = "Aux501_cows_LEScope_DC15A";
    count = 2;
};
class _item_optics_Aux501_reflex
{  
    name = "Aux501_cows_reflex_optic";
    count = 2;
};