

class _item_ACE_tourniquet
{
	name = "ACE_tourniquet";
	count = 16;
};
class _item_ACE_splint
{
	name = "ACE_splint";
	count = 16;
};
class _item_ACE_Needle
{
	name = "kat_IV_16";
	count = 16;
};
class _item_ACE_plasmaIV_250
{
	name = "ACE_plasmaIV_250";
	count = 12;
};
class _item_ACE_plasmaIV_500
{
	name = "ACE_plasmaIV_500";
	count = 20;
};
class _item_ACE_plasmaIV_1000
{
	name = "ACE_plasmaIV";
	count = 8;
};
class _item_ACE_elasticBandage
{
	name = "ACE_elasticBandage";
	count = 100;
};				
class _item_ACE_quikclot
{
	name = "ACE_quikclot";
	count = 20;
};					
class _item_ACE_packingBandage
{
	name = "ACE_packingBandage";
	count = 120;
};						
class _item_ACE_painkiller
{
	name = "RD501_Painkiller";
	count = 20;
};								
class _item_ACE_epinephrine
{
	name = "ACE_epinephrine";
	count = 15;
};
class _item_Necro_Enzyme_Refined
{
	name = "dev_enzymeCapsule_refined";
	count = 12;
};
class _item_Necro_Enzyme
{
	name = "dev_enzymeCapsule";
	count = 20;
};
//purple flare
class _item_SmokeShellPurple
{
	name = "SmokeShellPurple";
	count = 6;
};