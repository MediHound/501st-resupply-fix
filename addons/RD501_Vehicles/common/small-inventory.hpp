class TransportItems
	{
		class _transport_ToolKit
		{
			name = "ToolKit";
			count = 2;
		};
		class _item_ACE_tourniquet
		{
			name = "ACE_tourniquet";
			count = 12;
		};
		class _item_ACE_splint
		{
			name = "ACE_splint";
			count = 8;
		};
		class _item_ACE_Needle
		{
			name = "kat_IV_16";
			count = 6;
		};
		class _item_ACE_plasmaIV_1000
		{
			name = "ACE_plasmaIV";
			count = 6;
		};
		class _item_ACE_elasticBandage
		{
			name = "ACE_elasticBandage";
			count = 30;
		};				
		class _item_ACE_quikclot
		{
			name = "ACE_quikclot";
			count = 15;
		};					
		class _item_ACE_packingBandage
		{
			name = "ACE_packingBandage";
			count = 15;
		};						
		class _item_ACE_painkiller
		{
			name = "RD501_Painkiller";
			count = 4;
		};								
		class _item_ACE_epinephrine
		{
			name = "ACE_epinephrine";
			count = 4;
		};
		class _item_Necro_Enzyme
		{
			name = "dev_enzymeCapsule";
			count = 4;
		};
		class _item_surgicalKit
		{
			name = "ACE_surgicalKit";
			count = 1;
		};		
	};
class TransportWeapons
	{
	class _transport_dc15a
		{
			weapon = "Aux501_Weaps_DC15A";
			count = 3;
		};
	class _transport_dc15c
		{
			weapon = "Aux501_Weaps_DC15C";
			count = 3;
		};
	class _transport_rps1
		{
			weapon = "Aux501_Weaps_RPS1";
			count = 2;
		};
	};
class TransportMagazines
	{
	//dc-15C family
	class transport_20mw40
		{
			magazine = "Aux501_Weapons_Mags_20mw40";
			count = 30;
		};
	//dc-15a dc-17
	class _transport_20mwdp30
		{
			magazine = "Aux501_Weapons_Mags_20mwdp30";
			count = 30;
		};
	class _transport_C12_Remote_Mag
	{
		magazine = "C12_Remote_Mag";
		count = 2;
	};
	};
class TransportBackpacks
	{
		class _transport_gravpack
			{
				backpack="RD501_JLTS_Clone_Flip_backpack";
				count=1;
			};
	};