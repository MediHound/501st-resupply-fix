#include "../../../RD501_main/config_macros.hpp"


class CfgPatches
{
	class RD501_patch_Z95
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(z95,blue)
		};
		weapons[]=
		{
			
		};
	};
};
#include "../../common/sensor_templates.hpp"

class CfgVehicles
{
	class Plane_Fighter_03_base_F;
	class Plane_Fighter_03_dynamicLoadout_base_F:Plane_Fighter_03_base_F
	{
		class Components;
	};
	class 3as_Z95_base: Plane_Fighter_03_dynamicLoadout_base_F
	{
		class Components: Components
		{
			class TransportPylonsComponent;
		};
		class ACE_SelfActions;
	};
	class macro_new_vehicle(z95,blue):3as_Z95_base
	{
		soundSetSonicBoom[] = {"Plane_Fighter_SonicBoom_SoundSet"};
		scope=2;
		scopeCurator=2;
		forceInGarage = 1;
		side=1;
		stallSpeed=5;
		displayName = "Republic Z95";
		author = "RD501";
		faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat_air(Republic_vtol));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type_air(Republic));
		RD501_magclamp_large_offset[]={0.0,0.0,-2.0};
		draconicTorqueYCoef[]={0,1.5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		draconicForceYCoef=6;
		envelope[] = {0.1,0.2,0.3,1.5,1.72,2.69,3.87,5.27,6.89,8.72,9.7,9.6,9.2,8.5,8.2,8};
		elevatorCoef[] = {1.1,1.2,1.3,1.3,1.3,1.3,1.3,1.3,1.25,1.2,1.15,1.1,1};
		rudderCoef[] = {1.6,2,2,2.9,3,2.5,2};
		aileronSensitivity=2.9;
		aileronControlsSensitivityCoef=4;
		maxSpeed=1750;
		acceleration=200;
		thrustCoef[] = {2.0,2.0,2.2,2.6,3,3.5,4.5,5,4.5,4,3.3,3,2.2,2,2,2};
		irTarget = 1;
		threat[] = {.3, .4, .9};
		cost= 9000;
		irTargetSize = 1;
		radarTarget = 1;
		radarTargetSize = 1;
		altFullForce = 16000;
		altNoForce = 19000;
		class ACE_SelfActions
		{
			class ACE_Passengers
			{
				condition = "alive _target";
				displayName = "Passengers";
				insertChildren = "_this call ace_interaction_fnc_addPassengersActions";
				statement = "";
			};
			#include "../../common/universal_hud_color_changer.hpp"
		};
		#include "../../common/common_pilotCamera.hpp"
		weaponLockSystem = "2+4+8";
		incomingMissileDetectionSystem = 16;
		delete Turrets;
		weapons[]=
		{
			"RD501_CMFlareLauncher",
			"RD501_Republic_Aircraft_Laser_Repeater",
			"RD501_Republic_Aircraft_Laser_AA",
		};
		magazines[]=
		{
			"RD501_CMFlare_Magazine_200",
			"RD501_CMFlare_Magazine_200",
			"RD501_Republic_Aircraft_Laser_Repeater_Mag_1000",
			"RD501_Republic_Aircraft_Laser_Repeater_Mag_1000",
			"RD501_Republic_Aircraft_Laser_AA_Mag_600",
		};
		hiddenselections[]=
		{
			"camo",
			"camo1",
			"camo2"
		};
		hiddenselectionstextures[]=
		{
			"3AS\3as_Z95\Data\blue_co.paa",
			"3AS\3as_Z95\Data\cockpit_co.paa",
			"3AS\3as_saber\data\glass\glass_ca"
		};
		class Components: Components
		{
			class VehicleSystemsDisplayManagerComponentLeft: VehicleSystemsTemplateLeftPilot
			{
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000,32000};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			class VehicleSystemsDisplayManagerComponentRight: VehicleSystemsTemplateRightPilot
			{
				defaultDisplay = "SensorDisplay";
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000,32000};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			
			class SensorsManagerComponent
			{
				class Components
				{
					class IRSensorComponent: SensorTemplateIR
					{
						class AirTarget
						{
							minRange=0;
							maxRange=14500;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						class GroundTarget
						{
							minRange=0;
							maxRange=14500;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						componentType = "IRSensorComponent";

						typeRecognitionDistance = 10000;
						angleRangeHorizontal = 30;
						angleRangeVertical = 50;
						maxFogSeeThrough = 0.85;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						color[]={1,0,0,1};
					};

					class ActiveRadarSensorComponent: SensorTemplateActiveRadar
					{
						class AirTarget
						{
							minRange=0;
							maxRange=14500;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						class GroundTarget
						{
							minRange=0;
							maxRange=14500;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						componentType = "ActiveRadarSensorComponent";

						typeRecognitionDistance = 11000;
						angleRangeHorizontal = 90;
						angleRangeVertical = 90;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						color[]={0,1,1,1};
					};

					class VisualRadarSensorComponent: SensorTemplateVisual
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 1000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 1000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType = "VisualSensorComponent";

						typeRecognitionDistance = 800;
						angleRangeHorizontal = 90;
						angleRangeVertical = 90;
						maxFogSeeThrough = 0.85;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						color[]={1,1,0.5,0.80000001};
					};

					class DataLinkSensorComponent: SensorTemplateDataLink
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="DataLinkSensorComponent";
						allowsMarking=1;
						typeRecognitionDistance=0;
						color[]={1,1,1,0};
					};	
				};
			};
		
			class TransportPylonsComponent:TransportPylonsComponent
			{
				class pylons
				{
					class pylons1
					{
						hardpoints[]=
						{
							"RD501_Republic_Aircraft_Missile_AA_IR_Pylon",
							"RD501_Republic_Aircraft_Missile_AP_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_LR_Pylon",
							"RD501_Republic_Aircraft_Laser_Repeater_Pylon"
						};
						attachment = "empty";
						priority=1;
						maxweight=125;
						UIposition[]={0.15,0.45};
					};
					class pylons2: pylons1
					{
						hardpoints[]=
						{
							"RD501_Republic_Aircraft_Missile_AA_IR_Pylon",
							"RD501_Republic_Aircraft_Missile_AP_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_LR_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_XLR_Pylon",
							"RD501_Republic_Aircraft_Laser_Repeater_Pylon"
						};
						attachment = "empty";
						priority=1;
						maxweight=125;
						UIposition[]={0.2,0.35};
					};
					class pylons3: pylons2
					{
						priority=1;
						UIposition[]={0.25,0.25};
					};
					class pylons4: pylons2
					{
						
						priority=1;
						UIposition[]={0.6,0.25};
						mirroredMissilePos=3;
					};
					class pylons5: pylons2
					{
						priority=1;
						UIposition[]={0.65,0.35};
						mirroredMissilePos=2;
					};
					class pylons6: pylons1
					{
						priority=1;
						UIposition[]={0.7,0.45};
						mirroredMissilePos=1;
					};
				};
			
				class presets
				{
					class empty
					{
						displayName = "$STR_empty";
						attachment[] = {};
					};
					class CAPBVR
					{
						displayName = "CAP BVR";
						attachment[] =
						{
							"RD501_Republic_Aircraft_Missile_AR_LR_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_XLR_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_XLR_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_XLR_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_XLR_Pylon",
							"RD501_Republic_Aircraft_Missile_AR_LR_Pylon"
						};
					};
				};
			};
		};
	};
};