//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon nuclass
class CfgPatches
{
	class RD501_patch_nuclass
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(nuclass,mk1)
		};
		weapons[]=
		{
			
		};
	};
};
class Extended_init_EventHandlers
{
	class macro_new_vehicle(nuclass,mk1)
	{
		class nu_init_eh
		{
			init = "(_this) spawn ls_vehicle_fnc_ImpulsorMonitor; [_this select 0] execVM 'RD501_Main\functions\autocrate\autocrate_large.sqf';";
		};
	};
};
#include "../../common/sensor_templates.hpp"
class CfgVehicles
{
	class 3as_nuclass_base;
	class 3as_nuclass_f: 3as_nuclass_base
	{
		class ViewPilot;
		class UserActions;
	};
	class 3AS_Nuclass : 3as_nuclass_f
	{
		class ViewPilot: ViewPilot{};
		class UserActions: UserActions
		{
			class rampOpen;
			class rampClose;
			class frontrampOpen;
			class frontrampClose;
		};
		class Components;
		class Turrets;
	};

	class macro_new_vehicle(nuclass,mk1) : 3AS_Nuclass
	{
		ls_impulsor_fuelDrain_1 = 0.000035;
		ls_impulsor_fuelDrain_2 = 0.0001;
		ls_impulsor_boostSpeed_1 = 400;
		ls_impulsor_boostSpeed_2 = 600;
		ls_hasImpulse = 1;
		tas_can_impulse = 0;
		scope=2;
		displayName="Republic NuClass Mk.I";
		armor=800;
		faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat_air(Republic_heli));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type_air(Republic));
		transportSoldier=40;
		receiveRemoteTargets = true;
		reportRemoteTargets = true;
		reportOwnPosition = true;
		laserScanner = 1;
		soundSetSonicBoom[] = {"Plane_Fighter_SonicBoom_SoundSet"};
		#include "../../common/common_pilotCamera.hpp"
		irTarget = 1;
		irTargetSize = 1;
		radarTarget = 1;
		radarTargetSize = 1.1;
		threat[] = {.4, .7, .9};
		cost= 9500;
		altFullForce = 16000;
		altNoForce = 19000;

		fuelCapacity = 1400;
		fuelConsumptionRate = 0.2;

		RD501_magclamp_large[] = {0.0,1.5,-0.5};
		RD501_magclamp_small_forbidden=1;
		RD501_magclamp_large_offset[]={0.0,0.0,-10.0};

		TFAR_hasIntercom=1;

		weapons[]=
		{
			"RD501_CMFlareLauncher",
			"RD501_Aircraft_Laser_Cannon"
		};
		magazines[]=
		{
			"RD501_CMFlare_Magazine_200",
			"RD501_CMFlare_Magazine_200",
			"RD501_Republic_Aircraft_Laser_Cannon_Mag_100",
			"RD501_Republic_Aircraft_Laser_Cannon_Mag_100",
			"RD501_Republic_Aircraft_Laser_Cannon_Mag_100",

		};
		class UserActions:UserActions
		{
			class rampOpen: rampOpen
			{
				condition="(this animationSourcePhase 'ramp' == 0) AND (alive this)";
			};
			class rampClose: rampClose
			{
				condition="(this animationSourcePhase 'ramp' == 1) AND (alive this)";
			};
			class frontrampOpen: frontrampOpen
			{
				condition="(this animationSourcePhase 'rampfront' == 0) AND (alive this)";
			};
			class frontrampClose: frontrampClose
			{
				condition="(this animationSourcePhase 'rampfront' == 1) AND (alive this)";
			};
		};
		class ACE_SelfActions
		{
			class ACE_Passengers
			{
				condition = "alive _target";
				displayName = "Passengers";
				insertChildren = "_this call ace_interaction_fnc_addPassengersActions";
				statement = "";
			};
			#include "../../common/universal_hud_color_changer.hpp"
			class TFAR_IntercomChannel
			{
				displayName="$STR_tfar_core_Intercom_ACESelfAction_Name";
				condition="true";
				statement="";
				icon="";
				class TFAR_IntercomChannel_disabled
				{
					displayName="Disabled";
					condition="_vehicle = vehicle ACE_Player; _intercom = _vehicle getVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-2]; if (_intercom == -2) then {_intercom = _vehicle getVariable ['TFAR_defaultIntercomSlot',TFAR_defaultIntercomSlot]}; _intercom != -1";
					statement="(vehicle ACE_Player) setVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-1,true];";
				};
				class TFAR_IntercomChannel_1
				{
					displayName="$STR_tfar_core_Intercom_ACESelfAction_Channel1";
					condition="_vehicle = vehicle ACE_Player; _intercom = _vehicle getVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-2]; if (_intercom == -2) then {_intercom = _vehicle getVariable ['TFAR_defaultIntercomSlot',TFAR_defaultIntercomSlot]}; _intercom != 0";
					statement="(vehicle ACE_Player) setVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],0,true];";
				};
				class TFAR_IntercomChannel_2
				{
					displayName="$STR_tfar_core_Intercom_ACESelfAction_Channel2";
					condition="_vehicle = vehicle ACE_Player; _intercom = _vehicle getVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-2]; if (_intercom == -2) then {_intercom = _vehicle getVariable ['TFAR_defaultIntercomSlot',TFAR_defaultIntercomSlot]}; _intercom != 1";
					statement="(vehicle ACE_Player) setVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],1,true];";
				};
				class TFAR_IntercomChannel_Misc_1
				{
					displayName="Misc channel 1";
					condition="_vehicle = vehicle ACE_Player; _intercom = _vehicle getVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-2]; if (_intercom == -2) then {_intercom = _vehicle getVariable ['TFAR_defaultIntercomSlot',TFAR_defaultIntercomSlot]}; _intercom != 2";
					statement="(vehicle ACE_Player) setVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],2,true];";
				};
				class TFAR_IntercomChannel_Misc_2
				{
					displayName="Misc channel 2";
					condition="_vehicle = vehicle ACE_Player; _intercom = _vehicle getVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-2]; if (_intercom == -2) then {_intercom = _vehicle getVariable ['TFAR_defaultIntercomSlot',TFAR_defaultIntercomSlot]}; _intercom != 3";
					statement="(vehicle ACE_Player) setVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],3,true];";
				};
				class TFAR_IntercomChannel_Misc_3
				{
					displayName="Misc channel 3";
					condition="_vehicle = vehicle ACE_Player; _intercom = _vehicle getVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-2]; if (_intercom == -2) then {_intercom = _vehicle getVariable ['TFAR_defaultIntercomSlot',TFAR_defaultIntercomSlot]}; _intercom != 4";
					statement="(vehicle ACE_Player) setVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],4,true];";
				};
			};
		};
		class ViewPilot: ViewPilot
		{	
			initAngleX = 0;
		};
		class TransportWeapons
		{
			#include "../../common/common_weap.hpp"
		};
		class TransportMagazines
		{
			#include "../../common/common_ammo_mag.hpp"
		};
		class TransportItems
		{
			#include "../../common/common_items_medical.hpp"
			#include "../../common/common_items.hpp"
		};
	
		class Components: Components
		{

			class VehicleSystemsDisplayManagerComponentLeft: VehicleSystemsTemplateLeftPilot
			{
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000,2000};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			class VehicleSystemsDisplayManagerComponentRight: VehicleSystemsTemplateRightPilot
			{
				defaultDisplay = "SensorDisplay";
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000,2000};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			class SensorsManagerComponent
			{
				
				class Components
				{
					class PassiveRadarSensorComponen: SensorTemplatePassiveRadar
					{
						componentType = "PassiveRadarSensorComponent";
						class AirTarget
						{
							minRange = 0; 
							maxRange = 8000;                                              
							objectDistanceLimitCoef = -1; 
							viewDistanceLimitCoef = -1;             						
						};
						class GroundTarget
						{
							minRange = 0; 
							maxRange = 8000;                                              
							objectDistanceLimitCoef = -1; 
							viewDistanceLimitCoef = -1;             
						};	

						typeRecognitionDistance = 6000;
						angleRangeHorizontal = 360;
						angleRangeVertical = 360;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
					};				
					class DataLinkSensorComponent: SensorTemplateDataLink
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="DataLinkSensorComponent";
						allowsMarking=1;
						typeRecognitionDistance=0;
						color[]={1,1,1,0};
					};
				};
			};
		};
	};
	class macro_new_vehicle(nuclass,mk1_unarmed) : macro_new_vehicle(nuclass,mk1)
	{
		displayName="Republic NuClass Mk.I (Unarmed)";
		weapons[]=
		{
			"RD501_CMFlareLauncher"
		};
		magazines[]=
		{
			"RD501_CMFlare_Magazine_200",
			"RD501_CMFlare_Magazine_200"
		};
		class Turrets : Turrets
		{
			delete Copilot;
			delete Copilot2;
		};
	};
};