#include "../../../RD501_main/config_macros.hpp"
class CfgPatches
{
	class RD501_patch_MASH
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles,
			A3_Supplies_F_Heli
		};
		requiredVersion=0.1;
		units[]=
		{,
			RD501_MASH_Tent,
		};
		weapons[]=
		{
			
		};
	};
};

#include "../../common/sensor_templates.hpp"
class CfgVehicles
{
	class RD501_FX7Droid;
	class RD501_FX7Droid_mash: RD501_FX7Droid
    {
		scope = 1;
        scopeCurator = 1;
        displayName = "FX7 Medical Droid";
        editorPreview = "";
        editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
        icon = "iconObject_5x2";
        model = "RD501_Vehicles\static\FX7Droid\FX7Droid.p3d";
        vehicleClass = "Misc";
		
		class EventHandlers;
    };
	class RD501_MASH_Tent: RD501_FX7Droid_mash
	{
		displayName = "Republic M*A*S*H Droid";
		scope = 2;
		scopeCurator = 2;
		RD501_magclamp_large_offset[] = {0.0, 0.0, -2.0};
		RD501_magclamp_small_offset[] = {0.0, 2.0, -2.0};

		editorCategory = MACRO_QUOTE(macro_editor_cat(statics));
		simulation = "house";
		transportMaxWeapons = 25;
        transportMaxMagazines = 100;
        transportMaxBackpacks = 10;
        maximumLoad = 5000;
        supplyRadius = 5;
        class TransportBackpacks{};
        class TransportMagazines{};
        class TransportWeapons{};
        class TransportItems
		{
			#include "../../common/common_items_medical.hpp"				
		};
		class EventHandlers: EventHandlers 
		{
			init = "_this call rd501_fnc_deployCCPLocal";
		};
	};
};