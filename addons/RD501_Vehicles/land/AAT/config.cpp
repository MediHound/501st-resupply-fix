//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class RD501_patch_AAT
	{
		addonRootClass = "RD501_patch_vehicles";
		requiredAddons[] = {"RD501_patch_vehicles"};
		requiredVersion = 0.1;
		units[] = 
		{
			"RD501_AAT_Blue",
			"RD501_AAT_Snow",
			"RD501_AAT_Tropical",
			"RD501_AAT_Brown",
			"RD501_AAT_Desert",
			"RD501_AAT_Green",
			"RD501_AAT_Woodland",
			"RD501_AAT_Arid",
			"RD501_AAT_Aqua"
		};
		weapons[] = {};
	};
};


#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers;
class CfgVehicles
{
	class LandVehicle;
	class Tank: LandVehicle 
	{
		class NewTurret;
		class Sounds;
		class HitPoints;
	};
	class Tank_F: Tank
	{
		class Turrets
		{
			class MainTurret: NewTurret
			{
				class ViewGunner;
				class Turrets
				{
					class CommanderOptics;
				};
			};
		};
		class HitPoints: HitPoints
		{
			class HitHull;
			class HitFuel;
			class HitEngine;
			class HitLTrack;
			class HitRTrack;
		};
	};
	class 3AS_AAT_base_F : Tank_F 
	{
		class Turrets : Turrets 
		{
			class MainTurret: MainTurret 
			{
				class ViewGunner : ViewGunner {};
				class Turrets : Turrets 
				{
					class CommanderOptics : CommanderOptics {};
				};
			};
		};
		class HitPoints: HitPoints
		{
			class HitHull:HitHull{};
			class HitFuel:HitFuel{};
			class HitEngine:HitEngine{};
			class HitLTrack:HitLTrack{};
			class HitRTrack:HitRTrack{};
		};
	};
	class 3AS_CIS_AAT_base_F: 3AS_AAT_base_F
	{
		class Turrets : Turrets 
		{
			class MainTurret: MainTurret 
			{
				class ViewGunner : ViewGunner {};
				class Turrets : Turrets 
				{
					class CommanderOptics : CommanderOptics {};
				};
			};
		};
		class HitPoints: HitPoints
		{
			class HitHull:HitHull{};
			class HitFuel:HitFuel{};
			class HitEngine:HitEngine{};
			class HitLTrack:HitLTrack{};
			class HitRTrack:HitRTrack{};
		};
	};
	class 3AS_CIS_AAT_F: 3AS_CIS_AAT_base_F
	{
		class Turrets : Turrets 
		{
			class MainTurret: MainTurret 
			{
				class ViewGunner : ViewGunner {};
				class Turrets : Turrets 
				{
					class CommanderOptics : CommanderOptics {};
				};
			};
		};
		class HitPoints: HitPoints
		{
			class HitHull:HitHull{};
			class HitFuel:HitFuel{};
			class HitEngine:HitEngine{};
			class HitLTrack:HitLTrack{};
			class HitRTrack:HitRTrack{};
		};
	};
	class 3AS_AAT: 3AS_CIS_AAT_F
	{
		class Turrets : Turrets 
		{
			class MainTurret: MainTurret 
			{
				class ViewGunner : ViewGunner {};
				class Turrets : Turrets 
				{
					class CommanderOptics : CommanderOptics {};
				};
			};
		};
		class HitPoints: HitPoints
		{
			class HitHull:HitHull{};
			class HitFuel:HitFuel{};
			class HitEngine:HitEngine{};
			class HitLTrack:HitLTrack{};
			class HitRTrack:HitRTrack{};
		};
	};

	//RD501 AATs
	class RD501_AAT_Blue : 3AS_AAT_base_F 
	{
		displayName="AAT (Blue)";
		scope=2;
		side=0;
		scopeCurator=2;
		forceInGarage = 1;
		armor=300;
		crew=MACRO_QUOTE(macro_new_unit_class(opfor,B1_crew));
		faction = MACRO_QUOTE(macro_faction(CIS));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(tank));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(tank));
		hiddenSelectionsTextures[] = {"3AS\3AS_AAT\data\CIS_AAT_CO.paa"};
		RD501_magclamp_small_offset[] = {0.0,0.0,-3.0};
		RD501_magclamp_large_offset[] = {0.0,0.0,-3.0};
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[] = 
				{
					"SmokeLauncher",
					"RD501_aat_cannon_mbt"
				};
				magazines[] = 
				{
					"SmokeLauncherMag",
					"RD501_aat_mbt_x50_mag",
					"RD501_aat_mbt_x50_mag"
				};
				class Turrets : Turrets 
				{
					class CommanderOptics: CommanderOptics
					{
						weapons[] = 
						{
							"RD501_AAT_Repeater",
							"SmokeLauncher"
						};
						magazines[] =
						{ 
							macro_new_mag(CIS_eweb_cell,250),
							macro_new_mag(CIS_eweb_cell,250),
							macro_new_mag(CIS_eweb_cell,250),
							macro_new_mag(CIS_eweb_cell,250),
							macro_new_mag(CIS_eweb_cell,250),
							macro_new_mag(CIS_eweb_cell,250),
							macro_new_mag(CIS_eweb_cell,250),
							macro_new_mag(CIS_eweb_cell,250),
							macro_new_mag(CIS_eweb_cell,250),
							macro_new_mag(CIS_eweb_cell,250)	
						};
					};
				};
			};
		};
		class HitPoints: HitPoints
		{
			class HitHull: HitHull 
			{	// Handle internal damage
				armor=4.5;
				material=-1;
				name="telo";
				visual="zbytek";
				passThrough=1;
				minimalHit = 0.2;
				explosionShielding = 0.2;
				radius = 0.12;
			};
			class HitEngine: HitEngine {
				armor=0.75;
				material=-1;
				name="motor";
				passThrough=0.2;
				minimalHit = 0.2;
				explosionShielding = 0.2;
				radius = 0.33;
			};
			class HitLTrack: HitLTrack {
				armor=0.5;
				material=-1;
				name="track_l_hit";
				passThrough=0;
				minimalHit = 0.08;
				explosionShielding = 1.44;
				radius = 0.3;
			};
			class HitRTrack: HitRTrack {
				armor=0.5;
				material=-1;
				name="track_r_hit";
				passThrough=0;
				minimalHit = 0.08;
				explosionShielding = 1.44;
				radius = 0.3;
			};
			class HitFuel: HitFuel
			{
				armor = 1.5;
				material = -1;
				name = "palivo";
				passThrough = 0.1;
				minimalHit = 0.1;
				explosionShielding = 0.6;
				radius = 0.25;
			};
		};
		class AnimationSources
		{
			class muzzle_rot_cannon
			{
				source = "ammorandom";
				weapon = "RD501_AAT_Repeater";
			};
			class recoil_source
			{
				source = "reload";
				weapon = "RD501_aat_cannon_mbt";
			};
			class muzzle_rot_coax
			{
				source = "ammorandom";
				weapon = "RD501_AAT_Repeater";
			};
		};
	};
	class RD501_AAT_Snow : RD501_AAT_Blue
	{
		displayName="AAT (Snow)";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"3AS\3AS_AAT\data\Winter_AAT_CO.paa"};
	};
	class RD501_AAT_Tropical : RD501_AAT_Blue
	{ 
		displayName="AAT (Tropical)";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"3AS\3AS_AAT\data\Tropical_AAT_CO.paa"};
	};

	class RD501_AAT_Brown : RD501_AAT_Blue
	{
		displayName="AAT (Trade Federation)";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"3AS\3AS_AAT\data\Tan_AAT_CO.paa"};
	};
	class RD501_AAT_Desert : RD501_AAT_Blue
	{
		displayName="AAT (Desert)";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"3AS\3AS_AAT\data\Geonosis_AAT_CO.paa"};
	};
	class RD501_AAT_Green : RD501_AAT_Blue
	{
		displayName="AAT (Green)";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"3AS\3AS_AAT\data\Green_AAT_CO.paa"};
	};
	class RD501_AAT_Woodland : RD501_AAT_Blue
	{
		displayName="AAT (Woodland)";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"3AS\3AS_AAT\data\Woodland_AAT_CO.paa"};
	};
	class RD501_AAT_Arid : RD501_AAT_Blue
	{
		displayName="AAT (Arid)";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"3AS\3AS_AAT\data\Arid_AAT_CO.paa"};
	};
	class RD501_AAT_Aqua : RD501_AAT_Blue
	{
		displayName="AAT (Aqua)";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"3AS\3AS_AAT\data\Aqua_AAT_CO.paa"};
	};
};