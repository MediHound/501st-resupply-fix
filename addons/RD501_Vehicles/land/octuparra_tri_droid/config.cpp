#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class rd501_patch_name_tri_droid
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			// REMOVE TO HERE
			macro_new_vehicle(magna_tri_droid,Movable)
		};
		weapons[]=
		{
			
		};
	};
};

#include "../../common/sensor_templates.hpp"

class DefaultEventhandlers;
class CfgVehicles
{
	class LandVehicle;
	class StaticWeapon: LandVehicle
	{
		class Turrets
		{
			class MainTurret;
		};
	};
	class StaticMGWeapon: StaticWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{};
		};
	};
	class DBA_Octo_Base: StaticMGWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class ViewOptics;
			};
		};
	};
	class DBA_Octo : DBA_Octo_Base
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class ViewOptics;
			};
		};
	};
	class macro_new_vehicle(magna_tri_droid,Movable):DBA_Octo
	{
        class EventHandlers: DefaultEventhandlers {};
		displayName = "Octuptarra Tri-Droid";
		armor = 900;
		side = 0;
		model = "DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_Octo\Octo.p3d";
		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = 
		{
			"DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_Octo\data\_01_-_Default1.001_co.paa",
			"DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_Octo\data\material.001_co.paa"
		};
		hiddenSelectionsMaterials[]=
        {
            "DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_Octo\data\crab.rvmat"
        };
		icon = "3as\3AS_ADSD\data\ui\ADSD_top_ca.paa";
		explosionshielding = 1.5;
		scope = 2;
		radarTarget = 1;
		RadarTargetSize = 1.2;
		scopeCurator = 2;
		forceInGarage = 1;
		crew = MACRO_QUOTE(macro_new_unit_class(opfor,B1_crew));
		faction = MACRO_QUOTE(macro_faction(CIS));
		editorSubcategory = macro_editor_cat(heavy_armored_infantry);
		vehicleClass = macro_editor_vehicle_type(heavy_armored_infantry);
		class Armory
		{
			description = "";
		};
		threat[] = {1,0.3,0.3};
		cost = 150000;
		getInAction = "";
		getOutAction = "";
		class Damage
		{
			tex[] = {};
			mat[] = {"A3\Static_F_Gamma\data\StaticTurret_01.rvmat","A3\Static_F_Gamma\data\StaticTurret_01_damage.rvmat","A3\Static_F_Gamma\data\StaticTurret_01_destruct.rvmat","A3\Static_F_Gamma\data\StaticTurret_02.rvmat","A3\Static_F_Gamma\data\StaticTurret_02_damage.rvmat","A3\Static_F_Gamma\data\StaticTurret_02_destruct.rvmat","A3\Static_F_Gamma\data\StaticTurret_03.rvmat","A3\Static_F_Gamma\data\StaticTurret_03_damage.rvmat","A3\Static_F_Gamma\data\StaticTurret_03_destruct.rvmat"};
		};
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				turretInfoType = "RscOptics_crows";
				animationSourceBody = "Mainturret";
				Body = "Mainturret";
				animationSourceGun = "Maingun";
				Gun = "Maingun";
				optics = 0;
				minElev = -15;
				maxElev = 90;
				minTurn = -360;
				maxTurn = 360;
				selectionFireAnim = "zasleh";
				soundServo[] = {"A3\Sounds_F\vehicles\boat\Boat_Armed_01\servo_boat_comm",1.4125376,1,30};
				soundServoVertical[] = {"A3\Sounds_F\vehicles\boat\Boat_Armed_01\servo_boat_comm_vertical",1.4125376,1,30};
				weapons[] = 
                {
                    macro_new_weapon(otco,cannon)
                };
				magazines[] = 
                {
                    macro_new_mag(octo_cannon_charge,50),
                    macro_new_mag(octo_cannon_charge,50),
                    macro_new_mag(octo_cannon_charge,50),
                    macro_new_mag(octo_cannon_charge,50),
                    macro_new_mag(octo_cannon_charge,50),
                    macro_new_mag(octo_cannon_charge,50),
                    macro_new_mag(octo_cannon_charge,50),
                    macro_new_mag(octo_cannon_charge,50),
                    macro_new_mag(octo_cannon_charge,50)
                };
				memoryPointGunnerOptics = "gunnerview";
				gunnergetInAction = "";
				gunnergetOutAction = "";
				displayName = "";
				gunnerInAction = "Disabled";
				gunnerAction = "Disabled";
				gunnerForceOptics = 1;
				memoryPointGun[] = {"usti hlavne"};
				class ViewOptics: ViewOptics
				{
					minFov = 0.25;
					maxFov = 1.25;
					initFov = 0.75;
				};
				class HitPoints
				{
					class HitGun
					{
						armor = 2500;
						material = -1;
						name = "gun";
						visual = "autonomous_unhide";
						passThrough = 0;
						radius = 0.2;
					};
					class HitTurret: HitGun
					{
						armor = 2500;
						name = "turret";
					};
				};
			};
		};
		soundGetOut[] = {"A3\sounds_f\dummysound",0.001,1,5};
		soundGetIn[] = {"A3\sounds_f\dummysound",0.000316228,1,5};
        destrType = "DestructBuilding";
		explosionEffect="FuelExplosion";
		class DestructionEffects
		{
			class Dust
			{
				intensity = 0.1;
				interval = 1;
				lifeTime = 0.01;
				position = "destructionEffect2";
				simulation = "particles";
				type = "HousePartDust";
			};
			class Light1
			{
				enabled = "distToWater";
				intensity = 0.1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "light";
				type = "ObjectDestructionLightSmall";
			};
			class Fire1
			{
				intensity = 0.15;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "ObjectDestructionFire1Small";
			};
			class Refract1
			{
				intensity = 1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "SmallFireFRefract";
			};
			class Sound
			{
				intensity = 1;
				interval = 1;
				lifeTime = 1;
				position = "destructionEffect";
				simulation = "sound";
				type = "Fire";
			};
			class sparks1
			{
				intensity = 0.5;
				interval = 1;
				lifeTime = 0;
				position = "destructionEffect2";
				simulation = "particles";
				type = "ObjectDestructionSparks";
			};
			class Smoke1
			{
				simulation="particles";
				type="BarelDestructionSmoke";
				position[]={0,0,0};
				intensity=0.2;
				interval=1;
				lifeTime=1;
			};
			class HouseDestr
			{
				intensity=1;
				interval=1;
				lifeTime=5;
				position="";
				simulation="destroy";
				type="DelayedDestruction";
			};
		};
	};
};