//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon mortar
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define macro_new_mortar_class(name) vehicle_classname##_##name

class CfgPatches
{
	class RD501_patch_mortar
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(mortar,republic),
		};
		weapons[]=
		{
			macro_new_weapon(stat,mortar_carry)
		};
	};
};

//remember to do the flares later on when I do effects.
#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers;
class CfgVehicles
{
	class LandVehicle;
	class StaticWeapon: LandVehicle
	{
		class Turrets;
	};
	class StaticMortar: StaticWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret
			{
				class ViewOptics;
			};
		};
	};
	class Mortar_01_base_F: StaticMortar
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class ViewOptics;
			};
		};
		class assembleInfo;
	};
	class B_Mortar_01_F: Mortar_01_base_F
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret{};
		};
	};
	class macro_new_vehicle(mortar,republic):B_Mortar_01_F
	{
		displayname = "Republic Plasma Mortar";
		forceInGarage = 1;
		_generalMacro = "B_Mortar_01_F";
		availableForSupportTypes[] = {"Artillery"};
		class EventHandlers :DefaultEventhandlers {};
		faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_turrets));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(static_turrets));
		class Turrets: Turrets
		{
			class MainTurret: Mainturret
			{
				
				weapons[] = {"RD501_82mm_Plasma_Mortar"};
				magazines[] =
				{
					"RD501_8rnd_82mm_HE",
					"RD501_8rnd_82mm_Smoke",
					"RD501_8rnd_82mm_Illum",
					"RD501_8rnd_82mm_guided_HE",
					"RD501_8rnd_82mm_laserguided_HE"
				};
			};
		};
		class ACE_Actions
		{
			class ACE_MainActions
			{
				displayName = "Republic Mortar";
				selection = "";
				distance = 2.5;
				condition = "true";
				class ace_csw_pickUp
				{
					displayName = "Pick up Mortar";
					condition = "call ace_csw_fnc_assemble_canPickupTripod";
					statement = "_this call RD501_fnc_internal_ACECSW_pickup";
				};
				class rd501_external_reload 
				{
					displayName = "Reload";
					condition = "_this call rd501_fnc_canReloadExternal";
					statement = "_this call rd501_fnc_reloadExternal";
					exceptions[] = {};
					runOnHover = 0;
				};
			};
		};
		class ace_csw
		{
			disassembleTo = macro_new_weapon(stat,mortar_carry);
		};
	};
};
class CfgWeapons
{
	class Launcher;
	class Launcher_Base_F: Launcher
	{
		class WeaponSlotsInfo;
	};
	class macro_new_weapon(stat,mortar_carry):Launcher_Base_F
	{
		faction = MACRO_QUOTE(macro_faction(republic));
		scope = 2;
		displayName = "Folded Republic Plasma Mortar";
		model = "mortar\mortarlauncher.p3d";
		hiddenSelections[] = {"Camo_1","Camo_2"};
		hiddenSelectionsTextures[] = {"\3as\3as_static\Mortar\data\base.001_co.paa","\3as\3as_static\Mortar\data\tube.001_co.paa"};
		hiddenSelectionsMaterials[] = {"\3as\3as_static\Mortar\data\base.rvmat","\3as\3as_static\Mortar\data\tube.rvmat"};
		picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_backpack_ui_ca.paa";
		mass = 450;
		class ace_csw
		{
			type = "mount";
			deployTime = 2;
			pickupTime = 2;
			deploy = macro_new_vehicle(mortar,republic);
		};
	};
	class ACE_mortar_82mm;
	class RD501_82mm_Plasma_Mortar:ACE_mortar_82mm
	{
		magazines[] =
		{
			"RD501_8rnd_82mm_HE",
			"RD501_8rnd_82mm_Smoke",
			"RD501_8rnd_82mm_Illum",
			"RD501_8rnd_82mm_guided_HE",
			"RD501_8rnd_82mm_laserguided_HE"
		};
	};
};
class CfgMagazines
{
	class ACE_1Rnd_82mm_Mo_HE;
	class ACE_1Rnd_82mm_Mo_Smoke;
	class ACE_1Rnd_82mm_Mo_Illum;
	class ACE_1Rnd_82mm_Mo_HE_Guided;
	class ACE_1Rnd_82mm_Mo_HE_LaserGuided;
	class RD501_8rnd_82mm_HE:ACE_1Rnd_82mm_Mo_HE
	{
		displayName = "[501st] HE Mortar Magazine";
		count = 8;
		ace_arsenal_hide = -1;
	};
	class RD501_8rnd_82mm_Smoke:ACE_1Rnd_82mm_Mo_Smoke
	{
		displayName = "[501st] Smoke Mortar Magazine";
		count = 8;
		ace_arsenal_hide = -1;
	};
	class RD501_8rnd_82mm_Illum:ACE_1Rnd_82mm_Mo_Illum
	{
		displayName = "[501st] Illumination Mortar Magazine";
		count = 8;
		ace_arsenal_hide = -1;
	};
	class RD501_8rnd_82mm_guided_HE:ACE_1Rnd_82mm_Mo_HE_Guided
	{
		displayName = "[501st] Guided Mortar Magazine";
		count = 8;
		ace_arsenal_hide = -1;
	};
	class RD501_8rnd_82mm_laserguided_HE:ACE_1Rnd_82mm_Mo_HE_LaserGuided
	{
		displayName = "[501st] LasGuided Mortar Magazine";
		count = 8;
		ace_arsenal_hide = -1;
	};
};
