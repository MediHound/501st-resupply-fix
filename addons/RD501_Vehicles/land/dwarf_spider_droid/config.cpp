#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class rd501_patch_name_dwarf_spider_droid
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			// REMOVE TO HERE
			macro_new_vehicle(dwarf_spider_droid,MkI),
			macro_new_vehicle(dwarf_spider_droid,Movable_MkI),
			macro_new_vehicle(dwarf_spider_droid,MkII),
			macro_new_vehicle(dwarf_spider_droid,Movable_MkII)
		};
		weapons[]=
		{
			
		};
	};
};

#include "../../common/sensor_templates.hpp"

class DefaultEventhandlers;
class CfgVehicles
{
	class LandVehicle;
	class StaticWeapon: LandVehicle
	{
		class Turrets
		{
			class MainTurret;
		};
	};
	class StaticMGWeapon: StaticWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{};
		};
	};
	class 101st_Crab_Droid : StaticMGWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{};
		};
	};
	class DBA_DSD_base : StaticMGWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class ViewOptics;
			};
		};
	};
	class DBA_DSD : DBA_DSD_base
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class ViewOptics;
			};
		};
	};
	class DBA_DSD3_base : StaticMGWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class ViewOptics;
			};
		};
	};
	class DBA_DSD3 : DBA_DSD3_base
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class ViewOptics;
			};
		};
	};
	class macro_new_vehicle(dwarf_spider_droid,MkI):DBA_DSD
	{
		displayName = "DSD1";
		scope = 2;
		model = "DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_DSD\DSD.p3d";
		hiddenSelections[] = {"body","lights"};
		hiddenSelectionsTextures[] = {"DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_DSD\DSD_Main_CO.paa","DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_DSD\DSD_Lights_CO.paa"};
		icon = "iconStaticMG";
		extCameraPosition[] = {0,1.5,-10};
		forceInGarage = 1;
		class EventHandlers: DefaultEventhandlers {}; 
		faction = macro_faction(CIS);
		editorSubcategory = macro_editor_cat(heavy_armored_infantry);
		vehicleClass = macro_editor_vehicle_type(heavy_armored_infantry);
		ace_dragging_canCarry = 0;
		ace_dragging_canDrag = 0;
		armor = 150;
		armorStructural = 1.0;
		explosionShielding	= 0.1;
		minTotalDamageThreshold	= 0.01;
		impactDamageMultiplier = 0.1;
		enableGPS = 1;
		radarType = 4;
		gunnerCanSee = "1+2+4+16";
		commanderCanSee = "1+2+4+16";
		driverCanSee = "1+2+4+16";
		radarTarget = 1;
		radarTargetSize = 1.2;
		visualTarget = 1;
		visualTargetSize = 1.2;
		irTargetSize = 0;
		reportRemoteTargets = 1;
		receiveRemoteTargets = 1;
        uavCameraGunnerPos = "PiP_pos";
		uavCameraGunnerDir = "PiP_dir";
        irTarget = 1;
		irScanRangeMin = 50;
		irScanRangeMax = 10500;
		irScanToEyeFactor = 4;
		irScanGround = 1;
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				turretInfoType = "RscOptics_crows";
				animationSourceBody = "Mainturret";
				Body = "Mainturret";
				animationSourceGun = "Maingun";
				Gun = "Maingun";
				optics = 1;
				minElev = -5;
				maxElev = 35;
				minTurn = -360;
				maxTurn = 360;
				selectionFireAnim = "zasleh";
				soundServo[] = {"A3\Sounds_F\vehicles\boat\Boat_Armed_01\servo_boat_comm",1.4125376,1,30};
				soundServoVertical[] = {"A3\Sounds_F\vehicles\boat\Boat_Armed_01\servo_boat_comm_vertical",1.4125376,1,30};
				weapons[] = 
				{
					macro_new_weapon(dsd,cannon)
				};
				magazines[] = 
				{
					macro_new_mag(dsd_cannon_charge,20),
					macro_new_mag(dsd_cannon_charge,20),
					macro_new_mag(dsd_cannon_charge,20),
					macro_new_mag(dsd_cannon_charge,20),
					macro_new_mag(dsd_cannon_charge,20)
				};
				memoryPointGunnerOptics = "gunnerview";
				gunnergetInAction = "";
				gunnergetOutAction = "";
				displayName = "";
				gunnerInAction = "Disabled";
				gunnerAction = "Disabled";
				gunnerForceOptics = 1;
				memoryPointGun[] = {"usti hlavne"};
				class HitPoints
				{
					class HitGun
					{
						armor = 1;
						material = -1;
						name = "gun";
						visual = "autonomous_unhide";
						passThrough = 0;
						radius = 0.2;
					};
					class HitTurret: HitGun
					{
						armor = 1;
						name = "turret";
						class DestructionEffects
						{
							class Smoke
							{
								simulation = "particles";
								type = "WeaponWreckSmoke";
								position = "destructionEffect";
								intensity = 1;
								interval = 1;
								lifeTime = 5;
							};
						};
					};
				};
			};
		};
		destrType = "DestructBuilding";
		explosionEffect="FuelExplosion";
		class DestructionEffects
		{
			class Dust
			{
				intensity = 0.1;
				interval = 1;
				lifeTime = 0.01;
				position = "destructionEffect2";
				simulation = "particles";
				type = "HousePartDust";
			};
			class Light1
			{
				enabled = "distToWater";
				intensity = 0.1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "light";
				type = "ObjectDestructionLightSmall";
			};
			class Fire1
			{
				intensity = 0.15;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "ObjectDestructionFire1Small";
			};
			class Refract1
			{
				intensity = 1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "SmallFireFRefract";
			};
			class Sound
			{
				intensity = 1;
				interval = 1;
				lifeTime = 1;
				position = "destructionEffect";
				simulation = "sound";
				type = "Fire";
			};
			class sparks1
			{
				intensity = 0.5;
				interval = 1;
				lifeTime = 0;
				position = "destructionEffect2";
				simulation = "particles";
				type = "ObjectDestructionSparks";
			};
			class Smoke1
			{
				simulation="particles";
				type="BarelDestructionSmoke";
				position[]={0,0,0};
				intensity=0.2;
				interval=1;
				lifeTime=1;
			};
			class HouseDestr
			{
				intensity=1;
				interval=1;
				lifeTime=5;
				position="";
				simulation="destroy";
				type="DelayedDestruction";
			};
		};
	};
	class macro_new_vehicle(dwarf_spider_droid,Movable_MkI): macro_new_vehicle(dwarf_spider_droid,MkI)
	{
		displayName = "DSD1 (Moveable)";
		forceInGarage = 0;
	};
	class macro_new_vehicle(dwarf_spider_droid,MkII): DBA_DSD3
	{
		displayName = "DSD3";
		scope = 2;
		model = "DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_DSD3\DSD3.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_DSD3\data\yeet_co.paa"};
		icon = "iconStaticMG";
		forceInGarage = 1;
		armor = 100;
		armorStructural = 1.0;
		explosionShielding	= 0.1;
		minTotalDamageThreshold	= 0.01;
		impactDamageMultiplier = 0.1;
		class EventHandlers: DefaultEventhandlers {}; 
		faction = macro_faction(CIS);
		editorSubcategory = macro_editor_cat(heavy_armored_infantry);
		vehicleClass = macro_editor_vehicle_type(heavy_armored_infantry);
		ace_dragging_canCarry = 0;
		ace_dragging_canDrag = 0;
		gunnerCanSee = "1+2+4+16";
		commanderCanSee = "1+2+4+16";
		driverCanSee = "1+2+4+16";
		radarTarget = 1;
		radarTargetSize = 1.2;
		visualTarget = 1;
		visualTargetSize = 1.2;
		irTargetSize = 0;
		reportRemoteTargets = 1;
		receiveRemoteTargets = 1;
        uavCameraGunnerPos = "PiP_pos";
		uavCameraGunnerDir = "PiP_dir";
        irTarget = 1;
		irScanRangeMin = 50;
		irScanRangeMax = 10500;
		irScanToEyeFactor = 4;
		irScanGround = 1;
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[] = 
				{
					macro_new_weapon(dsd,cannon)
				};
				magazines[] = 
				{
					macro_new_mag(dsd_cannon_charge,20),
					macro_new_mag(dsd_cannon_charge,20),
					macro_new_mag(dsd_cannon_charge,20),
					macro_new_mag(dsd_cannon_charge,20),
					macro_new_mag(dsd_cannon_charge,20),
					macro_new_mag(dsd_cannon_charge,20),
					macro_new_mag(dsd_cannon_charge,20),
					macro_new_mag(dsd_cannon_charge,20)
				};
			};
		};
		destrType = "DestructBuilding";
		explosionEffect="FuelExplosion";
		class DestructionEffects
		{
			class Dust
			{
				intensity = 0.1;
				interval = 1;
				lifeTime = 0.01;
				position = "destructionEffect2";
				simulation = "particles";
				type = "HousePartDust";
			};
			class Light1
			{
				enabled = "distToWater";
				intensity = 0.1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "light";
				type = "ObjectDestructionLightSmall";
			};
			class Fire1
			{
				intensity = 0.15;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "ObjectDestructionFire1Small";
			};
			class Refract1
			{
				intensity = 1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "SmallFireFRefract";
			};
			class Sound
			{
				intensity = 1;
				interval = 1;
				lifeTime = 1;
				position = "destructionEffect";
				simulation = "sound";
				type = "Fire";
			};
			class sparks1
			{
				intensity = 0.5;
				interval = 1;
				lifeTime = 0;
				position = "destructionEffect2";
				simulation = "particles";
				type = "ObjectDestructionSparks";
			};
			class Smoke1
			{
				simulation="particles";
				type="BarelDestructionSmoke";
				position[]={0,0,0};
				intensity=0.2;
				interval=1;
				lifeTime=1;
			};
			class HouseDestr
			{
				intensity=1;
				interval=1;
				lifeTime=5;
				position="";
				simulation="destroy";
				type="DelayedDestruction";
			};
		};
	};
	class macro_new_vehicle(dwarf_spider_droid,Movable_MkII): macro_new_vehicle(dwarf_spider_droid,MkII)
	{
		displayName = "DSD3 (Mobile)";
		forceInGarage = 0;
	};
};