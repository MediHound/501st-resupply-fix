//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon saber
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define new_saber_class(name) vehicle_classname##_##name

#define macro_republic_skin hiddenSelectionsTextures[] = {"tx130s\imp_tank_rep_a_c.paa"};

//common mag-weap
#define macro_common_weap\
        "Laserdesignator_pilotCamera"

#define macro_common_mag\
        "Laserbatteries"

//super saber mag-weap
#define macro_super_saber_weap\
        macro_common_weap,\
        macro_new_weapon(saber_gun,mbt)

#define macro_super_saber_mag\
        macro_common_mag,\
        macro_new_mag(saber_tx130,10),\
        macro_new_mag(saber_tx130,10),\
        macro_new_mag(saber_tx130,10),\
        macro_new_mag(saber_tx130,10),\
        macro_new_mag(saber_tx130,10),

//normal saber mag-weap
#define macro_saber_weap\
        macro_common_weap,\
        macro_new_weapon(super_mg,z6)

#define macro_saber_mag\
        macro_common_mag,\
        macro_new_mag(supercharged_z6,400),\
        macro_new_mag(supercharged_z6,400),\
        macro_new_mag(supercharged_z6,400),\
        macro_new_mag(supercharged_z6,400),\
        macro_new_mag(supercharged_z6,400),\
        macro_new_mag(supercharged_z6,400),\
        macro_new_mag(supercharged_z6,400)

#define macro_saber_weap_imperial\
        macro_common_weap,\
        macro_new_weapon(super_mg,dlt19)

#define macro_saber_mag_imperial\
        macro_common_mag,\
        macro_new_mag(supercharged_dlt19,200),\
        macro_new_mag(supercharged_dlt19,200),\
        macro_new_mag(supercharged_dlt19,200),\
        macro_new_mag(supercharged_dlt19,200),\
        macro_new_mag(supercharged_dlt19,200),\
        macro_new_mag(supercharged_dlt19,200),\
        macro_new_mag(supercharged_dlt19,200)

#define macro_saber_main_weap\
        macro_common_weap,\
        macro_new_weapon(saber_gun,td),\
        "SmokeLauncher"

#define macro_saber_main_mag\
        macro_common_mag,\
        macro_new_mag(saber_tx130,10),\
        macro_new_mag(saber_tx130,10),\
        macro_new_mag(saber_tx130,10),\
        macro_new_mag(saber_tx130,10),\
        "smokelaunchermag",\
        "smokelaunchermag",\
        "smokelaunchermag",\
        "smokelaunchermag",\
        "smokelaunchermag",\
        "smokelaunchermag",\
        "smokelaunchermag",\
        "smokelaunchermag",\
        "smokelaunchermag"
        
#define macro_saber_driver\
        weapons[] = {"SmokeLauncher","Laserdesignator_pilotCamera","CMFlareLauncher"};\
        magazines[] = {"300Rnd_CMFlare_Chaff_Magazine","Laserbatteries","smokelaunchermag","smokelaunchermag","smokelaunchermag","smokelaunchermag","smokelaunchermag","smokelaunchermag","smokelaunchermag","smokelaunchermag"};

#define macro_command_saber_smoke\
        smokeLauncherGrenadeCount = 12;\
        smokeLauncherVelocity = 14;\
        smokeLauncherOnTurret = 1;\
        smokeLauncherAngle = 360;

#define macro_saber_smoke\
        smokeLauncherGrenadeCount = 24;\
        smokeLauncherVelocity = 14;\
        smokeLauncherOnTurret = 0;\
        smokeLauncherAngle = 160;

class CfgPatches
{
    class RD501_patch_saber
    {
        addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

        requiredAddons[]=
        {
            RD501_patch_vehicles
        };
        requiredVersion=0.1;
        units[]=
        {
            macro_new_vehicle(saber,republic_tx130_MkII),
            macro_new_vehicle(saber,republic_tx130_gl_MkII),
            macro_new_vehicle(saber,republic_tx130_super_MkII),
            macro_new_vehicle(saber,republic_tx130_recon_MkII)
            /*macro_new_vehicle(saber,empire_super_saber_MkII),
            macro_new_vehicle(saber,empire_saber_MkII),
            macro_new_vehicle(saber,empire_command_saber_MkII),
            macro_new_vehicle(saber,republic_super_saber_MkII),
            macro_new_vehicle(saber,republic_saber_MkII),
            macro_new_vehicle(saber,republic_command_saber_MkII)*/
        };
        weapons[]=
        {
            
        };
    };
};


#include "../../common/sensor_templates.hpp"

class DefaultEventhandlers;
class RCWSOptics;
class Optics_Armored;
class Optics_Gunner_APC_01: Optics_Armored
{
    class Wide;
    class Medium;
    class Narrow;
};
class CfgVehicles
{
    //O_JM_TX130_1  empire command
    //O_JM_TX130m1_1 normal
    //O_JM_TX130m2_1 super
class LandVehicle;
    class Tank: LandVehicle
    {
        class NewTurret;
        class Sounds;
        class HitPoints;
    };
    class Tank_F: Tank
    {
        class Turrets
        {
            class MainTurret: NewTurret
            {
                class Turrets
                {
                    class CommanderOptics;
                };
            };
        };
        class CargoTurret;
        class HitPoints: HitPoints
        {
            class HitHull;
            class HitEngine;
            class HitLTrack;
            class HitRTrack;
            class HitFuel;
        };
    };
    class 3as_saber_01_Base: Tank_F
    {
        RD501_magclamp_small_offset[] = {0.0,0.0,-3.0};
        RD501_magclamp_large_offset[] = {0.0,0.0,-3.0};
        class Turrets:Turrets
        {
            class MainTurret_bottom: NewTurret{};
            class Mainturret_top:MainTurret_bottom{};
            class CargoTurret_01;
            class CargoTurret_02;
            class CargoTurret_03;
            class CargoTurret_04;
        };
        class HitPoints: HitPoints
        {
            class HitHull:HitHull{};
            class HitEngine:HitEngine{};
            class HitLTrack:HitLTrack{};
            class HitRTrack:HitRTrack{};
            class HitFuel:HitFuel{};
        };
    };

     //#include "inheritance.hpp"
    class 3as_saber_Base:3as_saber_01_Base
    {
        class Turrets:Turrets
        {
            class MainTurret_bottom:MainTurret_bottom{};
            class Mainturret_top:Mainturret_top{};
            class CargoTurret_01: CargoTurret_01{};
            class CargoTurret_02: CargoTurret_02{};
            class CargoTurret_03: CargoTurret_03{};
            class CargoTurret_04: CargoTurret_04{};
        };
        class HitPoints: HitPoints
        {
            class HitHull:HitHull{};
            class HitEngine:HitEngine{};
            class HitLTrack:HitLTrack{};
            class HitRTrack:HitRTrack{};
            class HitFuel:HitFuel{};
        };
    };
    class 3as_saber_m1:3as_saber_Base
    {
        class Turrets: Turrets
        {
            class MainTurret_bottom:MainTurret_bottom{};
            class Mainturret_top:Mainturret_top{};
            class CargoTurret_01: CargoTurret_01{};
            class CargoTurret_02: CargoTurret_02{};
            class CargoTurret_03: CargoTurret_03{};
            class CargoTurret_04: CargoTurret_04{};
        };
        class HitPoints: HitPoints
        {
            class HitHull:HitHull{};
            class HitEngine:HitEngine{};
            class HitLTrack:HitLTrack{};
            class HitRTrack:HitRTrack{};
            class HitFuel:HitFuel{};
        };
    };
    class 3as_saber_02_Base: 3as_saber_01_Base
    {
        class HitPoints: HitPoints
        {
            class HitHull:HitHull{};
            class HitEngine:HitEngine{};
            class HitLTrack:HitLTrack{};
            class HitRTrack:HitRTrack{};
            class HitFuel:HitFuel{};
        };
    };
    class 3as_saber_m1G : 3as_saber_02_Base
    {
        class HitPoints: HitPoints
        {
            class HitHull:HitHull{};
            class HitEngine:HitEngine{};
            class HitLTrack:HitLTrack{};
            class HitRTrack:HitRTrack{};
            class HitFuel:HitFuel{};
        };
    };
    class 3as_saber_super_base:3as_saber_01_Base
    {
        class Turrets:Turrets
        {
            class Mainturret_super: MainTurret_bottom{};
            class CargoTurret_01: CargoTurret_01{};
            class CargoTurret_02: CargoTurret_02{};
            class CargoTurret_03: CargoTurret_03{};
            class CargoTurret_04: CargoTurret_04{};
        };
    };
    class 3as_saber_super:3as_saber_super_base
    {
        class Turrets: Turrets
        {
            class Mainturret_super: Mainturret_super{};
            class CargoTurret_01: CargoTurret_01{};
            class CargoTurret_02: CargoTurret_02{};
            class CargoTurret_03: CargoTurret_03{};
            class CargoTurret_04: CargoTurret_04{};
        };
    };
    class 3as_saber_03_Base:3as_saber_01_Base
    {
        class Turrets:Turrets
        {
            class MainTurret_top: Mainturret_top{};
            class MainTurret_bottom: MainTurret_bottom{};
            class CargoTurret_01: CargoTurret_01{};
            class CargoTurret_02: CargoTurret_02{};
            class CargoTurret_03: CargoTurret_03{};
            class CargoTurret_04: CargoTurret_04{};
        };
    };
    class 3as_saber_m1Recon:3as_saber_03_Base
    {
        class Turrets: Turrets
        {
            class MainTurret_top: Mainturret_top{};
            class MainTurret_bottom: MainTurret_bottom{};
            class CargoTurret_01: CargoTurret_01{};
            class CargoTurret_02: CargoTurret_02{};
            class CargoTurret_03: CargoTurret_03{};
            class CargoTurret_04: CargoTurret_04{};
        };
    };
    class macro_new_vehicle(saber,republic_tx130_MkII):3as_saber_m1
    {
        scope=2;
        forceInGarage = 1;
        scopeCurator=2;
        displayName = "Saber TX-130 Mk.II";
        faction = MACRO_QUOTE(macro_faction(republic));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(tank));
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(tank));
        #include "../../common/small-inventory.hpp"
        armor = 600;
        armorStructural=5;
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"3AS\3as_saber\data\saber_501_hull_co.paa","3AS\3as_saber\data\saber_501_weapons_co.paa"};
        editorPreview = "\3as\3as_saber\images\3as_saber_m1_501.jpg";
        class HitPoints: HitPoints
        {
            class HitHull: HitHull
            {
                armor = 4.5;
                material = -1;
                name = "telo";
                visual = "zbytek";
                passThrough = 1;
                minimalHit = 0.2;
                explosionShielding = 0.2;
                radius = 0.275;
            };
            class HitEngine: HitEngine
            {
                armor = 0.3;
                material = -1;
                name = "motor";
                passThrough = 0.2;
                minimalHit = 0.2;
                explosionShielding = 0.2;
                radius = 0.33;
            };
            class HitLTrack: HitLTrack
            {
                armor = 4.5;
                material = -1;
                name = "track_l_hit";
                passThrough = 1;
                minimalHit = 0.08;
                explosionShielding = 0.8;
                radius = 0.3;
            };
            class HitRTrack: HitRTrack
            {
                armor = 4.5;
                material = -1;
                name = "track_r_hit";
                passThrough = 1;
                minimalHit = 0.08;
                explosionShielding = 0.8;
                radius = 0.3;
            };
            class HitFuel: HitFuel
            {
                armor = 4.5;
                material = -1;
                name = "palivo";
                passThrough = 1;
                minimalHit = 0.1;
                explosionShielding = 0.6;
                radius = 0.25;
            };
        };
        class Turrets: Turrets
        {
            class MainTurret_bottom : MainTurret_bottom
            {
                weapons[]=
                {
                    macro_new_weapon(saber_main,cannon),
                    "SmokeLauncher",
                    "missiles_DAGR"
                };
                magazines[]=
                {
                    macro_new_mag(saber_main,10),
                    macro_new_mag(saber_main,10),
                    macro_new_mag(saber_main,10),
                    macro_new_mag(saber_main,10),
                    macro_new_mag(saber_main,10),
                    macro_new_mag(saber_main,10),
                    macro_new_mag(saber_main,10),
                    "SmokeLauncherMag",
                    "12Rnd_PG_missiles"
                };
            };
            class Mainturret_top : Mainturret_top
            {
                weapons[]=
                {
                    macro_new_weapon(saber_mg,50cal),
                    "SmokeLauncher"
                };
                magazines[]=
                {
                    macro_new_mag(vehicle_eweb,500),
                    macro_new_mag(vehicle_eweb,500),
                    macro_new_mag(vehicle_eweb,500),
                    macro_new_mag(vehicle_eweb,500),
                    macro_new_mag(vehicle_eweb,500),
                    macro_new_mag(vehicle_eweb,500),
                    macro_new_mag(vehicle_eweb,500),
                    "SmokeLauncherMag"
                };
            };
            class CargoTurret_01: CargoTurret_01
            {
                gunnerAction = "passenger_bench_1";
                canHideGunner = 0;
                gunnerCompartments = "Compartment2";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                gunnerName = "Passenger - Right (Front)";
                proxyIndex = 1;
                soundAttenuationTurret = "HeliAttenuationGunner";
                isPersonTurret = 1;
                ejectDeadGunner = 1;
                class dynamicViewLimits
                {
                    CargoTurret_04[] = {-65,95};
                };
                playerPosition = 4;
                gunnerGetInAction = "GetInHeli_Light_01bench";
            };
            class CargoTurret_02: CargoTurret_01
            {
                gunnerCompartments = "Compartment3";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                gunnerName = "Passenger - Left (Rear)";
                proxyIndex = 2;
                class dynamicViewLimits
                {
                    CargoTurret_03[] = {-65,95};
                };
            };
            class CargoTurret_03: CargoTurret_02
            {
                gunnerCompartments = "Compartment3";
                gunnerName = "Passenger - Left (Front)";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                proxyIndex = 3;
                class dynamicViewLimits
                {
                    CargoTurret_02[] = {-95,65};
                };
            };
            class CargoTurret_04: CargoTurret_01
            {
                gunnerCompartments = "Compartment2";
                gunnerName = "Passenger - Right (Rear)";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                proxyIndex = 4;
                class dynamicViewLimits
                {
                    CargoTurret_01[] = {-95,65};
                };
            };
        };
    };
    class macro_new_vehicle(saber,republic_tx130_gl_MkII):3as_saber_m1
    {
        scope=2;
        forceInGarage = 1;
        scopeCurator = 2;
        displayName = "Saber TX-130 Mk.II GL";
        faction = MACRO_QUOTE(macro_faction(republic));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(tank));
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(tank));
        #include "../../common/small-inventory.hpp"
        armor = 600;
        armorStructural=5;
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"3AS\3as_saber\data\saber_501_hull_co.paa","3AS\3as_saber\data\saber_501_weapons_co.paa"};
        editorPreview = "\3as\3as_saber\images\3as_saber_m1_501.jpg";
        class HitPoints: HitPoints
        {
            class HitHull: HitHull
            {
                armor = 4.5;
                material = -1;
                name = "telo";
                visual = "zbytek";
                passThrough = 1;
                minimalHit = 0.2;
                explosionShielding = 0.2;
                radius = 0.275;
            };
            class HitEngine: HitEngine
            {
                armor = 0.3;
                material = -1;
                name = "motor";
                passThrough = 0.2;
                minimalHit = 0.2;
                explosionShielding = 0.2;
                radius = 0.33;
            };
            class HitLTrack: HitLTrack
            {
                armor = 4.5;
                material = -1;
                name = "track_l_hit";
                passThrough = 1;
                minimalHit = 0.08;
                explosionShielding = 0.8;
                radius = 0.3;
            };
            class HitRTrack: HitRTrack
            {
                armor = 4.5;
                material = -1;
                name = "track_r_hit";
                passThrough = 1;
                minimalHit = 0.08;
                explosionShielding = 0.8;
                radius = 0.3;
            };
            class HitFuel: HitFuel
            {
                armor = 4.5;
                material = -1;
                name = "palivo";
                passThrough = 1;
                minimalHit = 0.1;
                explosionShielding = 0.6;
                radius = 0.25;
            };
        };
        class Turrets: Turrets
        {
            class MainTurret_bottom : MainTurret_bottom
            {
                weapons[]=
                {
                    macro_new_weapon(saber_main,cannon),
                    "SmokeLauncher",
                    "missiles_DAGR"
                };
                magazines[]=
                {
                    macro_new_mag(saber_main,10),
                    macro_new_mag(saber_main,10),
                    macro_new_mag(saber_main,10),
                    macro_new_mag(saber_main,10),
                    macro_new_mag(saber_main,10),
                    macro_new_mag(saber_main,10),
                    macro_new_mag(saber_main,10),
                    "SmokeLauncherMag",
                    "12Rnd_PG_missiles"
                };
            };
            class Mainturret_top : Mainturret_top
            {
                weapons[] = 
                {
                    macro_new_weapon(saber_gmg,40mm),
                    "SmokeLauncher"
                };
                magazines[] = 
                {
                    macro_new_mag(ragl40he,40),
                    macro_new_mag(ragl40he,40),
                    macro_new_mag(ragl40he,40),
                    macro_new_mag(ragl40he,40),
                    macro_new_mag(ragl40he,40),
                    macro_new_mag(ragl40he,40),
                    macro_new_mag(ragl40he,40),
                    "SmokeLauncherMag"
                };
            };
            class CargoTurret_01: CargoTurret_01
            {
                gunnerAction = "passenger_bench_1";
                canHideGunner = 0;
                gunnerCompartments = "Compartment2";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                gunnerName = "Passenger - Right (Front)";
                proxyIndex = 1;
                soundAttenuationTurret = "HeliAttenuationGunner";
                isPersonTurret = 1;
                ejectDeadGunner = 1;
                class dynamicViewLimits
                {
                    CargoTurret_04[] = {-65,95};
                };
                playerPosition = 4;
                gunnerGetInAction = "GetInHeli_Light_01bench";
            };
            class CargoTurret_02: CargoTurret_01
            {
                gunnerCompartments = "Compartment3";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                gunnerName = "Passenger - Left (Rear)";
                proxyIndex = 2;
                class dynamicViewLimits
                {
                    CargoTurret_03[] = {-65,95};
                };
            };
            class CargoTurret_03: CargoTurret_02
            {
                gunnerCompartments = "Compartment3";
                gunnerName = "Passenger - Left (Front)";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                proxyIndex = 3;
                class dynamicViewLimits
                {
                    CargoTurret_02[] = {-95,65};
                };
            };
            class CargoTurret_04: CargoTurret_01
            {
                gunnerCompartments = "Compartment2";
                gunnerName = "Passenger - Right (Rear)";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                proxyIndex = 4;
                class dynamicViewLimits
                {
                    CargoTurret_01[] = {-95,65};
                };
            };
        };
    };
    class macro_new_vehicle(saber,republic_tx130_super_MkII):3as_saber_super
    {
        scope=2;
        forceInGarage = 1;
        scopeCurator=2;
        displayName = "Saber TX-130 Super Mk.II";
        faction = MACRO_QUOTE(macro_faction(republic));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(tank));
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(tank));
        armor = 600;
        armorStructural=5;
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"3AS\3as_saber\data\saber_501_hull_co.paa","3AS\3as_saber\data\saber_501_weapons_co.paa"};
        editorPreview = "\3as\3as_saber\images\3as_saber_super_501.jpg";
        class HitPoints: HitPoints
        {
            class HitHull: HitHull
            {
                armor = 4.5;
                material = -1;
                name = "telo";
                visual = "zbytek";
                passThrough = 1;
                minimalHit = 0.2;
                explosionShielding = 0.2;
                radius = 0.275;
            };
            class HitEngine: HitEngine
            {
                armor = 0.3;
                material = -1;
                name = "motor";
                passThrough = 0.2;
                minimalHit = 0.2;
                explosionShielding = 0.2;
                radius = 0.33;
            };
            class HitLTrack: HitLTrack
            {
                armor = 4.5;
                material = -1;
                name = "track_l_hit";
                passThrough = 1;
                minimalHit = 0.08;
                explosionShielding = 0.8;
                radius = 0.3;
            };
            class HitRTrack: HitRTrack
            {
                armor = 4.5;
                material = -1;
                name = "track_r_hit";
                passThrough = 1;
                minimalHit = 0.08;
                explosionShielding = 0.8;
                radius = 0.3;
            };
            class HitFuel: HitFuel
            {
                armor = 4.5;
                material = -1;
                name = "palivo";
                passThrough = 1;
                minimalHit = 0.1;
                explosionShielding = 0.6;
                radius = 0.25;
            };
        };
        #include "../../common/small-inventory.hpp"
        class Turrets: Turrets
        {
            class Mainturret_super: Mainturret_super
            {
                weapons[]=
                {
                    macro_new_weapon(saber_cannon,supermkii),
                    macro_new_weapon(saber_super_mg,50cal),
                    "SmokeLauncher"
                };
                magazines[]=
                {
                    macro_new_mag(saber_super_3as,25),
                    macro_new_mag(saber_super_3as,25),
                    macro_new_mag(saber_super_3as,25),
                    macro_new_mag(saber_super_3as,25),
                    macro_new_mag(saber_super_3as,25),
                    macro_new_mag(saber_super_3as,25),
                    macro_new_mag(saber_super_3as,25),
                    macro_new_mag(vehicle_eweb,500),
                    macro_new_mag(vehicle_eweb,500),
                    macro_new_mag(vehicle_eweb,500),
                    macro_new_mag(vehicle_eweb,500),
                    macro_new_mag(vehicle_eweb,500),
                    "SmokeLauncherMag"
                };
            };
            class CargoTurret_01: CargoTurret_01
            {
                gunnerAction = "passenger_bench_1";
                canHideGunner = 0;
                gunnerCompartments = "Compartment2";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                gunnerName = "Passenger - Right (Front)";
                proxyIndex = 1;
                soundAttenuationTurret = "HeliAttenuationGunner";
                isPersonTurret = 1;
                ejectDeadGunner = 1;
                class dynamicViewLimits
                {
                    CargoTurret_04[] = {-65,95};
                };
                playerPosition = 4;
                gunnerGetInAction = "GetInHeli_Light_01bench";
            };
            class CargoTurret_02: CargoTurret_01
            {
                gunnerCompartments = "Compartment3";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                gunnerName = "Passenger - Left (Rear)";
                proxyIndex = 2;
                class dynamicViewLimits
                {
                    CargoTurret_03[] = {-65,95};
                };
            };
            class CargoTurret_03: CargoTurret_02
            {
                gunnerCompartments = "Compartment3";
                gunnerName = "Passenger - Left (Front)";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                proxyIndex = 3;
                class dynamicViewLimits
                {
                    CargoTurret_02[] = {-95,65};
                };
            };
            class CargoTurret_04: CargoTurret_01
            {
                gunnerCompartments = "Compartment2";
                gunnerName = "Passenger - Right (Rear)";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                proxyIndex = 4;
                class dynamicViewLimits
                {
                    CargoTurret_01[] = {-95,65};
                };
            };
        };
    };
    class macro_new_vehicle(saber,republic_tx130_recon_MkII):3as_saber_m1Recon
    {
        scope=2;
        forceInGarage = 1;
        scopeCurator=2;
        displayName = "Saber TX-130 Recon Mk.II";
        faction = MACRO_QUOTE(macro_faction(republic));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(tank));
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(tank));
        #include "../../common/small-inventory.hpp"
        armor = 600;
        armorStructural=5;
        hiddenSelections[] = {"camo1","camo2"};
        editorPreview = "\3as\3as_saber\images\3as_saber_m1Recon_501.jpg";
        hiddenSelectionsTextures[] = {"3AS\3as_saber\data\saber_501_hull_co.paa","3as\3as_saber\data\saber_weapons_scout_501_co.paa"};
        class HitPoints: HitPoints
        {
            class HitHull: HitHull
            {
                armor = 4.5;
                material = -1;
                name = "telo";
                visual = "zbytek";
                passThrough = 1;
                minimalHit = 0.2;
                explosionShielding = 0.2;
                radius = 0.275;
            };
            class HitEngine: HitEngine
            {
                armor = 0.3;
                material = -1;
                name = "motor";
                passThrough = 0.2;
                minimalHit = 0.2;
                explosionShielding = 0.2;
                radius = 0.33;
            };
            class HitLTrack: HitLTrack
            {
                armor = 4.5;
                material = -1;
                name = "track_l_hit";
                passThrough = 1;
                minimalHit = 0.08;
                explosionShielding = 0.8;
                radius = 0.3;
            };
            class HitRTrack: HitRTrack
            {
                armor = 4.5;
                material = -1;
                name = "track_r_hit";
                passThrough = 1;
                minimalHit = 0.08;
                explosionShielding = 0.8;
                radius = 0.3;
            };
            class HitFuel: HitFuel
            {
                armor = 4.5;
                material = -1;
                name = "palivo";
                passThrough = 1;
                minimalHit = 0.1;
                explosionShielding = 0.6;
                radius = 0.25;
            };
        };
        class Turrets: Turrets
        {
            class MainTurret_top: Mainturret_top{};
            class MainTurret_bottom: MainTurret_bottom
            {
                weapons[]=
                {
                    macro_new_weapon(saber_cannon,reconautocannon),
                    "RD501_Saber_MLRS_Weapon",
                    "SmokeLauncher",
                };
                magazines[]=
                {
                    macro_new_mag(saber_recon,100),
                    macro_new_mag(saber_recon,100),
                    macro_new_mag(saber_recon,100),
                    macro_new_mag(saber_recon,100),
                    macro_new_mag(saber_recon,100),
                    macro_new_mag(saber_recon,100),
                    "RD501_2Rnd_Saber_MLRS_ATGM_Magazine",
                    "RD501_2Rnd_Saber_MLRS_ATGM_Magazine",
                    //"RD501_2Rnd_Saber_MLRS_HEGM_Magazine",
                    //"RD501_2Rnd_Saber_MLRS_HEGM_Magazine",
                    "SmokeLauncherMag"
                };
            };
            class CargoTurret_01: CargoTurret_01
            {
                gunnerAction = "passenger_bench_1";
                canHideGunner = 0;
                gunnerCompartments = "Compartment2";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                gunnerName = "Passenger - Right (Front)";
                proxyIndex = 1;
                soundAttenuationTurret = "HeliAttenuationGunner";
                isPersonTurret = 1;
                ejectDeadGunner = 1;
                class dynamicViewLimits
                {
                    CargoTurret_04[] = {-65,95};
                };
                playerPosition = 4;
                gunnerGetInAction = "GetInHeli_Light_01bench";
            };
            class CargoTurret_02: CargoTurret_01
            {
                gunnerCompartments = "Compartment3";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                gunnerName = "Passenger - Left (Rear)";
                proxyIndex = 2;
                class dynamicViewLimits
                {
                    CargoTurret_03[] = {-65,95};
                };
            };
            class CargoTurret_03: CargoTurret_02
            {
                gunnerCompartments = "Compartment3";
                gunnerName = "Passenger - Left (Front)";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                proxyIndex = 3;
                class dynamicViewLimits
                {
                    CargoTurret_02[] = {-95,65};
                };
            };
            class CargoTurret_04: CargoTurret_01
            {
                gunnerCompartments = "Compartment2";
                gunnerName = "Passenger - Right (Rear)";
                memoryPointsGetInGunner = "pos driver";
                memoryPointsGetInGunnerDir = "pos driver dir";
                proxyIndex = 4;
                class dynamicViewLimits
                {
                    CargoTurret_01[] = {-95,65};
                };
            };
        };
    };
};

class CfgWeapons
{
    class MGun;
    class 3AS_Sabre_Missiles;
    class 3AS_Sabre_MG: MGun
    {
        class manual;			
    };
    class 3AS_Sabre_Cannons: MGun
    {
        class manual;			
    };
    class 3AS_Sabre_Cannons_Super:3AS_Sabre_Cannons
    {
        class manual;			
    };
    class LMG_M200;
    class 3as_saber_autocannon_30mm:LMG_M200
    {
        class HE;
    };
    class ACE_cannon_120mm_GT12;
    class macro_new_weapon(saber_main,cannon): 3AS_Sabre_Cannons
    {
        fireLightDiffuse[] = {0,0,1};
        fireLightAmbient[] = {0,0,1};
        magazines[]=
        {
            macro_new_mag(saber_main,10)
        };
    };
    class macro_new_weapon(saber_mg,50cal):3AS_Sabre_MG
    {
        magazineReloadTime=3;
        displayName = "Saber MG";
        fireLightDiffuse[] = {0,0,1};
        fireLightAmbient[] = {0,0,1};
        class manual: mgun
        {
            reloadTime = 0.105;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"LF_Weapon_Unit\main\sounds\DC15_1.ogg",1,1,1800};
                begin2[] = {"LF_Weapon_Unit\main\sounds\DC15_2.ogg",1,1,1800};
                begin3[] = {"LF_Weapon_Unit\main\sounds\DC15_3.ogg",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        magazines[]=
        {
            macro_new_mag(vehicle_eweb,500),
            macro_new_mag(opfor_mg,300)
        };
    };
    class macro_new_weapon(saber_gmg,40mm):3AS_Sabre_MG
    {
        magazineReloadTime=3;
        displayName = "Saber GL";
        class manual: Mgun
        {
            reloadTime = 0.24;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_01",1.12202,1,1200};
                begin2[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_02",1.12202,1,1200};
                begin3[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_03",1.12202,1,1200};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
                soundSetShot[] = {"GMG20mm_Shot_SoundSet","GMG20mm_Tail_SoundSet"};
            };
        };
        magazines[]=
        {
            macro_new_mag(ragl40he,40)
        };
    };
    class macro_new_weapon(saber_cannon,supermkii):3AS_Sabre_Cannons_Super
    {
        magazineReloadTime=6;
        magazines[]=
        {
            macro_new_mag(saber_super_3as,25)
        };
        fireLightDiffuse[] = {0,0,1};
        fireLightAmbient[] = {0,0,1};
    };
    class macro_new_weapon(saber_super_mg,50cal):3AS_Sabre_Cannons_Super
    {
        magazineReloadTime=3;
        class manual: manual
        {
            displayName = "Saber Coax EWEB";
            reloadTime = 0.105;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"LF_Weapon_Unit\main\sounds\DC15_1.ogg",1,1,1800};
                begin2[] = {"LF_Weapon_Unit\main\sounds\DC15_2.ogg",1,1,1800};
                begin3[] = {"LF_Weapon_Unit\main\sounds\DC15_3.ogg",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        magazines[]=
        {
            macro_new_mag(vehicle_eweb,500)
        };
    };
    class macro_new_weapon(saber_cannon,reconautocannon):3as_saber_autocannon_30mm
    {
        class HE: HE
        {
            magazines[]=
            {
                macro_new_mag(saber_recon,100)
            };
            magazineReloadTime=3;
        };
    };
    class macro_new_weapon(saber_cannon,recon):ACE_cannon_120mm_GT12
    {
        displayName = "Magic Missile Launcher";
        magazineReloadTime=6;
        magazines[]=
        {
            macro_new_mag(saber_recon_missile_he,4),
            "4Rnd_120mm_LG_cannon_missiles"
        };
    };
    class RD501_Saber_MLRS_Weapon : 3AS_Sabre_Missiles
    {
        magazines[]={"RD501_2Rnd_Saber_MLRS_ATGM_Magazine"}; //"RD501_2Rnd_Saber_MLRS_HEGM_Magazine",
        displayName = "Saber MLRS";
    };
};

class CfgMagazines
{
    class 3AS_300Rnd_SabreMG_Mag;
    class 3AS_50Rnd_Sabre_mag;
    class 3as_saber_80Rnd_autocannon_30mm_mag;
    class 3as_25rnd_Sabre_Super_Mag;
    class 4Rnd_120mm_LG_cannon_missiles;
    class macro_new_mag(vehicle_eweb,500):3AS_300Rnd_SabreMG_Mag
    {
        displayName="300 Rnd Heavy MG Ammo";
        displayNameShort="Heavy MG";
        ammo=MACRO_QUOTE(macro_new_ammo(saber_mg));

    };
    class macro_new_mag(opfor_mg,300):3AS_300Rnd_SabreMG_Mag
    {
        displayName="300 Rnd Heavy MG Ammo";
        displayNameShort="Heavy MG";
        ammo=MACRO_QUOTE(macro_new_ammo(opfor_mg));
    };
    class macro_new_mag(saber_recon,100):3as_saber_80Rnd_autocannon_30mm_mag
    {
        count=100;
        ammo=MACRO_QUOTE(macro_new_ammo(saber_auto));
    };
    class macro_new_mag(saber_main,10): 3AS_50Rnd_Sabre_mag
    {
        displayNameShort="HEAT";
        ammo=MACRO_QUOTE(macro_new_ammo(saber_main));
        count = 50;
    };
    class macro_new_mag(saber_super_3as,25): 3as_25rnd_Sabre_Super_Mag
    {
        ammo= MACRO_QUOTE(macro_new_ammo(saber_super_3as));
    };
    class macro_new_mag(saber_recon_missile_he,4) :4Rnd_120mm_LG_cannon_missiles
    {
        displayName="ATGM HE";
        displayNameShort="HE";
        ammo=MACRO_QUOTE(macro_new_ammo(saber_recon_missile_he));
    };
    class RD501_2Rnd_Saber_MLRS_HEGM_Magazine : 4Rnd_120mm_LG_cannon_missiles{
        displayName = "MLRS HEGM";
        displayNameShort = "HEGM";
        count = 2;
        ammo = "RD501_Saber_MLRS_HEGM";	
    };
    class RD501_2Rnd_Saber_MLRS_ATGM_Magazine : 4Rnd_120mm_LG_cannon_missiles{
        displayName = "MLRS ATGM";
        displayNameShort = "ATGM";
        count = 2;
        ammo = "RD501_Saber_MLRS_ATGM";	
    };	
};
class CfgAmmo
{
    class 3AS_EC60_BluePlasma;
    class 3AS_Sabre_HE;
    class 3as_saber_autocannon_ammo;
    class 3AS_Sabre_AT;
    class M_120mm_cannon_ATGM_LG;
    class ammo_Penetrator_120mm_missile;
    class macro_new_ammo(saber_mg): 3AS_EC60_BluePlasma
    {
        hit=30;
        indirectHit=0;
        indirectHitRange=0;
        cartridge="FxCartridge_127";
        visibleFire=8;
        audibleFire=120;
        dangerRadiusBulletClose=12;
        dangerRadiusHit=16;
        suppressionRadiusBulletClose=8;
        suppressionRadiusHit=12;
        cost=5;
        airLock=1;
        caliber=2.5999999;
        typicalSpeed=880;
        timeToLive=10;
        model="RD501_Vehicle_Weapons\_ammo\data\laser_medium_blue.p3d";
        effectFly = "Aux501_particle_effect_blasterbolt_fly_blue";
        tracerScale=1;
        tracerStartTime=0;
        tracerEndTime=10;
    };
    class macro_new_ammo(opfor_mg):3AS_EC60_BluePlasma
    {
        hit=30;
        indirectHit=0;
        indirectHitRange=0;
        cartridge="FxCartridge_127";
        visibleFire=8;
        audibleFire=120;
        dangerRadiusBulletClose=12;
        dangerRadiusHit=16;
        suppressionRadiusBulletClose=8;
        suppressionRadiusHit=12;
        cost=5;
        airLock=1;
        caliber=2.5999999;
        typicalSpeed=880;
        timeToLive=10;
        model = "\RD501_Vehicle_Weapons\_ammo\data\laser_medium_red.p3d";
        tracerScale=1;
        tracerStartTime=0;
        tracerEndTime=10;
    };
    class macro_new_ammo(saber_auto): 3as_saber_autocannon_ammo
    {
        model = "\RD501_Vehicle_Weapons\_ammo\data\laser_medium_green.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_green";
        tracerScale = 1;
        tracerStartTime=0;
        tracerEndTime=10;
    };
    class macro_new_ammo(saber_main): 3AS_Sabre_HE
    {
        model = "\RD501_Vehicle_Weapons\_ammo\data\laser_big_blue.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_blue";
        tracerScale=1;
        tracerStartTime=0;
        tracerEndTime=10;
    };
    class macro_new_ammo(saber_super_3as): 3AS_Sabre_AT
    {
        model = "\RD501_Vehicle_Weapons\_ammo\data\laser_big_green.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_green";
        tracerScale = 1;
        tracerStartTime=0;
        tracerEndTime=10;
    };
    class macro_new_ammo(saber_recon_missile_he):M_120mm_cannon_ATGM_LG
    {
        hit=100;
        inirectHit=150;
        indirectHitRange=30;
        submunitionAmmo = "";
        submunitionDirectionType = "";
    };
    class RD501_Saber_MLRS_HEGM : M_120mm_cannon_ATGM_LG //Roughly the same to a mortar round
    {
        warheadName = "HE";
        submunitionAmmo = "";
        submunitionDirectionType = "";
        triggerOnImpact = 1;
        deleteParentWhenTriggered = 0;
        hit = 185;
        indirectHit = 62;
        indirectHitRange = 14;
        laserlock = 1;
    };	
    class Rd501_ATGM_Penetrator_missile : ammo_Penetrator_120mm_missile
    {
        hit = 450; //1000 old
        caliber = 20; //54
        
        
    };		
    class RD501_Saber_MLRS_ATGM : M_120mm_cannon_ATGM_LG
    {
        submunitionAmmo = "Rd501_ATGM_Penetrator_missile";
        hit = 95; //200
        indirectHit = 40; //62
        indirectHitRange = 3.2; //20
        laserlock = 1;
    };
};