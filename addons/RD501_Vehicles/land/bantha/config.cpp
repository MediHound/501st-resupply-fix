//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"



class CfgPatches
{
	class RD501_patch_bantha
	{
        addonRootClass="RD501_patch_vehicles";

		requiredAddons[]=
		{
            "RD501_patch_vehicles",
            "212th_APC_test"

		};
		requiredVersion=0.1;
		units[]=
		{
            "RD501_bantha_mkii"
		};
		weapons[]=
		{
			
		};
	};
};


#include "../../common/sensor_templates.hpp"

class CfgVehicles
{
    class Car;
    class Car_F : Car
	{
		class NewTurret;
	};
    class Wheeled_APC_F: Car_F
	{
		class HitPoints;
        class Turrets
		{
            class MainTurret: NewTurret
			{
                class Turrets;
			};
		};
	};
    class 212th_APC_Wheeled_02_base_F : Wheeled_APC_F
	{	
		class HitPoints: HitPoints
		{
			class HitEngine;
			class HitFuel;
			class HitHull;
			class HitBody;
		};
		class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                class Turrets: Turrets
                {
                    class CommanderOptics;
                };
            };
        };
    };
    //this is not empty {} deletion as you would assume from the arma wiki, this falls under the "but not inheritance for skeleton classes" exception. 
    //this is how to correclty inherit something that itself is being inherited and isnt explicitly defined, in this case the CommanderOptics class is only defined
    //in 212th_APC_Wheeled_02_base_F, as we inherit from 212th_B_APC_Wheeled_02_cannon_F, to inherit the CommanderOptics class correctly, despite it not being
    //explicitly defined in the cannon_f class we directly inherit from, we have to use this empty "skeleton template class" to properly declare the inheritance
    //from our vic to the vic with the class we want.
    //(this took me 3 days of troubleshooting, researching and trial and error to figure out so im explaining it here for my future self.)
    class 212th_B_APC_Wheeled_02_base_F: 212th_APC_Wheeled_02_base_F {};
    class 212th_B_APC_Wheeled_02_cannon_F: 212th_B_APC_Wheeled_02_base_F {};
	class RD501_bantha_mkii : 212th_B_APC_Wheeled_02_cannon_F
	{
		displayName="Republic Bantha Mk.II";
		scope=2;
		scopeCurator=2;
		faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(APC));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(APC));
		crewVulnerable = false;
		armor = 500;
		armorLights = 0.1;
		armorStructural = 5;
		crewExplosionProtection = 0.9999;
		crewCrashProtection = 0.001;
		damageResistance = 0.00719;
		icon = "\A3\armor_f_beta\APC_Tracked_01\Data\UI\map_APC_Tracked_01_CA.paa";
		weapons[] = {"TruckHorn"};
		magazines[] = {""};
		class HitPoints: HitPoints
		{
			class HitEngine: HitEngine
			{
				armor = 0.9;
				material = -1;
				armorComponent = "hit_engine";
				name = "hit_engine_point";
				passThrough = 0.5;
				minimalHit = 0.1;
				explosionShielding = 0.8;
				radius = 0.3;
			};
			class HitFuel: HitFuel
			{
				armor = 0.9;
				material = -1;
				armorComponent = "hit_fuel";
				name = "hit_fuel_point";
				passThrough = 0;
				minimalHit = 0.1;
				explosionShielding = 0.4;
				radius = 0.3;
			};
			class HitHull: HitHull
			{
				armor = 0.9;
				material = -1;
				armorComponent = "hit_hull";
				name = "hit_hull_point";
				passThrough = 0.5;
				minimalHit = 0.1;
				explosionShielding = 0.8;
				radius = 0.3;
			};
			class HitBody: HitBody
			{
				armor = 0.9;
				material = -1;
				armorComponent = "body";
				name = "hit_body_point";
				passThrough = 0.5;
				minimalHit = 0.1;
				explosionShielding = 0.8;
				radius = 0.3;
			};
		};
		class Turrets: Turrets
		{
			class  MainTurret : MainTurret
			{
				weapons[] = {"212th_autocannon_Z20_HBB","missiles_DAGR"};
				magazines[] = {"212th_500Rnd_Z20_HBB","212th_500Rnd_Z20_HBB","212th_500Rnd_Z20_HBB","212th_500Rnd_Z20_HBB","24Rnd_PG_missiles"};
				class Turrets: Turrets
				{
					class CommanderOptics: CommanderOptics
					{
						weapons[] = 
						{
							"SmokeLauncher",
							"CMFlareLauncher",
							macro_new_weapon(saber_mg,50cal),
							"Laserdesignator_vehicle"
						};
						magazines[] = 
						{	
							"SmokeLauncherMag",
							"SmokeLauncherMag",
							"SmokeLauncherMag",
							"SmokeLauncherMag",
							"SmokeLauncherMag",
							"300Rnd_CMFlare_Chaff_Magazine",
							macro_new_mag(vehicle_eweb,500),
							macro_new_mag(vehicle_eweb,500),
							macro_new_mag(vehicle_eweb,500),
							macro_new_mag(vehicle_eweb,500),
							macro_new_mag(vehicle_eweb,500),
							macro_new_mag(vehicle_eweb,500),
							macro_new_mag(vehicle_eweb,500),
							macro_new_mag(vehicle_eweb,500),
							macro_new_mag(vehicle_eweb,500),
							macro_new_mag(vehicle_eweb,500),
							macro_new_mag(vehicle_eweb,500),
							"Laserbatteries"
						};
					};
				};
			};	
		};
		#include "../../common/small-inventory.hpp"
	};
};
