
params [
	["_vic",objNull,[player]], 
	["_paraArray",[],[[]],[]]
];

_releaseAlt = [
	typeOf _vic,
	"paraReleaseAlt",
	4
] call fnc_get_config_var;

// Wait till detach
waitUntil {(getPos _vic) select 2 < _releaseAlt};
_vel = velocity _vic;
detach _vic;
_vic setVelocity _vel;

// Play sound
_releaseSound = [
	typeOf _vic,
	"detachSound",
	"a3\sounds_f\weapons\Flare_Gun\flaregun_1_shoot.wss"
] call fnc_get_config_var;
playSound3D [
	_releaseSound,
	_vic
];

// Deteach all parachutes
{
	detach _x;
	_x disableCollisionWith _vic;   
} count (_this select 1);

// After 5 seconds delete them
_time = time + 5;
waitUntil {time > _time};
{
	if (!isNull _x) then {deleteVehicle _x};
} count (_this select 1);

_vic setVariable ["Para_Deployed",false,true];
