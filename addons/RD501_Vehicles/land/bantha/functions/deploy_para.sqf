params [
	["_vic",objNull,[player]]
];

// If no param given
if (isNull _vic) exitWith {
	hint "null";
};

// Check if alive
if (!(alive _vic)) exitWith {

	[
		format["<t color='#FF0000'>Vehicle is destroyed, can not paradrop.</t>"],
		format["<t color='#FF0000'>Lmao you ded.</t>"],
		.2
	] spawn fnc_meme_text;
};

// Check if already deployed
_isParaDeployed = _vic getVariable ["Para_Deployed",false];
if(_isParaDeployed) exitWith {
	[
		format["<t color='#FF0000'>Can not deploy parachute again.</t>"],
		format["<t color='#FF0000'>This isn't Warzone.</t>"],
		.2
	] spawn fnc_meme_text;
};

// Must be above certain height
_minAlt = [
	typeOf _vic,
	"minAlt",
	6
] call fnc_get_config_var;
_currentZHeight = (getPos _vic) select 2;
if (_currentZHeight<= _minAlt) exitWith
{
	[
		format["<t color='#FF0000'>Too low, min alt: %1, currently at: %2.</t>",_minAlt,_currentZHeight],
		format["<t color='#FF0000'>Go fuck your self.</t>"],
		.2
	] spawn fnc_meme_text;
};

// Now begin deployment
_vic setVariable ["Para_Deployed",true,true];

// Get paraclass
_paraClass = [
	typeOf _vic,
	"paraClass",
	"B_Parachute_02_F"
] call fnc_get_config_var;

// Create the main parachute
_paraRelativeAttachOffset = [
	typeOf _vic,
	"relativeAttachOffset",
	[0,0,0]
] call fnc_get_config_var;
_paraMain = createVehicle [_paraClass, [0,0,0], [], 0, "FLY"];
_paraMain setDir getDir _vic;
_paraMain setPos ((getPos _vic)); // Move main para to _vic
_paras =  [_paraMain]; // Attached parachutes to _vic
_vic attachTo [_paraMain, _paraRelativeAttachOffset]; // Attach vehicle to main parachute

// Create visual parachutes
_extraParaVectors = [
	typeOf _vic,
	"extraParaVectors",
	[
		[0,0,0]
	]
] call fnc_get_config_var;
{
	_p = createVehicle [_paraClass, [0,0,0], [], 0, "FLY"];
	_paras set [count _paras, _p];
	_p attachTo [_paraMain, [0,0,0]];
	_p setVectorUp _x;
} count _extraParaVectors;

// Wait till low enough
sleep 1;

0 = [_vic, _paras] spawn fnc_detach_para;