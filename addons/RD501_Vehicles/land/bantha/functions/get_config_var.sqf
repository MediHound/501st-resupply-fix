params[
	["_vicClass","",["string"]],
	["_variable","",["string"]],
	["_defaultVal",""]
];

//hint format["class: %1, time:%2",_vicClass,time];
if(_vicClass isEqualTo "") exitWith {player sideChat format ["vic class nil"];};
if(_variable isEqualTo "") exitWith {player sideChat format ["var nil"];};
if(_defaultVal isEqualTo "") exitWith {player sideChat format ["default nil"];};

[(configFile >> "CfgVehicles" >> _vicClass >> "BanthaParachuteClass"),_variable,_defaultVal] call BIS_fnc_returnConfigEntry