
//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon atap
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

class CfgPatches
{
	class RD501_patch_atap
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(walker,republic_atap_MkII)
		};
		weapons[]=
		{
			
		};
	};
};

class CfgVehicles
{
	class MBT_01_base_F;
	class MBT_01_arty_base_F: MBT_01_base_F
	{
		class Turrets;
	};
	class 3as_ATAP_base: MBT_01_arty_base_F
	{
		class Turrets:Turrets
		{
			class MainTurret1;
			class MainTurretBottom;
			class MainTurretTop;
			
		};
		class HitPoints
		{
			class HitHull;
			class HitEngine;
			class HitLTrack;
			class HitRTrack;
			class HitFuel;
		};
	};
	class macro_new_vehicle(walker,republic_atap_MkII):3as_ATAP_base
	{
		scope=2;
		scopeCurator=2;
		displayName= "Republic AT-AP";
		faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(Walkers));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(tank));
		RD501_magclamp_small_forbidden=1;
		RD501_magclamp_large_offset[]={0.0, -5.5, -10.5};
		maxFordingDepth = 10;
		artilleryScanner = 0;
		armor = 275;
		armorStructural = 5;
		class Turrets:Turrets
		{
			class MainTurret1: MainTurret1
			{
				elevationMode=0;
				minElev=-25;
				weapons[]=
				{
					"RD501_atap_heavy_turret"
				};
				magazines[]=
				{
					macro_new_mag(mynock_mbt_ap,12),
					macro_new_mag(mynock_mbt_he,12),
					macro_new_mag(mynock_mbt_ap,12),
					macro_new_mag(mynock_mbt_he,12)
				};
				fireLightDiffuse[] = {0,0,1};
				fireLightAmbient[] = {0,0,1};	
				class HitPoints
				{
					class HitTurret
					{
						armor = 50;
						material = -1;
						name = "turret2_hit";
						visual = "main_turret_hit";
						passThrough = 0;
						minimalHit = 0.1;
						explosionShielding = 0.2;
						radius = 0.25;
					};
					class HitGun
					{
						armor = 50;
						material = -1;
						name = "gun2_hit";
						visual = "main_gun_hit";
						passThrough = 0;
						minimalHit = 0.1;
						explosionShielding = 0.4;
						radius = 0.25;
					};
				};
			};
			class MainTurretTop:MainTurretTop
			{
				weapons[]=
				{
					"RD501_atap_top_turret",
					"SmokeLauncher"
				};
				magazines[] = 
				{
					"RD501_15rnd_atap_top_mag",
					"RD501_15rnd_atap_top_mag",
					"RD501_15rnd_atap_top_mag",
					"RD501_15rnd_atap_top_mag",
					"RD501_15rnd_atap_top_mag",
					"RD501_15rnd_atap_top_mag",
					"SmokeLauncherMag"
				};
				fireLightDiffuse[] = {0,0,1};
				fireLightAmbient[] = {0,0,1};
				class HitPoints
				{
					class HitTurret
					{
						armor = 50;
						material = -1;
						name = "turret2_hit";
						visual = "main_turret_hit";
						passThrough = 0;
						minimalHit = 0.1;
						explosionShielding = 0.2;
						radius = 0.25;
					};
					class HitGun
					{
						armor = 50;
						material = -1;
						name = "gun2_hit";
						visual = "main_gun_hit";
						passThrough = 0;
						minimalHit = 0.1;
						explosionShielding = 0.4;
						radius = 0.25;
					};
				};
			};
			class MainTurretBottom:MainTurretBottom
			{	
				weapons[] = 
				{
					macro_new_weapon(saber_mg,50cal)
				};
				magazines[]=
				{
					macro_new_mag(vehicle_eweb,500),
					macro_new_mag(vehicle_eweb,500),
					macro_new_mag(vehicle_eweb,500),
					macro_new_mag(vehicle_eweb,500),
					macro_new_mag(vehicle_eweb,500),
					macro_new_mag(vehicle_eweb,500)
				};
				
				fireLightDiffuse[] = {0,0,1};
				fireLightAmbient[] = {0,0,1};
				class HitPoints
				{
					class HitTurret
					{
						armor = 50;
						material = -1;
						name = "turret2_hit";
						visual = "main_turret_hit";
						passThrough = 0;
						minimalHit = 0.1;
						explosionShielding = 0.2;
						radius = 0.25;
					};
					class HitGun
					{
						armor = 50;
						material = -1;
						name = "gun2_hit";
						visual = "main_gun_hit";
						passThrough = 0;
						minimalHit = 0.1;
						explosionShielding = 0.4;
						radius = 0.25;
					};
				};
			};
		};
		class HitPoints: HitPoints
		{
			class HitHull: HitHull
			{
				armor = 2.1;
				material = -1;
				name = "telo";
				visual = "zbytek";
				passThrough = 0.8;
				minimalHit = 0.14;
				explosionShielding = 1.75;
				radius = 0.275;
			};
			class HitEngine: HitEngine
			{
				armor = 2.1;
				material = -1;
				name = "motor";
				passThrough = 0.8;
				minimalHit = 0.24;
				explosionShielding = 1;
				radius = 0.33;
			};
			class HitLTrack: HitLTrack
			{
				armor = 2.1;
				material = -1;
				name = "track_l_hit";
				passThrough = 0;
				minimalHit = 0.08;
				explosionShielding = 1.44;
				radius = 0.3;
			};
			class HitRTrack: HitRTrack
			{
				armor = 2.1;
				material = -1;
				name = "track_r_hit";
				passThrough = 0;
				minimalHit = 0.08;
				explosionShielding = 1.44;
				radius = 0.3;
			};
			class HitFuel: HitFuel
			{
				armor = 2.1;
				material = -1;
				name = "palivo";
				passThrough = 1;
				minimalHit = 0.1;
				explosionShielding = 0.4;
				radius = 0.25;
			};
		};
	};
};