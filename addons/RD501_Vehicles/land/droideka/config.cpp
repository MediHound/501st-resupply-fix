#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
    class RD501_patch_droideka
    {
        addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

        requiredAddons[]=
        {
            "RD501_patch_vehicles"
        };
        requiredVersion=0.1;
        units[]=
        {
            macro_new_vehicle(droideka,normal),
            macro_new_vehicle(droideka,mobile),
            macro_new_vehicle(droideka,sniper)
        };
        weapons[]=
        {
            
        };
    };
};

#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers;

class CfgVehicles
{
    class LandVehicle;
    class StaticWeapon: LandVehicle
    {
        class Turrets
        {
            class MainTurret;
        };
    };
    class StaticMGWeapon: StaticWeapon
    {
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                class ViewOptics;
            };
        };
    };

    
    class macro_new_vehicle(droideka_mk2,base): StaticMGWeapon
    {
        author = "501st Aux Team";
        _generalMacro = "HMG_01_base_F";
        scope = 0;
        displayName = "Droideka";
        accuracy = 0.12;
		cost = 250000;
		threat[] = {0.6,0.6,1.0};
        enableGPS = 1;
		radarType = 4;
		gunnerCanSee = "1+2+4+16";
		commanderCanSee = "1+2+4+16";
		driverCanSee = "1+2+4+16";
		radarTarget = 1;
		radarTargetSize = 1.2;
		visualTarget = 1;
		visualTargetSize = 1.2;
		irTargetSize = 0;
		reportRemoteTargets = 1;
		receiveRemoteTargets = 1;
        uavCameraGunnerPos = "PiP_pos";
		uavCameraGunnerDir = "PiP_dir";
        irTarget = 1;
		irScanRangeMin = 50;
		irScanRangeMax = 10500;
		irScanToEyeFactor = 4;
		irScanGround = 1;
        class Armory
        {
            description = "";
        };
        class EventHandlers: DefaultEventhandlers {};
        model = "\3AS\3AS_Deka\Deka.p3d";
        hiddenselections[] = 
        {
            "camo"
        };
        hiddenselectionstextures[] = 
        {
            "\RD501_Vehicles\textures\droideka\rd501_deka_co.paa"
        };
        editorSubcategory = macro_editor_cat(heavy_armored_infantry);
        picture = "\A3\Static_f_gamma\data\ui\gear_StaticTurret_MG_CA.paa";
        UiPicture = "\A3\Static_f_gamma\data\ui\gear_StaticTurret_MG_CA.paa";
        getInAction = "";
        getOutAction = "";
        destrType = "DestructBuilding";
		explosionEffect="FuelExplosion";
		class DestructionEffects
		{
			class Dust
			{
				intensity = 0.1;
				interval = 1;
				lifeTime = 0.01;
				position = "destructionEffect2";
				simulation = "particles";
				type = "HousePartDust";
			};
			class Light1
			{
				enabled = "distToWater";
				intensity = 0.1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "light";
				type = "ObjectDestructionLightSmall";
			};
			class Fire1
			{
				intensity = 0.15;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "ObjectDestructionFire1Small";
			};
			class Refract1
			{
				intensity = 1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "SmallFireFRefract";
			};
			class Sound
			{
				intensity = 1;
				interval = 1;
				lifeTime = 1;
				position = "destructionEffect";
				simulation = "sound";
				type = "Fire";
			};
			class sparks1
			{
				intensity = 0.5;
				interval = 1;
				lifeTime = 0;
				position = "destructionEffect2";
				simulation = "particles";
				type = "ObjectDestructionSparks";
			};
			class Smoke1
			{
				simulation="particles";
				type="BarelDestructionSmoke";
				position[]={0,0,0};
				intensity=0.2;
				interval=1;
				lifeTime=1;
			};
			class HouseDestr
			{
				intensity=1;
				interval=1;
				lifeTime=5;
				position="";
				simulation="destroy";
				type="DelayedDestruction";
			};
		};
        class Damage
        {
            tex[] = {};
            mat[] = {"3as\3as_deka\data\deka.rvmat","3as\3as_deka\data\deka.rvmat","A3\armor_f_gamma\MBT_01\Data\MBT_01_body_destruct.rvmat"};
        };
        damageResistance = 0.02;
        crewVulnerable = 0;
        armor = 30;
        armorStructural = 2;
        class Hitpoints
        {
            class HitHull
            {
                armor = 1000;
                material = -1;
                name = "hull_hit";
                visual = "zbytek";
                passthrough = 0.03;
                minimalhit = 0.14;
                explosionshielding = 2;
                radius = 0.25;
            };
            class HitEngine
            {
                armor = 300;
                material = -1;
                name = "engine_hit";
                passThrough = 0.08;
                minimalHit = 0.24;
                explosionShielding = 1;
                radius = 0.33;
            };
            class HitShield
            {
                armorComponent = "Deka_Shield_Component";
                armor = 1;
                material = -1;
                name = "Shield_hit";
                passThrough = 0;
                minimalHit = 0.24;
                explosionShielding = 1;
                radius = 0.33;
            };
            class hitammo_l: HitEngine
            {
                name = "ammo_l_hit";
            };
            class hitammo_r: hitammo_l
            {
                name = "ammo_r_hit";
            };
        };
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                memoryPointGunnerOptics = "gunner_view";
                gunBeg[] = {"Usti hlavne","Usti hlavne2"};
                gunEnd[] = {"Konec hlavne","Konec hlavne2"};
                primary = 1;
                primaryGunner = 1;
                enableManualFire = 1;
                optics = 0;
                minElev = -15;
                maxElev = 35;
                minTurn = -360;
                maxTurn = 360;
                maxHorizontalRotSpeed = 2.7;
				maxVerticalRotSpeed = 2.7;
                selectionFireAnim = "zasleh";
                soundServo[] = {"A3\Sounds_F\vehicles\boat\Boat_Armed_01\servo_boat_comm",1.4125376,1,30};
                soundServoVertical[] = {"A3\Sounds_F\vehicles\boat\Boat_Armed_01\servo_boat_comm_vertical",1.4125376,1,30};
                weapons[] = 
                {
                    macro_new_weapon(droideka,dual)
                };
                magazines[] = 
                {
                    macro_new_mag(droideka_dual_blaster_cell,60),
                    macro_new_mag(droideka_dual_blaster_cell,60),
                    macro_new_mag(droideka_dual_blaster_cell,60),
                    macro_new_mag(droideka_dual_blaster_cell,60),
                    macro_new_mag(droideka_dual_blaster_cell,60),
                    macro_new_mag(droideka_dual_blaster_cell,60),
                    macro_new_mag(droideka_dual_blaster_cell,60),
                    macro_new_mag(droideka_dual_blaster_cell,60),
                    macro_new_mag(droideka_dual_blaster_cell,60),
                    macro_new_mag(droideka_dual_blaster_cell,60),
                    macro_new_mag(droideka_dual_blaster_cell,60)
                };
                gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_02_F";
                turretInfoType = "RscOptics_UAV_gunner";
                LODTurnedIn = -1;
                LODTurnedOut = -1;
                visionMode[] = {"Normal","NVG","TI"};
                gunnergetInAction = "";
                gunnergetOutAction = "";
                displayName = "";
                gunnerOutOpticsModel = "";
                uavCameraGunnerPos = "PiP_pos";
				uavCameraGunnerDir = "PiP_dir";
				gunnerForceOptics = 1;
                gunnerOutOpticsEffect[] = {};
                gunnerOpticsEffect[] = {};
                gunnerInAction = "Disabled";
                gunnerAction = "Disabled";
                forceHideGunner = 1;
                inGunnerMayFire = 1;
                viewGunnerInExternal = 1;
                memoryPointGun[] = {"usti hlavne","usti hlavne2"};
                class Turrets{};
                class ViewOptics: ViewOptics
                {
                    visionMode[] = {"Normal","NVG","TI"};
                    initAngleX = 0;
					minAngleX = -45;
					maxAngleX = 75;
					initAngleY = 0;
					minAngleY = -120;
					maxAngleY = 120;
					initFov = 0.4;
					minFov = 0.23;
					maxFov = 0.4;
                };
                class HitPoints
                {
                    class HitTurret
                    {
                        armor = 0.8;
                        material = -1;
                        name = "main_turret_hit";
                        visual = "vez";
                        passThrough = 0;
                        minimalHit = 0.02;
                        explosionShielding = 0.3;
                        radius = 0.25;
                    };
                    class HitGun
                    {
                        armor = 0.3;
                        material = -1;
                        name = "main_gun_hit";
                        visual = "";
                        passThrough = 0;
                        minimalHit = 0;
                        explosionShielding = 1;
                        radius = 0.25;
                    };
                };
            };
        };
        isUav = 1;
        crew = "O_UAV_AI";
        class simpleobject
        {
            animate[] = {{"maingun",0},{"mainturret",0},{"damagehide",0}};
        };
        class animationSources
        {
            class ShieldLayer_BaseFront
            {
                animPeriod = 0.001;
                initPhase = 1;
                source = "user";
                forceAnimatePhase = 1;
            };
        };
    };
    class macro_new_vehicle(droideka,normal): macro_new_vehicle(droideka_mk2,base)
    {
        _generalMacro = "B_HMG_01_F";
        scope = 2;
        side = 0;
        faction = macro_faction(CIS);
        vehicleClass = macro_editor_vehicle_type(heavy_armored_infantry);
        scopearsenal = 2;
        scopecurator = 2;
    };
    class macro_new_vehicle(droideka,mobile): macro_new_vehicle(droideka,normal)
    {
        displayName = "Droideka (Mobile)";
    };
    

    class 3as_Deka_Static_Sniper_Base:StaticMGWeapon
    {
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                class ViewOptics;
            };
        };
    };
    class macro_new_vehicle(droideka_sniper,base):3as_Deka_Static_Sniper_Base
    {
        model = "\3as\3as_deka\deka_Sniper.p3d";
        hiddenselections[] = {"camo"};
        hiddenselectionstextures[] = {"RD501_Vehicles\textures\droideka\rd501_sniperdroideka_co.paa"};
        armor = 100;
        armorStructural = 2;
        class Damage
        {
            tex[] = {};
            mat[] = {"3as\3as_deka\data\dekaSniper.rvmat","3as\3as_deka\data\dekaSniper.rvmat","A3\armor_f_gamma\MBT_01\Data\MBT_01_body_destruct.rvmat"};
        };
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                gunBeg[] = {"Usti hlavne"};
                gunEnd[] = {"Konec hlavne"};
                memoryPointGun[] = {"usti hlavne"};
                gunnerForceOptics = 1;
                optics = 1;
                minElev = -15;
                maxElev = 35;
                minTurn = -360;
                maxTurn = 360;
                weapons[] = 
                {
                    macro_new_weapon(droideka,sniper)
                };
                magazines[] = 
                {
                    macro_new_mag(CIS_droideka_sniper_cell,10),
                    macro_new_mag(CIS_droideka_sniper_cell,10),
                    macro_new_mag(CIS_droideka_sniper_cell,10),
                    macro_new_mag(CIS_droideka_sniper_cell,10)
                };
            };
        };
    };
    class macro_new_vehicle(droideka,sniper): macro_new_vehicle(droideka_sniper,base)
    {
        _generalMacro = "B_HMG_01_F";
        scope = 2;
        side = 0;
        faction = macro_faction(CIS);
        editorSubcategory = macro_editor_cat(heavy_armored_infantry);
        vehicleClass = macro_editor_vehicle_type(heavy_armored_infantry);
        crew = "O_UAV_AI";
        scopearsenal = 2;
        scopecurator = 2;
        destrType = "DestructBuilding";
        explosionEffect="FuelExplosion";
        class DestructionEffects
        {
            class Dust
            {
                intensity = 0.1;
                interval = 1;
                lifeTime = 0.01;
                position = "destructionEffect2";
                simulation = "particles";
                type = "HousePartDust";
            };
            class Light1
            {
                enabled = "distToWater";
                intensity = 0.1;
                interval = 1;
                lifeTime = 3;
                position = "destructionEffect";
                simulation = "light";
                type = "ObjectDestructionLightSmall";
            };
            class Fire1
            {
                intensity = 0.15;
                interval = 1;
                lifeTime = 3;
                position = "destructionEffect";
                simulation = "particles";
                type = "ObjectDestructionFire1Small";
            };
            class Refract1
            {
                intensity = 1;
                interval = 1;
                lifeTime = 3;
                position = "destructionEffect";
                simulation = "particles";
                type = "SmallFireFRefract";
            };
            class Sound
            {
                intensity = 1;
                interval = 1;
                lifeTime = 1;
                position = "destructionEffect";
                simulation = "sound";
                type = "Fire";
            };
            class sparks1
            {
                intensity = 0.5;
                interval = 1;
                lifeTime = 0;
                position = "destructionEffect2";
                simulation = "particles";
                type = "ObjectDestructionSparks";
            };
            class Smoke1
            {
                simulation="particles";
                type="BarelDestructionSmoke";
                position[]={0,0,0};
                intensity=0.2;
                interval=1;
                lifeTime=1;
            };
            class HouseDestr
            {
                intensity=1;
                interval=1;
                lifeTime=5;
                position="";
                simulation="destroy";
                type="DelayedDestruction";
            };
        };
    };
};

class Extended_Init_EventHandlers
{
    class macro_new_vehicle(droideka,normal)
    {
        class rd501_deka_shield
        {
            init = "_this call rd501_fnc_deka_shield_init";
        };
    };
    class macro_new_vehicle(droideka,normal2)
    {
        class rd501_deka_shield
        {
            init = "_this call rd501_fnc_deka_shield_init";
        };
    };
};
