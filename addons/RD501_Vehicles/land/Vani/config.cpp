//Get this addons macro

//get the macro for the air macro_patch_name(vehicles)

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon Mynock
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define new_Mynock_class(name) vehicle_classname##_##name

class CfgPatches
{
    class macro_patch_name(mynock)
    {
        addonRootClass=macro_patch_name(vehicles);

        requiredAddons[]=
        {
            macro_patch_name(vehicles),
            RD501_patch_saber
        };
        requiredVersion=0.1;
        units[]=
        {
            macro_new_vehicle(T1000,Republic),
            macro_new_vehicle(Claymore,Republic),
            macro_new_vehicle(Warhammer,Republic),
            macro_new_vehicle(Glaive,Mynock),
            macro_new_vehicle(Glaive,Republic),
            macro_new_vehicle(Longbow,Republic),
            //macro_new_vehicle(Courser,Republic)
        };
        weapons[]=
        {
            
        };
    };
};


#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers;

class CfgVehicles
{
    #include "inheritance.hpp"
    class macro_new_vehicle(T1000,Republic): O_MBT_02_Cannon_F
    {
        scope=2;
        scopeCurator = 2;
        forceInGarage = 1;
        side=1;
        displayName="T1000 MBT (Mynock)";
        crew = "JLTS_Clone_P2_Rex_501";
        author="RD501";
        
        hiddenSelections[] = {"Camo1", "Camo2", "Camo3", "Camo4", "Camo5", "CamoSlat"};
        textureList[] = {};
        hiddenSelectionsTextures[] = 
        {
            "RD501_Vehicles\textures\t1000\t1000_ext1_mynock_co.paa",
            "RD501_Vehicles\textures\t1000\t1000_turret_mynock_co.paa",
            "RD501_Vehicles\textures\t1000\t1000_ext2_mynock_co.paa",
            "RD501_Vehicles\textures\grey.paa"
        };

        faction = macro_republic_faction;
        editorSubcategory = macro_editor_cat(Tank);
        vehicleClass = macro_editor_vehicle_type(Tank);
        #include "../../common/small-inventory.hpp"

        #include "common.hpp"

        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                weapons[] = 
                {
                    "RD501_mynock_cannon_mbt",
                    "RD501_saber_mg_50cal"
                };
                magazines[] = 
                {
                    "RD501_mynock_mbt_ap_x12_mag",
                    "RD501_mynock_mbt_ap_x12_mag",
                    "RD501_mynock_mbt_ap_x12_mag",
                    "RD501_mynock_mbt_he_x12_mag",
                    "RD501_mynock_mbt_he_x12_mag",
                    "RD501_mynock_mbt_he_x12_mag",
                    "RD501_vehicle_eweb_x500_mag",
                    "RD501_vehicle_eweb_x500_mag"
                };
                class Turrets: Turrets
                {
                    class CommanderOptics: CommanderOptics
                    {
                        weapons[] = 
                        {
                            "RD501_saber_mg_50cal",
                            "CMFlareLauncher",
                            "SmokeLauncher"
                        };
                        magazines[] = 
                        {
                            "300Rnd_CMFlare_Chaff_Magazine",
                            "RD501_vehicle_eweb_x500_mag",
                            "RD501_vehicle_eweb_x500_mag",
                            "SmokeLauncherMag"
                        };
                    };
                };
            };
        };
        class AnimationSources: AnimationSources 
        {
            class muzzle_hide_cannon 
            {
                source = "reload";
                weapon = "RD501_mynock_cannon_mbt";
            };
            class recoil_source 
            {
                source = "reload";
                weapon = "RD501_mynock_cannon_mbt";
            };
            class Smoke_source 
            {
                source = "revolving";
                weapon = "SmokeLauncher";
            };
        };
    };

    class macro_new_vehicle(Claymore,Republic):B_MBT_01_TUSK_F
    {	
        scope=2;
        side=1;
        scopeCurator=2;
        forceInGarage = 1;
        displayName="Claymore MBT (Mynock)";
        crew = "JLTS_Clone_P2_Rex_501";
        author="RD501";
        
        hiddenSelections[] = {"Camo1", "Camo2", "Camo3", "Camo4", "Camo5", "CamoSlat"};
        textureList[] = {};
        hiddenSelectionsTextures[] = 
        {
            "RD501_Vehicles\textures\mbt01\mbt01_ext1_mynock_co.paa",
            "RD501_Vehicles\textures\mbt01\claymore\claymore_tower_mynock_co.paa",
            "RD501_Vehicles\textures\mbt01\claymore\claymore_addons_mynock_co.paa",
            "a3\Armor_F\Data\camonet_NATO_Desert_CO.paa"
        };

        faction = macro_republic_faction;
        editorSubcategory = macro_editor_cat(Tank);
        vehicleClass = macro_editor_vehicle_type(Tank);
        #include "../../common/small-inventory.hpp"

        #include "common.hpp"

        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                weapons[] = 
                {
                    "RD501_mynock_cannon_mbt",
                    "RD501_saber_mg_50cal"
                };
                magazines[] = 
                {
                    "RD501_mynock_mbt_ap_x12_mag",
                    "RD501_mynock_mbt_ap_x12_mag",
                    "RD501_mynock_mbt_ap_x12_mag",
                    "RD501_mynock_mbt_he_x12_mag",
                    "RD501_mynock_mbt_he_x12_mag",
                    "RD501_mynock_mbt_he_x12_mag",
                    "RD501_vehicle_eweb_x500_mag",
                    "RD501_vehicle_eweb_x500_mag"
                };
                class Turrets: Turrets
                {
                    class CommanderOptics: CommanderOptics
                    {
                        weapons[] = 
                        {
                            "RD501_saber_mg_50cal",
                            "CMFlareLauncher",
                            "SmokeLauncher"
                        };
                        magazines[] = 
                        {
                            "300Rnd_CMFlare_Chaff_Magazine",
                            "RD501_vehicle_eweb_x500_mag",
                            "RD501_vehicle_eweb_x500_mag",
                            "SmokeLauncherMag"
                        };
                    };
                };
            };
        };
        class AnimationSources: AnimationSources 
        {
            class commander_gun_recoil 
            {
                source = "reload";
                weapon = "RD501_saber_mg_50cal";
            };
            class muzzle_hide_cannon 
            {
                source = "reload";
                weapon = "RD501_mynock_cannon_mbt";
            };
            class recoil_source 
            {
                source = "reload";
                weapon = "RD501_mynock_cannon_mbt";
            };
            class Smoke_source 
            {
                source = "revolving";
                weapon = "SmokeLauncher";
            };
        };
    };
    class macro_new_vehicle(Warhammer,Republic): I_MBT_03_cannon_F
    {	
        scope=2;
        side=1;
        scopeCurator=2;
        forceInGarage = 1;
        displayName="Warhammer MBT (Mynock)";
        crew = "JLTS_Clone_P2_Rex_501";
        author="RD501";
        
        hiddenSelections[] = {"Camo1", "Camo2", "Camo3", "Camo4", "Camo5", "CamoSlat"};
        textureList[] = {};
        hiddenSelectionsTextures[] = 
        {
            "RD501_Vehicles\textures\warhammer\warhammer_ext01_mynock_co.paa",
            "RD501_Vehicles\textures\warhammer\warhammer_ext02_mynock_co.paa",
            "RD501_Vehicles\textures\warhammer\warhammer_ext03_mynock_co.paa",
            "A3\Armor_F\Data\camonet_AAF_Digi_Green_CO.paa"
        };

        faction = macro_republic_faction;
        editorSubcategory = macro_editor_cat(Tank);
        vehicleClass = macro_editor_vehicle_type(Tank);
        #include "../../common/small-inventory.hpp"

        #include "common.hpp"

        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                weapons[] = 
                {
                    "RD501_mynock_cannon_mbt",
                    "RD501_saber_mg_50cal"
                };
                magazines[] = 
                {
                    "300Rnd_CMFlare_Chaff_Magazine",
                    "RD501_mynock_mbt_ap_x12_mag",
                    "RD501_mynock_mbt_ap_x12_mag",
                    "RD501_mynock_mbt_ap_x12_mag",
                    "RD501_mynock_mbt_he_x12_mag",
                    "RD501_mynock_mbt_he_x12_mag",
                    "RD501_mynock_mbt_he_x12_mag",
                    "RD501_vehicle_eweb_x500_mag",
                    "RD501_vehicle_eweb_x500_mag"
                };
                class Turrets: Turrets
                {
                    class CommanderOptics: CommanderOptics
                    {
                        weapons[] = 
                        {
                            "RD501_saber_mg_50cal",
                            "CMFlareLauncher",
                            "SmokeLauncher"
                        };
                        magazines[] = 
                        {
                            "RD501_vehicle_eweb_x500_mag",
                            "RD501_vehicle_eweb_x500_mag",
                            "SmokeLauncherMag"
                        };
                    };
                };
            };
        };
        class AnimationSources: AnimationSources 
        {
            class muzzle_hide_coax 
            {
                source = "reload";
                weapon = "RD501_saber_mg_50cal";
            };
            class recoil_source
            {
                source = "reload";
                weapon = "RD501_mynock_cannon_mbt";
            };
            class Smoke_source 
            {
                source = "revolving";
                weapon = "SmokeLauncher";
            };
        };
    };
    
    class macro_new_vehicle(Glaive,Mynock): B_AFV_Wheeled_01_up_cannon_F
    {	
        scope=2;
        side=1;
        scopeCurator=2;
        forceInGarage = 1;
        displayName="Glaive TD (Mynock)";
        crew = "JLTS_Clone_P2_Rex_501";
        author="RD501";
        
        hiddenSelections[] = {"Camo1", "Camo2", "Camo3", "Camo4", "Camo5", "CamoSlat"};
        textureList[] = {};
        hiddenSelectionsTextures[] = 
        {
            "RD501_Vehicles\textures\glaive\glaive_ext1_mynock_co.paa",
            "RD501_Vehicles\textures\glaive\glaive_ext2_mynock_co.paa",
            "RD501_Vehicles\textures\glaive\glaive_wheel_mynock_co.paa",
            "RD501_Vehicles\textures\glaive\glaive_ext3_mynock_co.paa",
            "RD501_Vehicles\textures\glaive\glaive_turret_mynock_co.paa",
            "RD501_Vehicles\textures\grey.paa",
            "RD501_Vehicles\textures\glaive\glaive_ext3_mynock_co.paa"
        };

        faction = macro_republic_faction;
        editorSubcategory = macro_editor_cat(APC);
        vehicleClass = macro_editor_vehicle_type(APC);
        #include "../../common/small-inventory.hpp"
        class EventHandlers: DefaultEventhandlers
        {
            fired = "_this call (uinamespace getviariable 'BIS_fnc_effectFired');";
            init = "[_this select 0] execVM 'RD501_Vehicles\_init_functions\wheel_dmg.sqf';";
        };

        class Turrets: Turrets
        {
            turretInfoType = "RscOptics_APC_Wheeled_01_gunner";
            class MainTurret: MainTurret
            {
                weapons[] = 
                {
                    "RD501_saber_cannon_supermkii",
                    "RD501_saber_super_mg_50cal"
                };
                magazines[] = 
                {
                    "RD501_saber_super_3as_x25_mag",
                    "RD501_saber_super_3as_x25_mag",
                    "RD501_saber_super_3as_x25_mag",
                    "RD501_saber_super_3as_x25_mag",
                    "RD501_saber_super_3as_x25_mag",
                    "RD501_saber_super_3as_x25_mag",
                    "RD501_vehicle_eweb_x500_mag",
                    "RD501_vehicle_eweb_x500_mag"
                };
                class Turrets: Turrets
                {
                    class CommanderOptics: CommanderOptics
                    {
                        weapons[] = 
                        {
                            macro_new_weapon(saber_mg,50cal),
                            "SmokeLauncher",
                            "CMFlareLauncher"
                        };
                        magazines[] = 
                        {
                            "300Rnd_CMFlare_Chaff_Magazine",
                            macro_new_mag(vehicle_eweb,500),
                            macro_new_mag(vehicle_eweb,500), 
                            "SmokeLauncherMag"
                        };
                    };
                };
            };
        };
        animationList[] = {"showCamonetHull",0,"showCamonetCannon",0,"showCamonetTurret",0,"showSLATHull",0};
        class AnimationSources: AnimationSources
        {
            class cannon_120mm_revolving 
            {
                source = "revolving";
                weapon = "RD501_saber_cannon_supermkii";
            };
            class com_gun_reload
            {
                source = "reload";
                weapon = "RD501_saber_mg_50cal";
            };
            class muzzle_hide_cannon
            {
                source = "reload";
                weapon = "RD501_saber_cannon_supermkii";
            };
            class muzzle_rot_cannon
            {
                source = "ammorandom";
                weapon = "RD501_saber_cannon_supermkii";
            };
            class muzzle_rot_HMG
            {
                source = "ammorandom";
                weapon = "RD501_saber_super_mg_50cal";
            };
        };
    };	
    class macro_new_vehicle(Glaive,Republic): macro_new_vehicle(Glaive,Mynock)
    {
        displayName="Glaive TD";
        author="RD501";
        #include "../../common/small-inventory.hpp"
        hiddenSelectionsTextures[]=
        {
            "RD501_Vehicles\textures\glaive\glaive_ext1_republic_co.paa",
            "RD501_Vehicles\textures\glaive\glaive_ext2_republic_co.paa",
            "RD501_Vehicles\textures\glaive\glaive_wheel_republic_co.paa",
            "RD501_Vehicles\textures\glaive\glaive_ext3_republic_co.paa",
            "RD501_Vehicles\textures\glaive\glaive_turret_republic_co.paa",
            "RD501_Vehicles\textures\cream.paa",
            "RD501_Vehicles\textures\glaive\glaive_ext3_republic_co.paa"
        };
    };
    class macro_new_vehicle(Longbow,Republic):O_APC_Tracked_02_AA_F
    {
        scope=2;
        side=1;
        scopeCurator=2;
        forceInGarage = 1;
        displayName="Republic Longbow AA (Mynock)";
        crew = "JLTS_Clone_P2_Rex_501";
        author="RD501";
        
        hiddenSelections[] = {"Camo1", "Camo2", "Camo3", "Camo4", "Camo5", "CamoSlat"};
        textureList[] = {};
        hiddenSelectionsTextures[] = 
        {
            "RD501_Vehicles\textures\longbow\longbow_ext1_mynock_co.paa",
            "RD501_Vehicles\textures\longbow\longbow_ext2_mynock_co.paa",
            "RD501_Vehicles\textures\longbow\longbow_tower_mynock_co.paa",
            "A3\Armor_F\Data\camonet_CSAT_HEX_Desert_CO.paa",
            "RD501_Vehicles\textures\grey.paa",
        };

        faction = macro_republic_faction;
        editorSubcategory = macro_editor_cat(AA);
        vehicleClass = macro_editor_vehicle_type(AA);
        #include "../../common/small-inventory.hpp"

        #include "common.hpp"

        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                weapons[] = 
                {
                    "RD501_Republic_Aircraft_Laser_AA",
                    "missiles_titan_AA"
                };
                magazines[] = 
                {
                    "RD501_Republic_Aircraft_Laser_AA_Mag_600",
                    "RD501_Republic_Aircraft_Laser_AA_Mag_600",
                    "RD501_Republic_Aircraft_Laser_AA_Mag_600",
                    "RD501_Republic_Aircraft_Laser_AA_Mag_600",
                    "4Rnd_Titan_long_missiles_O",
                    "4Rnd_Titan_long_missiles_O"
                };
                class Turrets: Turrets
                {
                    class CommanderOptics: CommanderOptics
                    {
                        weapons[] = {"CMFlareLauncher","SmokeLauncher"};
                        magazines[] = {"300Rnd_CMFlare_Chaff_Magazine","SmokeLauncherMag"};
                    };
                };
            };
        };
        class AnimationSources: AnimationSources
        {
            class muzzle_rot 
            {
                source = "ammorandom";
                weapon = "RD501_Republic_Aircraft_Laser_AA";
            };
            class Smoke_source 
            {
                source = "revolving";
                weapon = "SmokeLauncher";
            };
        };
    };	
};