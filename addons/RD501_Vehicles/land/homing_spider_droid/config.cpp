//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon homing_spider_droid
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define new_og_class(name) vehicle_classname##_##name

class CfgPatches
{
	class RD501_patch_homing_spider_droid
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(homing_spider_droid,MKII)
		};
		weapons[]=
		{
			
		};
	};
};


#include "../../common/sensor_templates.hpp"

class DefaultEventhandlers ;

class CfgVehicles
{

	class All;
	class Strategic;
	class Land;
	class LandVehicle: Land
	{
		class ViewPilot;
		class ViewGunner;
		class NewTurret;
	};
	class StaticWeapon: LandVehicle
	{
		class Turrets
		{
			class MainTurret;
		};
	};
	class StaticMGWeapon: StaticWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class ViewOptics;
			};
		};
	};
	class macro_new_vehicle(homing_spider_droid,MKII):StaticMGWeapon
	{
		class EventHandlers: DefaultEventhandlers {};
		_generalMacro = "HMG_01_base_F";
		scope = 2;
		side = 0;
		faction = MACRO_QUOTE(macro_faction(CIS));
		crew = MACRO_QUOTE(macro_new_unit_class(opfor,B1_crew));
		displayName = "OG10 HSD";
		armor = 500;
		class Armory
		{
			description = "";
		};
		model = "3AS\3as_static\OG9\3AS_OG9.p3d";
		icon = "3AS\3as_static\OG9\Data\ui\OG9_top_ca.paa";
		editorSubcategory = macro_editor_cat(heavy_armored_infantry);
		vehicleClass = macro_editor_vehicle_type(heavy_armored_infantry);
		picture = "\A3\Static_f_gamma\data\ui\gear_StaticTurret_MG_CA.paa";
		UiPicture = "\A3\Static_f_gamma\data\ui\gear_StaticTurret_MG_CA.paa";
		threat[] = {1,0.3,0.3};
		cost = 150000;
		radarTarget = 1;
		RadarTargetSize = 1.2;
		getInAction = "";
		getOutAction = "";
		ace_dragging_canCarry = 0;
		ace_dragging_canDrag = 0;
		class Damage
		{
			tex[] = {};
			mat[] = {"3AS\3as_static\OG9\data\og_9base.rvmat","3AS\3as_static\OG9\data\og_9base.rvmat","A3\Static_F_Gamma\data\StaticTurret_01_destruct.rvmat"};
		};
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				displayName = "Bottom Gun";
				optics = 1;
				discreteDistance[] = {100,200,300,400,600,800,1000,1200,1500,1600,1700,1800,1900,2000};
				discreteDistanceInitIndex = 2;
				turretInfoType = "RscOptics_crows";
				gunnerforceoptics = 1;
				gunnerOpticsModel = "\a3\weapons_f_gamma\reticle\HMG_01_Optics_Gunner_F";
				minElev = -20;
				maxElev = 15;
				minTurn = -360;
				soundServo[] = {"A3\Sounds_F\vehicles\boat\Boat_Armed_01\servo_boat_comm",1.4125376,1,30};
				soundServoVertical[] = {"A3\Sounds_F\vehicles\boat\Boat_Armed_01\servo_boat_comm_vertical",1.4125376,1,30};
				maxTurn = 360;
				weapons[] = 
				{
					macro_new_weapon(spider,homing_blaster)
				};
				magazines[] = 
				{
					macro_new_mag(homing_spider_eweb,250),
					macro_new_mag(homing_spider_eweb,250),
					macro_new_mag(homing_spider_eweb,250),
					macro_new_mag(homing_spider_eweb,250),
					macro_new_mag(homing_spider_eweb,250)
				};
				memoryPointGun = "Muzzle";
				selectionFireAnim = "zasleh";
				gunBeg = "Muzzle";
				gunEnd = "Chamber";
				gunnerAction = "FieldCannon_Gunner";
				viewGunnerInExternal = 0;
				gunnergetInAction = "";
				gunnergetOutAction = "";
				memoryPointsGetInGunner = "pos_gunner";
				memoryPointsGetInGunnerDir = "pos_gunner_dir";
				memorypointgunneroptics = "gunnerview";
				class ViewOptics: ViewOptics
				{
					initAngleX = 0;
					minAngleX = -30;
					maxAngleX = 30;
					initAngleY = 0;
					minAngleY = -100;
					maxAngleY = 100;
					initFov = 0.117;
					minFov = 0.029;
					maxFov = 0.117;
					visionMode[] = {"Normal","NVG","Ti"};
					thermalMode[] = {0,1};
				};
				gunnerRightHandAnimName = "OtocHlaven_shake";
				gunnerLeftHandAnimName = "OtocHlaven_shake";
				ejectDeadGunner = 0;
			};
			class Top_Turret: MainTurret
			{
				displayName = "Top Gun";
				optics = 1;
				discreteDistance[] = {100,200,300,400,600,800,1000,1200,1500,1600,1700,1800,1900,2000};
				discreteDistanceInitIndex = 2;
				turretInfoType = "RscOptics_crows";
				gunnerforceoptics = 1;
				gunnerOpticsModel = "\a3\weapons_f_gamma\reticle\HMG_01_Optics_Gunner_F";
				minElev = -25;
				maxElev = 45;
				minTurn = -360;
				soundServo[] = {"A3\Sounds_F\vehicles\boat\Boat_Armed_01\servo_boat_comm",1.4125376,1,30};
				soundServoVertical[] = {"A3\Sounds_F\vehicles\boat\Boat_Armed_01\servo_boat_comm_vertical",1.4125376,1,30};
				maxTurn = 360;
				body = "mainTurret_2";
				gun = "mainGun_2";
				animationSourceBody = "mainTurret_2";
				animationSourceGun = "mainGun_2";
				weapons[] = 
				{
					macro_new_weapon(spider,homing_cannon)
				};
				magazines[] = 
				{
					macro_new_mag(homing_spider,10),
					macro_new_mag(homing_spider,10),
					macro_new_mag(homing_spider,10),
					macro_new_mag(homing_spider,10)
				};
				memoryPointGun = "Muzzle_T";
				selectionFireAnim = "zasleh2";
				gunBeg = "Muzzle_T";
				gunEnd = "Chamber_T";
				gunnerAction = "FieldCannon_Gunner";
				viewGunnerInExternal = 0;
				gunnergetInAction = "";
				gunnergetOutAction = "";
				memoryPointsGetInGunner = "pos_gunner";
				memoryPointsGetInGunnerDir = "pos_gunner_dir";
				memorypointgunneroptics = "gunnerview_T";
				class ViewOptics: ViewOptics
				{
					initAngleX = 0;
					minAngleX = -30;
					maxAngleX = 30;
					initAngleY = 0;
					minAngleY = -100;
					maxAngleY = 100;
					initFov = 0.117;
					minFov = 0.029;
					maxFov = 0.117;
					visionMode[] = {"Normal","NVG","Ti"};
					thermalMode[] = {0,1};
				};
				gunnerRightHandAnimName = "OtocHlaven_shake";
				gunnerLeftHandAnimName = "OtocHlaven_shake";
				ejectDeadGunner = 0;
			};
		};
		class AnimationSources{};
		soundGetOut[] = {"A3\sounds_f\dummysound",0.001,1,5};
		soundGetIn[] = {"A3\sounds_f\dummysound",0.000316228,1,5};
		destrType = "DestructBuilding";
		explosionEffect="FuelExplosion";
		class DestructionEffects
		{
			class Dust
			{
				intensity = 0.1;
				interval = 1;
				lifeTime = 0.01;
				position = "destructionEffect2";
				simulation = "particles";
				type = "HousePartDust";
			};
			class Light1
			{
				enabled = "distToWater";
				intensity = 0.1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "light";
				type = "ObjectDestructionLightSmall";
			};
			class Fire1
			{
				intensity = 0.15;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "ObjectDestructionFire1Small";
			};
			class Refract1
			{
				intensity = 1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "SmallFireFRefract";
			};
			class Sound
			{
				intensity = 1;
				interval = 1;
				lifeTime = 1;
				position = "destructionEffect";
				simulation = "sound";
				type = "Fire";
			};
			class sparks1
			{
				intensity = 0.5;
				interval = 1;
				lifeTime = 0;
				position = "destructionEffect2";
				simulation = "particles";
				type = "ObjectDestructionSparks";
			};
			class Smoke1
			{
				simulation="particles";
				type="BarelDestructionSmoke";
				position[]={0,0,0};
				intensity=0.2;
				interval=1;
				lifeTime=1;
			};
			class HouseDestr
			{
				intensity=1;
				interval=1;
				lifeTime=5;
				position="";
				simulation="destroy";
				type="DelayedDestruction";
			};
		};
	};
	
	class macro_new_vehicle(homing_spider_droid,Movable_MKII): macro_new_vehicle(homing_spider_droid,MKII)
	{
		displayName = "OG10 HSD (Mobile)";;
		forceInGarage = 0;
	};
};