//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class RD501_patch_av7_artillary
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			"RD501_av7_heavy_cannon"
		};
		weapons[]=
		{
			
		};
	};
};


#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers ;

class CfgVehicles
{
	class LandVehicle;
	class Tank: LandVehicle
	{
		class NewTurret;
		class Sounds;
		class HitPoints;
	};
	class Tank_F: Tank
	{
		class Turrets
		{
			class MainTurret: NewTurret
			{
				class ViewGunner;
				class Turrets
				{
					class CommanderOptics;
				};
			};
		};
		class HitPoints: HitPoints
		{
			class HitHull;
			class HitFuel;
			class HitEngine;
			class HitLTrack;
			class HitRTrack;
		};
	};
	class MBT_01_base_F: Tank_F{};
	class MBT_01_arty_base_F: MBT_01_base_F
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class Turrets: Turrets{};
			};
		};
	};
	class 3as_01_arty_base_F: MBT_01_arty_base_F
    {
        class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class Turrets: Turrets{};
			};
		};
    };
    class 3as_AV7: 3as_01_arty_base_F
    {
        class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class Turrets: Turrets{};
			};
		};
		class AnimationSources;
    };
	class RD501_av7_heavy_cannon : 3as_AV7
	{
		displayName = "AV7 Heavy Cannon";
		scope = 2;
		side=1;
		faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(arty));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(arty));
		author = "RD501";
		forceInGarage=1;
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[] = {"RD501_AV7_300mm_howtizer"};
				magazines[] = 
                {
                    "RD501_32Rnd_300mm_HE_shells",
                    "3AS_12Rnd_300mm_Mo_smoke",
                    "3AS_4Rnd_300mm_Mo_guided",
                    "3AS_4Rnd_300mm_Mo_LG",
                    "3AS_4Rnd_300mm_Mo_LG",
                    "3AS_12Rnd_300mm_Mo_mine",
                    "3AS_4Rnd_300mm_Mo_Cluster",
                    "3AS_4Rnd_300mm_Mo_Cluster",
                    "3AS_12Rnd_300mm_Mo_AT_mine"
                };
			};
		};
		class AnimationSources: AnimationSources
		{
			class recoil_source
			{
				source = "reload";
				weapon = "RD501_AV7_300mm_howtizer";
			};
			class Legs
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class showLegs
			{
				animPeriod = 0.001;
				author = "$STR_3as_Studio";
				displayName = "Deploy Legs";
				forceAnimate[] = {"Legs",0.5};
				forceAnimatePhase = 0;
				initphase = 0;
				source = "user";
			};
		};
	};
};