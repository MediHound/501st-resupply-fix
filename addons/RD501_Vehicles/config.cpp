
#include "../RD501_main/config_macros.hpp"
#include "_init_functions/common_xeh.hpp"


#define macro_remove_turret\
    class remove_attched_turrets \
    {\
        deleted = [_this select 0] spawn macro_fnc_name(remove_attached_objects);\
    };\


#define macro_remove_attached_stuff_XEH\
    class remove_atached_stuff \
    {\
        deleted = [_this select 0] spawn macro_fnc_name(remove_attached_objects);\
    };\

#define macro_fix_xeh\
    class anti_wobble\
    {\
        init = "(_this select 0) setVelocity [0,0,100]";\
    };

class CfgPatches
{
	class RD501_patch_vehicles
	{
		author=MACRO_QUOTE(DANKAUTHORS);
		requiredAddons[]=
		{
			macro_lvl2_req
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={};
	};
};

class RD501_WeaponFireGun
{
	cloudletDuration=0.2;
	cloudletAnimPeriod=1;
	cloudletSize=1;
	cloudletAlpha=1;
	cloudletGrowUp=0.2;
	cloudletFadeIn=0.0099999998;
	cloudletFadeOut=0.5;
	cloudletAccY=0;
	cloudletMinYSpeed=-100;
	cloudletMaxYSpeed=100;
	cloudletShape="cloudletFire";
	cloudletColor[]={1,1,1,0};
	interval=0.0099999998;
	size=3;
	sourceSize=0.5;
	timeToLive=0;
	initT=4500;
	deltaT=-3000;
	class Table
	{
		class T0
		{
			maxT=0;
			color[]={0.81999999,0.94999999,0.93000001,0};
		};
		class T1
		{
			maxT=200;
			color[]={0.75,0.76999998,0.89999998,0};
		};
		class T2
		{
			maxT=400;
			color[]={0.56,0.62,0.67000002,0};
		};
		class T3
		{
			maxT=600;
			color[]={0.38999999,0.46000001,0.47,0};
		};
		class T4
		{
			maxT=800;
			color[]={0.23999999,0.31,0.31,0};
		};
		class T5
		{
			maxT=1000;
			color[]={0.23,0.31,0.28999999,0};
		};
		class T6
		{
			maxT=1500;
			color[]={0.20999999,0.28999999,0.27000001,0};
		};
		class T7
		{
			maxT=2000;
			color[]={0.19,0.23,0.20999999,0};
		};
		class T8
		{
			maxT=2300;
			color[]={0.22,0.19,0.1,0};
		};
		class T9
		{
			maxT=2500;
			color[]={0.34999999,0.2,0.02,0};
		};
		class T10
		{
			maxT=2600;
			color[]={0.62,0.28999999,0.029999999,0};
		};
		class T11
		{
			maxT=2650;
			color[]={0.58999997,0.34999999,0.050000001,0};
		};
		class T12
		{
			maxT=2700;
			color[]={0.75,0.37,0.029999999,0};
		};
		class T13
		{
			maxT=2750;
			color[]={0.88,0.34,0.029999999,0};
		};
		class T14
		{
			maxT=2800;
			color[]={0.91000003,0.5,0.17,0};
		};
		class T15
		{
			maxT=2850;
			color[]={1,0.60000002,0.2,0};
		};
		class T16
		{
			maxT=2900;
			color[]={1,0.70999998,0.30000001,0};
		};
		class T17
		{
			maxT=2950;
			color[]={0.98000002,0.82999998,0.41,0};
		};
		class T18
		{
			maxT=3000;
			color[]={0.98000002,0.91000003,0.54000002,0};
		};
		class T19
		{
			maxT=3100;
			color[]={0.98000002,0.99000001,0.60000002,0};
		};
		class T20
		{
			maxT=3300;
			color[]={0.95999998,0.99000001,0.72000003,0};
		};
		class T21
		{
			maxT=3600;
			color[]={1,0.98000002,0.91000003,0};
		};
		class T22
		{
			maxT=4200;
			color[]={1,1,1,0};
		};
	};
};

class RD501_WeaponCloudsGun
{
	cloudletDuration=0.30000001;
	cloudletAnimPeriod=1;
	cloudletSize=1;
	cloudletAlpha=1;
	cloudletGrowUp=1;
	cloudletFadeIn=0.0099999998;
	cloudletFadeOut=1;
	cloudletAccY=0.40000001;
	cloudletMinYSpeed=0.2;
	cloudletMaxYSpeed=0.80000001;
	cloudletShape="cloudletClouds";
	cloudletColor[]={1,1,1,0};
	interval=0.050000001;
	size=3;
	sourceSize=0.5;
	timeToLive=0;
	initT=0;
	deltaT=0;
	class Table
	{
		class T0
		{
			maxT=0;
			color[]={1,1,1,0};
		};
	};
};

class RD501_WeaponFireMGun: RD501_WeaponFireGun
{
	cloudletDuration=0;
	cloudletGrowUp=0.059999999;
	cloudletFadeIn=0;
	cloudletFadeOut=0.12;
	interval=0.0049999999;
	size=0.12;
	sourceSize=0.0099999998;
	initT=3200;
	deltaT=-4000;
};

class RD501_WeaponCloudsMGun: RD501_WeaponCloudsGun
{
	cloudletGrowUp=0.050000001;
	cloudletFadeIn=0;
	cloudletFadeOut=0.1;
	cloudletDuration=0.050000001;
	cloudletAnimPeriod=1;
	cloudletSize=1;
	cloudletAlpha=0.30000001;
	cloudletAccY=0;
	cloudletMinYSpeed=-100;
	cloudletMaxYSpeed=100;
	cloudletShape="cloudletClouds";
	cloudletColor[]={1,1,1,0};
	timeToLive=0;
	interval=0.02;
	size=0.30000001;
	sourceSize=0.02;
	initT=0;
	deltaT=0;
	class Table
	{
		class T0
		{
			maxT=0;
			color[]={1,1,1,0};
		};
	};
};
class DefaultEventhandlers;
class CfgDustEffectsAir;
class RD501_AirplaneHUD
{
	topLeft="HUD LH";
	topRight="HUD PH";
	bottomLeft="HUD LD";
	color[]={0,1,0,0.1};
	class Pos10Vector
	{
		type="vector";
		pos0[]={0.5,0.27000001};
		pos10[]=
		{
			"0.5+0.9",
			"0.27+0.7"
		};
	};
	class Bones
	{
		class AGLMove1
		{
			type="linear";
			source="altitudeAGL";
			min=0;
			max=100;
			minPos[]={0.050000001,0.1};
			maxPos[]={0.050000001,0.80000001};
		};
		class AGLMove2
		{
			type="fixed";
			pos[]={0.050000001,0.80000001};
		};
		class ASLMove1
		{
			type="linear";
			source="altitudeASL";
			min=0;
			max=500;
			minPos[]={0.1,0.1};
			maxPos[]={0.1,0.80000001};
		};
		class ASLMove2
		{
			type="fixed";
			pos[]={0.1,0.80000001};
		};
		class VertSpeed
		{
			type="linear";
			source="vSpeed";
			min=-25;
			max=25;
			minPos[]={0,-0.40000001};
			maxPos[]={0,0.40000001};
		};
		class SpdMove2
		{
			source="speed";
			min=33;
			max=200;
			type="linear";
			minPos[]={0.94,0.1};
			maxPos[]={0.94,0.87};
		};
		class ILS
		{
			type="ils";
			pos0[]={0.5,0.40000001};
			pos3[]={0.69999999,0.60000002};
		};
		class WeaponAim: Pos10Vector
		{
			source="weapon";
		};
		class Target: Pos10Vector
		{
			source="target";
		};
		class TargetDistanceMissile
		{
			type="rotational";
			source="targetDist";
			center[]={0,0};
			min=100;
			max=3000;
			minAngle=-120;
			maxAngle=120;
		};
		class TargetDistanceMGun
		{
			type="rotational";
			source="targetDist";
			center[]={0,0};
			min=100;
			max=1000;
			minAngle=-180;
			maxAngle=90;
		};
		class Level0: Pos10Vector
		{
			type="horizon";
			angle=0;
		};
		class LevelP5: Level0
		{
			angle=5;
		};
		class LevelM5: Level0
		{
			angle=-5;
		};
		class LevelP10: Level0
		{
			angle=10;
		};
		class LevelM10: Level0
		{
			angle=-10;
		};
		class LevelP15: Level0
		{
			angle=15;
		};
		class LevelM15: Level0
		{
			angle=-15;
		};
		class Velocity: Pos10Vector
		{
			type="vector";
			source="velocity";
		};
		class PlaneW
		{
			type="fixed";
			pos[]={0.5,0.27000001};
		};
	};
	class Draw
	{
		alpha=0.80000001;
		color[]={0.1,0.5,0.050000001};
		clipTL[]={0,0.050000001};
		clipBR[]={1,0.89999998};
		condition="on";
		class Altitude
		{
			type="line";
			points[]=
			{
				
				{
					"AGLMove1",
					1
				},
				
				{
					"AGLMove2",
					1
				},
				{},
				
				{
					"ASLMove2",
					1
				},
				
				{
					"ASLMove1",
					1
				},
				
				{
					"ASLMove1",
					{0.02,0},
					1
				},
				
				{
					"ASLMove1",
					{0.02,0},
					1,
					"VertSpeed",
					1
				}
			};
		};
		class DimmedBase
		{
			alpha=0.30000001;
			class AltitudeBase
			{
				type="line";
				points[]=
				{
					
					{
						"AGLMove2",
						1
					},
					
					{
						"ASLMove2",
						1
					}
				};
			};
		};
		class Speed
		{
			type="line";
			points[]=
			{
				
				{
					{0.94999999,0.87},
					1
				},
				
				{
					{0.94999999,0.1},
					1
				},
				{},
				
				{
					"SpdMove2",
					{-0.050000001,0},
					1
				},
				
				{
					"SpdMove2",
					1
				}
			};
		};
		class SpeedNumber
		{
			type="text";
			align="left";
			scale=1;
			source="speed";
			sourceScale=3.5999999;
			pos[]=
			{
				"SpdMove2",
				{-0.050000001,-0.029999999},
				1
			};
			right[]=
			{
				"SpdMove2",
				{0.0099999998,-0.029999999},
				1
			};
			down[]=
			{
				"SpdMove2",
				{-0.050000001,0.029999999},
				1
			};
		};
		class PlaneW
		{
			clipTL[]={0,0.1};
			clipBR[]={1,0.89999998};
			class LineHL
			{
				type="line";
				points[]=
				{
					
					{
						"PlaneW",
						{-0.07,0},
						1
					},
					
					{
						"PlaneW",
						{-0.02,0},
						1
					},
					
					{
						"PlaneW",
						{0,-0.02},
						1
					},
					
					{
						"PlaneW",
						{0.02,0},
						1
					},
					
					{
						"PlaneW",
						{0,0.02},
						1
					},
					
					{
						"PlaneW",
						{-0.02,0},
						1
					},
					{},
					
					{
						"PlaneW",
						{0.02,0},
						1
					},
					
					{
						"PlaneW",
						{0.07,0},
						1
					}
				};
			};
			class Velocity
			{
				type="line";
				points[]=
				{
					
					{
						"Velocity",
						{0,-0.02},
						1
					},
					
					{
						"Velocity",
						{0.02,0},
						1
					},
					
					{
						"Velocity",
						{0,0.02},
						1
					},
					
					{
						"Velocity",
						{-0.02,0},
						1
					},
					
					{
						"Velocity",
						{0,-0.02},
						1
					}
				};
			};
		};
		class MGun
		{
			condition="mgun";
			class Circle
			{
				type="line";
				points[]=
				{
					
					{
						"WeaponAim",
						{0,-0.07},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"+0.7*0.07",
							"-0.7*0.07"
						},
						1
					},
					
					{
						"WeaponAim",
						{0.07,0},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"+0.7*0.07",
							"+0.7*0.07"
						},
						1
					},
					
					{
						"WeaponAim",
						{0,0.07},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"-0.7*0.07",
							"+0.7*0.07"
						},
						1
					},
					
					{
						"WeaponAim",
						{-0.07,0},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"-0.7*0.07",
							"-0.7*0.07"
						},
						1
					},
					
					{
						"WeaponAim",
						{0,-0.07},
						1
					},
					{},
					
					{
						"WeaponAim",
						{0,-0.0099999998},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"+0.7*0.01",
							"-0.7*0.01"
						},
						1
					},
					
					{
						"WeaponAim",
						{0.0099999998,0},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"+0.7*0.01",
							"+0.7*0.01"
						},
						1
					},
					
					{
						"WeaponAim",
						{0,0.0099999998},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"-0.7*0.01",
							"+0.7*0.01"
						},
						1
					},
					
					{
						"WeaponAim",
						{-0.0099999998,0},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"-0.7*0.01",
							"-0.7*0.01"
						},
						1
					},
					
					{
						"WeaponAim",
						{0,-0.0099999998},
						1
					},
					{},
					
					{
						"WeaponAim",
						
						{
							"0.03*sin(-180)",
							"-0.03*cos(-180)"
						},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"0.07*sin(-180)",
							"-0.07*cos(-180)"
						},
						1
					},
					{},
					
					{
						"WeaponAim",
						
						{
							"0.03*sin(+90)",
							"-0.03*cos(+90)"
						},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"0.07*sin(+90)",
							"-0.07*cos(+90)"
						},
						1
					},
					{},
					
					{
						"WeaponAim",
						1,
						"TargetDistanceMGun",
						{0,0.039999999},
						1
					},
					
					{
						"WeaponAim",
						1,
						"TargetDistanceMGun",
						{0,0.07},
						1
					}
				};
			};
		};
		class Missile
		{
			condition="missile";
			class Circle
			{
				type="line";
				points[]=
				{
					
					{
						"WeaponAim",
						{0,-0.1},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"+0.7*0.1",
							"-0.7*0.1"
						},
						1
					},
					
					{
						"WeaponAim",
						{0.1,0},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"+0.7*0.1",
							"+0.7*0.1"
						},
						1
					},
					
					{
						"WeaponAim",
						{0,0.1},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"-0.7*0.1",
							"+0.7*0.1"
						},
						1
					},
					
					{
						"WeaponAim",
						{-0.1,0},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"-0.7*0.1",
							"-0.7*0.1"
						},
						1
					},
					
					{
						"WeaponAim",
						{0,-0.1},
						1
					},
					{},
					
					{
						"WeaponAim",
						
						{
							"0.1*0.8*sin(-120)",
							"-0.1*0.8*cos(-120)"
						},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"0.1*1.2*sin(-120)",
							"-0.1*1.2*cos(-120)"
						},
						1
					},
					{},
					
					{
						"WeaponAim",
						
						{
							"0.1*0.8*sin(+120)",
							"-0.1*0.8*cos(+120)"
						},
						1
					},
					
					{
						"WeaponAim",
						
						{
							"0.1*1.2*sin(+120)",
							"-0.1*1.2*cos(+120)"
						},
						1
					},
					{},
					
					{
						"WeaponAim",
						1,
						"TargetDistanceMissile",
						
						{
							0,
							"0.1*0.8"
						},
						1
					},
					
					{
						"WeaponAim",
						1,
						"TargetDistanceMissile",
						
						{
							0,
							"0.1*1.2"
						},
						1
					}
				};
			};
			class Target
			{
				type="line";
				points[]=
				{
					
					{
						"Target",
						{-0.050000001,-0.050000001},
						1
					},
					
					{
						"Target",
						{0.050000001,-0.050000001},
						1
					},
					
					{
						"Target",
						{0.050000001,0.050000001},
						1
					},
					
					{
						"Target",
						{-0.050000001,0.050000001},
						1
					},
					
					{
						"Target",
						{-0.050000001,-0.050000001},
						1
					}
				};
			};
		};
		class Horizont
		{
			clipTL[]={0.2,0.1};
			clipBR[]={0.80000001,0.89999998};
			class Dimmed
			{
				alpha=0.60000002;
				class Level0
				{
					type="line";
					points[]=
					{
						
						{
							"Level0",
							{-0.40000001,0},
							1
						},
						
						{
							"Level0",
							{-0.13,0},
							1
						},
						{},
						
						{
							"Level0",
							{0.13,0},
							1
						},
						
						{
							"Level0",
							{0.40000001,0},
							1
						}
					};
				};
			};
			class LevelP5
			{
				type="line";
				points[]=
				{
					
					{
						"LevelP5",
						{-0.15000001,0.029999999},
						1
					},
					
					{
						"LevelP5",
						{-0.15000001,0},
						1
					},
					
					{
						"LevelP5",
						{0.15000001,0},
						1
					},
					
					{
						"LevelP5",
						{0.15000001,0.029999999},
						1
					}
				};
			};
			class LevelM5
			{
				type="line";
				points[]=
				{
					
					{
						"LevelM5",
						{-0.15000001,-0.029999999},
						1
					},
					
					{
						"LevelM5",
						{-0.15000001,0},
						1
					},
					
					{
						"LevelM5",
						{0.15000001,0},
						1
					},
					
					{
						"LevelM5",
						{0.15000001,-0.029999999},
						1
					}
				};
			};
			class LevelP10
			{
				type="line";
				points[]=
				{
					
					{
						"LevelP10",
						{-0.2,0.050000001},
						1
					},
					
					{
						"LevelP10",
						{-0.2,0},
						1
					},
					
					{
						"LevelP10",
						{0.2,0},
						1
					},
					
					{
						"LevelP10",
						{0.2,0.050000001},
						1
					}
				};
			};
			class LevelM10
			{
				type="line";
				points[]=
				{
					
					{
						"LevelM10",
						{-0.2,-0.050000001},
						1
					},
					
					{
						"LevelM10",
						{-0.2,0},
						1
					},
					
					{
						"LevelM10",
						{0.2,0},
						1
					},
					
					{
						"LevelM10",
						{0.2,-0.050000001},
						1
					}
				};
			};
			class LevelP15
			{
				type="line";
				points[]=
				{
					
					{
						"LevelP15",
						{-0.2,0.07},
						1
					},
					
					{
						"LevelP15",
						{-0.2,0},
						1
					},
					
					{
						"LevelP15",
						{0.2,0},
						1
					},
					
					{
						"LevelP15",
						{0.2,0.07},
						1
					}
				};
			};
			class LevelM15
			{
				type="line";
				points[]=
				{
					
					{
						"LevelM15",
						{-0.2,-0.07},
						1
					},
					
					{
						"LevelM15",
						{-0.2,0},
						1
					},
					
					{
						"LevelM15",
						{0.2,0},
						1
					},
					
					{
						"LevelM15",
						{0.2,-0.07},
						1
					}
				};
			};
		};
		class ILS
		{
			condition="ils";
			class Glideslope
			{
				clipTL[]={0.28999999,0.28999999};
				clipBR[]={0.70999998,0.70999998};
				class ILS
				{
					type="line";
					points[]=
					{
						
						{
							"ILS",
							{-10,0},
							1
						},
						
						{
							"ILS",
							{10,0},
							1
						},
						{},
						
						{
							"ILS",
							{0,-10},
							1
						},
						
						{
							"ILS",
							{0,10},
							1
						}
					};
				};
			};
			class AOABracket
			{
				type="line";
				points[]=
				{
					
					{
						{0.41999999,0.77999997},
						1
					},
					
					{
						{0.40000001,0.77999997},
						1
					},
					
					{
						{0.40000001,0.88},
						1
					},
					
					{
						{0.41999999,0.88},
						1
					}
				};
			};
		};
	};
	helmetPosition[]={-0.025,0.025,0.1};
	helmetRight[]={0.050000001,0,0};
	helmetDown[]={-0,-0.050000001,0};
	borderLeft=0.090000004;
	borderRight=0.02;
	borderTop=0.02;
	borderBottom=0.1;
	helmetMountedDisplay=1;
};

class CfgVehicles
{
	class CBA_Extended_EventHandlers_base;
	class RD501_All
	{
		scope=0;
		reversed=1;
		autocenter=1;
		animated=1;
		shadow=1;
		featureType=0;
		vehicleClass="";
		icon="iconObject";
		side=4;
		faction="Default";
		displayName="";
		speechSingular[]={};
		speechPlural[]={};
		maxDetectRange=20;
		detectSkill=20;
		mineAlertIconRange=200;
		killFriendlyExpCoef=1;
		weaponSlots=0;
		camouflage=2;
		audible=1;
		accuracy=0;
		spotableDarkNightLightsOff=0.001;
		spotableNightLightsOff=0.02;
		spotableNightLightsOn=4;
		accuracyDarkNightLightsOff=0.001;
		accuracyNightLightsOff=0.0060000001;
		accuracyNightLightsOn=0.1;
		attenuationEffectType="";
		insideSoundCoef=0.5;
		outsideSoundFilter=0;
		occludeSoundsWhenIn=0.31622776;
		obstructSoundsWhenIn=0.56234133;
		obstructSoundLFRatio=0;
		occludeSoundLFRatio=0.25;
		unloadInCombat=0;
		antiRollbarForceCoef=0;
		antiRollbarForceLimit=5;
		antiRollbarSpeedMin=20;
		antiRollbarSpeedMax=60;
		slowSpeedForwardCoef=0.30000001;
		normalSpeedForwardCoef=0.85000002;
		epeImpulseDamageCoef=30;
		gunnerHasFlares=1;
		enableManualFire=1;
		sensitivity=2.5;
		sensitivityEar=0.0074999998;
		picture="";
		portrait="";
		ghostPreview="";
		simulation="";
		destrType="DestructDefault";
		armor=100;
		armorLights=0.40000001;
		armorStructural=1;
		crewVulnerable=1;
		damageResistance=0.0040000002;
		crewCrashProtection=1;
		replaceDamaged="";
		replaceDamagedLimit=0.89999998;
		replaceDamagedHitpoints[]={};
		keepInEPESceneAfterDeath=0;
		fuelCapacity=0;
		fuelConsumptionRate=0.0099999998;
		extCameraPosition[]={0,2,-20};
		groupCameraPosition[]={0,5,-30};
		extCameraParams[]={1};
		steerAheadSimul=0.30000001;
		steerAheadPlan=0.40000001;
		cameraSmoothSpeed=5;
		minFireTime=20;
		predictTurnSimul=1.2;
		predictTurnPlan=1;
		indirectHitEnemyCoefAI=10;
		indirectHitFriendlyCoefAI=-20;
		indirectHitCivilianCoefAI=-20;
		indirectHitUnknownCoefAI=-0.5;
		precision=5;
		brakeDistance=5;
		formationX=10;
		formationZ=20;
		formationTime=5;
		alwaysTarget=0;
		irTarget=1;
		irScanRangeMin=0;
		irScanRangeMax=0;
		irScanToEyeFactor=1;
		irScanGround=1;
		laserTarget=0;
		laserScanner=0;
		nvTarget=0;
		nvScanner=0;
		artilleryTarget=0;
		artilleryScanner=0;
		canUseScanners=1;
		preferRoads=0;
		unitInfoType="RscUnitInfoTank";
		unitInfoTypeLite=0;
		hideUnitInfo=0;
		nightVision=0;
		commanderCanSee=31;
		gunnerCanSee="4+8+16";
		driverCanSee="2+8+16";
		showCrewAim=7;
		radarType=0;
		maxSpeed=80;
		limitedSpeedCoef=0.22;
		secondaryExplosion=-1;
		hasDriver=1;
		driverForceOptics=0;
		hideWeaponsDriver=1;
		hideWeaponsCargo=0;
		getInRadius=2.5;
		selectionClan="";
		selectionDashboard="";
		selectionShowDamage="";
		selectionBackLights="";
		memoryPointSupply="doplnovani";
		supplyRadius=-1;
		enableGPS=0;
		enableWatch=0;
		enableRadio=0;
		lockDetectionSystem=0;
		incomingMissileDetectionSystem=0;
		headGforceLeaningFactor[]={0.0099999998,0.0020000001,0.0099999998};
		usePreciseGetInAction=0;
		allowTabLock=1;
		showAllTargets=0;
		dustFrontLeftPos="dustFrontLeft";
		dustFrontRightPos="dustFrontRight";
		dustBackLeftPos="dustBackLeft";
		dustBackRightPos="dustBackRight";
		wheelCircumference=1;
		waterLeakiness=0.5;
		waterResistanceCoef=0.5;
		waterLinearDampingCoefX=0;
		waterLinearDampingCoefY=0;
		waterAngularDampingCoef=0;
		showNVGDriver=0;
		showNVGCommander=0;
		showNVGGunner=0;
		showNVGCargo[]={0};
		soundAttenuationCargo[]={1};
		countsForScoreboard=1;
		hullDamageCauseExplosion=0;
		class EventHandlers
		{
			class CBA_Extended_EventHandlers: CBA_Extended_EventHandlers_base
			{
			};
		};
		class MarkerLights
		{
		};
		class NVGMarkers
		{
		};
		class NVGMarker
		{
			diffuse[]={1,1,1,1};
			ambient[]={1,1,1,1};
			brightness=1;
			blinking=0;
			onlyInNvg=0;
		};
		class Turrets
		{
		};
		class HeadLimits
		{
			initAngleX=5;
			minAngleX=-30;
			maxAngleX=40;
			initAngleY=0;
			minAngleY=-90;
			maxAngleY=90;
			minAngleZ=-45;
			maxAngleZ=45;
			rotZRadius=0.2;
		};
		class ViewPilot
		{
			initAngleX=5;
			minAngleX=-55;
			maxAngleX=85;
			initAngleY=0;
			minAngleY=-150;
			maxAngleY=150;
			minFov=0.25;
			maxFov=1.25;
			initFov=0.75;
			minMoveX=0;
			maxMoveX=0;
			minMoveY=0;
			maxMoveY=0;
			minMoveZ=0;
			maxMoveZ=0;
			speedZoomMaxSpeed=1e+010;
			speedZoomMaxFOV=0;
		};
		class ViewCargo
		{
			initAngleX=5;
			minAngleX=-85;
			maxAngleX=85;
			initAngleY=0;
			minAngleY=-150;
			maxAngleY=150;
			minFov=0.25;
			maxFov=1.25;
			initFov=0.75;
			minMoveX=0;
			maxMoveX=0;
			minMoveY=0;
			maxMoveY=0;
			minMoveZ=0;
			maxMoveZ=0;
			speedZoomMaxSpeed=1e+010;
			speedZoomMaxFOV=0;
		};
		class ViewOptics
		{
			initAngleX=0;
			minAngleX=-30;
			maxAngleX=30;
			initAngleY=0;
			minAngleY=-100;
			maxAngleY=100;
			initFov=0.69999999;
			minFov=0.41999999;
			maxFov=0.85000002;
			minMoveX=0;
			maxMoveX=0;
			minMoveY=0;
			maxMoveY=0;
			minMoveZ=0;
			maxMoveZ=0;
			speedZoomMaxSpeed=1e+010;
			speedZoomMaxFOV=0;
		};
		class PilotSpec
		{
			showHeadPhones=0;
		};
		class CargoSpec
		{
			class Cargo1
			{
				showHeadPhones=0;
			};
		};
		transportSoldier=0;
		transportAmmo=0;
		transportMaxMagazines=0;
		transportMaxWeapons=0;
		transportMaxBackpacks=0;
		isbackpack=0;
		transportFuel=0;
		transportRepair=0;
		transportVehiclesCount=0;
		transportVehiclesMass=0;
		class TransportWeapons
		{
		};
		class TransportMagazines
		{
		};
		attendant=0;
		engineer=0;
		uavHacker=0;
		soundEngine[]=
		{
			"",
			1,
			1
		};
		soundEnviron[]=
		{
			"",
			1,
			1
		};
		class SoundEnvironExt
		{
		};
		class SoundEquipment
		{
		};
		class SoundGear
		{
		};
		class SoundBreath
		{
		};
		class SoundBreathSwimming
		{
		};
		class SoundBreathInjured
		{
		};
		class SoundHitScream
		{
		};
		class SoundInjured
		{
		};
		class SoundBreathAutomatic
		{
		};
		class SoundDrown
		{
		};
		class SoundChoke
		{
		};
		class SoundRecovered
		{
		};
		class SoundBurning
		{
		};
		class PulsationSound
		{
		};
		class SoundDrowning
		{
		};
		soundCrash[]=
		{
			"",
			0.31622776,
			1
		};
		soundLandCrash[]=
		{
			"",
			1,
			1
		};
		soundWaterCrash[]=
		{
			"",
			0.17782794,
			1
		};
		soundGetIn[]=
		{
			"",
			0.00031622776,
			1
		};
		soundGetOut[]=
		{
			"",
			0.00031622776,
			1
		};
		soundServo[]=
		{
			"",
			0.0031622776,
			0.5
		};
		soundElevation[]=
		{
			"",
			0.0031622776,
			0.5
		};
		sounddamage[]=
		{
			"",
			1,
			1
		};
		soundEngineOnInt[]=
		{
			"",
			1,
			1
		};
		soundEngineOffInt[]=
		{
			"",
			1,
			1
		};
		soundEngineOnExt[]=
		{
			"",
			1,
			1
		};
		soundEngineOffExt[]=
		{
			"",
			1,
			1
		};
		soundGearUp[]=
		{
			"",
			1,
			1
		};
		soundGearDown[]=
		{
			"",
			1,
			1
		};
		soundFlapsUp[]=
		{
			"",
			1,
			1
		};
		soundFlapsDown[]=
		{
			"",
			1,
			1
		};
		cabinOpenSound[]=
		{
			"",
			1,
			1
		};
		cabinCloseSound[]=
		{
			"",
			1,
			1
		};
		cabinOpenSoundInternal[]=
		{
			"",
			1,
			1
		};
		cabinCloseSoundInternal[]=
		{
			"",
			1,
			1
		};
		soundCrashes[]=
		{
			"soundCrash",
			1
		};
		soundLandCrashes[]=
		{
			"soundLandCrash",
			1
		};
		soundWaterCrashes[]=
		{
			"soundWaterCrash",
			1
		};
		emptySound[]=
		{
			"",
			0,
			1
		};
		soundWoodCrash[]=
		{
			"emptySound",
			0
		};
		soundBushCrash[]=
		{
			"emptySound",
			0
		};
		soundBuildingCrash[]=
		{
			"emptySound",
			0
		};
		soundArmorCrash[]=
		{
			"emptySound",
			0
		};
		soundLocked[]=
		{
			"",
			1,
			1
		};
		soundIncommingMissile[]=
		{
			"",
			1,
			1
		};
		soundDammage[]=
		{
			"",
			1,
			1
		};
		weapons[]={};
		magazines[]={};
		type=1;
		threat[]={0.69999999,0.5,0.30000001};
		typicalCargo[]={};
		class Reflectors
		{
		};
		aggregateReflectors[]={};
		getInAction="";
		getOutAction="";
		driverAction="";
		driverInAction="";
		cargoGetInAction[]={};
		cargoGetOutAction[]={};
		cargoAction[]={};
		cargoIsCoDriver[]={0};
		driverCompartments="Compartment1";
		cargoCompartments[]=
		{
			"Compartment1"
		};
		driverOpticsModel="";
		driverOpticsEffect[]={};
		driverOpticsColor[]={1,1,1,1};
		hideProxyInCombat=0;
		forceHideDriver=0;
		canHideDriver=-1;
		castDriverShadow=0;
		castCargoShadow=0;
		viewDriverShadow=1;
		viewDriverShadowDiff=1;
		viewDriverShadowAmb=1;
		viewCargoShadow=1;
		viewCargoShadowDiff=1;
		viewCargoShadowAmb=1;
		ejectDeadDriver=0;
		ejectDeadCargo=0;
		crew="Civilian";
		hiddenSelections[]={};
		hiddenSelectionsTextures[]={};
		hiddenSelectionsMaterials[]={};
		hiddenUnderwaterSelections[]={};
		shownUnderWaterSelections[]={};
		hiddenUnderwaterSelectionsTextures[]={};
		class FxExplo
		{
		};
		class AnimationSources
		{
		};
		selectionFireAnim="";
		class GunFire: RD501_WeaponFireGun
		{
		};
		class GunClouds: RD501_WeaponCloudsGun
		{
		};
		class MGunClouds: RD501_WeaponCloudsMGun
		{
		};
		class Damage
		{
			tex[]={};
			mat[]={};
		};
		selectionDamage="zbytek";
		HeadAimDown=0;
		class DestructionEffects
		{
			class Light1
			{
				simulation="light";
				type="ObjectDestructionLight";
				position="destructionEffect1";
				intensity=0.001;
				interval=1;
				lifeTime=3;
			};
			class Smoke1
			{
				simulation="particles";
				type="ObjectDestructionSmoke";
				position="destructionEffect1";
				intensity=0.15000001;
				interval=1;
				lifeTime=3.5;
			};
			class Fire1
			{
				simulation="particles";
				type="ObjectDestructionFire1";
				position="destructionEffect1";
				intensity=0.15000001;
				interval=1;
				lifeTime=3;
			};
			class Sparks1
			{
				simulation="particles";
				type="ObjectDestructionSparks";
				position="destructionEffect1";
				intensity=0;
				interval=1;
				lifeTime=0;
			};
			class Sound
			{
				simulation="sound";
				position="destructionEffect1";
				intensity=1;
				interval=1;
				lifeTime=1;
				type="Fire";
			};
			class Light2
			{
				simulation="light";
				type="ObjectDestructionLight";
				position="destructionEffect2";
				intensity=0.001;
				interval=1;
				lifeTime=3;
			};
			class Fire2
			{
				simulation="particles";
				type="ObjectDestructionFire2";
				position="destructionEffect2";
				intensity=0.15000001;
				interval=1;
				lifeTime=3;
			};
			class Smoke1_2
			{
				simulation="particles";
				type="ObjectDestructionSmoke1_2";
				position="destructionEffect2";
				intensity=0.15000001;
				interval=1;
				lifeTime=3.5;
			};
		};
		cargoCanEject=1;
		driverCanEject=1;
		fireResistance=10;
		airCapacity=10;
		waterResistance=10;
		waterDamageEngine=0.2;
		maxFordingDepth=1;
		impactEffectsSea="ImpactEffectsSea";
		damageTexDelay=0;
		coefInside=2;
		coefInsideHeur=2;
		slingLoadCargoMemoryPoints[]={};
		slingLoadCargoMemoryPointsDir[]={};
		numberPhysicalWheels=0;
		damageHalf[]={};
		damageFull[]={};
		class camShakeGForce
		{
			power=1;
			frequency=20;
			distance=0;
			minSpeed=1;
			duration=3;
		};
		minGForce=0.2;
		maxGForce=2;
		gForceShakeAttenuation=0.5;
		class camShakeDamage
		{
			power=0.5;
			frequency=60;
			distance=-1;
			minSpeed=1;
			attenuation=0.5;
			duration=3;
		};
		weaponsGroup1="1 + 2";
		weaponsGroup2=4;
		weaponsGroup3="8 + 16 + 32";
		weaponsGroup4="64 + 128";
		soundTurnIn[]=
		{
			"",
			0.00031622776,
			1
		};
		soundTurnOut[]=
		{
			"",
			0.00031622776,
			1
		};
		soundTurnInInternal[]=
		{
			"",
			0.00031622776,
			1
		};
		soundTurnOutInternal[]=
		{
			"",
			0.00031622776,
			1
		};
		features="";
		author="$STR_A3_Bohemia_Interactive";
		mapSize=2.2;
		class SpeechVariants
		{
			class Default
			{
				speechSingular[]=
				{
					"veh_unknown_s"
				};
				speechPlural[]=
				{
					"veh_unknown_p"
				};
			};
		};
		textSingular="$STR_A3_nameSound_veh_unknown_s";
		textPlural="$STR_A3_nameSound_veh_unknown_p";
		nameSound="veh_unknown_s";
		_generalMacro="All";
		model="\A3\Weapons_F\empty.p3d";
		cost=100;
		insideDetectCoef=0.050000001;
		coefSpeedInside=1;
		windSockExist=0;
		SLX_XEH_DISABLED=0;
	
	};
	
	class RD501_AllVehicles : RD501_All
	{
		icon="iconVehicle";
		nameSound="unknown";
		class SquadTitles
		{
			name="clan_sign";
			color[]={0,0,0,0.75};
		};
		memoryPointDriverOptics[]=
		{
			"driverview",
			"pilot"
		};
		memoryPointsGetInDriver="pos driver";
		memoryPointsGetInDriverDir="pos driver dir";
		memoryPointsGetInCargo="pos cargo";
		memoryPointsGetInCargoDir="pos cargo dir";
		memoryPointsGetInCoDriver="pos codriver";
		memoryPointsGetInCoDriverDir="pos codriver dir";
		memoryPointsGetInDriverPrecise="pos driver";
		memoryPointsGetInCargoPrecise[]=
		{
			"pos cargo"
		};
		memoryPointsLeftWaterEffect="waterEffectL";
		memoryPointsRightWaterEffect="waterEffectR";
		memoryPointTaskMarker="";
		memoryPointTaskMarkerOffset[]={0,0,0};
		selectionClan="clan";
		selectionDashboard="podsvit pristroju";
		selectionShowDamage="poskozeni";
		selectionBackLights="zadni svetlo";
		class NewTurret
		{
			body="mainTurret";
			gun="mainGun";
			animationSourceBody="mainTurret";
			animationSourceGun="mainGun";
			animationSourceHatch="hatchGunner";
			animationSourceCamElev="camElev";
			proxyType="CPGunner";
			proxyIndex=1;
			gunnerName="$STR_POSITION_GUNNER";
			gunnerType="";
			primaryGunner=1;
			primaryObserver=0;
			weapons[]={};
			magazines[]={};
			soundServo[]=
			{
				"",
				0.0031622776,
				1
			};
			soundElevation[]=
			{
				"",
				0.0031622776,
				1
			};
			minElev=-4;
			maxElev=20;
			initElev=0;
			minTurn=-360;
			maxTurn=360;
			initTurn=0;
			minOutElev=-4;
			maxOutElev=20;
			initOutElev=0;
			minOutTurn=-60;
			maxOutTurn=60;
			initOutTurn=0;
			maxHorizontalRotSpeed=1.2;
			maxVerticalRotSpeed=1.2;
			minCamElev=-90;
			maxCamElev=90;
			initCamElev=0;
			stabilizedInAxes=3;
			primary=1;
			hasGunner=1;
			commanding=1;
			gunnerGetInAction="";
			gunnerGetOutAction="";
			turretCanSee=0;
			canUseScanners=1;
			class ViewGunner: ViewOptics
			{
				initAngleX=5;
				minAngleX=-75;
				maxAngleX=85;
				initAngleY=0;
				minAngleY=-150;
				maxAngleY=150;
				minFov=0.25;
				maxFov=1.25;
				initFov=0.75;
				minMoveX=0;
				maxMoveX=0;
				minMoveY=0;
				maxMoveY=0;
				minMoveZ=0;
				maxMoveZ=0;
				continuous=0;
			};
			class TurretSpec
			{
				showHeadPhones=0;
			};
			gunnerOpticsModel="";
			gunnerOpticsColor[]={0,0,0,1};
			gunnerForceOptics=1;
			gunnerOpticsShowCursor=0;
			turretInfoType="";
			gunnerOutOpticsModel="";
			gunnerOutOpticsColor[]={0,0,0,1};
			gunnerOpticsEffect[]={};
			gunnerOutOpticsEffect[]={};
			memoryPointGunnerOutOptics="";
			gunnerOutForceOptics=0;
			gunnerOutOpticsShowCursor=0;
			gunnerFireAlsoInInternalCamera=1;
			gunnerOutFireAlsoInInternalCamera=1;
			gunnerUsesPilotView=0;
			castGunnerShadow=0;
			viewGunnerShadow=1;
			viewGunnerShadowDiff=1;
			viewGunnerShadowAmb=1;
			ejectDeadGunner=0;
			hideWeaponsGunner=1;
			canHideGunner=-1;
			forceHideGunner=0;
			outGunnerMayFire=0;
			inGunnerMayFire=1;
			showHMD=0;
			viewGunnerInExternal=0;
			lockWhenDriverOut=0;
			lockWhenVehicleSpeed=-1;
			gunnerCompartments="Compartment1";
			LODTurnedIn=-1;
			LODTurnedOut=-1;
			startEngine=1;
			memoryPointsGetInGunnerPrecise="";
			missileBeg="spice rakety";
			missileEnd="konec rakety";
			armorLights=0.40000001;
			class Reflectors
			{
			};
			aggregateReflectors[]={};
			class GunFire: RD501_WeaponFireGun
			{
			};
			class GunClouds: RD501_WeaponCloudsGun
			{
			};
			class MGunClouds: RD501_WeaponCloudsMGun
			{
			};
			class HitPoints
			{
				class HitTurret
				{
					armor=0.80000001;
					material=51;
					name="turret";
					visual="turret";
					passThrough=1;
					explosionShielding=1;
				};
				class HitGun
				{
					armor=0.60000002;
					material=52;
					name="gun";
					visual="gun";
					passThrough=1;
					explosionShielding=1;
				};
			};
			class Turrets
			{
			};
			class ViewOptics
			{
				initAngleX=0;
				minAngleX=-30;
				maxAngleX=30;
				initAngleY=0;
				minAngleY=-100;
				maxAngleY=100;
				initFov=0.30000001;
				minFov=0.07;
				maxFov=0.34999999;
				minMoveX=0;
				maxMoveX=0;
				minMoveY=0;
				maxMoveY=0;
				minMoveZ=0;
				maxMoveZ=0;
				speedZoomMaxSpeed=1e+010;
				speedZoomMaxFOV=0;
			};
			forceNVG=0;
			isCopilot=0;
			canEject=1;
			gunnerLeftHandAnimName="";
			gunnerRightHandAnimName="";
			gunnerLeftLegAnimName="";
			gunnerRightLegAnimName="";
			gunnerDoor="";
			preciseGetInOut=0;
			turretFollowFreeLook=0;
			allowTabLock=1;
			showAllTargets=0;
			dontCreateAI=0;
			disableSoundAttenuation=0;
			slingLoadOperator=0;
			playerPosition=0;
			allowLauncherIn=0;
			allowLauncherOut=0;
			class TurnIn
			{
				turnOffset=0;
			};
			class TurnOut: TurnIn
			{
			};
			gunnerInAction="ManActTestDriver";
			gunnerAction="ManActTestDriver";
			gunBeg="usti hlavne";
			gunEnd="konec hlavne";
			memoryPointGunnerOptics="gunnerview";
			memoryPointsGetInGunner="pos gunner";
			memoryPointsGetInGunnerDir="pos gunner dir";
			memoryPointGun="kulas";
			selectionFireAnim="zasleh";
			showCrewAim=0;
			ace_fcs_Enabled=0;
			ace_fcs_MinDistance=200;
			ace_fcs_MaxDistance=5500;
			ace_fcs_DistanceInterval=5;
		};
		class ViewPilot
		{
			initAngleX=5;
			minAngleX=-85;
			maxAngleX=85;
			initAngleY=0;
			minAngleY=-150;
			maxAngleY=150;
			minFov=0.25;
			maxFov=1.25;
			initFov=0.75;
			minMoveX=0;
			maxMoveX=0;
			minMoveY=0;
			maxMoveY=0;
			minMoveZ=0;
			maxMoveZ=0;
			speedZoomMaxSpeed=1e+010;
			speedZoomMaxFOV=0;
		};
		class ViewCargo
		{
			initAngleX=5;
			minAngleX=-75;
			maxAngleX=85;
			initAngleY=0;
			minAngleY=-150;
			maxAngleY=150;
			minFov=0.25;
			maxFov=1.25;
			initFov=0.75;
			minMoveX=0;
			maxMoveX=0;
			minMoveY=0;
			maxMoveY=0;
			minMoveZ=0;
			maxMoveZ=0;
			speedZoomMaxSpeed=1e+010;
			speedZoomMaxFOV=0;
		};
		class ViewOptics
		{
			initAngleX=0;
			minAngleX=-30;
			maxAngleX=30;
			initAngleY=0;
			minAngleY=-100;
			maxAngleY=100;
			initFov=0.69999999;
			minFov=0.41999999;
			maxFov=0.85000002;
			minMoveX=0;
			maxMoveX=0;
			minMoveY=0;
			maxMoveY=0;
			minMoveZ=0;
			maxMoveZ=0;
			speedZoomMaxSpeed=1e+010;
			speedZoomMaxFOV=0;
		};
		class PilotSpec
		{
			showHeadPhones=0;
		};
		class CargoSpec
		{
			class Cargo1
			{
				showHeadPhones=0;
			};
		};
		class MFD
		{
		};
		class Sounds
		{
			class Engine
			{
				sound="soundEngine";
				frequency="rpm";
				volume="rpm + speed";
			};
			class Movement
			{
				sound="soundEnviron";
				frequency="speed";
				volume="speed";
			};
		};
		class SoundEvents
		{
		};
		tracksSpeed=0;
		selectionLeftOffset="";
		selectionRightOffset="";
		class RenderTargets
		{
		};
		cargoProxyIndexes[]={};
		driverLeftHandAnimName="";
		driverRightHandAnimName="";
		driverLeftLegAnimName="";
		driverRightLegAnimName="";
		driverDoor="";
		cargoDoors[]={};
		hasTerminal=0;
		getInOutOnProxy=0;
		preciseGetInOut=0;
		cargoPreciseGetInOut[]={0};
		availableForSupportTypes[]={};
		waterPPInVehicle=1;
		author="$STR_A3_Bohemia_Interactive";
		_generalMacro="AllVehicles";
		weaponsGroup1="1 + 		2";
		weaponsGroup2=4;
		weaponsGroup3="8 + 	16 + 	32";
		weaponsGroup4="64 + 		128";
		impactEffectsSea="ImpactEffectsSea";
		impactEffectSpeedLimit=8;
		showCrewAim=0;
		class CargoTurret: NewTurret
		{
			class ViewGunner: ViewCargo
			{
			};
			class Hitpoints
			{
			};
			animationSourceBody="";
			animationSourceGun="";
			body="";
			canEject=1;
			commanding=0;
			dontCreateAI=1;
			gun="";
			gunnerGetInAction="GetInLow";
			gunnerGetOutAction="GetOutLow";
			gunnerName="cargo";
			hideWeaponsGunner=0;
			isCopilot=0;
			memoryPointsGetInGunner="pos cargo";
			memoryPointsGetInGunnerDir="pos cargo dir";
			primaryGunner=0;
			proxyType="CPCargo";
			startEngine=0;
			turretFollowFreeLook=0;
			viewGunnerInExternal=1;
			disableSoundAttenuation=1;
			outGunnerMayFire=1;
			isPersonTurret=1;
			showAsCargo=1;
			maxElev=45;
			minElev=-45;
			maxTurn=95;
			minTurn=-95;
		};
		tf_hasLRradio=0;
		tf_isolatedAmount=0;
		ace_refuel_flowRate=1;
		htMin=60;
		htMax=1800;
		afMax=70;
		mfMax=50;
		mFact=0;
		tBody=0;
		curatorInfoType="";
		curatorInfoTypeEmpty="";
	
	};
	
	class RD501_Air: RD501_AllVehicles
	{
		icon="iconAir";
		displayName="";
		nameSound="veh_aircraft";
		formationX=50;
		formationZ=100;
		precision=100;
		brakeDistance=200;
		formationTime=10;
		gearsUpFrictionCoef=0.5;
		airBrakeFrictionCoef=3;
		airFrictionCoefs2[]={0.001,0.00050000002,5.9999998e-005};
		airFrictionCoefs1[]={0.1,0.050000001,0.0060000001};
		airFrictionCoefs0[]={0,0,0};
		class Components
		{
			class TransportCountermeasuresComponent
			{
			};
		};
		altFullForce=2000;
		altNoForce=6000;
		armor=20;
		cost=10000000;
		fuelCapacity=1000;
		maxSpeed=400;
		attenuationEffectType="";
		insideSoundCoef=0.031622775;
		outsideSoundFilter=1;
		occludeSoundsWhenIn=0.31622776;
		obstructSoundsWhenIn=0.31622776;
		weapons[]=
		{
			"FakeWeapon"
		};
		magazines[]=
		{
			"FakeWeapon"
		};
		type=2;
		irScanRangeMin=2000;
		irScanRangeMax=10000;
		irScanToEyeFactor=2;
		nightVision=0;
		audible=10;
		unitInfoType="RscUnitInfoAir";
		threat[]={0.30000001,1,0.69999999};
		driverAction="";
		driverCompartments=0;
		cargoCompartments[]={0};
		gunnerCanSee="31+32";
		driverCanSee="31+32";
		typicalCargo[]=
		{
			"Soldier"
		};
		getInRadius=5;
		transportMaxMagazines=20;
		transportMaxWeapons=3;
		enableGPS=1;
		class MarkerLights
		{
			class RedStill
			{
				name="cerveny pozicni";
				color[]={0.30000001,0.029999999,0.029999999,1};
				ambient[]={0.029999999,0.003,0.003,1};
				brightness=0.0099999998;
				blinking=0;
			};
			class GreenStill
			{
				name="zeleny pozicni";
				color[]={0.029999999,0.30000001,0.029999999,1};
				ambient[]={0.003,0.029999999,0.003,1};
				brightness=0.0099999998;
				blinking=0;
			};
			class WhiteStill
			{
				name="bily pozicni";
				color[]={0.30000001,0.30000001,0.30000001,1};
				ambient[]={0.029999999,0.029999999,0.029999999,1};
				brightness=0.0099999998;
				blinking=0;
			};
			class WhiteBlinking
			{
				name="bily pozicni blik";
				color[]={1,1,1,1};
				ambient[]={0.1,0.1,0.1,1};
				brightness=0.0099999998;
				blinking=1;
			};
			class RedBlinking
			{
				name="cerveny pozicni blik";
				color[]={0.5,0.050000001,0.050000001,1};
				ambient[]={0.050000001,0.0049999999,0.0049999999,1};
				brightness=0.0099999998;
				blinking=1;
			};
			class PositionRed
			{
				color[]={0.80000001,0,0};
				ambient[]={0.079999998,0,0};
				intensity=75;
				name="PositionLight_red_1_pos";
				drawLight=1;
				drawLightSize=0.15000001;
				drawLightCenterSize=0.039999999;
				activeLight=0;
				blinking=0;
				dayLight=0;
				useFlare=0;
				class Attenuation
				{
					start=0;
					constant=0;
					linear=25;
					quadratic=50;
					hardLimitStart=0.75;
					hardLimitEnd=1;
				};
			};
			class PositionGreen: PositionRed
			{
				color[]={0,0.80000001,0};
				ambient[]={0,0.079999998,0};
				name="PositionLight_green_1_pos";
			};
			class PositionWhite: PositionRed
			{
				color[]={1,1,1};
				ambient[]={0.1,0.1,0.1};
				name="PositionLight_white_1_pos";
				drawLightSize=0.2;
			};
			class CollisionRed: PositionRed
			{
				color[]={0.89999998,0.15000001,0.1};
				ambient[]={0.090000004,0.015,0.0099999998};
				name="CollisionLight_red_1_pos";
				blinking=1;
				blinkingPattern[]={0.2,1.3};
				blinkingPatternGuarantee=0;
				drawLightSize=0.25;
				drawLightCenterSize=0.079999998;
			};
			class CollisionWhite: PositionRed
			{
				color[]={1,1,1};
				ambient[]={0.1,0.1,0.1};
				name="CollisionLight_white_1_pos";
				blinking=1;
				blinkingPattern[]={0.1,0.89999998};
				blinkingPatternGuarantee=0;
				drawLightSize=0.2;
				drawLightCenterSize=0.039999999;
			};
		};
		class ACE_SelfActions
		{
		};
		weaponsGroup1="1 + 		2";
		weaponsGroup2=4;
		weaponsGroup3="8 + 	16 + 	32";
		weaponsGroup4="64 + 		128";
		editorSubcategory="EdSubcat_Planes";
		memoryPointTaskMarkerOffset[]={0,0.30000001,0};
		rightDustEffects[]=
		{
			
			{
				"ZI_Tembelan_sand",
				"RDustEffectsAir"
			},
			
			{
				"ZI_Tembelan_sand",
				"RSandEffects"
			},
			
			{
				"ZI_Tembelan_grass_green",
				"RGrassEffects"
			},
			
			{
				"ZI_Tembelan_grass_green",
				"RDustEffectsAir"
			},
			
			{
				"ZI_Tembelan_dirt",
				"RDirtEffects"
			},
			
			{
				"ZI_Tembelan_dirt",
				"RDustEffectsAir"
			},
			
			{
				"ZI_Tembelan_soil",
				"RDirtEffects"
			},
			
			{
				"ZI_Tembelan_soil",
				"RDustEffectsAir"
			},
			
			{
				"ZI_Tembelan_pine",
				"RGrassDryEffects"
			},
			
			{
				"ZI_Tembelan_pine",
				"RDustEffectsAir"
			},
			
			{
				"ZI_Tembelan_concrete",
				"RDustEffectsAir"
			}
		};
		leftDustEffects[]=
		{
			
			{
				"ZI_Tembelan_sand",
				"LDustEffectsAir"
			},
			
			{
				"ZI_Tembelan_sand",
				"LSandEffects"
			},
			
			{
				"ZI_Tembelan_grass_green",
				"LGrassEffects"
			},
			
			{
				"ZI_Tembelan_grass_green",
				"LDustEffectsAir"
			},
			
			{
				"ZI_Tembelan_dirt",
				"LDirtEffects"
			},
			
			{
				"ZI_Tembelan_dirt",
				"LDustEffectsAir"
			},
			
			{
				"ZI_Tembelan_soil",
				"LDirtEffects"
			},
			
			{
				"ZI_Tembelan_soil",
				"LDustEffectsAir"
			},
			
			{
				"ZI_Tembelan_pine",
				"LGrassDryEffects"
			},
			
			{
				"ZI_Tembelan_pine",
				"LDustEffectsAir"
			},
			
			{
				"ZI_Tembelan_concrete",
				"LDustEffectsAir"
			}
		};
		class DustEffects: CfgDustEffectsAir
		{
		};
		author="$STR_A3_Bohemia_Interactive";
		mapSize=10.56;
		_generalMacro="Air";
		waterLeakiness=100;
		maxFordingDepth=0.001;
		waterResistance=1;
		impactEffectsSea="ImpactEffectsAir";
		accuracy=0;
		flareVelocity=100;
		enableRadio=1;
		class AnimationSources
		{
			class CollisionLightRed_source
			{
				source="MarkerLight";
				markerLight="CollisionRed";
			};
			class CollisionLightWhite_source
			{
				source="MarkerLight";
				markerLight="CollisionWhite";
			};
		};
		memoryPointCM[]=
		{
			"flare_launcher1",
			"flare_launcher2"
		};
		memoryPointCMDir[]=
		{
			"flare_launcher1_dir",
			"flare_launcher2_dir"
		};
		radarType=4;
		radarTarget=1;
		radarTargetSize=1;
		visualTarget=1;
		visualTargetSize=1;
		irTargetSize=1;
		LockDetectionSystem="8 + 4";
		incomingMissileDetectionSystem="8 + 16";
		countermeasureActivationRadius=10000;
		class TransportItems
		{
			class _xx_FirstAidKit
			{
				name="FirstAidKit";
				count=4;
			};
		};
		class Exhausts
		{
			class Exhaust1
			{
				position="exhaust1";
				direction="exhaust1_dir";
				effect="ExhaustsEffectPlane";
			};
			class Exhaust2
			{
				position="exhaust2";
				direction="exhaust2_dir";
				effect="ExhaustsEffectPlane";
			};
		};
		class camShakeGForce
		{
			power=0.2;
			frequency=3;
			distance=0;
			minSpeed=1;
		};
		class camShakeDamage
		{
			power=0.5;
			frequency=60;
			distance=-1;
			minSpeed=1;
			attenuation=0.5;
			duration=3;
		};
		minGForce=0.2;
		maxGForce=2;
		gForceShakeAttenuation=0.5;
		secondaryExplosion=-1;
		fuelExplosionPower=1;
		class EventHandlers
		{
			class babe_int_initEH
			{
				init="(_this select 0) call babe_int_fnc_seats3d";
			};
		};
		tf_hasLRradio=1;
		tf_isolatedAmount=0.1;
		ace_refuel_flowRate=8;
	};

	class RD501_Plane: RD501_Air
	{
		vehicleClass="Air";
		icon="iconPlane";
		displayName="$STR_DN_PLANE";
		maxSpeed=450;
		landingSpeed=0;
		flapsFrictionCoef=0.5;
		formationX=200;
		formationZ=300;
		precision=200;
		brakeDistance=500;
		steerAheadSimul=1;
		steerAheadPlan=2;
		wheelSteeringSensitivity=1;
		gearRetracting=1;
		cabinOpening=1;
		durationGetIn=0.99000001;
		durationGetOut=0.99000001;
		flaps=1;
		airBrake=1;
		vtol=0;
		tailHook=0;
		lightOnGear=1;
		gearUpTime=3.3299999;
		gearDownTime=2;
		ejectSpeed[]={0,40,0};
		ejectDamageLimit=0.44999999;
		minFireTime=60;
		cost=2000000;
		simulation="airplanex";
		minGunElev=0;
		maxGunElev=0;
		minGunTurn=0;
		maxGunTurn=0;
		gunAimDown=0;
		rudderInfluence=0.96194696;
		envelope[]={0,0.2,0.89999998,2.0999999,2.5,3.3,3.5,3.2,2.5,2,1.5,1};
		angleOfIndicence=0.052359872;
		draconicForceXCoef=7.5;
		draconicForceYCoef=1;
		draconicForceZCoef=1;
		draconicTorqueXCoef=0.15000001;
		draconicTorqueYCoef=1;
		thrustCoef[]={};
		elevatorCoef[]={};
		aileronCoef[]={};
		rudderCoef[]={};
		elevatorControlsSensitivityCoef=4;
		aileronControlsSensitivityCoef=4;
		rudderControlsSensitivityCoef=4;
		weapons[]=
		{
			"FakeWeapon"
		};
		magazines[]=
		{
			"FakeWeapon"
		};
		threat[]={0.1,1,0.5};
		aileronSensitivity=1;
		elevatorSensitivity=1;
		VTOLYawInfluence=2;
		VTOLPitchInfluence=2;
		VTOLRollInfluence=2;
		landingAoa="10*3.1415/180";
		stallWarningTreshold=0.2;
		class ViewPilot: ViewPilot
		{
			initFov=0.75;
			minFov=0.25;
			maxFov=1.25;
			initAngleX=0;
			initAngleY=0;
			minAngleX=-65;
			maxAngleX=85;
			minAngleY=-150;
			maxAngleY=150;
			minMoveX=-0.2;
			maxMoveX=0.2;
			minMoveY=-0.1;
			maxMoveY=0.1;
			minMoveZ=-0.1;
			maxMoveZ=0.2;
		};
		class ViewOptics: ViewOptics
		{
			initAngleX=0;
			minAngleX=0;
			maxAngleX=0;
			initAngleY=0;
			minAngleY=0;
			maxAngleY=0;
			initFov=0.5;
			minFov=0.5;
			maxFov=0.5;
		};
		extCameraPosition[]={0,3.4000001,-25};
		selectionRotorStill="vrtule staticka";
		selectionRotorMove="vrtule blur";
		memoryPointLRocket="L raketa";
		memoryPointRRocket="P raketa";
		memoryPointLDust="levy prach";
		memoryPointRDust="pravy prach";
		selectionFireAnim="zasleh";
		leftDustEffect="LDustEffects";
		rightDustEffect="RDustEffects";
		dustEffect="HeliDust";
		waterEffect="HeliWater";
		class WingVortices
		{
			class WingTipLeft
			{
				effectName="WingVortices";
				position="cerveny pozicni";
			};
			class WingTipRight
			{
				effectName="WingVortices";
				position="zeleny pozicni";
			};
		};
		class Reflectors
		{
			class Reflector
			{
				color[]={0.89999998,0.80000001,0.80000001,1};
				ambient[]={0.1,0.1,0.1,1};
				position="L svetlo";
				direction="konec L svetla";
				hitpoint="L svetlo";
				selection="L svetlo";
				size=0.5;
				brightness=2;
			};
		};
		class MFD
		{
			class HUD: RD501_AirplaneHUD
			{
				borderLeft=0.090000004;
				borderRight=0.02;
				borderTop=0.02;
				borderBottom=0.1;
				helmetMountedDisplay=1;
				helmetPosition[]={-0.025,0.025,0.1};
				helmetRight[]={0.050000001,0,0};
				helmetDown[]={-0,-0.050000001,0};
				class pos10vector: Pos10Vector
				{
				};
				class bones: Bones
				{
				};
				class Draw: Draw
				{
					class DimmedBase: DimmedBase
					{
					};
					class PlaneW: PlaneW
					{
					};
					class MGun: MGun
					{
					};
					class Missile: Missile
					{
					};
					class Horizont: Horizont
					{
						class Dimmed: Dimmed
						{
						};
					};
					class ILS: ILS
					{
						class Glideslope: Glideslope
						{
						};
					};
				};
			};
		};
		class GunFire: RD501_WeaponFireGun
		{
		};
		class GunClouds: RD501_WeaponCloudsGun
		{
		};
		class MGunFire: RD501_WeaponFireMGun
		{
		};
		class MGunClouds: RD501_WeaponCloudsMGun
		{
		};
		class Sounds: Sounds
		{
			class Engine: Engine
			{
				sound="soundEngine";
				frequency="(1-randomizer*0.05)*(thrust+0.5)*rpm";
				volume="thrust+0.5";
			};
			class Movement: Movement
			{
				sound="soundEnviron";
				frequency="1";
				volume="speed*0.03334";
			};
		};
		numberPhysicalWheels=3;
		TFAR_hasIntercom=1;
		class ACE_SelfActions
		{
			class TFAR_IntercomChannel
			{
				displayName="$STR_tfar_core_Intercom_ACESelfAction_Name";
				condition="true";
				statement="";
				icon="";
				class TFAR_IntercomChannel_disabled
				{
					displayName="Disabled";
					condition="_vehicle = vehicle ACE_Player; _intercom = _vehicle getVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-2]; if (_intercom == -2) then {_intercom = _vehicle getVariable ['TFAR_defaultIntercomSlot',TFAR_defaultIntercomSlot]}; _intercom != -1";
					statement="(vehicle ACE_Player) setVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-1,true];";
				};
				class TFAR_IntercomChannel_1
				{
					displayName="$STR_tfar_core_Intercom_ACESelfAction_Channel1";
					condition="_vehicle = vehicle ACE_Player; _intercom = _vehicle getVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-2]; if (_intercom == -2) then {_intercom = _vehicle getVariable ['TFAR_defaultIntercomSlot',TFAR_defaultIntercomSlot]}; _intercom != 0";
					statement="(vehicle ACE_Player) setVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],0,true];";
				};
				class TFAR_IntercomChannel_2
				{
					displayName="$STR_tfar_core_Intercom_ACESelfAction_Channel2";
					condition="_vehicle = vehicle ACE_Player; _intercom = _vehicle getVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-2]; if (_intercom == -2) then {_intercom = _vehicle getVariable ['TFAR_defaultIntercomSlot',TFAR_defaultIntercomSlot]}; _intercom != 1";
					statement="(vehicle ACE_Player) setVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],1,true];";
				};
				class TFAR_IntercomChannel_Misc_1
				{
					displayName="Misc channel 1";
					condition="_vehicle = vehicle ACE_Player; _intercom = _vehicle getVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-2]; if (_intercom == -2) then {_intercom = _vehicle getVariable ['TFAR_defaultIntercomSlot',TFAR_defaultIntercomSlot]}; _intercom != 2";
					statement="(vehicle ACE_Player) setVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],2,true];";
				};
				class TFAR_IntercomChannel_Misc_2
				{
					displayName="Misc channel 2";
					condition="_vehicle = vehicle ACE_Player; _intercom = _vehicle getVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-2]; if (_intercom == -2) then {_intercom = _vehicle getVariable ['TFAR_defaultIntercomSlot',TFAR_defaultIntercomSlot]}; _intercom != 3";
					statement="(vehicle ACE_Player) setVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],3,true];";
				};
				class TFAR_IntercomChannel_Misc_3
				{
					displayName="Misc channel 3";
					condition="_vehicle = vehicle ACE_Player; _intercom = _vehicle getVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],-2]; if (_intercom == -2) then {_intercom = _vehicle getVariable ['TFAR_defaultIntercomSlot',TFAR_defaultIntercomSlot]}; _intercom != 4";
					statement="(vehicle ACE_Player) setVariable [format ['TFAR_IntercomSlot_%1',(netID ACE_Player)],4,true];";
				};
			};
			class ACE_Passengers
			{
				displayName="$STR_ace_interaction_Passengers";
				condition="alive _target";
				statement="";
				insertChildren="_this call ace_interaction_fnc_addPassengersActions";
			};
			class ace_quickmount_ChangeSeat
			{
				displayName="$STR_ace_quickmount_ChangeSeat";
				icon="\z\ace\addons\quickmount\UI\Seats_ca.paa";
				condition="call ace_quickmount_fnc_canShowFreeSeats";
				insertChildren="call ace_quickmount_fnc_addFreeSeatsActions";
			};
			class ACE_unlockVehicle
			{
				displayName="$STR_ace_vehiclelock_Action_UnLock";
				condition="([_player,  _target] call ace_vehiclelock_fnc_hasKeyForVehicle) && {(locked _target) in [2, 3]}";
				statement="[""ace_vehiclelock_setVehicleLock"",  [_target, false],  [_target]] call CBA_fnc_targetEvent";
				icon="\z\ace\addons\vehiclelock\ui\key_menuIcon_ca.paa";
			};
			class ACE_lockVehicle
			{
				displayName="$STR_ace_vehiclelock_Action_Lock";
				condition="([_player,  _target] call ace_vehiclelock_fnc_hasKeyForVehicle) && {(locked _target) in [0, 1]}";
				statement="[""ace_vehiclelock_setVehicleLock"",  [_target, true],  [_target]] call CBA_fnc_targetEvent";
				icon="\z\ace\addons\vehiclelock\ui\key_menuIcon_ca.paa";
			};
			class ACE_lockpickVehicle
			{
				displayName="$STR_ace_vehiclelock_Action_Lockpick";
				condition="[_player,  _target,  'canLockpick'] call ace_vehiclelock_fnc_lockpick";
				statement="[_player,  _target,  'startLockpick'] call ace_vehiclelock_fnc_lockpick";
			};
		};
		class SpeechVariants
		{
			class Default
			{
				speechSingular[]=
				{
					"veh_air_plane_s"
				};
				speechPlural[]=
				{
					"veh_air_plane_p"
				};
			};
		};
		author="$STR_A3_Bohemia_Interactive";
		mapSize=17.780001;
		textSingular="$STR_A3_nameSound_veh_air_plane_s";
		textPlural="$STR_A3_nameSound_veh_air_plane_p";
		nameSound="veh_air_plane_s";
		_generalMacro="Plane";
		type=2;
		memoryPointGun="kulomet";
		soundDammage[]=
		{
			"",
			9.9999983e-005,
			1
		};
		fuelExplosionPower=10;
		epeImpulseDamageCoef=10;
		crewCrashProtection=0.15000001;
		unitInfoType="RscUnitInfoAirPlane";
		damageEffect="AirDestructionEffects";
		getInAction="GetInHigh";
		getOutAction="GetOutHigh";
		cargoGetInAction[]=
		{
			"GetInHigh"
		};
		cargoGetOutAction[]=
		{
			"GetOutHigh"
		};
		gunnerGetInAction="GetInHigh";
		gunnerGetOutAction="GetOutHigh";
		getInRadius=1.2;
		supplyRadius=1.2;
		accuracy=0.02;
		camouflage=100;
		audible=60;
		class CamShake
		{
			power=50;
			frequency=20;
			distance=50;
			minSpeed=200;
		};
		camShakeCoef=0;
		headGforceLeaningFactor[]={0.0049999999,0.001,0.025};
		armor=20;
		armorStructural=1;
		explosionShielding=2;
		minTotalDamageThreshold=0.0049999999;
		class HitPoints
		{
			class HitHull
			{
				armor=1;
				material=50;
				name="telo";
				visual="trup";
				passThrough=1;
			};
		};
		class EventHandlers: DefaultEventhandlers
		{
		};
		class DestructionEffects
		{
		};
		class ACE_Actions
		{
			class ACE_MainActions
			{
				displayName="$STR_ace_interaction_MainAction";
				position="[_target,  ace_interact_menu_cameraPosASL] call ace_interaction_fnc_getVehiclePosComplex";
				selection="";
				distance=4;
				condition="true";
				class ACE_Passengers
				{
					displayName="$STR_ace_interaction_Passengers";
					condition="alive _target";
					statement="";
					exceptions[]=
					{
						"isNotSwimming"
					};
					insertChildren="_this call ace_interaction_fnc_addPassengersActions";
				};
				class ace_quickmount_GetIn
				{
					displayName="$STR_rscMenu.hppRscGroupRootMenu_Items_GetIn1";
					condition="call ace_quickmount_fnc_canShowFreeSeats";
					statement="call ace_quickmount_fnc_getInNearest";
					exceptions[]=
					{
						"isNotSwimming"
					};
					insertChildren="(_this select 2) param [0,  []]";
				};
				class ace_rearm_Rearm
				{
					displayName="$STR_ace_rearm_Rearm";
					distance=9;
					condition="_this call ace_rearm_fnc_canRearm";
					statement="_this call ace_rearm_fnc_rearm";
					exceptions[]=
					{
						"isNotInside"
					};
					icon="\z\ace\addons\rearm\ui\icon_rearm_interact.paa";
				};
				class ace_repair_Repair
				{
					displayName="$STR_ace_repair_Repair";
					condition="true";
					statement="";
					runOnHover=1;
					showDisabled=0;
					icon="\A3\ui_f\data\igui\cfg\actions\repair_ca.paa";
					distance=4;
					exceptions[]=
					{
						"isNotSwimming",
						"isNotOnLadder"
					};
				};
				class ACE_unlockVehicle
				{
					displayName="$STR_ace_vehiclelock_Action_UnLock";
					distance=4;
					condition="([_player,  _target] call ace_vehiclelock_fnc_hasKeyForVehicle) && {(locked _target) in [2, 3]}";
					statement="[""ace_vehiclelock_setVehicleLock"",  [_target, false],  [_target]] call CBA_fnc_targetEvent";
					exceptions[]=
					{
						"isNotSwimming"
					};
					icon="\z\ace\addons\vehiclelock\ui\key_menuIcon_ca.paa";
				};
				class ACE_lockVehicle
				{
					displayName="$STR_ace_vehiclelock_Action_Lock";
					distance=4;
					condition="([_player,  _target] call ace_vehiclelock_fnc_hasKeyForVehicle) && {(locked _target) in [0, 1]}";
					statement="[""ace_vehiclelock_setVehicleLock"",  [_target, true],  [_target]] call CBA_fnc_targetEvent";
					exceptions[]=
					{
						"isNotSwimming"
					};
					icon="\z\ace\addons\vehiclelock\ui\key_menuIcon_ca.paa";
				};
				class ACE_lockpickVehicle
				{
					displayName="$STR_ace_vehiclelock_Action_Lockpick";
					distance=4;
					condition="[_player,  _target,  'canLockpick'] call ace_vehiclelock_fnc_lockpick";
					statement="[_player,  _target,  'startLockpick'] call ace_vehiclelock_fnc_lockpick";
					exceptions[]=
					{
						"isNotSwimming"
					};
				};
				class UPSL_aime_vehicle_seats_getin_action
				{
					displayName="Get In";
					condition="call UPSL_aime_vehicle_seats_fnc_getin_condition";
					statement="call UPSL_aime_vehicle_seats_fnc_getin_run";
					icon="\a3\ui_f\data\igui\cfg\actions\obsolete\ui_action_getin_ca.paa";
					insertChildren="call UPSL_aime_vehicle_seats_fnc_change_submenus";
				};
				class ace_attach_AttachVehicle
				{
					displayName="$STR_ace_attach_AttachDetach";
					condition="_this call ace_attach_fnc_canAttach";
					insertChildren="_this call ace_attach_fnc_getChildrenActions";
					exceptions[]=
					{
						"isNotSwimming"
					};
					showDisabled=0;
					icon="\z\ace\addons\attach\UI\attach_ca.paa";
				};
				class ace_attach_DetachVehicle
				{
					displayName="$STR_ace_attach_Detach";
					condition="_this call ace_attach_fnc_canDetach";
					statement="_this call ace_attach_fnc_detach ";
					exceptions[]=
					{
						"isNotSwimming"
					};
					showDisabled=0;
					icon="\z\ace\addons\attach\UI\detach_ca.paa";
				};
				class ace_captives_LoadCaptive
				{
					displayName="$STR_ace_captives_LoadCaptive";
					distance=4;
					condition="[_player,  objNull,  _target] call ace_captives_fnc_canLoadCaptive";
					statement="[_player,  objNull,  _target] call ace_captives_fnc_doLoadCaptive";
					exceptions[]=
					{
						"isNotEscorting",
						"isNotSwimming"
					};
				};
				class UPSL_aime_inventory_open_action
				{
					displayName="Inventory";
					condition="UPSL_aime_inventory_setting_open_action				 && { alive _target }				 && { _target call UPSL_aime_inventory_fnc_has_inventory }";
					statement="_player action [""Gear"", _target]";
					icon="\A3\ui_f\data\igui\cfg\actions\gear_ca.paa";
					exceptions[]=
					{
						"isNotSwimming"
					};
				};
			};
		};
		ace_refuel_canReceive=1;
		ace_refuel_flowRate=16;
		ace_cargo_space=0;
		ace_cargo_hasCargo=0;
	};

};

class Extended_Init_EventHandlers 
{

    class macro_new_vehicle(droideka,mobile)
    {
    	class droideka_movable
        {
            init = [_this select 0] spawn macro_fnc_name(droidekka_move);
        };
    };
	class macro_new_vehicle(dwarf_spider_droid,Movable_MkI)
    {
        class dwarf_movable 
        {
            init = [_this select 0] spawn macro_fnc_name(dwarf_move);
        };	                               
    };
    class macro_new_vehicle(dwarf_spider_droid,Movable_MkII)
    {
        class dwarf_movable 
        {
            init = [_this select 0] spawn macro_fnc_name(dwarf_move);
        };	                               
    };
	class macro_new_vehicle(crab_droid,Movable_MkII)
    {
        class crab_movable 
        {
            init = [_this select 0] spawn macro_fnc_name(dwarf_move);
        };	                               
    };
	class macro_new_vehicle(mining_droid,Movable_MkI)
    {
        class mining_movable 
        {
            init = [_this select 0] spawn macro_fnc_name(dwarf_move);
        };	                               
    };
	class macro_new_vehicle(magna_tri_droid,Movable)
    {
        class otco_tri_droid_movable 
        {
            init = [_this select 0] spawn macro_fnc_name(dwarf_move);
        };	                               
    };
	class macro_new_vehicle(homing_spider_droid,Movable_MKII)
    {
        class og10_droid_movable
        {
            init = [_this select 0] spawn macro_fnc_name(og10_move);
        };	                               
    };
    class macro_new_vehicle(bacta,healing)//add doggo to spell,no heal
    {
        class add_heal
        {
            init = [_this select 0] spawn macro_fnc_name(add_heal_action);
        };
        // class add_doggo
        // {
        //     init = [_this select 0] spawn macro_fnc_name(add_doggo);
        // }
    };

    class macro_new_vehicle(warden_tank,mobile)
    {
        class adsd_tow
        {
             init = [_this select 0] spawn macro_fnc_name(warden_tow);
        };
    };  


    #include "_init_functions/cis_aircraft_turrets.hpp"

};

class Extended_Deleted_EventHandlers 
{
	
    class macro_new_vehicle(droideka,normal)
    {
    	macro_remove_attached_stuff_XEH
    };

	class macro_new_vehicle(dwarf_spider_droid,Movable_MkI)
    {
        macro_remove_attached_stuff_XEH
    };

    class macro_new_vehicle(dwarf_spider_droid,Movable_MkII)
    {
        macro_remove_attached_stuff_XEH
    };

	class macro_new_vehicle(mining_droid,Movable_MkI)
    {
        macro_remove_attached_stuff_XEH
    };

	class macro_new_vehicle(magna_tri_droid,Movable)
    {
        macro_remove_attached_stuff_XEH
    };

	class macro_new_vehicle(homing_spider_droid,Movable_MKII)
    {
        macro_remove_attached_stuff_XEH
    };


    #include "_delete_functions/remove_aircraft_turret.hpp"
    };

class Extended_GetIn_EventHandlers 
{
    class Plane
    {
        class fuck_auto_vectoring 
        {
            GetIn = "(_this select 2) action ['VTOLVectoring', (_this select 0)];";
        };
    };

};

class CfgGroups
{
	class EAST
	{
		class macro_faction(CIS)
		{
			name="RD501 C.I.S Faction";
			class CISFlights
			{
				name="Flights";
				class macro_new_composition(opfor,vulture_flight)
				{
					name="Vulture Flight";
					side=0;
					faction = macro_faction(CIS);
					icon="\A3\ui_f\data\map\markers\nato\o_plane.paa";
					rarityGroup=1;
					class Unit0
					{
						position[]={0,0,0};
						rank="CORPORAL";
						side=0;
						vehicle="RD501_Vulture";
					};
					class Unit1
					{
						position[]={10,-10,0};
						rank="PRIVATE";
						side=0;
						vehicle="RD501_Vulture";
					};
					class Unit2
					{
						position[]={-10,-10,0};
						rank="PRIVATE";
						side=0;
						vehicle="RD501_Vulture";
					};
					class Unit3
					{
						position[]={20,-20,0};
						rank="PRIVATE";
						side=0;
						vehicle="RD501_Vulture";
					};
				};
			};
		};
	};
};
