#include "../../RD501_main/config_macros.hpp"
class CfgPatches
{
    class RD501_patch_staticturret
    {
        addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

        requiredAddons[]=
        {
            RD501_patch_vehicles
        };
        requiredVersion=0.1;
        units[]=
        {
            macro_new_vehicle(stat,reweb),
            macro_new_vehicle(stat,ragl40),
            macro_new_vehicle(stat,Striker),
            macro_new_vehicle(stat,Railgun)
        };
        weapons[]=
        {
            macro_new_weapon(stat,eweb),
            macro_new_weapon(stat,ragl40),
            macro_new_weapon(stat,aap4),
            macro_new_weapon(stat,mar1)
        };
    };
};

class CfgVehicles
{
    class All;
    class Strategic;
    class Land;
    class LandVehicle: Land
    {
        class ViewPilot;
        class ViewGunner;
        class NewTurret;
    };
    class StaticWeapon: LandVehicle
    {
        class Turrets
        {
            class MainTurret;
        };
    };
    class StaticAAWeapon:StaticWeapon
    {
        class Turrets;
    };
    class StaticMGWeapon: StaticWeapon
    {
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                class ViewOptics;
            };
        };
    };
    class OPTRE_LAU65D_pod:StaticAAWeapon  
    {
        class Turrets:Turrets{
        class MainTurret;};
    };
    class OPTRE_Static_M41:StaticMGWeapon
    {
        class Turrets;
    };
    class OPTRE_Static_Gauss : OPTRE_Static_M41
    {
        class Turrets: Turrets
        {
            class MainTurret;
        };
    };
    class 3AS_HeavyRepeater_base:StaticMGWeapon
    {
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                class ViewOptics;
            };
        };
    };
    class macro_new_vehicle(stat,reweb_base): 3AS_HeavyRepeater_base
    {
        scope = 0;
        displayName = "Republic EWHB-12";
        author = "501st Aux Team";
        class Armory
        {
            description = "";
        };
        armor = 150;
        model = "3AS\3AS_Static\HeavyRepeater\3AS_HeavyRepeaterStatic.p3d";
        magazineReloadTime=0.5;
        picture = "\A3\Static_f_gamma\data\ui\gear_StaticTurret_MG_CA.paa";
        UiPicture = "\A3\Static_f_gamma\data\ui\gear_StaticTurret_MG_CA.paa";
        threat[] = {1,0.3,0.3};
        cost = 150000;
        getInAction = "";
        getOutAction = "";
        class Damage
        {
            tex[] = {};
            mat[] = {"A3\Static_F_Gamma\data\StaticTurret_01.rvmat","A3\Static_F_Gamma\data\StaticTurret_01_damage.rvmat","A3\Static_F_Gamma\data\StaticTurret_01_destruct.rvmat","A3\Static_F_Gamma\data\StaticTurret_02.rvmat","A3\Static_F_Gamma\data\StaticTurret_02_damage.rvmat","A3\Static_F_Gamma\data\StaticTurret_02_destruct.rvmat","A3\Static_F_Gamma\data\StaticTurret_03.rvmat","A3\Static_F_Gamma\data\StaticTurret_03_damage.rvmat","A3\Static_F_Gamma\data\StaticTurret_03_destruct.rvmat"};
        };
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                discreteDistance[] = {100,200,300,400,600,800,1000,1200,1500,1600,1700,1800,1900,2000};
                discreteDistanceInitIndex = 2;
                turretInfoType = "RscOptics_crows";
                gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_AAA_01_m_F";
                optics = 0;
                minElev = -10;
                maxElev = 40;
                minTurn = -360;
                maxTurn = 360;
                animationSourceBody = "Mainturret";
                Body = "Mainturret";
                animationSourceGun = "Maingun";
                Gun = "Maingun";
                inGunnerMayFire = 1;
                selectionFireAnim = "zasleh";
                soundServo[] = {"A3\Sounds_F\vehicles\boat\Boat_Armed_01\servo_boat_comm",1.4125376,1,30};
                soundServoVertical[] = {"A3\Sounds_F\vehicles\boat\Boat_Armed_01\servo_boat_comm_vertical",1.4125376,1,30};
                weapons[] = 
                {
                    "RD501_stat_eweb"
                };
                magazines[] = 
                {
                    "RD501_eweb_x500_mag"
                };
                gunnerAction = "RepeaterGunner";
                memoryPointGunnerOptics = "gunnerview";
                gunnerLeftHandAnimName = "Heavygun";
                gunnerRightHandAnimName = "Heavygun";
                gunnergetInAction = "";
                gunnergetOutAction = "";
                GunnerName = "Gunner";
                gunnerForceOptics = 0;
                hiddenSelections[] = {"camo1"};
                hiddenSelectionsTextures[] = {"3as\3as_static\StationaryTurret\Data\Textures\turret2_co.paa"};
                hiddenSelectionsMaterials[] = {"3as\3as_static\StationaryTurret\Data\Textures\turret.rvmat"};
                memoryPointsGetInGunner = "pos_gunner";
                memoryPointsGetInGunnerDir = "pos_gunner_dir";
                class ViewOptics: ViewOptics
                {
                    minFov = 0.25;
                    maxFov = 1.25;
                    initFov = 0.75;
                    visionMode[] = {"Normal","NVG"};
                };
            };
        };
        class AnimationSources
        {
            class Minigun
            {
                source = "revolving";
                weapon = "RD501_stat_eweb";
            };
        };
        soundGetOut[] = {"A3\sounds_f\dummysound",0.001,1,5};
        soundGetIn[] = {"A3\sounds_f\dummysound",0.000316228,1,5};
    };
    class macro_new_vehicle(stat,reweb): macro_new_vehicle(stat,reweb_base)
    {
        editorPreview = "\3as\3as_static\images\3AS_HeavyRepeater_Unarmoured.jpg";
        _generalMacro = "B_HMG_01_F";
        author = "501st Aux Team";
        scope = 2;
        side = 1;
        faction = MACRO_QUOTE(macro_faction(republic));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(turrets));
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(turrets));
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                magazines[] = 
                {

                };
            };
        };
        class ACE_Actions
        {
            class ACE_MainActions
            {
                displayName = "Republic EWHB";
                selection = "gunnerview";
                distance = 2.5;
                condition = "true";
                class ace_csw_pickUp
                {
                    displayName = "Pick up EWHB";
                    condition = "call ace_csw_fnc_assemble_canPickupTripod";
                    statement = "_this call RD501_fnc_internal_ACECSW_pickup";
                };
                class rd501_external_reload 
                {
                    displayName = "Reload";
                    condition = "_this call rd501_fnc_canReloadExternal";
                    statement = "_this call rd501_fnc_reloadExternal";
                    exceptions[] = {};
                    runOnHover = 0;
                };
                class ace_reload_CheckAmmo
                {
                    displayName = "$STR_ace_reload_checkAmmo";
                    distance = 2.0;
                    condition = "call ace_reload_fnc_canCheckAmmo";
                    statement = "call ace_reload_fnc_checkAmmo";
                    exceptions[] = {"isNotInside","isNotSwimming","isNotSitting"};
                };
            };
        };
        class ace_csw
        {
            disassembleTo = macro_new_weapon(stat,eweb_carry);
        };
        class AnimationSources
        {
            class Minigun
            {
                source = "revolving";
                weapon = macro_new_weapon(stat,eweb);
            };
        };
    };
    class macro_new_vehicle(stat,Striker): OPTRE_LAU65D_pod
    {
        armor=150;
        scope = 2;
        author="501st Aux Mod";
        displayName = "AAP4 'Striker'";
        faction = MACRO_QUOTE(macro_faction(republic));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(turrets));
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(turrets));
        model = "\OPTRE_Weapons\static\LAU65D\LAU65D_pod.p3d";
        ace_cargo_size = 1;
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                animationSourceBody = "Turret_rot";
                animationSourceCamElev = "camElev";
                animationSourceGun = "camElev";
                memoryPointsGetInGunner = "pos_gunner";
                memoryPointsGetInGunnerDir = "pos_gunner_dir";
                body = "Turret_rot";
                discreteDistanceCameraPoint[] = {"eye"};
                stabilizedInAxes = "StabilizedInAxesNone";
                gunnerAction = "Gunner_OPTRE_Lau";
                maxTurn = 360;
                weapons[] = 
                {
                    macro_new_weapon(stat,aap4)
                };
                magazines[] = 
                {
                    
                };
                //weapons[]={"ace_javelin_Titan_Static"};
                //magazines[]={"1Rnd_GAT_missiles"};
                turretInfoType = "ACE_RscOptics_javelin";
                gunnerOpticsColor[] = {0,0,0,1};
                gunnerOpticsEffect[] = {};
                gunnerOpticsModel = "\z\ace\addons\javelin\data\reticle_titan.p3d";
                gunnerOpticsShowCursor = 0;
            };
        };
        class ACE_Actions
        {
            class ACE_MainActions
            {
                displayName = "Republic Striker";
                selection = "";
                distance = 2.5;
                condition = "true";
                class ace_csw_pickUp
                {
                    displayName = "Pick up Turret";
                    condition = "call ace_csw_fnc_assemble_canPickupTripod";
                    statement = "_this call RD501_fnc_internal_ACECSW_pickup";
                };
                class rd501_external_reload 
                {
                    displayName = "Reload";
                    condition = "_this call rd501_fnc_canReloadExternal";
                    statement = "_this call rd501_fnc_reloadExternal";
                    exceptions[] = {};
                    runOnHover = 0;
                };
            };
        };
        class ace_csw
        {
            disassembleTo = macro_new_weapon(stat,aap4_carry);
        };
    };
    class macro_new_vehicle(stat,Railgun): OPTRE_Static_Gauss
    {
        armor=150;
        scope = 2;
        author="501st Aux Mod";
        displayName = "MAR1 'Driver'";
        faction = MACRO_QUOTE(macro_faction(republic));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(turrets));
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(turrets));
        ace_cargo_size = 1;
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                weapons[]=
                {
                    macro_new_weapon(stat,mar1)
                };
                magazines[]=
                {
        
                };
                class Viewoptics
                {
                    initAngleX = 0;
                    initAngleY = 0;
                    initFov = 0.75;
                    maxAngleX = 30;
                    maxAngleY = 100;
                    maxFov = 1.1;
                    maxMoveX = 0;
                    maxMoveY = 0;
                    maxMoveZ = 0;
                    minAngleX = -30;
                    minAngleY = -100;
                    minFov = 0.0125;
                    minMoveX = 0;
                    minMoveY = 0;
                    minMoveZ = 0;
                    opticsZoomInit = 0.75;
                    opticsZoomMax = 0.75;
                    opticsZoomMin = 0.25;
                    thermalMode[] = {5,6};
                    visionMode[] = {"Normal","NVG","Ti"};
                };
            };
        };
        class ACE_Actions
        {
            class ACE_MainActions
            {
                displayName = "Republic Driver";
                selection = "";
                distance = 2.5;
                condition = "true";
                class ace_csw_pickUp
                {
                    displayName = "Pick up Turret";
                    condition = "call ace_csw_fnc_assemble_canPickupTripod";
                    statement = "_this call RD501_fnc_internal_ACECSW_pickup";
                };
                class rd501_external_reload 
                {
                    displayName = "Reload";
                    condition = "_this call rd501_fnc_canReloadExternal";
                    statement = "_this call rd501_fnc_reloadExternal";
                    exceptions[] = {};
                    runOnHover = 0;
                };
            };
        };
        class ace_csw
        {
            disassembleTo = macro_new_weapon(stat,mar1_carry);
            enabled=1;
        };
    };
};
class Mode_SemiAuto;
class CfgWeapons
{
    class LMG_RCWS;
    class 3AS_HeavyRepeater_W: LMG_RCWS
    {
        class manual;
    };
    class macro_new_weapon(stat,eweb):3AS_HeavyRepeater_W
    {
        displayName = "EWHB-12 Boomer Cannon";
        author = "501st Aux Team";
        magazineReloadTime=2;
        modes[] = {"manual","Single"};
        class manual: manual
        {
            dispersion=0.011;
            reloadTime=0.05;
            multiplier=1;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                begin2[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                begin3[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        magazines[]=
        {
            macro_new_mag(eweb,500),
            macro_new_mag(ragl40he,40)
        };
        class Single: Mode_SemiAuto
        {
            reloadTime = 0.09;
            multiplier=1;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                begin2[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                begin3[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
    };
    class Launcher;
    class Launcher_Base_F: Launcher
    {
        class WeaponSlotsInfo;
    };
    class macro_new_weapon(stat,eweb_carry):Launcher_Base_F
    {
        faction = MACRO_QUOTE(macro_faction(republic));
        author = "501st Aux Team";
        scope = 2;
        displayName = "Folded Republic EWHB-12 'Boomer'";
        model = "\MRC_BLUFOR_Weapons\Turret\data\EWHB_Folded\EWHB_Folded.p3d";
        picture = "\RD501_Vehicles\static\data\rd501_launcher_staticweapons_boomer.paa";
        mass = 450;
        class ace_csw
        {
            type = "mount";
            deployTime = 2;
            pickupTime = 2;
            deploy = macro_new_vehicle(stat,reweb);
        };
    };
    class ace_javelin_Titan_Static;
    class macro_new_weapon(stat,aap4):ace_javelin_Titan_Static
    {
        magazineReloadTime=2;
        ace_javelin_enabled = 1;  // Enable Javelin-style locking (0-disabled, 1-enabled)
        weaponInfoType = "ACE_RscOptics_javelin";  // Inteface
        modelOptics = "\z\ace\addons\javelin\data\reticle_titan.p3d";  // Optics model
        canLock = 0;  // Disable vanilla locking (0-disabled, 1-enabled)
        lockingTargetSound[] = {"", 0, 1};  // Locking sound
        lockedTargetSound[] = {"", 0, 1};  // Target acquired sound
        hiddenSelectionsTextures[]=
        {
            "RD501_Vehicles\textures\static\warthog_addons_grey_co.paa"
        };
        magazines[]=
        {
            macro_new_mag(aap,4)
        };
    };
    class macro_new_weapon(stat,aap4_carry):Launcher_Base_F
    {
        faction = MACRO_QUOTE(macro_faction(republic));
        scope = 2;
        displayName = "Folded Republic AAP4 'Striker'";
        author = "501st Aux Team";
        model = "ls_weapons\tertiary\plx1\ls_weapon_plx1.p3d";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_backpack_ui_ca.paa";
        mass = 450;
        class ace_csw
        {
            type = "mount";
            deployTime = 2;
            pickupTime = 2;
            deploy = macro_new_vehicle(stat,Striker);
        };
    };
    class macro_new_weapon(stat,mar1):3AS_HeavyRepeater_W
    {
        magazineReloadTime=2;
        class manual:manual
        {
            dispersion=0.00001;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                closure1[] = {"A3\sounds_f\weapons\gatling\gatling_rotation_short_2",0.316228,1,20};
                closure2[] = {"A3\sounds_f\weapons\gatling\gatling_rotation_short_3",0.316228,1,20};
                soundClosure[] = {"closure1",0.5,"closure2",0.5};
            };
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"3AS\3AS_Static\data\Sounds\HeavyRepeater\Blaster_Cannon_1",0.95,1,2000};
                begin2[] = {"3AS\3AS_Static\data\Sounds\HeavyRepeater\Blaster_Cannon_1",0.95,1,2000};
                begin3[] = {"3AS\3AS_Static\data\Sounds\HeavyRepeater\Blaster_Cannon_1",0.95,1,2000};
                begin4[] = {"3AS\3AS_Static\data\Sounds\HeavyRepeater\Blaster_Cannon_1",0.95,1,2000};
                begin5[] = {"3AS\3AS_Static\data\Sounds\HeavyRepeater\Blaster_Cannon_1",0.95,1,2000};
                begin6[] = {"3AS\3AS_Static\data\Sounds\HeavyRepeater\Blaster_Cannon_1",0.95,1,2000};
                begin7[] = {"3AS\3AS_Static\data\Sounds\HeavyRepeater\Blaster_Cannon_1",0.95,1,2000};
                begin8[] = {"3AS\3AS_Static\data\Sounds\HeavyRepeater\Blaster_Cannon_1",0.95,1,2000};
                begin9[] = {"3AS\3AS_Static\data\Sounds\HeavyRepeater\Blaster_Cannon_1",0.95,1,2000};
                begin10[] = {"3AS\3AS_Static\data\Sounds\HeavyRepeater\Blaster_Cannon_1",0.95,1,2000};
                soundBegin[] = {"begin1",0.1,"begin2",0.1,"begin3",0.1,"begin4",0.1,"begin5",0.1,"begin6",0.1,"begin7",0.1,"begin8",0.1,"begin9",0.1,"begin10",0.1};
            };
        };
        magazines[]=
        {
            macro_new_mag(mar,1)
        };
    };
    class macro_new_weapon(stat,mar1_carry):Launcher_Base_F
    {
        faction = MACRO_QUOTE(macro_faction(republic));
        scope = 2;
        displayName = "Folded Republic MAR1 'Driver'";
        author = "501st Aux Team";
        model = "ls_weapons\tertiary\rps6\ls_weapon_rps6.p3d";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_backpack_ui_ca.paa";
        mass = 450;
        class ace_csw
        {
            type = "mount";
            deployTime = 2;
            pickupTime = 2;
            deploy = macro_new_vehicle(stat,Railgun);
        };
    };
};
class CfgMagazines
{
    class 3AS_HeavyRepeater_1000Rnd_mag;
    class 40Rnd_20mm_G_belt;
    class 1Rnd_GAT_missiles;
    class macro_new_mag(eweb,500): 3AS_HeavyRepeater_1000Rnd_mag
    {
        scope=2;
        scopeArsenal = 2;
        type=256;
        picture="\RD501_Vehicles\static\data\rd501_icon_mag_staticeweb.paa";
        displayName= "[501st] Republic E-Web Charge";
        model = "SWLW_merc_trando\machineguns\ls150\ls150_mag.p3d";
        ACE_isBelt = 1;
        count=500;
        ammo = "Aux501_Weapons_Ammo_z1000";
        weaponpoolavailable=1;
        mass = 50;		
    };
    class macro_new_mag(ragl40he,40): 40Rnd_20mm_G_belt
    {
        scope=2;
        scopeArsenal = 2;
        type=256;
        picture="\RD501_Vehicles\static\data\rd501_icon_mag_staticgl.paa";
        muzzleImpulseFactor[] = {0,0};
        displayName= "[501st] RAGL40 'Boomer' belt";
        model = "\A3\Structures_F_EPB\Items\Military\Ammobox_rounds_F.p3d";
        count=40;
        ammo="Aux501_Weapons_Ammo_GL_HE";
        weaponpoolavailable=1;
        mass = 95;	
    };
    class macro_new_mag(aap,4): 1Rnd_GAT_missiles
    {
        scope=2;
        scopeArsenal = 2;
        type=256;
        picture="\RD501_Weapons\_mag\data\rd501_icon_mag_battery.paa";
        displayName= "[501st] AAP4 'Striker' Pod";
        count=4;
        ammo="ACE_Javelin_FGM148_Static";
        model = "\MRC\JLTS\weapons\PLX1\PLX1_mag.p3d";
        //ammo=macro_new_ammo(striker)
        weaponpoolavailable=1;
        mass = 95;	
    };
    class macro_new_mag(mar,1): 3AS_HeavyRepeater_1000Rnd_mag
    {
        scope=2;
        scopeArsenal = 2;
        type=256;
        picture="\RD501_Vehicles\static\data\rd501_icon_mag_staticrailgun.paa";
        displayName= "[501st] MAR1 'Driver' Slug";
        count=1;
        initSpeed=1500;
        ammo=macro_new_ammo(mar1);
        model = "SWLW_clones\launchers\rps6\RPS6_mag.p3d";
        weaponpoolavailable=1;
        mass = 95;
    };
};