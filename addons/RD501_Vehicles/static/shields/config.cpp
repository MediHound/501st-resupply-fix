//Get this addons macro

//get the macro for the air subaddon

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class RD501_patch_shields
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			MACRO_QUOTE(RD501_patch_vehicles)
		};
		requiredVersion=0.1;
		units[]=
		{
			"RD501_Squad_Shield",
			"RD501_Squad_Shield_trench",
			"RD501_Droideka_Shield",
			"RD501_Personal_Shield",
			"RD501_Shield_Wall",
			"RD501_Shield_Gate_L",
			"RD501_Shield_Gate_S",
			"RD501_Shield_Door"			
		};
		weapons[]=
		{
			
		};
	};
};

class CfgVehicles
{
	class Land_House_Small_03_V1_ruins_F;
	class Car_F;
    class RD501_Squad_Shield: Land_House_Small_03_V1_ruins_F
    {
        author="$STR_A3_Bohemia_Interactive";
        mapSize=21.1;
        armor = 999999;
        armorStructural = 999;
        explosionShielding=999;
        class SimpleObject
        {
            eden=0;
            animate[]={};
            hide[]={};
            verticalOffset=-0.023;
            verticalOffsetWorld=0;
            init="''";
        };
        editorPreview="\A3\EditorPreviews_F\Data\CfgVehicles\Land_House_Small_03_V1_ruins_F.jpg";
        scope=2;
        scopeCurator=2;
        side=3;
		simulation="house";
        displayName="Squad Shield";
        model="\RD501_Vehicles\static\shields\bubble\bubble.p3d";
        icon="iconObject_1x1";
        rd501_fired_deployable_loopSound = "rd501_squad_shield_loop";
        rd501_fired_deployable_loopDuration = 14;
        rd501_fired_deployable_endSound = "rd501_squad_shield_end";
        rd501_fired_deployable_endDuration = 1;
        rd501_fired_deployable_soundDistance = 300;
        rd501_fired_deployable_ignoreDamage = 1;
		rd501_shield_isShield=1;
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Vehicles\static\shields\bubble\deka_shield.paa"};
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(statics));
        editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
    };
    class RD501_Shield_Wall: Car_F
    {
        author="$STR_A3_Bohemia_Interactive";
        mapSize=21.1;
        armor=200000;
        class SimpleObject
        {
            eden=0;
            animate[]={};
            hide[]={};
            verticalOffset=-0.023;
            verticalOffsetWorld=0;
            init="''";
        };
        editorPreview="\A3\EditorPreviews_F\Data\CfgVehicles\Land_House_Small_03_V1_ruins_F.jpg";
        scope=2;
        scopeCurator=2;
        side=3;
		simulation="house";
        displayName="Shield wall";
        model="\RD501_Vehicles\static\shields\wall\shield_wall.p3d";
        icon="iconObject_1x1";
        rd501_fired_deployable_loopSound = "rd501_squad_shield_loop";
        rd501_fired_deployable_loopDuration = 14;
        rd501_fired_deployable_endSound = "rd501_squad_shield_end";
        rd501_fired_deployable_endDuration = 1;
        rd501_fired_deployable_soundDistance = 300;
        rd501_fired_deployable_ignoreDamage = 1;
        rd501_shield_isShield=1;
        rd501_shield_type="CIS";
        rd501_shield_health=100000;
        hiddenSelections[] = { "camo1" };
        hiddenSelectionsTextures[] = { "\RD501_Vehicles\static\shields\shared\rd501_shield_cis_normal.paa" };
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(statics));
        editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
    };
	class RD501_Shield_Gate_L: Car_F
    {
        author="$STR_A3_Bohemia_Interactive";
        mapSize=21.1;
        armor=200000;
        class SimpleObject
        {
            eden=0;
            animate[]={};
            hide[]={};
            verticalOffset=-0.023;
            verticalOffsetWorld=0;
            init="''";
        };
        editorPreview="\A3\EditorPreviews_F\Data\CfgVehicles\Land_House_Small_03_V1_ruins_F.jpg";
        scope=2;
        scopeCurator=2;
        side=3;
		simulation="house";
        displayName="Shield Gate(Large)";
        model="\RD501_Vehicles\static\shields\wall\shield_gate_l.p3d";
        icon="iconObject_1x1";
        rd501_fired_deployable_loopSound = "rd501_squad_shield_loop";
        rd501_fired_deployable_loopDuration = 14;
        rd501_fired_deployable_endSound = "rd501_squad_shield_end";
        rd501_fired_deployable_endDuration = 1;
        rd501_fired_deployable_soundDistance = 300;
        rd501_fired_deployable_ignoreDamage = 1;
        rd501_shield_isShield=1;
        rd501_shield_type="CIS";
        rd501_shield_health=20000;
        hiddenSelections[] = { "camo1" };
        hiddenSelectionsTextures[] = { "\RD501_Vehicles\static\shields\shared\rd501_shield_cis_normal.paa" };
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(statics));
        editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
    };
	class RD501_Shield_Gate_S: Car_F
    {
        author="$STR_A3_Bohemia_Interactive";
        mapSize=21.1;
        armor=200000;
        class SimpleObject
        {
            eden=0;
            animate[]={};
            hide[]={};
            verticalOffset=-0.023;
            verticalOffsetWorld=0;
            init="''";
        };
        editorPreview="\A3\EditorPreviews_F\Data\CfgVehicles\Land_House_Small_03_V1_ruins_F.jpg";
        scope=2;
        scopeCurator=2;
        side=3;
		simulation="house";
        displayName="Shield Gate(Small)";
        model="\RD501_Vehicles\static\shields\wall\shield_gate_s.p3d";
        icon="iconObject_1x1";
        rd501_fired_deployable_loopSound = "rd501_squad_shield_loop";
        rd501_fired_deployable_loopDuration = 14;
        rd501_fired_deployable_endSound = "rd501_squad_shield_end";
        rd501_fired_deployable_endDuration = 1;
        rd501_fired_deployable_soundDistance = 300;
        rd501_fired_deployable_ignoreDamage = 1;
        rd501_shield_isShield=1;
        rd501_shield_type="CIS";
        rd501_shield_health=10000;
        hiddenSelections[] = { "camo1" };
        hiddenSelectionsTextures[] = { "\RD501_Vehicles\static\shields\shared\rd501_shield_cis_normal.paa" };
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(statics));
        editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
    };
	class RD501_Shield_Door: Car_F
    {
        author="$STR_A3_Bohemia_Interactive";
        mapSize=21.1;
        armor=200000;
        class SimpleObject
        {
            eden=0;
            animate[]={};
            hide[]={};
            verticalOffset=-0.023;
            verticalOffsetWorld=0;
            init="''";
        };
        editorPreview="\A3\EditorPreviews_F\Data\CfgVehicles\Land_House_Small_03_V1_ruins_F.jpg";
        scope=2;
        scopeCurator=2;
        side=3;
		simulation="house";
        displayName="Shield Door";
        model="\RD501_Vehicles\static\shields\wall\shield_door.p3d";
        icon="iconObject_1x1";
        rd501_fired_deployable_loopSound = "rd501_squad_shield_loop";
        rd501_fired_deployable_loopDuration = 14;
        rd501_fired_deployable_endSound = "rd501_squad_shield_end";
        rd501_fired_deployable_endDuration = 1;
        rd501_fired_deployable_soundDistance = 300;
        rd501_fired_deployable_ignoreDamage = 1;
        rd501_shield_isShield=1;
        rd501_shield_type="CIS";
        rd501_shield_health=1000;
        hiddenSelections[] = { "camo1" };
        hiddenSelectionsTextures[] = { "\RD501_Vehicles\static\shields\shared\rd501_shield_cis_normal.paa" };
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(statics));
        editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
    };
	class RD501_Squad_Shield_trench: Land_House_Small_03_V1_ruins_F
	{
		author="$STR_A3_Bohemia_Interactive";
		mapSize=21.1;
		class SimpleObject
		{
			eden=0;
			animate[]={};
			hide[]={};
			verticalOffset=-0.023;
			verticalOffsetWorld=0;
			init="''";
		};
		editorPreview="\A3\EditorPreviews_F\Data\CfgVehicles\Land_House_Small_03_V1_ruins_F.jpg";
		scope=2;
		scopeCurator=2;
		displayName="Trench Shield";
		model="\RD501_Vehicles\static\shields\trench\trench.p3d";
		icon="iconObject_1x1";
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(statics));
		editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
	};

	class RD501_Droideka_Shield: Land_House_Small_03_V1_ruins_F
	{
		author="$STR_A3_Bohemia_Interactive";
		mapSize=21.1;
		class SimpleObject
		{
			eden=0;
			animate[]={};
			hide[]={};
			verticalOffset=-0;
			verticalOffsetWorld=0;
			init="''";
		};
		editorPreview="\A3\EditorPreviews_F\Data\CfgVehicles\Land_House_Small_03_V1_ruins_F.jpg";
		scope=2;
		scopeCurator=2;
		displayName="Droideka Shield";
		model="\RD501_Vehicles\static\shields\bubble\deka_shield.p3d";
		icon="iconObject_1x1";
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(statics));
		editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
	};
	
	class RD501_Personal_Shield: Land_House_Small_03_V1_ruins_F
	{
		author="$STR_A3_Bohemia_Interactive";
		mapSize=21.1;
		class SimpleObject
		{
			eden=0;
			animate[]={};
			hide[]={};
			verticalOffset=-0.023;
			verticalOffsetWorld=0;
			init="''";
		};
		editorPreview="\A3\EditorPreviews_F\Data\CfgVehicles\Land_House_Small_03_V1_ruins_F.jpg";
		scope=2;
		scopeCurator=2;
        armor = 999999;
        armorStructural = 999;
        sound = "Shield";
        simulation = "Fountain";
		displayName="Trench Shield";
		model="\RD501_Vehicles\static\shields\personal\personal_shield.p3d";
        hiddenSelectionsTextures[] = {"\RD501_Vehicles\static\shields\personal\personal_shield.paa"};
		icon="iconObject_1x1";
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(statics));
		editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
	};
    class JLTS_portable_shield_gar;
    class RD501_portable_shield:JLTS_portable_shield_gar
	{
		author = "MrClock";
		scope = 2;
		displayName = "Portable Shield Generator";
        armor = 999999;
        armorStructural = 999999;
        explosionShielding=99999;
		model = "\MRC\JLTS\weapons\Statics\generator\generator_shield.p3d";
		hiddenSelections[] = {"camo1","glow"};
		hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\Statics\generator\data\generator_co.paa","\MRC\JLTS\weapons\Statics\generator\data\generator_co.paa"};
		hiddenSelectionsMaterials[] = {"","\a3\characters_f_bootcamp\common\data\vrarmoremmisive.rvmat"};
		editorPreview = "\MRC\JLTS\weapons\Statics\generator\data\ui\editorPreviews\JLTS_portable_shield.jpg";
		editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
		class AnimationSources
		{
			class shield_source
			{
				AnimPeriod = 0.1;
				source = "user";
				InitPhase = 1;
			};
		};
		class UserActions
		{
			class shield_on
			{
				displayName = "$STR_JLTS_names_UAActivateShield";
				displayNameDefault = "<img image='\MRC\JLTS\weapons\Statics\generator\data\ui\activate_shield_ca.paa' size='3' shadow='false' />";
				priority = 0;
				radius = 1.5;
				position = "camera";
				showWindow = 1;
				hideOnUse = 1;
				onlyForPlayer = 1;
				shortcut = "";
				condition = "((this animationSourcePhase 'shield_source') < 0.5) && (cameraOn isKindOf 'CAManBase')";
				statement = "this animateSource ['shield_source',1]";
			};
			class shield_off
			{
				displayName = "$STR_JLTS_names_UADeactivateShield";
				displayNameDefault = "<img image='\MRC\JLTS\weapons\Statics\generator\data\ui\deactivate_shield_ca.paa' size='3' shadow='false' />";
				priority = 0;
				radius = 1.5;
				position = "camera";
				showWindow = 1;
				hideOnUse = 1;
				onlyForPlayer = 1;
				shortcut = "";
				condition = "((this animationSourcePhase 'shield_source') > 0.5) && (cameraOn isKindOf 'CAManBase')";
				statement = "this animateSource ['shield_source',0]";
			};
		};
		class assembleInfo
		{
			primary = 1;
			base = "";
			assembleTo = "";
			displayName = "";
			dissasembleTo[] = {"RD501_portable_shield_backpack"};
		};
		class Attributes
		{
			class Shield_on
			{
				displayName = "$STR_JLTS_names_UAActivateShield";
				tooltip = "";
				property = "JLTS_portable_shield_gar_shieldOn";
				control = "CheckboxNumber";
				expression = "_this animateSource ['shield_source',_value]";
				defaultValue = 0;
			};
		};
	};
};
class CfgSounds
{
	class rd501_squad_shield_loop
	{
		// how the sound is referred to in the editor (e.g. trigger effects)
		name = "shield_loop";

		// filename, volume, pitch, distance (optional)
		sound[] = { "\RD501_Vehicles\static\shields\shield_loop.wss", 4, 1, 300 };

		// subtitle delay in seconds, subtitle text
		titles[] = { 1, "*Electric Shield Hum*" };
	};
	class rd501_squad_shield_end
	{
		// how the sound is referred to in the editor (e.g. trigger effects)
		name = "shield_loop";

		// filename, volume, pitch, distance (optional)
		sound[] = { "\RD501_Vehicles\static\shields\shield_end.wss", 4, 1, 300 };

		// subtitle delay in seconds, subtitle text
		titles[] = { 1, "*Shield Retracting Hiss*" };
	};
};


class Extended_Init_EventHandlers 
{
    class RD501_Shield_Wall
    {
        class rd501_init_shield {
            init = "[_this select 0] call rd501_fnc_shield_init";
        };
    };
    class RD501_Shield_Door
    {
        class rd501_init_shield {
            init = "[_this select 0] call rd501_fnc_shield_init";
        };
    };
    class RD501_Shield_Gate_L
    {
        class rd501_init_shield {
            init = "[_this select 0] call rd501_fnc_shield_init";
        };
    };
    class RD501_Shield_Gate_S
    {
        class rd501_init_shield {
            init = "[_this select 0] call rd501_fnc_shield_init";
        };
    };
};