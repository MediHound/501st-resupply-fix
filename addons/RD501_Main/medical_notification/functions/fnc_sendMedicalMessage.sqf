/*
 * Author: M3ales
 *
 * Arguments:
 * Player
 * The player sending the message
 * Target
 * The player receiving the message
 *
 * Example:
 * [player, cursorTarget] call rd501_medical_notification_fnc_sendMedicalMessage
 *
 * Public: No
 */

#include "function_macros.hpp"

#define ICON_PATH(icon_name) ADDON_PATH##\data\icons\##icon_name##.paa
#define QICON_PATH(icon_name) QUOTE(ICON_PATH(icon_name))

params["_player", "_target", ""];
if(!GVAR(enabled)) exitWith {};
if(_player == _target) exitWith {};
if(time - GVAR(rateLimitTime) < 1) exitWith {};

if(ace_medical_gui_pendingReopen) exitWith {
	// Already showed message, skip
	LOG("Pending reopen, not displaying");
};

if(isNil QGVAR(message) || GVAR(message) == "") then {
	GVAR(message) = "%1 is asking that you kindly hold still.";
	LOGF_1("Message not initialised, setting to default ('%1')", GVAR(message));	
};

private _name = [_player] call ace_common_fnc_getName;
private _targetName = [_target] call ace_common_fnc_getName;

if(_target getVariable["ACE_Unconscious", false]) exitWith {
	LOGF_1("%1 is unconscious, ignoring", _targetName);
};

private _message = format[GVAR(message), _name];
if([_player] call ace_medical_treatment_fnc_isMedic) then {
	LOGF_1("Displaying medic message to %1", _targetName);
	private _image = QICON_PATH(medical_emblem);
	private _textHTML = format["<t align='center'>%1</t>", _message];
	private _merged = composeText [parseText format["<img size='2' align='center' color='%2' image='%1'/>", _image, [1,1,1] call BIS_fnc_colorRGBtoHTML], lineBreak, _message];
	["ace_common_displayTextStructured", [_merged, 2, _target], [_target]] call CBA_fnc_targetEvent;
}else {
	LOGF_1("Displaying non-medic message to %1", _targetName);
	["ace_common_displayTextStructured", [_message, 2, _target], [_target]] call CBA_fnc_targetEvent;
};
GVAR(rateLimitTime) = time;