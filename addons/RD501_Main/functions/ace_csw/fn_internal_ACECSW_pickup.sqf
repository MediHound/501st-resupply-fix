#include "\z\ace\addons\csw\functions\script_component.hpp"
/*
 * Author:Dani (TCVM)
 * Picks up the tripod and adds it to the player launcher slot
 *
 * Arguments:
 * 0: Tripod <OBJECT>
 * 1: Unit <OBJECT>
 *
 * Return Value:
 * None
 *
 * Example:
 * [tripod, player] call ace_csw_fnc_assemble_pickupTripod
 *
 * Public: No
 */

params ["_target", "_player", "_params"];

[{
    params ["_tripod", "_player"];
    TRACE_2("assemble_pickupTripod",_tripod,_player);

    private _tripodClassname = getText(configOf _tripod >> QUOTE(ADDON) >> "disassembleTo");
    private _pickupTime = getNumber(configFile >> "CfgWeapons" >> _tripodClassname >> QUOTE(ADDON) >> "pickupTime");

    private _onFinish = {
        params ["_args"];
        _args params ["_tripod", "_player", "_tripodClassname"];
        TRACE_3("assemble_pickupTripod finish",_tripod,_player,_tripodClassname);
		// Start custom tripod code ...
		private _state = weaponState [_tripod, [0]];

		private _mag = _state select 3;
		private _size = _state select 4;

		[_player, _mag, _size] call ace_csw_fnc_reload_handleReturnAmmo;
		// ... end custom tripod code.
        deleteVehicle _tripod;
        _player addWeaponGlobal _tripodClassname;
        [_player, "PutDown"] call EFUNC(common,doGesture);
    };

    private _condition = {
        params ["_args"];
        _args params ["_tripod", "_player"];

        !(isNull _tripod) && { (secondaryWeapon _player) isEqualTo "" }

    };

    TRACE_3("",_pickupTime,typeOf _tripod,_tripodClassname);
    [TIME_PROGRESSBAR(_pickupTime), [_tripod, _player, _tripodClassname], _onFinish, {}, localize LSTRING(PickupTripod_progressBar), _condition] call EFUNC(common,progressBar);
}, [_target, _player]] call CBA_fnc_execNextFrame;