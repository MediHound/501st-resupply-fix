// Handles UAV landing. Requires a UAV and the trigger position.
params ["_oneTrueUav", "_position", "_key"];

if (not (local _oneTrueUav)) exitWith {
	["RD501_event_UAVNET_removeTrigger", [_key]] call CBA_fnc_serverEvent;
};

_oneTrueUav call ace_common_fnc_fixCollision;

private _t1 = time;
private _t2 = time + 2;

private _handle = [
{
    params["_args", "_handle"];
    _args params["_vehicle", "_destination", "_t1", "_t2", "_key"];
    _vehicle setVelocityTransformation
    [
        getPosASL _vehicle,
        (_destination vectorAdd [0,0,0.2]),
        velocity _vehicle,
        [0,0,0],
        vectorDir _vehicle,
        vectorDir _vehicle,
        vectorUp _vehicle,
        [0,0,1],
        linearConversion [_t1, _t2, time, 0, 0.08]
    ];

    if(time > _t2) exitWith {
        [_handle] call CBA_fnc_removePerFrameHandler;
        ["RD501_event_UAVNET_removeTrigger", [_key]] call CBA_fnc_serverEvent;
    };
}, 0, [_oneTrueUav, _position, _t1, _t2, _key]] call CBA_fnc_addPerFrameHandler;