["RD501_event_internal_UAVNET_spawnTrigger", {
	_this call RD501_fnc_internal_UAVNET_spawn;
}] call CBA_fnc_addEventHandler;

["RD501_event_internal_UAVNET_handleUAV", {
	_this call RD501_fnc_internal_UAVNET_handleUAV;
}] call CBA_fnc_addEventHandler;

["RD501_event_UAVNET_removeTrigger", {
	_this call RD501_fnc_UAVNET_removeTrigger;
}] call CBA_fnc_addEventHandler;

if (isServer) then {
	[] call RD501_fnc_UAVNET_removeTrigger;
};