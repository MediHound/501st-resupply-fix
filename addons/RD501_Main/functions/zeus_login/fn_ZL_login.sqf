params ["_target", "_player", "_params"];

if(!RD501_ZL_Enabled) exitWith{ diag_log "ZL Disabled"; };

private _curZeus = missionnamespace getVariable ["RD501_ZL_activeZeusUser", false];

diag_log text (["[RD501]", "[ZEUS LOGIN]", "DEBUG:", "Attempting login to Zeus with player", _player, "| is there a current zeus?", _curZeus] joinString " ");

if (!_curZeus) then {
	missionNamespace setVariable ["RD501_ZL_activeZeusUser", true, true];
	missionNamespace setVariable ["RD501_ZL_activeZeusUserObject", _player, true];

	[_player] remoteExecCall ["RD501_fnc_ZL_grant", 2];
};