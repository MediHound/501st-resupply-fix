#include "..\loglevel.hh"

["Initialiing cruise missile events.", LOG_DEBUG, "CRML POSTINIT"] call RD501_fnc_logMessage;

["RD501_event_internal_CRML_registerLauncher", {
	_this call RD501_fnc_internal_CRML_registerLauncher;
}] call CBA_fnc_addEventHandler;

["RD501_event_internal_CRML_removeLauncher", {
	_this call RD501_fnc_internal_CRML_removeLauncher;
}] call CBA_fnc_addEventHandler;

["RD501_event_internal_CRML_launchMissile", {
	_this call RD501_fnc_internal_CRML_launchMissile;
}] call CBA_fnc_addEventHandler;

["RD501_event_internal_CRML_launchMissileFromMashGrenade", {
	_this call RD501_fnc_internal_CRML_launchMissileFromMashGrenade;
}] call CBA_fnc_addEventHandler;

["RD501_event_internal_CRML_reloadVLSMagazine", {
	(_this select 0) setVehicleAmmo 1;
	[["Reloaded", _this select 0] joinString " ", LOG_DEBUG, "CRML RELOAD EVENT"] call RD501_fnc_logMessage;
}] call CBA_fnc_addEventHandler;

["RD501_event_internal_CRML_deleteVLSLauncher", {
	params ["_vic"];
	{
		_vic deleteVehicleCrew _x;
	} forEach (crew _vic);

	private _log = ["Deleted", _vic, "and all crew."] joinString " ";

	deleteVehicle _vic;

	[_log, LOG_DEBUG, "CRML"] call RD501_fnc_logMessage;
}] call CBA_fnc_addEventHandler;

["Initalized cruise missile events.", LOG_DEBUG, "CRML POSTINIT"] call RD501_fnc_logMessage;

if (isServer) then {
	if (isNil "RD501_CRML_LauncherNodes") then {
		RD501_CRML_LauncherNodes = [];
		publicVariable "RD501_CRML_LauncherNodes";
	};

	if (isNil "RD501_CRML_RandomSpawnLastLaunch") then {
		RD501_CRML_RandomSpawnLastLaunch = [-1];
		publicVariable "RD501_CRML_RandomSpawnLastLaunch";
	};
};