#include "..\loglevel.hh"

// Launches a cruise missle.
params [
	// The caller of this action.
	["_caller", player],
	// The target to guide the missile to.
	["_target", laserTarget player],
	// The type of missile. It must be a type
	// that is configured in the launchers
	// magazines. If no launcher contains this
	// missile type, no missile will be fired.
	["_missileType", "RD501_Cruise_Missile_Ammo_Base"],
	// Script to execute on the death of the missile.
	["_onDeath", {}],

	// The tracking settings.
	// 0 - A boolean value that when set to true will spawn
	// 		an asset that can be tracked on map (or with a cTab, etc.)
	// 1 - The side for the object. Currently supports east and west.
	// 2 - The group name for the object.
	["_trackingSettings", [false, objNull, objNull]],
	// True if the target should be deleted
	// after the missile impacts.
	["_deleteTarget", false],
	
	// Further Options
	// The start point for the missile. Leave as objNull
	// to get a random start point from the registered
	// launchers or a random position near the target if
	// random missile spawn is enabled.
	// The value for this must follow the following:
	// 0 - Start object
	// 1 - Reload speed (seconds)
	// 2 - Last launch (seconds)
	// 3 - Delete launcher after launch (bool)
	["_missileStart", []]
];

["RD501_event_internal_CRML_launchMissile", [_caller, _target, _missileType, _onDeath, _trackingSettings, _deleteTarget, _missileStart]] call cba_fnc_serverEvent;

["Client requested missile launch from server.", LOG_DEBUG, "CRML"] call RD501_fnc_logMessage;