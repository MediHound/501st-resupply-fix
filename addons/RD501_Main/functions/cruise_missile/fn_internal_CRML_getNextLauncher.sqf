#include "..\loglevel.hh"

// Returns the next launcher configuration if applicable.

// If no launcher exist, return -1
// Otherwise, return the index of the least recent launcher.

params ["_missileType"];

private _lowValSet = [];
if ((count RD501_CRML_LauncherNodes) != 0) then {
	private _initalDone = false;
	private _lastVal = 1;
	private _lastCanLaunch = false;
	private _curTime = time;
	{
		if ((isNull (_x select 0)) or !(alive (_x select 0))) then {
			[["Launcher", _x select 0, "is no longer alive, removing launcher from list."] joinString " ", LOG_INFO, "CRML NEXT LAUNCHER"] call RD501_fnc_logMessage;
			[_x select 0] call RD501_fnc_CRML_removeLauncher;
		} else {
			try {
				private _timeSinceLastLaunch = _curTime - ((_x select 3) select 0);
				private _canLaunch = _timeSinceLastLaunch > (_x select 2);
				private _mags = [_x select 0, _missileType] call RD501_fnc_internal_CRML_getMagazine;
				if (((not _initalDone) or (not (_lastCanLaunch) and _canLaunch)) and (_mags select 0)) then {
					_initalDone = true;
					_lastVal = _timeSinceLastLaunch;
					_lastCanLaunch = _canLaunch;
					_lowValSet = [_x];
				} else {
					if (_lastCanLaunch == _canLaunch) then {
						if ((_timeSinceLastLaunch == _lastVal) and (_mags select 0)) then {
							_lowValSet pushBack _x;
						};

						if ((_timeSinceLastLaunch > _lastVal) and (_mags select 0)) then {
							_lastVal = _timeSinceLastLaunch;
							_lastCanLaunch = _canLaunch;
							_lowValSet = [_x];
						};
					};
				};
			} catch {
				[["Find next launcher errored on iteration", _x] joinString " ", LOG_WARN, "CRML NEXT LAUNCHER"] call RD501_fnc_logMessage;
			};
		};
	} forEach RD501_CRML_LauncherNodes;
};

private _lowVal = selectRandom _lowValSet;
if (isNil "_lowVal") then {
	_lowVal = [];
};

_lowVal;
