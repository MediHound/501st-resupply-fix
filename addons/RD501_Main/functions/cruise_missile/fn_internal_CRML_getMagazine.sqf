// Finds a magazine in the vehicle tha tmatches the proivded ammo type.
params ["_launcherVic", "_missileType"];

private _actualWeapons = _launcherVic weaponsTurret [0];

private _turretMags = getArray ((configFile >> "CfgVehicles" >> typeOf _launcherVic >> "Turrets") select 0 >> "magazines");

private _foundAmmoType = false;
private _foundAmmoWeapon = "";
private _foundAmmoMag = "";
{
	private _weapon = _x;
	{
		if (_x in _turretMags) then {
			if ((getText (configFile >> "CfgMagazines" >> _x >> "ammo")) == _missileType) exitWith
			{
				_foundAmmoType = true;
				_foundAmmoMag = _x;
				_foundAmmoWeapon = _weapon;
			};
		};
	} forEach (getArray (configFile >> "CfgWeapons" >> _x >> "magazines"));
} forEach _actualWeapons;

private _res = [_foundAmmoType, _foundAmmoWeapon, _foundAmmoMag];
_res;