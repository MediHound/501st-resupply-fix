//Exported via Arma Dialog Creator (https://github.com/kayler-renslow/arma-dialog-creator)

#include "TextureSelector_CustomControlClasses.h"
class RD501_TEXSEL_TextureSelectionMenu
{
	idd = 501001;
	
	class ControlsBackground
	{
		class BackgroundMain
		{
			type = 0;
			idc = -1;
			x = safeZoneX + safeZoneW * 0.19375;
			y = safeZoneY + safeZoneH * 0.15111112;
			w = safeZoneW * 0.6375;
			h = safeZoneH * 0.67222223;
			style = 0;
			text = "";
			colorBackground[] = {0,0,0,1};
			colorText[] = {1,1,1,1};
			font = "PuristaMedium";
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			
		};
		
	};
	class Controls
	{
		class TextureGroupSelector
		{
			type = 4;
			idc = 001;
			x = safeZoneX + safeZoneW * 0.218125;
			y = safeZoneY + safeZoneH * 0.22333334;
			w = safeZoneW * 0.2875;
			h = safeZoneH * 0.03111112;
			style = 16;
			arrowEmpty = "\A3\ui_f\data\GUI\RscCommon\RscCombo\arrow_combo_ca.paa";
			arrowFull = "\A3\ui_f\data\GUI\RscCommon\RscCombo\arrow_combo_active_ca.paa";
			colorBackground[] = {1,1,1,1};
			colorDisabled[] = {0.8,0.2,0.2,1};
			colorSelect[] = {0.2,0.2,0.2,1};
			colorSelectBackground[] = {0.8,0.8,0.8,1};
			colorText[] = {0,0,0,1};
			font = "PuristaMedium";
			maxHistoryDelay = 0;
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			soundCollapse[] = {"\A3\ui_f\data\sound\RscCombo\soundCollapse",0.1,1.0};
			soundExpand[] = {"\A3\ui_f\data\sound\RscCombo\soundExpand",0.1,1.0};
			soundSelect[] = {"\A3\ui_f\data\sound\RscCombo\soundSelect",0.1,1.0};
			wholeHeight = 0.3;
			onLBSelChanged = "[] call RD501_fnc_internal_TEXSEL_onGroupChanged;";
			class ComboScrollBar
			{
				color[] = {1,1,1,1};
				thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
				arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
				arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
				border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
				
			};
			
		};
		class AvalibleTextures
		{
			type = 5;
			idc = 002;
			x = safeZoneX + safeZoneW * 0.21875;
			y = safeZoneY + safeZoneH * 0.27333334;
			w = safeZoneW * 0.2875;
			h = safeZoneH * 0.47111112;
			style = 16;
			colorBackground[] = {1,1,1,1};
			colorDisabled[] = {0.8,0.2,0.2,1};
			colorSelect[] = {0.4,0.502,0.902,1};
			colorText[] = {0,0,0,1};
			font = "PuristaMedium";
			maxHistoryDelay = 0;
			rowHeight = 0;
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			soundSelect[] = {"\A3\ui_f\data\sound\RscListbox\soundSelect",0.09,1.0};
			class ListScrollBar
			{
				color[] = {1,1,1,1};
				thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
				arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
				arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
				border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
				
			};
			
		};
		class TextSelectorLabel
		{
			type = 0;
			idc = -1;
			x = safeZoneX + safeZoneW * 0.31875;
			y = safeZoneY + safeZoneH * 0.17555556;
			w = safeZoneW * 0.1875;
			h = safeZoneH * 0.03666667;
			style = 2+192;
			text = "Texture Selector";
			colorBackground[] = {1,1,1,1};
			colorText[] = {0,0,0,1};
			font = "PuristaMedium";
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			
		};
		class ApplyTextureButton
		{
			type = 1;
			idc = 003;
			x = safeZoneX + safeZoneW * 0.21875;
			y = safeZoneY + safeZoneH * 0.76222223;
			w = safeZoneW * 0.1625;
			h = safeZoneH * 0.03666667;
			style = 192+2;
			text = "Apply";
			borderSize = 0;
			colorBackground[] = {0.6,0.8,0.8,1};
			colorBackgroundActive[] = {0.2,0.4,0.4,1};
			colorBackgroundDisabled[] = {0.2,0.2,0.2,1};
			colorBorder[] = {0,0,0,0};
			colorDisabled[] = {0.2,0.2,0.2,1};
			colorFocused[] = {0.2,0.4,0.4,1};
			colorShadow[] = {0.2,0.4,0.4,1};
			colorText[] = {0,0,0,1};
			font = "PuristaMedium";
			offsetPressedX = 0.01;
			offsetPressedY = 0.01;
			offsetX = 0.01;
			offsetY = 0.01;
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			soundClick[] = {"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1.0};
			soundEnter[] = {"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1.0};
			soundEscape[] = {"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1.0};
			soundPush[] = {"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1.0};
			onButtonClick = "[] call RD501_fnc_internal_TEXSEL_changeTexture;";
			
		};
		class SaveToDefaultButton
		{
			type = 1;
			idc = 004;
			x = safeZoneX + safeZoneW * 0.39375;
			y = safeZoneY + safeZoneH * 0.76222223;
			w = safeZoneW * 0.1125;
			h = safeZoneH * 0.03666667;
			style = 2+192;
			text = "Save As Default";
			borderSize = 0;
			colorBackground[] = {0.2,0.302,0.702,1};
			colorBackgroundActive[] = {0.102,0.2,0.6,1};
			colorBackgroundDisabled[] = {0.2,0.2,0.2,1};
			colorBorder[] = {0,0,0,0};
			colorDisabled[] = {0.2,0.2,0.2,1};
			colorFocused[] = {0.102,0.2,0.6,1};
			colorShadow[] = {0.102,0.2,0.6,1};
			colorText[] = {1,1,1,1};
			font = "PuristaMedium";
			offsetPressedX = 0.01;
			offsetPressedY = 0.01;
			offsetX = 0.01;
			offsetY = 0.01;
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			soundClick[] = {"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1.0};
			soundEnter[] = {"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1.0};
			soundEscape[] = {"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1.0};
			soundPush[] = {"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1.0};
			onButtonClick = "[] call RD501_fnc_internal_TEXSEL_addDefaultOption;";
			
		};
		class DefaultTextures
		{
			type = 5;
			idc = 005;
			x = safeZoneX + safeZoneW * 0.51875;
			y = safeZoneY + safeZoneH * 0.22444445;
			w = safeZoneW * 0.2875;
			h = safeZoneH * 0.52;
			style = 16;
			colorBackground[] = {1,1,1,1};
			colorDisabled[] = {0.2,0.2,0.2,1};
			colorSelect[] = {0.4,0.502,0.902,1};
			colorText[] = {0,0,0,1};
			font = "PuristaMedium";
			maxHistoryDelay = 0;
			rowHeight = 0;
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			soundSelect[] = {"\A3\ui_f\data\sound\RscListbox\soundSelect",0.09,1.0};
			class ListScrollBar
			{
				color[] = {1,1,1,1};
				thumb = "\A3\ui_f\data\gui\cfg\scrollbar\thumb_ca.paa";
				arrowFull = "\A3\ui_f\data\gui\cfg\scrollbar\arrowFull_ca.paa";
				arrowEmpty = "\A3\ui_f\data\gui\cfg\scrollbar\arrowEmpty_ca.paa";
				border = "\A3\ui_f\data\gui\cfg\scrollbar\border_ca.paa";
				
			};
			
		};
		class TextureDefaultsLabel
		{
			type = 0;
			idc = -1;
			x = safeZoneX + safeZoneW * 0.51875;
			y = safeZoneY + safeZoneH * 0.17555556;
			w = safeZoneW * 0.2125;
			h = safeZoneH * 0.03666667;
			style = 2+192;
			text = "Texture Defaults";
			colorBackground[] = {1,1,1,1};
			colorText[] = {0,0,0,1};
			font = "PuristaMedium";
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			
		};
		class DeleteDefaultButton
		{
			type = 1;
			idc = 006;
			x = safeZoneX + safeZoneW * 0.51875;
			y = safeZoneY + safeZoneH * 0.76222223;
			w = safeZoneW * 0.1125;
			h = safeZoneH * 0.03666667;
			style = 2+192;
			text = "Delete Default";
			borderSize = 0;
			colorBackground[] = {0.8,0.2,0.2,1};
			colorBackgroundActive[] = {0.6,0,0,1};
			colorBackgroundDisabled[] = {0.2,0.2,0.2,1};
			colorBorder[] = {0,0,0,0};
			colorDisabled[] = {0.2,0.2,0.2,1};
			colorFocused[] = {0.6,0,0,1};
			colorShadow[] = {0.6,0,0,1};
			colorText[] = {1,1,1,1};
			font = "PuristaMedium";
			offsetPressedX = 0.01;
			offsetPressedY = 0.01;
			offsetX = 0.01;
			offsetY = 0.01;
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			soundClick[] = {"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1.0};
			soundEnter[] = {"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1.0};
			soundEscape[] = {"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1.0};
			soundPush[] = {"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1.0};
			onButtonClick = "[] call RD501_fnc_internal_TEXSEL_removeDefaultOption;";
			
		};
		class MoveDefaultUpButton
		{
			type = 1;
			idc = 007;
			x = safeZoneX + safeZoneW * 0.66875;
			y = safeZoneY + safeZoneH * 0.76222223;
			w = safeZoneW * 0.0625;
			h = safeZoneH * 0.03666667;
			style = 2+192;
			text = "Move Up";
			borderSize = 0;
			colorBackground[] = {1,1,1,1};
			colorBackgroundActive[] = {0.8,0.8,0.8,1};
			colorBackgroundDisabled[] = {0.2,0.2,0.2,1};
			colorBorder[] = {0,0,0,0};
			colorDisabled[] = {0.2,0.2,0.2,1};
			colorFocused[] = {0.8,0.8,0.8,1};
			colorShadow[] = {0.8,0.8,0.8,1};
			colorText[] = {0,0,0,1};
			font = "PuristaMedium";
			offsetPressedX = 0.01;
			offsetPressedY = 0.01;
			offsetX = 0.01;
			offsetY = 0.01;
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			soundClick[] = {"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1.0};
			soundEnter[] = {"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1.0};
			soundEscape[] = {"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1.0};
			soundPush[] = {"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1.0};
			onButtonClick = "[true] call RD501_fnc_internal_TEXSEL_moveDefaultOption;";
			
		};
		class MoveDefaultDownButton
		{
			type = 1;
			idc = 008;
			x = safeZoneX + safeZoneW * 0.74375;
			y = safeZoneY + safeZoneH * 0.76222223;
			w = safeZoneW * 0.0625;
			h = safeZoneH * 0.03666667;
			style = 2+192;
			text = "Move Down";
			borderSize = 0;
			colorBackground[] = {1,1,1,1};
			colorBackgroundActive[] = {0.8,0.8,0.8,1};
			colorBackgroundDisabled[] = {0.2,0.2,0.2,1};
			colorBorder[] = {0,0,0,0};
			colorDisabled[] = {0.8,0.8,0.8,1};
			colorFocused[] = {0.8,0.8,0.8,1};
			colorShadow[] = {0.8,0.8,0.8,1};
			colorText[] = {0,0,0,1};
			font = "PuristaMedium";
			offsetPressedX = 0.01;
			offsetPressedY = 0.01;
			offsetX = 0.01;
			offsetY = 0.01;
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			soundClick[] = {"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1.0};
			soundEnter[] = {"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1.0};
			soundEscape[] = {"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1.0};
			soundPush[] = {"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1.0};
			onButtonClick = "[false] call RD501_fnc_internal_TEXSEL_moveDefaultOption;";
			
		};
		class CloseMenuButton
		{
			type = 1;
			idc = 009;
			x = safeZoneX + safeZoneW * 0.74375;
			y = safeZoneY + safeZoneH * 0.17555556;
			w = safeZoneW * 0.0625;
			h = safeZoneH * 0.03666667;
			style = 2+192;
			text = "Close";
			borderSize = 0;
			colorBackground[] = {0.8,0.2,0.2,1};
			colorBackgroundActive[] = {0.6,0,0,1};
			colorBackgroundDisabled[] = {0.2,0.2,0.2,1};
			colorBorder[] = {0,0,0,0};
			colorDisabled[] = {0.2,0.2,0.2,1};
			colorFocused[] = {0.6,0,0,1};
			colorShadow[] = {0.6,0,0,1};
			colorText[] = {1,1,1,1};
			font = "PuristaMedium";
			offsetPressedX = 0.01;
			offsetPressedY = 0.01;
			offsetX = 0.01;
			offsetY = 0.01;
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			soundClick[] = {"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1.0};
			soundEnter[] = {"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1.0};
			soundEscape[] = {"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1.0};
			soundPush[] = {"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1.0};
			onButtonClick = "closeDialog 1;";
			
		};
		class ResetTexturesButton
		{
			type = 1;
			idc = 009;
			x = safeZoneX + safeZoneW * 0.21875;
			y = safeZoneY + safeZoneH * 0.17555556;
			w = safeZoneW * 0.0875;
			h = safeZoneH * 0.03666667;
			style = 2+192;
			text = "Reset Textures";
			borderSize = 0;
			colorBackground[] = {0.8,0.2,0.2,1};
			colorBackgroundActive[] = {0.6,0,0,1};
			colorBackgroundDisabled[] = {0.2,0.2,0.2,1};
			colorBorder[] = {0,0,0,0};
			colorDisabled[] = {0.2,0.2,0.2,1};
			colorFocused[] = {0.6,0,0,1};
			colorShadow[] = {0.6,0,0,1};
			colorText[] = {1,1,1,1};
			font = "PuristaMedium";
			offsetPressedX = 0.01;
			offsetPressedY = 0.01;
			offsetX = 0.01;
			offsetY = 0.01;
			sizeEx = (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);
			soundClick[] = {"\A3\ui_f\data\sound\RscButton\soundClick",0.09,1.0};
			soundEnter[] = {"\A3\ui_f\data\sound\RscButton\soundEnter",0.09,1.0};
			soundEscape[] = {"\A3\ui_f\data\sound\RscButton\soundEscape",0.09,1.0};
			soundPush[] = {"\A3\ui_f\data\sound\RscButton\soundPush",0.09,1.0};
			onButtonClick = "[] call RD501_fnc_internal_TEXSEL_resetTextures;";
			
		};
		
	};
	
};
