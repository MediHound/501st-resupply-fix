#include "..\loglevel.hh"

private _diag = findDisplay 501001;
private _groupSelector = _diag displayCtrl 001;
private _textureSelector = _diag displayCtrl 002;
private _defaultsSelector = _diag displayCtrl 005;

private _textures = localNamespace getVariable ["RD501_TEXSEL_EligibleTextureConfigurations", createHashMap];
private _defaults = profileNamespace getVariable "RD501_TEXSEL_DefaultTextureConfigurations";
if (isNil '_defaults') then {
	_defaults = createHashMap;
	profileNamespace setVariable ["RD501_TEXSEL_DefaultTextureConfigurations", _defaults];
};

private _group = _groupSelector lbText (lbCurSel _groupSelector);
private _name = _textureSelector lbText (lbCurSel _textureSelector);

private _groupData = _textures getOrDefault [_group, createHashMap];
private _textureDataIndex = _groupData findIf { (_x select 0) == _name };

if (_textureDataIndex == -1) exitWith {
	[["No texture data found for", _group, _name] joinString " ", LOG_ERROR, "TEXSEL"] call RD501_fnc_logMessage;
};

private _textureData = _groupData select _textureDataIndex;
private _cfgName = _textureData select 2;

if (_cfgName isEqualTo "") exitWith {
	["No config selected to add to defaults.", LOG_INFO, "TEXSEL"] call RD501_fnc_logMessage;
};

private _vicClass = typeOf (vehicle player);

if (not (_vicClass in _defaults)) then {
	_defaults set [_vicClass, []];
};

(_defaults get _vicClass) pushBack [_name, _cfgName];

_defaultsSelector lbAdd _name;
