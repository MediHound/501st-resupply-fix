params ["_player", "_didJIP"];

[_player, _didJIP] call RD501_fnc_KT_localInit;
[_player, _didJIP] call RD501_fnc_ZL_localInit;

[_player, _didJIP] call RD501_fnc_internal_UTIL_blinkerLightsACEActions;

["ace_firedPlayer", {
    _this call RD501_fnc_internal_EVNT_handlePlayerFired;
}] call CBA_fnc_addEventHandler;

[] call RD501_fnc_TEXSEL_initLocal;