if(isServer) then {
    if(isDedicated) then {
        addMissionEventHandler ["EntityKilled", {
            params ["_a", "_b", "_c", "_d"];
            [_a, _b, _c, _d, false] remoteExecCall ["RD501_fnc_KT_saveNewKill", -2];
        }];
    } else {
        addMissionEventHandler ["EntityKilled", {
            params ["_a", "_b", "_c", "_d"];
            [_a, _b, _c, _d, false] remoteExecCall ["RD501_fnc_KT_saveNewKill", 0];
        }];
    };
};

["RD501_event_logMessage", {
	_this call RD501_fnc_logMessageGlobal;
}] call CBA_fnc_addEventHandler;

// Run script inits.

_this call RD501_fnc_CRML_init;
_this call RD501_fnc_UTIL_init;
_this call RD501_fnc_UAVNET_init;
_this call RD501_fnc_CAT_init;

// Register Spawn Pad Vics

["", "----------"] call RD501_fnc_VSM_registerSpawnOptions;
["", "Aircraft"] call RD501_fnc_VSM_registerSpawnOptions;
["", "----------"] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_arc_170_blue", "ARC-170 (Blue)"] call RD501_fnc_VSM_registerSpawnOptions;
["RD501_arc_170_razor", "ARC-170 (Razor)"] call RD501_fnc_VSM_registerSpawnOptions;
["RD501_arc_170_red", "ARC-170 (Red)"] call RD501_fnc_VSM_registerSpawnOptions;

["", ""] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_YWing", "Y-Wing Mk.X"] call RD501_fnc_VSM_registerSpawnOptions;

["", ""] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_v19_MKI", "Republic V19 Torrent"] call RD501_fnc_VSM_registerSpawnOptions;

["", ""] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_z95_blue", "Republic Z95"] call RD501_fnc_VSM_registerSpawnOptions;

["", ""] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_LAAT_MKIII_Balls", "LAAT/I MK.III"] call RD501_fnc_VSM_registerSpawnOptions;
["RD501_LAAT_C", "Republic LAAT/C"] call RD501_fnc_VSM_registerSpawnOptions;
["RD501_LAAT_LE", "Republic LAAT/LE"] call RD501_fnc_VSM_registerSpawnOptions;

["", ""] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_nuclass_mk1", "Republic NuClass Mk.I"] call RD501_fnc_VSM_registerSpawnOptions;

["", "----------"] call RD501_fnc_VSM_registerSpawnOptions;
["", "Drones"] call RD501_fnc_VSM_registerSpawnOptions;
["", "----------"] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_Dragon", "Dragon", true] call RD501_fnc_VSM_registerSpawnOptions;

["", ""] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_drone_rep_heli", "Elix", true] call RD501_fnc_VSM_registerSpawnOptions;
["RD501_drone_rep_stealth_heli", "Elix (Stealth)", true] call RD501_fnc_VSM_registerSpawnOptions;

["", ""] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_drone_rep_stealth_fixedwing", "Phantom", true] call RD501_fnc_VSM_registerSpawnOptions;

["", ""] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_drone_razor_ammo", "Prime Ammo Drone", true] call RD501_fnc_VSM_registerSpawnOptions;
["RD501_drone_razor_medical", "Prime Medical Drone", true] call RD501_fnc_VSM_registerSpawnOptions;
["RD501_drone_razor_turret", "Prime Turret Drone", true] call RD501_fnc_VSM_registerSpawnOptions;

["", ""] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_drone_Clone_Recon_Droid", "R-2 Recon Droid", true] call RD501_fnc_VSM_registerSpawnOptions;

["", "----------"] call RD501_fnc_VSM_registerSpawnOptions;
["", "Vehicles"] call RD501_fnc_VSM_registerSpawnOptions;
["", "----------"] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_warden_tank_mobile", "Republic Overseer ASV"] call RD501_fnc_VSM_registerSpawnOptions;

["", ""] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_fast_infantry_transport_republic", "Republic Pathfinder"] call RD501_fnc_VSM_registerSpawnOptions;
["RD501_fast_infantry_transport_republic_medic", "Republic Pathfinder (Medical)"] call RD501_fnc_VSM_registerSpawnOptions;

["", ""] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_saber_republic_tx130_MkII", "Saber TX 130 Mk.II"] call RD501_fnc_VSM_registerSpawnOptions;
["RD501_saber_republic_tx130_gl_MkII", "Saber TX 130 GL Mk.II"] call RD501_fnc_VSM_registerSpawnOptions;
["RD501_saber_republic_tx130_recon_MkII", "Saber TX 130 Recon Mk.II"] call RD501_fnc_VSM_registerSpawnOptions;
["RD501_saber_republic_tx130_super_MkII", "Saber TX 130 Super Mk.II"] call RD501_fnc_VSM_registerSpawnOptions;

["", ""] call RD501_fnc_VSM_registerSpawnOptions;

["RD501_walker_republic_atap_MkII", "Republic ATAP Mk.II"] call RD501_fnc_VSM_registerSpawnOptions;
