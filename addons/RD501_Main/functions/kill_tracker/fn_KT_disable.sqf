params ["_player"];

if(not (typeName _player == "string")) then {
	profileNamespace setVariable ["RD501_Kill_Feed_Active", false];

	private _feed = profileNamespace getVariable ["RD501_Kill_Feed", ""];
	_feed = [_feed, ["[DISABLED KILL FEED] Mission", missionName, "on", worldName] joinString " "] joinString (endl + endl + endl);
	profileNamespace setVariable ["RD501_Kill_Feed", _feed];
};
