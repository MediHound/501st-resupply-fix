#include "../config_macros.hpp"

class CfgPatches
{
	class RD501_patch_main_scripts
	{
		author=DANKAUTHORS;
		requiredAddons[]={};
		requiredVersion=0.1;
		units[]={};
		weapons[]={};
	};
};

class CfgFunctions
{
	class RD501
	{
		tag = "RD501";
		class magClamp
		{
			file = "RD501_Main\functions\magclamp";
			class MC_setFuel {
                file = "RD501_Main\functions\magclamp\setFuel.sqf";
            };
		};

		class catapult
		{
			file = "RD501_Main\functions\catapult";
			class CAT_init {};

			class CAT_loadMenuDefaults {};
			class CAT_openMenu {};
			
			class init_CAT_register {};

			class internal_CAT_detectVic {};
			class internal_CAT_launch {};
			class internal_CAT_onLaunchButtonPressed {};
		};

		class killTracker
		{
			file = "RD501_Main\functions\kill_tracker";
			class KT_saveNewKill {
				preInit = 1;
			};
			class KT_display {};
			class KT_enable {};
			class KT_disable {};
			class KT_registerZeusObject {};
			class KT_localInit {};
		};

		class spawnMenu
		{
			file = "RD501_Main\functions\spawn_menu";
			class VSM_openMenu {};
			class VSM_spawnVic {};
			class VSM_registerSpawnpoint {};
			class VSM_registerSpawnOptions {};
		};

		class uavNet
		{
			file = "RD501_Main\functions\uav_net";

			class init_UAVNET_register {};
			class internal_UAVNET_handleUAV {};
			class internal_UAVNET_spawn {};
			class UAVNET_init {};
			class UAVNET_onActivation {};
			class UAVNET_removeTrigger {};
		};

		class zeusLogin
		{
			file = "RD501_Main\functions\zeus_login";
			class ZL_globalMessage {};
			class ZL_grant {};
			class ZL_localInit {};
			class ZL_logout {};
			class ZL_login {};
			class ZL_revoke {};
		};

		class cruiseMissile
		{
			file = "RD501_Main\functions\cruise_missile";
			class CRML_addCruiseMissileSupportRegistrationActions {};
			class CRML_init {};
			class CRML_launchMashMissile {};
			class CRML_launchMissile {};
			class CRML_mashMissileOnDeath {};
			class CRML_preInit {};
			class CRML_registerLauncher {};
			class CRML_removeLauncher {};
			class CRML_vlsInit {};
			
			class init_CRML_registerLauncher {};

			class internal_CRML_bottomLaunchFireEventHandler {};
			class internal_CRML_getMagazine {};
			class internal_CRML_getNextlauncher {};
			class internal_CRML_guideMissilePFH {};
			class internal_CRML_launchMissile {};
			class internal_CRML_launchMissileFromCommMenu {};
			class internal_CRML_launchMissileFromMashGrenade {};
			class internal_CRML_registerLauncher {};
			class internal_CRML_removeLauncher {};
		};

		class startup
		{
			file = "RD501_Main\functions\startup";
			class missionInit {};
			class missionLocalInit {};
			class missionPreInit {};
		};

		class ACECSWUtils {
			file = "RD501_Main\functions\ace_csw";

			class internal_ACECSW_pickup {};
		};
		
		class utility {
			file = "RD501_Main\functions\utility";

			class logMessage {};
			class logMessageGlobal {};

			class localMessage {};

			class UTIL_blinkerLights {};
			class UTIL_init {};

			class internal_UTIL_blinkerLightsPFH {};
			class internal_UTIL_blinkerLightsACEActions {};
		};

		class events {
			file = "RD501_Main\functions\events";

			class internal_EVNT_handlePlayerFired {};
		};

		class texture_selector {
			file = "RD501_Main\functions\texture_selector";

			class TEXSEL_openTextureChangeMenu {};

			class TEXSEL_initLocal {};

			class internal_TEXSEL_addDefaultOption {};
			class internal_TEXSEL_canChangeTextures {};
			class internal_TEXSEL_changeTexture {};
			class internal_TEXSEL_moveDefaultOption {};
			class internal_TEXSEL_onGroupChanged {};
			class internal_TEXSEL_onVehicle {};
			class internal_TEXSEL_removeDefaultOption {};
			class internal_TEXSEL_resetTextures {};
		};
	};
};

#include "catapult\RD501_CAT_LaunchController.hh"
#include "spawn_menu\RD501_VSM_SpawnMenu.hh"
#include "texture_selector\RD501_TEXSEL_TextureSelectionMenu.h"

class Extended_PostInit_EventHandlers {
	class RD501_RunningMissionInit {
		init = "_this call RD501_fnc_missionInit";
		clientInit = "_this call RD501_fnc_missionLocalInit";
	};
};

class Extended_PreInit_EventHandlers {
	class RD501_RunningMissionPreInit {
		init = "_this call RD501_fnc_missionPreInit";
	};
};