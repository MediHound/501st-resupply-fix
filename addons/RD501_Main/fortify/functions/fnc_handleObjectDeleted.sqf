#include "function_macros.hpp"
params ["_player", "_side", "_objectDeleted"];
if!(local _player) exitWith {};
if (GVAR(useAmmo)) then
{
	LOGF_1("Player deleted %1, refunding currency item", _objectDeleted);
	[QGVAR(addAmmo), [_player]] call CBA_fnc_localEvent;
};
