/*
 * Author: M3ales, values prob from Namenai. Same with icons.
 *
 * Arguments:
 * Nothing
 *
 * Return Value:
 * 2d Array of colours specified in the format [[actionDisplayName, pathToIcon, rgbaValue], [actionDisplayName...]
 *
 * Example:
 * [] call rd501_mfd_colours_availableColours
 *
 * Public: No
 */

#include "function_macros.hpp"
#define ICON_PATH(icon_name) ADDON_PATH##\icons\##icon_name##.paa
#define QICON_PATH(icon_name) QUOTE(ICON_PATH(icon_name))

private _colours = [
	["Red", QICON_PATH(red), [1, 0, 0, 1]],
	["Orange", QICON_PATH(orange), [1, 0.05, 0, 1]],
	["Yellow", QICON_PATH(yellow), [1, 1, 0, 1]],
	["Green", QICON_PATH(green), [0, 1, 0, 1]],
	["Cyan", QICON_PATH(cyan), [0, 1, 1, 1]],
	["Blue", QICON_PATH(blue), [0, 0, 1, 1]],
	["Purple", QICON_PATH(purple), [0.5, 0, 0.5, 1]],
	["White", QICON_PATH(white), [1, 1, 1, 1]],
	["Black", QICON_PATH(black), [0, 0, 0, 1]],
	["None", QICON_PATH(noHud), [0, 0, 0, 0]]
];

_colours