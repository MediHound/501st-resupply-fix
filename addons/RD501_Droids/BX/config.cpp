#include "../../RD501_Main/config_macros.hpp"
#include "../droid_macros.hpp"

class CfgPatches
{
    class RD501_patch_BX
    {
        addonRootClass=RD501_patch_droids_config;
        requiredAddons[] = {
            RD501_patch_droids_config
        };
        requiredVersion = 0.1;
        units[] = {
            macro_new_unit_class(opfor,BX),
			macro_new_unit_class(opfor,BX_shield)
        };
    };
};

class CfgVehicles
{
    class lsd_cis_bxdroid_specops;
    class RD501_opfor_unit_BX: lsd_cis_bxdroid_specops
    {
        displayName = "BX Commando";
		author = "RD501";
		scope = 2;
		faction = macro_faction(CIS);
		editorSubcategory = macro_editor_cat(CIS_SpecOps);
		identityTypes[] = {"lsd_voice_b1Droid"};
		impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
		nakedUniform = "RD501_opfor_uniform_BX_armor";
		icon = "LSiconRifleman";
		canBleed = 0;
		cost = 4;
		armor=30;
		uniformClass = "RD501_opfor_uniform_BX_armor";
		armorStructural = 3;
		explosionShielding = 0.3;
		model = "\ls_armor_redfor\uniform\cis\bx\lsd_cis_bx_uniform.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"ls_armor_redfor\uniform\cis\bx\data\body_co.paa"};
		weapons[]=
		{
			macro_bx_gun,
			"Throw",
			"Put"
		};
		respawnWeapons[]=
		{
			macro_bx_gun,
			"Throw",
			"Put"
		};
		magazines[]=
		{
			macro_bx_gun_mags,
			macro_thermal_imp,
			macro_thermal_imp,
			macro_flashbang,
			macro_flashbang,
			macro_dioxis_grenade
		};
		respawnMagazines[]=
		{
			macro_bx_gun_mags,
			macro_thermal_imp,
			macro_thermal_imp,
			macro_flashbang,
			macro_flashbang,
			macro_dioxis_grenade
		};
		linkeditems[]=
		{
			"ItemGPS",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_2"
		};
		respawnlinkeditems[]=
		{
			"ItemGPS",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_2"
		};
		items[]={
			_3(macro_zip_tie)
		};
		respawnItems[]={
			_3(macro_zip_tie)
		};
		class HitPoints
		{
			class HitFace
			{
				armor = 100;
				material = -1;
				name = "face_hub";
				passThrough = 0.8;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 100;
				material = -1;
				name = "neck";
				passThrough = 0.8;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 100;
				material = -1;
				name = "head";
				passThrough = 0.8;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 25;
				material = -1;
				name = "pelvis";
				passThrough = 0.8;
				radius = 0.24;
				explosionShielding = 1;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 85;
				material = -1;
				name = "spine1";
				passThrough = 0.8;
				radius = 0.16;
				explosionShielding = 1;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 95;
				material = -1;
				name = "spine2";
				passThrough = 0.8;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 110;
				material = -1;
				name = "spine3";
				passThrough = 0.8;
				radius = 0.18;
				explosionShielding = 10;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 3;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 3;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 30;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.14;
				explosionShielding = 1;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 1;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 15;
				material = -1;
				name = "hand_l";
				passThrough = 1;
				radius = 0.08;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
			};
			class HitLeftLeg
			{
				armor = 15;
				material = -1;
				name = "leg_l";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
		class SoundEnvironExt
		{		
			generic[] = {
             {"walk", {"\WebKnightsRobotics\sounds\b2_step_1.ogg", 2, 1, 30}}, 
             {"walk", {"\WebKnightsRobotics\sounds\b2_step_2.ogg", 2, 1, 30}},
             {"walk", {"\WebKnightsRobotics\sounds\b2_step_3.ogg", 2, 1, 30}},
			 {"run", {"\WebKnightsRobotics\sounds\b2_step_1.ogg", 2, 1, 30}}, 
             {"run", {"\WebKnightsRobotics\sounds\b2_step_2.ogg", 2, 1, 30}},
             {"run", {"\WebKnightsRobotics\sounds\b2_step_3.ogg", 2, 1, 30}},
			 {"sprint", {"\WebKnightsRobotics\sounds\b2_step_1.ogg", 2, 1, 30}}, 
             {"sprint", {"\WebKnightsRobotics\sounds\b2_step_2.ogg", 2, 1, 30}},
             {"sprint", {"\WebKnightsRobotics\sounds\b2_step_3.ogg", 2, 1, 30}}
			};
		};
		class SoundEquipment 
		{	
		    soldier[] = {
             {"walk", {"\WebKnightsRobotics\sounds\b2_step_1.ogg", 2, 1, 30}}, 
             {"walk", {"\WebKnightsRobotics\sounds\b2_step_2.ogg", 2, 1, 30}},
             {"walk", {"\WebKnightsRobotics\sounds\b2_step_3.ogg", 2, 1, 30}},
			 {"run", {"\WebKnightsRobotics\sounds\b2_step_1.ogg", 2, 1, 30}}, 
             {"run", {"\WebKnightsRobotics\sounds\b2_step_2.ogg", 2, 1, 30}},
             {"run", {"\WebKnightsRobotics\sounds\b2_step_3.ogg", 2, 1, 30}},
			 {"sprint", {"\WebKnightsRobotics\sounds\b2_step_1.ogg", 2, 1, 30}}, 
             {"sprint", {"\WebKnightsRobotics\sounds\b2_step_2.ogg", 2, 1, 30}},
             {"sprint", {"\WebKnightsRobotics\sounds\b2_step_3.ogg", 2, 1, 30}}
            };
		};
		class SoundBreath
		{
			breath[] = {};
		};
		class SoundDrown
		{
			breath[] = {};
		};
		class SoundInjured
		{
			breath[] = {};
		};
		class SoundBleeding
		{
			breath[] = {};
		};
		class SoundBurning
		{
			breath[] = {};
		};
		class SoundChoke
		{
			breath[] = {};
		};
		class SoundRecovered
		{
			breath[] = {};
		};
    };
    class RD501_opfor_unit_BX_shield: RD501_opfor_unit_BX
    {
        displayName = "BX Commando (Shield)";
		author = "RD501";
		scope = 2;
		model = "\ls_armor_redfor\uniform\cis\bx\lsd_cis_bx_uniform.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"ls_armor_redfor\uniform\cis\bx\data\body_co.paa"};
		faction = macro_faction(CIS);
		editorSubcategory = macro_editor_cat(CIS_SpecOps);
		cost = 4;
		
		weapons[]=
		{
			"JLTS_E5",
			"Throw",
			"Put"
		};
		respawnWeapons[]=
		{
			"JLTS_E5",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			"JLTS_E5_mag",
			"JLTS_E5_mag",
			"JLTS_E5_mag",
			macro_thermal_imp,
			macro_thermal_imp,
			macro_smoke,
			macro_smoke,
			macro_flashbang,
			macro_flashbang,
			macro_dioxis_grenade
		};
		respawnMagazines[]=
		{
			"JLTS_E5_mag",
			"JLTS_E5_mag",
			"JLTS_E5_mag",
			macro_thermal_imp,
			macro_thermal_imp,
			macro_smoke,
			macro_smoke,
			macro_flashbang,
			macro_flashbang,
			macro_dioxis_grenade
		};
		linkeditems[]=
		{
			"ItemGPS",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_1"
		};
		respawnlinkeditems[]=
		{
			"ItemGPS",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_1"
		};
		items[]={
			_3(macro_zip_tie),
			"JLTS_riot_shield_droid_item"
		};
		respawnItems[]={
			_3(macro_zip_tie),
			"JLTS_riot_shield_droid_item"
		};
    };
	class RD501_opfor_unit_BX_captain:RD501_opfor_unit_BX
	{
		displayName = "BX Commando (Captain)";
		author = "RD501";
		scope = 2;
		faction = macro_faction(CIS);
		editorSubcategory = macro_editor_cat(CIS_SpecOps);
		identityTypes[] = {"lsd_voice_b1Droid"};
		impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
		nakedUniform = "RD501_opfor_uniform_BX_captain_armor";
		icon = "LSiconLeader";
		canBleed = 0;
		cost = 4;
		armor=30;
		uniformClass = "RD501_opfor_uniform_BX_captain_armor";
		armorStructural = 3;
		explosionShielding = 0.3;
		model = "\ls_armor_redfor\uniform\cis\bx\lsd_cis_bx_uniform.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"lsd_units_redfor\cis\specops\textures\bx_captain_co.paa"};
		weapons[]=
		{
			macro_bx_gun,
			"Throw",
			"Put"
		};
		respawnWeapons[]=
		{
			macro_bx_gun,
			"Throw",
			"Put"
		};
		magazines[]=
		{
			macro_bx_gun_mags,
			macro_thermal_imp,
			macro_thermal_imp,
			macro_flashbang,
			macro_flashbang,
			macro_dioxis_grenade
		};
		respawnMagazines[]=
		{
			macro_bx_gun_mags,
			macro_thermal_imp,
			macro_thermal_imp,
			macro_flashbang,
			macro_flashbang,
			macro_dioxis_grenade
		};
		linkeditems[]=
		{
			"ItemGPS",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_2"
		};
		respawnlinkeditems[]=
		{
			"ItemGPS",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_2"
		};
		items[]={
			_3(macro_zip_tie)
		};
		respawnItems[]={
			_3(macro_zip_tie)
		};
	};
};
class CfgWeapons
{
	//Uniform
	class U_I_CombatUniform;
	class UniformItem;
	class RD501_opfor_uniform_BX_armor : U_I_CombatUniform
	{
		scope = 2;
		displayName = "[CIS] BX Droid Chassis";
		picture = "\RD501_Droids\BX\BX_armor_ui.paa";
		model = "\ls_armor_redfor\uniform\cis\bx\lsd_cis_bx_uniform.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"ls_armor_redfor\uniform\cis\bx\data\body_co.paa"};
		armor = 15;
		class ItemInfo:UniformItem
		{
			uniformModel = "\ls_armor_redfor\uniform\cis\bx\lsd_cis_bx_uniform.p3d";
			uniformClass = "RD501_opfor_unit_BX";
			containerClass = "Supply150";
			mass = 40;
		};
	};
	class RD501_opfor_uniform_BX_captain_armor : RD501_opfor_uniform_BX_armor
	{
		scope = 2;
		displayName = "[CIS] BX Captain Chassis";
		picture = "\RD501_Droids\BX\BX_armor_ui.paa";
		model = "\ls_armor_redfor\uniform\cis\bx\lsd_cis_bx_uniform.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"lsd_units_redfor\cis\specops\textures\bx_captain_co.paa"};
		armor = 15;

		class ItemInfo:UniformItem
		{
			uniformModel = "\ls_armor_redfor\uniform\cis\bx\lsd_cis_bx_uniform.p3d";
			uniformClass = "RD501_opfor_unit_BX_captain";
			containerClass = "Supply150";
			mass = 40;
		};
	};
};