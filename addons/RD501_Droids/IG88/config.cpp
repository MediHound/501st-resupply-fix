class CfgPatches
{
	class rd501_patch_name_ig88_units
	{
		requiredAddons[]=
		{
			RD501_patch_units
		};
		requiredVersion=0.1;
		units[]=
		{
			"RD501_opfor_unit_ig88_droid_suicide",
			"RD501_opfor_unit_ig88_droid_standard"
		};
		weapons[]=
		{
			
		};
	};
};
class DefaultEventhandlers;
class CfgVehicles
{
	class O_soldier_base_F;
	class O_Soldier_F: O_soldier_base_F
	{
		class HitPoints;
	};
	class RD501_opfor_unit_ig88_droid_base: O_Soldier_F
	{
		JLTS_isDroid = 1; 
		JLTS_hasEMPProtection = 0; 
		scope = 0;
		scopeCurator = 0;
		displayName = "IG";
		editorSubcategory="RD501_Editor_Category_CIS_SpecOps";
		vehicleClass ="RD501_Editor_Category_CIS_SpecOps";
		side = 0;
		faction="RD501_CIS_Faction";
		genericNames = "JLTS_DroidsB1";
		identityTypes[] = {"lsd_voice_b1Droid"};
		impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
		facewear = "";
		uniformClass = "";
		linkedItems[] = {"ItemMap","ItemGPS","ItemCompass"};
		respawnLinkedItems[] = {"ItemMap","ItemGPS","ItemCompass"};
		weapons[] = {};
		respawnWeapons[] = {};
		magazines[] = {};
		respawnMagazines[] = {};
		items[] = {};
		respawnItems[] = {};
		armor = 60;
		armorStructural = 10;
		explosionShielding = 0.4;
		minTotalDamageThreshold = 0.001;
		impactDamageMultiplier = 0.5;
		class HitPoints
		{
			class HitFace
			{
				armor = 10;
				material = -1;
				name = "face_hub";
				passThrough = 0.8;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 10;
				material = -1;
				name = "neck";
				passThrough = 0.8;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 10;
				material = -1;
				name = "head";
				passThrough = 0.8;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 15;
				material = -1;
				name = "pelvis";
				passThrough = 0.8;
				radius = 0.24;
				explosionShielding = 0.5;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 15;
				material = -1;
				name = "spine1";
				passThrough = 0.8;
				radius = 0.16;
				explosionShielding = 0.5;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 15;
				material = -1;
				name = "spine2";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 0.5;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 20;
				material = -1;
				name = "spine3";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 2;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 2;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 15;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 0.5;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 15;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 15;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.14;
				explosionShielding = 0.5;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 0.5;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 15;
				material = -1;
				name = "hand_l";
				passThrough = 1;
				radius = 0.08;
				explosionShielding = 0.5;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
			};
			class HitLeftLeg
			{
				armor = 15;
				material = -1;
				name = "leg_l";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 0.5;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
	};
	class RD501_opfor_unit_ig88_droid_suicide: RD501_opfor_unit_ig88_droid_base
	{
		scope = 2;
		scopeCurator = 2;
		displayName = "IG-88 Suicide Assassin";
		uniformClass = "RD501_opfor_uniform_ig88_armor";
		model = "DBA_CIS\Addons\DBA_Armour\DBA_IG88\IG88.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"DBA_CIS\Addons\DBA_Armour\DBA_IG88\data\IG88_CO.paa"};
		hiddenSelectionsMaterial[] = {"DBA_CIS\Addons\DBA_Armour\DBA_IG88\data\IG88.rvmat"};
		linkedItems[]=
		{
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_2"
		};
		respawnLinkedItems[] = 
		{
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_2"
		};
		weapons[] = 
		{
			"RD501_zh_scope_73",
			"Throw",
			"Put"
		};
		respawnWeapons[] = 
		{
			"RD501_zh_scope_73",
			"Throw",
			"Put"
		};
		magazines[] = 
        {
            "RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_thermaldet_cis_x1_mag",
			"RD501_thermaldet_cis_x1_mag",
        };
		respawnMagazines[] =  
        {
            "RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_thermaldet_cis_x1_mag",
			"RD501_thermaldet_cis_x1_mag",
        };
		items[] = {};
		respawnItems[] = {};
		class EventHandlers: DefaultEventhandlers
		{
			Killed = "[_this select 0] execVM 'DBA_Core\Addons\DBA_Func\fn_SelfDestruct.sqf';";
		};
	};
	class RD501_opfor_unit_ig88_droid_standard: RD501_opfor_unit_ig88_droid_base
	{
		scope = 2;
		scopeCurator = 2;
		displayName = "IG-88 Assassin Droid";
		uniformClass = "RD501_opfor_uniform_ig88_armor";
		model = "DBA_CIS\Addons\DBA_Armour\DBA_IG88\IG88.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"DBA_CIS\Addons\DBA_Armour\DBA_IG88\data\IG88_CO.paa"};
		hiddenSelectionsMaterial[] = {"DBA_CIS\Addons\DBA_Armour\DBA_IG88\data\IG88.rvmat"};
		linkedItems[]=
		{
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_1"
		};
		respawnLinkedItems[] = 
		{
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_1"
		};
		weapons[] = 
		{
			"RD501_zh_scope_73",
			"Throw",
			"Put"
		};
		respawnWeapons[] = 
		{
			"RD501_zh_scope_73",
			"Throw",
			"Put"
		};
		magazines[] = 
        {
            "RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_thermaldet_cis_x1_mag",
			"RD501_thermaldet_cis_x1_mag",
        };
		respawnMagazines[] =  
        {
            "RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_30mw_x15_mag",
			"RD501_thermaldet_cis_x1_mag",
			"RD501_thermaldet_cis_x1_mag",
        };
		items[] = {};
		respawnItems[] = {};
	};
};
class CfgWeapons
{
	class U_I_CombatUniform;
	class UniformItem;
	class RD501_opfor_uniform_ig88_armor: U_I_CombatUniform
	{
		JLTS_isDroid = 1;
		JLTS_hasEMPProtection = 1;
		JLTS_deathSounds = "DeathDroid";
		scope = 2;
		displayName = "[CIS] IG-88 Chassis";
		picture = "\RD501_Droids\IG88\ig88_armor_ui.paa";
		model = "DBA_CIS\Addons\DBA_Armour\DBA_IG88\IG88.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"DBA_CIS\Addons\DBA_Armour\DBA_IG88\data\IG88_CO.paa"};
		hiddenSelectionsMaterial[] = {"DBA_CIS\Addons\DBA_Armour\DBA_IG88\data\IG88.rvmat"};
		class ItemInfo: UniformItem
		{
			uniformModel = "-";
			uniformClass = "RD501_opfor_unit_ig88_droid_suicide";
			containerClass = "Supply150";
			mass = 40;
		};
	};
};