class JLTS_B1_backpack;
class JLTS_B1_backpack_prototype;
class JLTS_B1_antenna;

class macro_new_backpack_class(opfor,B1): JLTS_B1_backpack
{
	author = "RD501";
	scope = 1;
};
class macro_new_backpack_class(opfor,B1_prototype): JLTS_B1_backpack_prototype
{
	author = "RD501";
	scope = 1;
};
class macro_new_backpack_class(opfor,B1_antenna): JLTS_B1_antenna
{
	author = "RD501";
	scope = 1;
};
class macro_new_backpack_class(opfor,B1_AT): JLTS_B1_backpack
{
	author = "RD501";
	scope=1;
	class TransportMagazines
	{
		class _xx_at_mag
		{
			count=4;
			magazine="RD501_E60R_AT";
		};
		class _xx_he_mag
		{
			count=2;
			magazine="RD501_E60R_he";
		};
	};
};
class macro_new_backpack_class(opfor,B1_AA): JLTS_B1_backpack
{
	author = "RD501";
	scope=1;
	class TransportMagazines
	{
		class _xx_aa_mag
		{
			count=3;
			magazine=macro_aa_mag;
		};
	};
};
class macro_new_backpack_class(opfor,B1_jammer): JLTS_B1_backpack
{
	author = "RD501";
	displayName="[CIS] B1 Jammer backpack";
	scope = 1;
	hiddenSelectionsTextures[]=
	{
		"\RD501_Droids\data\b1_jammer_backpack.paa"
	};
};

class B_UAV_01_backpack_F;
class EWEB_Bag: B_UAV_01_backpack_F
{

	class assembleInfo;

};